import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import NavBar from './Components/NavBar';
import Footer from './Components/Footer';
import Loader from './Components/Loader';
import StorefrontStorage from './Util/StorefrontStorage';
import ApiInteractor from './Util/ApiInteractor';
import CheckoutActions from './Actions/CheckoutActions';

class DefaultRoute extends Component {
  render() {
    return (
      <div className="nm-layout nm-layout__grey">
        <NavBar secondary={true} hideCart={true}/>
        <div className="nm-clear"/>
        <div className="nm-layout nm-layout__gray">
          { this.props.children }
          <Footer/>
        </div>
      </div>
    )
  }
}

export default DefaultRoute;
