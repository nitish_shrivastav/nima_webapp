import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import moment from 'moment';
import NavBar from './Components/NavBar';
import Footer from './Components/Footer';
import AuthStorage from './Util/AuthStorage';
import AuthActions from './Actions/AuthActions';

class SSORoute extends Component {
  constructor() {
    super();
    this._updateAuthTokens = this._updateAuthTokens.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  componentWillMount() {
    this._updateAuthTokens();
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (location.pathname !== prevProps.location.pathname && !this.state.refreshing && AuthStorage.needsRefreshing()) {
      this._updateAuthTokens();
    }
  }

  handleLogoutClick() {
    this.props.dispatch(AuthActions.logoutSSOUser());
  }

  _updateAuthTokens() {
    let refreshBody = AuthStorage.getRefreshBody();
    if (refreshBody) {
      this.props.dispatch(AuthActions.refreshToken(refreshBody))
      .then((user) => {
        this.setState({ refreshing: false });
      })
      .catch();
    }
  }

  render() {
    if (this.props.auth.sso || AuthStorage.canRefresh()) {
      return (
        <div className="nm-layout nm-layout__grey">
          <div>
            <NavBar secondary={true}/>
            <div className="nm-clear"/>
            <div className="nm-layout">
              <div className="nm-layout">
                <div className="nm-layout__max-center nm-layout__max-center--mobile-support nm-layout__max-center--large">
                  <h3
                    style={{padding: 16, margin: 0, textDecoration: 'underline'}}
                    className="nm-link nm-pull--right"
                    onClick={this.handleLogoutClick}>
                    Logout
                  </h3>
                  <div className="nm-clear"/>
                </div>
              </div>
              {
                this.props.auth.sso && moment(this.props.auth.sso.tokens.Refresh.Expiration) > moment() &&
                <div className="nm-layout">
                  { this.props.children }
                </div>
              }
              <Footer/>
            </div>
          </div>
        </div>
      )
    }
    else {
      return ( <Redirect to={{
        pathname: "/account/login",
        state: { from: this.props.location }
       }} /> );
    }
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(SSORoute);
