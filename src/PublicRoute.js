import React, { Component } from 'react';
import { connect } from 'react-redux';
import NavBar from './Components/NavBar';
import Footer from './Components/Footer';
import CheckoutActions from './Actions/CheckoutActions';
import PromotionActions from './Actions/PromotionActions';
import SubscriptionProductActions from './Actions/SubscriptionProductActions';
import SegmentActions from './Actions/SegmentActions';
import SegmentConstants from './Constants/segment';
import Close from './SVG/Close';

class PublicRoute extends Component {
  constructor() {
    super();
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleRouteClick = this.handleRouteClick.bind(this);
    this.alkElement = undefined;
  }

  componentWillMount() {
    this.props.dispatch(CheckoutActions.createOrRetrieveCheckout(this.props.client));
    this.props.dispatch(PromotionActions.list());
    this.props.dispatch(SubscriptionProductActions.list());
    this.setState({ showForeignModal: false });
  }

  componentDidMount() {
    setTimeout(() => {
      if (!localStorage.getItem("alk_modal_shown")) {
        this.setState({ showForeignModal: true });
      }
    }, 3000);
  }

  handleCloseClick() {
    localStorage.setItem("alk_modal_shown", "true");
    this.setState({ showForeignModal: false });
  }

  handleRouteClick() {
    this.props.dispatch(SegmentActions.track(SegmentConstants.ALK_LINK_CLICKED));
    this.alkElement.click();
  }

  render() {
    const { showForeignModal } = this.state;
    return (
      <div className="nm-layout nm-layout__grey">
        <NavBar user={this.props.auth.user} secondary={true}/>
        <div className="nm-clear"/>
        <div className="nm-layout nm-layout__gray">
          {
            this.props.subscription_product.list &&
            this.props.children
          }
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
          <Footer/>
        </div>
        <div className={`nm-layout nm-slider`}>
          <img
            alt="Nima Sparkle"
            src={`${process.env.PUBLIC_URL}/images/nima-sparkle.png`}
            style={{
              position: 'absolute',
              left: 0,
              top: 0,
              width: 55
            }}
            className="nm-image"/>
          <div
            onClick={this.handleCloseClick}
            className="nm-image__svg-hover-wrap"
            style={{ 
              width: 36,
              height: 36,
              float: 'right',
              position: 'relative',
              top: -8,
              right: -50,
              padding: 5
            }}>
            <Close className="nm-image__svg-hover"/>
          </div>
          <h3 className="nm-layout nm-text-center">
            Not in the USA or Canada? 
          </h3>
          <p className="nm-layout nm-text-center">
            Buy Nima from our partner:
          </p>
          <p
            onClick={this.handleRouteClick}
            className="nm-layout nm-text-center nm-link">
            - Germany/UK
          </p>
          <a
            ref={(element) => this.alkElement = element }
            href="https://de.klarify.me/collections/nima"
            className="nm-hidden"
            target="_blank">
          </a>
        </div>
        <div className="nm-clear"/>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    subscription_product: state.subscription_product
  })
)(PublicRoute);
