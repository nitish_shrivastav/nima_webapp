import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import store from './store'
import './index.scss';
import config from './config';
import Routes from './Routes';
import registerServiceWorker, { unregister } from './registerServiceWorker';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo'; 

// Pass your GraphQL endpoint to uri
const client = new ApolloClient({
  uri: `${config.SHOP_URL}/api/graphql`,
  fetchOptions: {
    credentials: 'include'
  },
  request: (operation) => {
    operation.setContext({
      headers: {
        "X-Shopify-Storefront-Access-Token": config.STOREFRONT_TOKEN
      }
    });
  }
});

const ApolloApp = (
    <Provider store={store}>
      <ApolloProvider client={client}>
        <Routes />
      </ApolloProvider>
    </Provider>
);

ReactDOM.render(ApolloApp, document.getElementById('root'));
//registerServiceWorker();
unregister();
