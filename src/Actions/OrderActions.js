import _ from 'lodash';

import constants from '../Constants/order';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';
import SegmentActions from './SegmentActions';
import SegmentConstants from '../Constants/segment';
import TalkableInteractor from '../Util/TalkableInteractor';

const setOrderDetail = (detail) => ({ type: constants.SET_ORDER_DETAIL, detail });
const setOrderError = (error) => ({ type: constants.SET_ORDER_ERROR, error });
const setOrderList = (list) => ({ type: constants.SET_ORDER_LIST, list });

class OrderActions {

  static _trackOrder(order, body) {
    return (dispatch, getState) => {
      const checkout = getState().checkout;
      const segmentPayload = {
        email: order.email,
        shipping_method: _.get(order, 'shipping_line.title'),
        payment_method: body.provider,
        shipping_zip: _.get(order, 'shipping_address.zip'),
        customer_shopify_id: _.get(order, 'customer.id'),
        first_name: _.get(order, 'customer.first_name'),
        last_name: _.get(order, 'customer.last_name'),
        cart_id: _.get(body, 'checkout_id'),
        order_id: order.order_id,
        // we are sending subtotal as total to Segment, due to business decisions, and limitations from Iterable to accept total and not revenue
        total: parseFloat(order.subtotal_price),
        shipping: parseFloat(_.get(order, 'shipping_line.price')), //this is shipping cost
        tax: parseFloat(order.total_tax),
        discount: parseFloat(_.get(order, 'applied_discount.amount')),
        coupon: _.get(checkout, 'validDiscount.id'),
        currency: 'USD',
        products: order.line_items.map((item) => ({
          ...item,
          product_id: item.product_id.toString(),
          price: parseFloat(item.price)
        }))
      };
      dispatch(SegmentActions.identify(order.customer.id, order.email, {hasPurchased: true}));
      dispatch(SegmentActions.track(SegmentConstants.ORDER_COMPLETED, segmentPayload));
      TalkableInteractor.registerPurchase(order);
    }
  }

  static createOrder(body, cb) {
    return (dispatch, getState) => {
      dispatch(setOrderError(null))
      dispatch(LoaderActions.setIsLoading(1, "Creating your order.. (Do not refresh)"))
      ApiInteractor.createOrder(body)
      .then((order) => {
        order = order.draft_order;
        dispatch(LoaderActions.setIsLoading(-1));
        localStorage.setItem('firstName', body.first_name)
        localStorage.setItem('needsToVerify', body.type_ === "guest")
        localStorage.setItem('email', order.email)
        dispatch(setOrderDetail(order));
        dispatch(OrderActions._trackOrder(order, body));
        if (cb) cb(null, order);
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1))
        dispatch(setOrderError(err))
        if (cb) cb(err, null);
      })
    }
  }

  static createAuthenticatedOrder(body) {
    return (dispatch, getState) => {
      dispatch(setOrderError(null));
      dispatch(LoaderActions.setIsLoading(1, "Creating your order.. (Do not refresh)"))
      return ApiInteractor.createAuthenticatedOrder(body)
      .then((order) => {
        dispatch(LoaderActions.setIsLoading(-1));
        localStorage.setItem('firstName', _.get(order, 'draft_order.customer.first_name'));
        localStorage.setItem('needsToVerify', false);
        localStorage.setItem('email', _.get(order, 'draft_order.customer.email'));
        dispatch(setOrderDetail(order.draft_order));
        dispatch(OrderActions._trackOrder(order.draft_order, body));
        return order;
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setOrderError(err));
      })
    }
  }

  static list() {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting past orders.."));
      return ApiInteractor.listOrders()
      .then((list) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setOrderList(list.orders));
        return list.orders;
      })
      .catch((err) => {
        dispatch(setOrderError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static setError(err) {
    return (dispatch) => {
      dispatch(setOrderError(err));
    }
  }
}

export default OrderActions;
