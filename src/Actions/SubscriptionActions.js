import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';
import {
  SET_SUBSCRIPTION_LIST,
  SET_SUBSCRIPTION_ERROR,
  CLEAR_SUBSCRIPTIONS,
  SET_SUBSCRIPTION_DETAIL
} from '../Constants/subscription';

const setSubscriptionList = (list) => ({ type: SET_SUBSCRIPTION_LIST, list});
const setSubscriptionError = (error) => ({ type: SET_SUBSCRIPTION_ERROR, error });
const setSubscriptionDetail = (detail) => ({ type: SET_SUBSCRIPTION_DETAIL, detail });
const clearSubscriptions = () => ({ type: CLEAR_SUBSCRIPTIONS });


class SubscriptionActions {

  static list() {
    return (dispatch) => {
      dispatch(setSubscriptionError(null));
      dispatch(LoaderActions.setIsLoading(1, "Getting subscriptions.."));
      ApiInteractor.listSubsciptions()
      .then((subscriptions) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setSubscriptionList(subscriptions));
      })
      .catch((err) => {
        dispatch(setSubscriptionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static update(order, body) {
    return (dispatch) => {
      dispatch(setSubscriptionError(null));
      dispatch(LoaderActions.setIsLoading(1, "Updating subscription.."));
      return ApiInteractor.updateSubsciption(encodeURIComponent(order), body)
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setSubscriptionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }
  
  static updateDiscount(order, body) {
    return (dispatch) => {
      dispatch(setSubscriptionError(null));
      dispatch(LoaderActions.setIsLoading(1, "Updating subscription discount.."));
      return ApiInteractor.updateSubsciptionDiscount(encodeURIComponent(order), body)
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setSubscriptionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static delete(order_id) {
    return (dispatch) => {
      dispatch(setSubscriptionError(null));
      dispatch(LoaderActions.setIsLoading(1, "Deleteing subscription.."));
      return ApiInteractor.deleteSubscription(encodeURIComponent(order_id))
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setSubscriptionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static deleteDiscount(order) {
    return (dispatch) => {
      dispatch(setSubscriptionError(null));
      dispatch(LoaderActions.setIsLoading(1, "Removing discount.."));
      return ApiInteractor.deleteSubsciptionDiscount(encodeURIComponent(order))
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setSubscriptionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  
  static setDetail(subscription) {
    return (dispatch) => {
      dispatch(setSubscriptionDetail(subscription));
    }
  }

  static clearDetail() {
    return (dispatch) => {
      dispatch(setSubscriptionDetail(null));
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearSubscriptions());
    }
  }

}

export default SubscriptionActions;
