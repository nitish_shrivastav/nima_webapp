import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';
import { SET_VOUCHER_DETAIL, SET_VOUCHER_ERROR, CLEAR_VOUCHERS } from '../Constants/voucher';

const setVoucherDetail = (detail) => ({ type: SET_VOUCHER_DETAIL, detail });
const setVoucherError = (error) => ({ type: SET_VOUCHER_ERROR, error });
const clearVouchers = () => ({ type: CLEAR_VOUCHERS });


class VoucherActions {

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading voucher.."));
      ApiInteractor.retrieveVoucher(id)
      .then((voucher) => {
        dispatch(setVoucherDetail(voucher));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setVoucherError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static redeem(id, body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Redeeming voucher.."));
      ApiInteractor.redeemVoucher(id, body)
      .then((voucher) => {
        dispatch(setVoucherDetail(voucher));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setVoucherError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      });
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearVouchers());
    }
  }

}

export default VoucherActions;
