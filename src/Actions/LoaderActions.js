import constants from '../Constants/loader';

const setLoading = (isLoading, message) => ({ type: constants.SET_IS_LOADING, isLoading, message })

class LoaderActions {

  static setIsLoading(isLoading, message) {
    return (dispatch) => {
      dispatch(setLoading(isLoading, message))
    }
  }
}


export default LoaderActions;