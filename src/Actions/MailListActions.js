import constants from '../Constants/mailList';
import MailingListInteractor from '../Util/MailingListInteractor';
import LoaderActions from './LoaderActions';

const setMailError = (error) => ({ type: constants.SET_MAIL_ADD_ERROR, error });
const setMailSuccess = (message) => ({ type: constants.SET_MAIL_ADD_SUCCESS, message });
const clearMail = () => ({ type: constants.CLEAR_MAIL_LIST });


class MailListActions {

  static add(id, body) {
    return (dispatch) => {
      dispatch(clearMail());
      dispatch(LoaderActions.setIsLoading(1, "Reserving your Nima."));
      MailingListInteractor.create(id, body)
      .then((response) => {
        dispatch(setMailSuccess(response));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setMailError(err.msg));
        dispatch(LoaderActions.setIsLoading(-1));
      });
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearMail());
    }
  }
}


export default MailListActions;
