import constants from '../Constants/product';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import LoaderActions from './LoaderActions';
import _ from 'lodash';
import SegmentActions from './SegmentActions';
import SegmentConstants from '../Constants/segment';
import SegmentPayloadConverter from '../Util/SegmentPayloadConverter';
import SubscriptionProductActions from './SubscriptionProductActions';

const setProductError = (error) => ({ type: constants.SET_PRODUCT_ERROR, error })
const setProductList = (products) => ({ type: constants.SET_PRODUCT_LIST, products })
const setProductDetail = (product) => ({ type: constants.SET_PRODUCT_DETAIL, product })


const updateAffirm = () => {
  if (window && window.affirm) {
    setTimeout(() => {
      window.affirm.ui.ready(() => {
        window.affirm.ui.refresh()
      })
    }, 200)
  }
}

const getProductMeta = (product) => {
  try {
    let meta = product.description.replace(new RegExp('&lt;', 'g'), '<');
    meta = meta.replace(new RegExp('&gt;', 'g'), '>');
    meta = meta.replace(/\|/g, '');
    meta = JSON.parse(meta);
    return meta;
  }
  catch(err) {
    return null;
  }
}

class ProductActions {

  static list(client) {
    return (dispatch, getState) => {
      if (getState().product.list.length === 0) {
        dispatch(LoaderActions.setIsLoading(1, "Finding products.."))
        StorefrontInteractor.getShopProducts(client)
        .then((products) => {
          console.log(products);
          products = products.map((product) => {
            product.node.meta = getProductMeta(product.node);
            return product;
          })
          dispatch(SegmentActions.track(SegmentConstants.PRODUCT_LIST_VIEWED, SegmentPayloadConverter.productListViewed(products)));
          dispatch(setProductList(products));
          //Everytime you get products, you need to get subscriptionProducts as well.
          dispatch(SubscriptionProductActions.list());
          dispatch(LoaderActions.setIsLoading(-1));
        })
        .catch((err) => {
          dispatch(setProductError(err));
          dispatch(LoaderActions.setIsLoading(-1));
        })
      }
    }
  }

  static retrieve(client, handle) {
    return (dispatch, getState) => {
      const { product } = getState();
      if (product.list.length > 0) {
        for (let productItem of product.list) {
          if (productItem.node.handle === handle) {
            dispatch(setProductDetail(productItem.node));
            const payload = SegmentPayloadConverter.productViewed(productItem.node);
            dispatch(SegmentActions.track(SegmentConstants.PRODUCT_VIEWED, payload));
            updateAffirm();
            break;
          }
        }
      }
      else {
        dispatch(LoaderActions.setIsLoading(1, "Loading Product"))
        StorefrontInteractor.getShopProduct(client, handle)
        .then((product) => {
          product.meta = getProductMeta(product);
          dispatch(setProductDetail(product));
          dispatch(LoaderActions.setIsLoading(-1));
          const payload = SegmentPayloadConverter.productViewed(product);
          dispatch(SegmentActions.track(SegmentConstants.PRODUCT_VIEWED, payload));
          updateAffirm();
        })
        .catch((err) => {
          dispatch(setProductError(err))
          dispatch(LoaderActions.setIsLoading(-1))
        })
        
      }
    }
  }
}

export default ProductActions;
