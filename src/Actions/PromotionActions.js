import {
  SET_PROMOTION_LIST,
  SET_PROMOTION_ERROR,
  CLEAR_PROMOTIONS
} from '../Constants/promotion';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';
import CheckoutActions from './CheckoutActions';

const setPromotionError = (error) => ({ type: SET_PROMOTION_ERROR, error });
const setPromotionList = (list) => ({ type: SET_PROMOTION_LIST, list });
const clearPromotions = () => ({ type: CLEAR_PROMOTIONS });


class PromotionActions {

  static list() {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Finding promotions"));
      return ApiInteractor.listPromotions()
      .then((list) => {
        dispatch(setPromotionList(list));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(CheckoutActions.setTotals());
        return list;
      })
      .catch((err) => {
        dispatch(setPromotionError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static setError(error) {
    return (dispatch) => {
      dispatch(setPromotionError(error));
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearPromotions());
    }
  }
}


export default PromotionActions;
