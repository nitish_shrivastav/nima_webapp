import { initialize } from 'redux-form';
import constants from '../Constants/checkout';
import ApiInteractor from '../Util/ApiInteractor';
import StorefrontStorage from '../Util/StorefrontStorage';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import CheckoutHelper from '../Util/CheckoutHelper';
import LoaderActions from './LoaderActions';
import SegmentActions from '../Actions/SegmentActions';
import SegmentConstants from '../Constants/segment';
import _ from 'lodash';

const setCheckoutError = (error) => ({ type: constants.SET_CHECKOUT_ERROR, error });
const setCheckoutTotals = (totals) => ({ type: constants.SET_CHECKOUT_TOTALS, totals });
const setRetrieveCheckout = (checkout) => ({ type: constants.RETRIEVE_CHECKOUT, checkout });
const checkoutClear = () => ({ type: constants.CLEAR_CHECKOUT });
const setValidDiscount = (validDiscount) => ({ type: constants.SET_VALID_DISCOUNT, validDiscount });
const setDiscountError = (error) => ({ type: constants.SET_DISCOUNT_ERROR, error });
const setShippingAddressLoading = (isLoading) => ({ type: constants.SET_SHIPPING_ADDRESS_LOADING, isLoading });
const setShippingRatesLoading = (isLoading) => ({ type: constants.SET_SHIPPING_RATES_LOADING, isLoading });

class CheckoutActions {

  static retrieveCheckout(client, id) {
    return (dispatch, getState) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting your cart.."))
      StorefrontInteractor.retrieveCheckout(client, { id })
      .then((checkout) => {
        StorefrontStorage.setCheckoutId(checkout.id);
        dispatch(setRetrieveCheckout(checkout));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(CheckoutActions.setTotals());
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setCheckoutError(err));
      })
    }
  }

  //Creates a checkout if one doesn't exist
  static createOrRetrieveCheckout(client) {
    return (dispatch) => {
      let checkoutId = StorefrontStorage.getCheckoutId();
      if (checkoutId) {
        dispatch(CheckoutActions.retrieveCheckout(client, checkoutId));
      }
      else {
        StorefrontInteractor.createCheckout(client)
        .then((checkout) => {
          StorefrontStorage.setCheckoutId(checkout.id)
          dispatch(setRetrieveCheckout(checkout))
        })
        .catch((err) => {
          dispatch(setCheckoutError(err))
        })
      }
    }
  }

  static updateCheckoutEmail(client, body) {
    return (dispatch) => {
      StorefrontInteractor.updateCheckoutEmail(client, body)
      .then((checkout) => {
        dispatch(setRetrieveCheckout(checkout))
      })
      .catch((err) => {
        dispatch(setCheckoutError(err))
      })
    }
  }

  static updateCheckoutShippingLine(client, body, cb) {
    return (dispatch, getState) => {
      dispatch(setShippingRatesLoading(true));
      StorefrontInteractor.updateCheckoutShippingLine(client, body)
      .then((checkout) => {
        dispatch(setShippingRatesLoading(false));
        dispatch(setRetrieveCheckout(checkout));
        dispatch(CheckoutActions.setTotals());
        if (cb) cb(null, checkout);
      })
      .catch((err) => {
        dispatch(setShippingRatesLoading(false));
        dispatch(setCheckoutError(err))
        if (cb) cb(err, null);
      })
    }
  }

  static updateCheckout(client, body, cb) {
    return (dispatch, getState) => {
      dispatch(setShippingAddressLoading(true));
      StorefrontInteractor.updateCheckout(client, body)
      .then((checkout) => {
        dispatch(setShippingAddressLoading(false));
        dispatch(setRetrieveCheckout(checkout));
        dispatch(CheckoutActions.setTotals());
        dispatch(initialize('cart', checkout));
        if (cb) cb(null, checkout);
      })
      .catch((err) => {
        dispatch(setShippingAddressLoading(false));
        dispatch(setCheckoutError(err));
        if (cb) cb(err, null);
      })
    }
  }

  static checkoutLineItemsReplace(client, body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Updating cart..."))
      return StorefrontInteractor.checkoutLineItemsReplace(client, body)
      .then((checkout) => {
        dispatch(setRetrieveCheckout(checkout));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(CheckoutActions.setTotals());
        dispatch(initialize('cart', checkout));
        return checkout;
      })
      .catch((err) => {
        dispatch(setCheckoutError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static setTotals() {
    return (dispatch, getState) => {
      const { checkout, membership, promotion } = getState();
      if (checkout.detail) {
        let totals = CheckoutHelper.getOrderTotals(checkout.detail, checkout.validDiscount, membership.detail, promotion.list);
        dispatch(setCheckoutTotals(totals));
      }
    }
  }

  static clearCheckout() {
    return (dispath) => {
      dispath(checkoutClear());
    }
  }

  static validateDiscount(body, cb) {
    return (dispatch) => {
      const coupon_id = _.get(body, 'discount_codes[0].id');
      const cart_id = _.get(body, 'checkout_id');
      dispatch(setDiscountError(null));
      dispatch(LoaderActions.setIsLoading(1, "Validating Discounts..."))
      dispatch(SegmentActions.track(SegmentConstants.COUPON_ENTERED, { cart_id, coupon_id }));
      ApiInteractor.validateDiscount(body)
      .then((discountRes) => {
        dispatch(LoaderActions.setIsLoading(-1));
        if (discountRes && discountRes[0]) {
          dispatch(setValidDiscount(discountRes[0]));
          dispatch(SegmentActions.track(SegmentConstants.COUPON_APPLIED, { cart_id, coupon_id }));
          dispatch(CheckoutActions.setTotals());
          if (cb) cb(null, discountRes[0]);
        }
        else {
          dispatch(SegmentActions.track(SegmentConstants.COUPON_DENIED, { cart_id, coupon_id }));
          throw "Invalid Discount";
        }
      })
      .catch((err) => {
        dispatch(setDiscountError("Invalid Discount"));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(CheckoutActions.setTotals());
        if (cb) cb(err, null);
      })
    }
  }

  static discountError(err) {
    return (dispatch) => {
      dispatch(setDiscountError(err));
    }
  }

}


export default CheckoutActions;
