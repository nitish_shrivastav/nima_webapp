import constants from '../Constants/reading';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setReadingError = (error) => ({ type: constants.SET_READING_ERROR, error });
const setRetrieveReading = (detail) => ({ type: constants.SET_READING_DETAIL, detail });
const setListReadings = (list) => ({ type: constants.SET_READING_LIST, list });
const readingClear = () => ({ type: constants.CLEAR_READING });
const readingErrorClear = () => ({ type: constants.CLEAR_READING_ERROR });

class ReadingActions {

  static list(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting readings.."));
      ApiInteractor.listReadings(params)
      .then((readings) => {
        dispatch(setListReadings(readings));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in list readings: ', err);
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setReadingError(err));
      })
    }
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting reading.."));
      ApiInteractor.retrieveReading(id)
      .then((reading) => {
        dispatch(setRetrieveReading(reading));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in retrieve reading: ', err);
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setReadingError(err));
      })
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(readingClear());
    }
  }

  static clearError() {
    return (dispatch) => {
      dispatch(readingErrorClear());
    }
  }
}


export default ReadingActions;
