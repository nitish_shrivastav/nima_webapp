import { SET_MEMBERSHIP_DETAIL, SET_MEMBERSHIP_ERROR, CLEAR_MEMBERSHIP } from '../Constants/membership';
import ApiInteractor from '../Util/ApiInteractor';
import CheckoutActions from './CheckoutActions';
import LoaderActions from './LoaderActions';

const membershipSet = (detail) => ({ type: SET_MEMBERSHIP_DETAIL, detail });
const clearMembership = () => ({ type: CLEAR_MEMBERSHIP })
const setMembershipError = (error) => ({ type: SET_MEMBERSHIP_ERROR, error });

class MembershipActions {

  static list() {
    return (dispatch) => {
      dispatch(setMembershipError(null));
      dispatch(LoaderActions.setIsLoading(1, "Checking memberships.."));
      ApiInteractor.retrieveMemberships()
      .then((memberships) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(membershipSet(memberships));
        dispatch(CheckoutActions.setTotals());
      })
      .catch((err) => {
        dispatch(setMembershipError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearMembership());
    }
  }
}


export default MembershipActions;
