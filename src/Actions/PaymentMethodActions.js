import {
  SET_PAYMENT_METHOD_LIST,
  SET_PAYMENT_METHOD_DETAIL,
  SET_PAYMENT_METHOD_ERROR,
  CLEAR_PAYMENT_METHOD
} from '../Constants/payment_method';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setPaymentMethodError = (error) => ({ type: SET_PAYMENT_METHOD_ERROR, error });
const setPaymentMethodList = (list) => ({ type: SET_PAYMENT_METHOD_LIST, list });
const setPaymentMethodDetail = (detail) => ({ type: SET_PAYMENT_METHOD_DETAIL, detail });
const clearPaymentMethod = () => ({ type: CLEAR_PAYMENT_METHOD });


class PaymentMethodActions {

  static list() {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Finding payment methods.."));
      return ApiInteractor.listPaymentMethod()
      .then((list) => {
        dispatch(setPaymentMethodList(list));
        dispatch(LoaderActions.setIsLoading(-1));
        return list;
      })
      .catch((err) => {
        dispatch(setPaymentMethodError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static create(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Creating payment method.."));
      return ApiInteractor.createPaymentMethod(body)
      .then((detail) => {
        dispatch(setPaymentMethodDetail(detail));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(PaymentMethodActions.list());
        return detail;
      })
      .catch((err) => {
        dispatch(setPaymentMethodError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static delete(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Deleting payment method.."));
      return ApiInteractor.deletePaymentMethod(id)
      .then(() => {
        dispatch(PaymentMethodActions.list());
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setPaymentMethodError(err));
        dispatch(LoaderActions.setIsLoading(-1))
        return;
      })
    }
  }

  static setDefault(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Changing default payment method.."));
      return ApiInteractor.updateDefaultPaymentMethod(id)
      .then(() => {
        dispatch(PaymentMethodActions.list());
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setPaymentMethodError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static setDetail(paymentMethod) {
    return (dispatch) => {
      dispatch(setPaymentMethodDetail(paymentMethod));
      dispatch(setPaymentMethodError(null));
    }
  }

  static setError(error) {
    return (dispatch) => {
      dispatch(setPaymentMethodError(error));
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearPaymentMethod());
    }
  }
}


export default PaymentMethodActions;
