import { EventTypes } from 'redux-segment';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import StorefrontStorage from '../Util/StorefrontStorage';
import config from '../config';

const setIdentify = (shopifyCustomerId, email, traits) => {
  if (!traits) traits = {};
  const data = {
    type: 'identify', 
    meta: {
      analytics: {
        eventType: EventTypes.identify,
        eventPayload: {
          traits: {
            ...traits,
            storefront_customer_id: shopifyCustomerId,
            email: email,
            cart_id: StorefrontStorage.getCheckoutId() || undefined,
            website_version: config.version
          }
        }
      }
    }
  }
  return data;
}
const setTrack = (eventName, payload) => ({
  type: eventName,
  meta: {
    analytics: { 
      eventType: EventTypes.track,
      eventPayload: {
        event: eventName,
        properties: {
          ...payload,
          cart_id: StorefrontStorage.getCheckoutId() || undefined,
          website_version: config.version
        }
      }
    }
  }
})

class SegmentActions {

  static identify(payload, userId) {
    return (dispatch, getState) => dispatch(setIdentify(payload, userId));
  }

  static track(eventName, payload) {
    return (dispatch, getState) => dispatch(setTrack(eventName, payload));
  }

}

export default SegmentActions;
