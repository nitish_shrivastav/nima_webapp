import {
  SET_ADDRESS_LIST,
  SET_ADDRESS_DETAIL,
  SET_ADDRESS_ERROR,
  CLEAR_ADDRESSES
} from '../Constants/address';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setAddressError = (error) => ({ type: SET_ADDRESS_ERROR, error });
const setAddressList = (list) => ({ type: SET_ADDRESS_LIST, list });
const setAddressDetail = (detail) => ({ type: SET_ADDRESS_DETAIL, detail });
const clearAddress = () => ({ type: CLEAR_ADDRESSES });


class AddressActions {

  static list(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Finding addresses.."));
      return ApiInteractor.listAddress(params)
      .then((list) => {
        dispatch(setAddressList(list));
        dispatch(LoaderActions.setIsLoading(-1));
        return list;
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting address.."));
      ApiInteractor.retrieveAddress(id)
      .then((detail) => {
        dispatch(setAddressDetail(detail));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static create(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Creating address.."));
      return ApiInteractor.createAddress(body)
      .then((detail) => {
        dispatch(setAddressDetail(detail));
        dispatch(LoaderActions.setIsLoading(-1));
        return detail;
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static update(id, body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Updating address.."));
      return ApiInteractor.updateAddress(id, body)
      .then((detail) => {
        dispatch(setAddressDetail(detail));
        dispatch(LoaderActions.setIsLoading(-1));
        return detail;
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static updateDefault(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Updating default address.."));
      return ApiInteractor.updateDefaultAddress(id)
      .then((detail) => {
        dispatch(setAddressDetail(detail));
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1))
        return;
      })
    }
  }

  static delete(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Deleting address.."));
      return ApiInteractor.deleteAddress(id)
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setAddressError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static setDetail(address) {
    return (dispatch) => {
      dispatch(setAddressDetail(address));
      dispatch(setAddressError(null));
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearAddress());
    }
  }
}


export default AddressActions;
