import constants from '../Constants/place';
import ApiInteractor from '../Util/ApiInteractor';
import MapInteractor from '../Util/MapInteractor';
import LoaderActions from './LoaderActions';

const setPlaceError = (error) => ({ type: constants.SET_PLACE_ERROR, error });
const setPlaceList = (list) => ({ type: constants.SET_PLACE_LIST, list });
const setPlaceDetail = (detail) => ({ type: constants.SET_PLACE_DETAIL, detail });
const setPlaceLocation = (location) => ({ type: constants.SET_PLACE_LOCATION, location });


class PlaceActions {

  static list(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Finding places.."));
      ApiInteractor.listPlaces(params)
      .then((places) => {
        dispatch(setPlaceList(places));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static publicSearch(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Searching places.."));
      ApiInteractor.searchPublicPlaces(params)
      .then((places) => {
        dispatch(setPlaceList(places));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setPlaceError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static search(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Searching places.."));
      ApiInteractor.searchPlaces(params)
      .then((places) => {
        dispatch(setPlaceList(places))
        dispatch(LoaderActions.setIsLoading(-1))
      })
      .catch((err) => {
        dispatch(setPlaceError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static publicRetrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading place.."));
      return ApiInteractor.publicRetrievePlace(id, {allergen: "*"})
      .then((place) => {
        dispatch(setPlaceDetail(place));
        dispatch(LoaderActions.setIsLoading(-1));
        return place;
      })
      .catch((err) => {
        dispatch(setPlaceError(err));
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading place.."));
      ApiInteractor.retrievePlace(id)
      .then((place) => {
        dispatch(setPlaceDetail(place));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        dispatch(setPlaceError(err));
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static reverseGeoCodeCity(coordinates, showLoader) {
    return (dispatch) => {
      if (showLoader) {
        dispatch(LoaderActions.setIsLoading(1, "Finding location.."));
      }
      MapInteractor.getCityNameByLatLng(coordinates.lat, coordinates.lng)
      .then((geoInfo) => {
        if (showLoader) {
          dispatch(LoaderActions.setIsLoading(-1));
        }
        geoInfo.lat = coordinates.lat;
        geoInfo.lng = coordinates.lng;
        dispatch(setPlaceLocation(geoInfo));
      })
      .catch((err) => {
        if (showLoader) {
          dispatch(LoaderActions.setIsLoading(-1));
        }
      })
    }
  }

}


export default PlaceActions;
