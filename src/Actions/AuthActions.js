import constants from '../Constants/auth';
import ApiInteractor from '../Util/ApiInteractor';
import AuthStorage from '../Util/AuthStorage';
import LoaderActions from './LoaderActions';
import PaymentMethodActions from './PaymentMethodActions';
import AddressActions from './AddressActions';
import MembershipActions from './MembershipActions';
import SubscriptionActions from './SubscriptionActions';
import BrowserHelper from '../Util/BrowserHelper';

const setAuth0User = (user) => ({ type: constants.SET_SSO_USER, user });
const setAuthError = (error) => ({ type: constants.SET_USER_ERROR, error });
const setAuthVerified = (verified) => ({ type: constants.SET_USER_VERIFIED, verified });
const clearAuthError = () => ({ type: constants.CLEAR_USER_ERROR });

class AuthActions {

  static refreshToken(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Gathering info.."));
      return ApiInteractor.refreshToken(body)
      .then((userTokens) => {
        dispatch(LoaderActions.setIsLoading(-1));
        let user = AuthStorage.getUser();
        user.tokens = userTokens.tokens;
        AuthStorage.setUser(user);
        dispatch(setAuth0User(user));
        return user;
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static registerSSOUser(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Creating account.."));
      return ApiInteractor.ssoRegister(body)
      .then((user) => {
        dispatch(LoaderActions.setIsLoading(-1));
        AuthStorage.setUser(user);
        dispatch(setAuth0User(user));
        return user;
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setAuthError(err));
        return;
      })
    }
  }

  static logoutSSOUser() {
    return (dispatch) => {
      AuthStorage.clearUser();
      dispatch(PaymentMethodActions.clear());
      dispatch(AddressActions.clear());
      dispatch(MembershipActions.clear());
      dispatch(SubscriptionActions.clear());
      dispatch(AuthActions.setUser(null));
    }
  }

  static loginAuth0User(credentials) {
    return (dispatch) => {
      dispatch(clearAuthError());
      dispatch(LoaderActions.setIsLoading(1, "Logging you in."))
      credentials.device_id = BrowserHelper.getName();
      return ApiInteractor.ssoLogin(credentials)
      .then((user) => {
        dispatch(LoaderActions.setIsLoading(-1))
        AuthStorage.setUser(user);
        dispatch(setAuth0User(user));
        return user;
      })
      .catch((err) => {
        dispatch(setAuthError(err));
        dispatch(LoaderActions.setIsLoading(-1))
        return;
      })
    }
  }

  static verify(body) {
    return (dispatch) => {
      dispatch(clearAuthError());
      dispatch(LoaderActions.setIsLoading(1, "Checking your account.."))
      ApiInteractor.ssoVerify(body)
      .then((res) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setAuthVerified(true));
      })
      .catch((err) => {
        dispatch(setAuthError(err));
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static loginFacebookUser(credentials) {
    return (dispatch) => {
      dispatch(clearAuthError());
      dispatch(LoaderActions.setIsLoading(1, "Logging you in through Facebook."))
      credentials.device_id = BrowserHelper.getName();
      return ApiInteractor.facebookLogin(credentials)
      .then((user) => {
        dispatch(LoaderActions.setIsLoading(-1));
        AuthStorage.setUser(user);
        dispatch(setAuth0User(user));
        return user;
      })
      .catch((err) => {
        dispatch(setAuthError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static ssoSendResetPasswordEmail(body) {
    return (dispatch) => {
      dispatch(clearAuthError());
      dispatch(LoaderActions.setIsLoading(1, "Sending password reset email.."))
      return ApiInteractor.ssoSendResetPassword(body)
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setAuthError(err));
        dispatch(LoaderActions.setIsLoading(-1))
        return;
      })
    }
  }

  static resendVerificationEmail(body) {
    return (dispatch) => {
      dispatch(clearAuthError());
      dispatch(LoaderActions.setIsLoading(1, `Resending email to ${body.email}`))
      return ApiInteractor.ssoResendVerifyEmail(body)
      .then(() => {
        dispatch(LoaderActions.setIsLoading(-1));
        return true;
      })
      .catch((err) => {
        dispatch(setAuthError(err));
        dispatch(LoaderActions.setIsLoading(-1));
        return;
      })
    }
  }

  static setUser(user) {
    return (dispatch) => {
      dispatch(setAuth0User(user));
    }
  }

  static setError(error) {
    return (dispatch) => {
      dispatch(setAuthError(error));
    }
  }

  static clearUserError() {
    return (dispatch) => {
      dispatch(clearAuthError());
    }
  }

}


export default AuthActions;
