import { change } from 'redux-form';

class ReduxFormHelper {

  static bulkChange(form, values) {
    return (dispatch) => {
      for (let value of values) {
        dispatch(change(form, value, values[value]));
      }
    }
  }
}


export default ReduxFormHelper;
