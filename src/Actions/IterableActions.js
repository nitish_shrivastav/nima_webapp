import _ from 'lodash';
import { initialize } from 'redux-form';

import constants from '../Constants/iterable';
import LoaderActions from './LoaderActions';
import IterableInteractor from '../Util/IterableInteractor';

const setMessageTypes = (messageTypes) => ({ type: constants.SET_MESSAGE_TYPES, messageTypes});
const setSuccessMessage = () => ({ type: constants.SET_SUCCESS_MESSAGE });
const setErrorMessage = (error) => ({ type: constants.SET_ERROR_MESSAGE, error});


class IterableActions {

  static getUserByEmail(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Fetching your email preferences"))
      IterableInteractor.getByEmail(body)
      .then((res) => {
        dispatch(LoaderActions.setIsLoading(-1));
        let initialValues = {}
        for (let key of res.data.messageTypes) {
          initialValues[`MessageTypeIds_${key.id}`] = key.type_is_subscribed;
        }
        dispatch(setMessageTypes(res.data.messageTypes));
        dispatch(initialize('emailPreferences',initialValues));
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
      });
    }
  }

  static updateSubscriptions(body) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Updating your email preferences"))
      return IterableInteractor.updateSubscriptions(body)
      .then((res) => {
        dispatch(IterableActions.getUserByEmail(body));
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setSuccessMessage());
        return res;
      })
      .catch((err) => {
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setErrorMessage({ err: 'error message'}));
        return;
      })
    }
  }

}

export default IterableActions;
