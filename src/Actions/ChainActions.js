import constants from '../Constants/chain';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setChainError = (error) => ({ type: constants.SET_CHAIN_ERROR, error });
const setChainDetail = (detail) => ({ type: constants.SET_CHAIN_DETAIL, detail });
const setPopularChains = (chains) => ({type: constants.SET_POPULAR_CHAINS, chains});


class ChainActions {

  static publicRetrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading chain.."));
      return ApiInteractor.publicRetrieveChain(id, {allergen: "*"})
      .then((chain) => {
        console.log("CHAIN RES: ", chain)
        dispatch(setChainDetail(chain));
        dispatch(LoaderActions.setIsLoading(-1));
        return chain;
      })
      .catch((err) => {
        console.log('Error in chain retrieve: ', err);
        dispatch(setChainError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      });
    };
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading chain.."));
      ApiInteractor.retrieveChain(id)
      .then((chain) => {
        dispatch(setChainDetail(chain));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in chain retrieve: ', err);
        dispatch(setChainError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      });
    };
  }

  static publicPopularRetrieve() {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading chain data.."));
      ApiInteractor.publicRetrievePopularChain()
      .then((popularChains) => {
        console.log('popularChains', popularChains)
        dispatch(setPopularChains(popularChains));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in chain retrieve: ', err);
        dispatch(setChainError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      });
    };
  }
}


export default ChainActions;
