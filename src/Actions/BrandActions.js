import constants from '../Constants/brand';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setBrandError = (error) => ({ type: constants.SET_BRAND_ERROR, error });
const setRetrieveBrand = (detail) => ({ type: constants.RETRIEVE_BRAND, detail });
const setListBrands = (list) => ({ type: constants.LIST_BRAND, list });
const brandClear = () => ({ type: constants.CLEAR_BRAND });
const brandErrorClear = () => ({ type: constants.CLEAR_BRAND_ERROR });

class BrandActions {

  static list(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting brands.."));
      ApiInteractor.listBrands(params)
      .then((brands) => {
        dispatch(setListBrands(brands));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in list brands: ', err);
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setBrandError(err));
      })
    }
  }

  static search(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Searching brands.."));
      ApiInteractor.searchBrands(params)
      .then((brands) => {
        dispatch(setListBrands(brands));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in list brands: ', err);
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setBrandError(err));
      })
    }
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Getting brands.."));
      ApiInteractor.retrieveBrand(id)
      .then((brand) => {
        dispatch(setRetrieveBrand(brand));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in retrieve brands: ', err);
        dispatch(LoaderActions.setIsLoading(-1));
        dispatch(setBrandError(err));
      })
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(brandClear());
    }
  }

  static clearError() {
    return (dispatch) => {
      dispatch(brandErrorClear());
    }
  }
}


export default BrandActions;
