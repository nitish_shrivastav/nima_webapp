import constants from '../Constants/review';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setReviewError = (error) => ({ type: constants.SET_REVIEW_ERROR, error })
const setReviewList = (list) => ({ type: constants.SET_REVIEW_LIST, list })
const setReviewDetail = (detail) => ({ type: constants.SET_REVIEW_DETAIL, detail })
const clearReview = () => ({ type: constants.CLEAR_REVIEW })
const clearReviewError = () => ({ type: constants.CLEAR_REVIEW_ERROR })


class ReviewActions {

  static list(params) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Finding reviews.."));
      ApiInteractor.listReviews(params)
      .then((reviews) => {
        dispatch(setReviewList(reviews))
        dispatch(LoaderActions.setIsLoading(-1))
      })
      .catch((err) => {
        console.log('Error in review list: ', err)
        dispatch(setReviewError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static retrieve(id) {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading review.."));
      ApiInteractor.retrieveReview(id)
      .then((review) => {
        dispatch(setReviewDetail(review));
        dispatch(LoaderActions.setIsLoading(-1));
      })
      .catch((err) => {
        console.log('Error in review retrieve: ', err)
        dispatch(setReviewError(err))
        dispatch(LoaderActions.setIsLoading(-1))
      })
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearReview());
    }
  }

  static clearError() {
    return (dispatch) => {
      dispatch(clearReviewError());
    }
  }
}


export default ReviewActions;
