import {
  SET_SUBSCRIPTION_PRODUCT_LIST,
  SET_SUBSCRIPTION_PRODUCT_ERROR,
  CLEAR_SUBSCRIPTION_PRODUCTS
} from '../Constants/subscription_product';
import ApiInteractor from '../Util/ApiInteractor';
import LoaderActions from './LoaderActions';

const setSubscriptionProductError = (error) => ({ type: SET_SUBSCRIPTION_PRODUCT_ERROR, error });
const setSubscriptionProductList = (list) => ({ type: SET_SUBSCRIPTION_PRODUCT_LIST, list });
const clearSubscriptionProduct = () => ({ type: CLEAR_SUBSCRIPTION_PRODUCTS });


class SubscriptionProductActions {

  static list() {
    return (dispatch) => {
      dispatch(LoaderActions.setIsLoading(1, "Loading product data.."));
      return ApiInteractor.listSubscriptionProducts()
      .then((list) => {
        dispatch(setSubscriptionProductList(list));
        dispatch(LoaderActions.setIsLoading(-1));
        return list;
      })
      .catch((err) => {
        console.log('err', err)
        dispatch(setSubscriptionProductError(err));
        dispatch(LoaderActions.setIsLoading(-1));
      })
    }
  }

  static setError(error) {
    return (dispatch) => {
      dispatch(setSubscriptionProductError(error));
    }
  }

  static clear() {
    return (dispatch) => {
      dispatch(clearSubscriptionProduct());
    }
  }
}


export default SubscriptionProductActions;
