
const fields = {
  email: {
    value: '',
    error: null,
    label: "Email",
    name: "email",
    isRequired: true
  },
  password: {
    value: '',
    error: null,
    type: "password",
    label: "Password",
    name: "password",
    isRequired: true
  },
  showPassword: {
    value: false,
    label: "Show Password",
    name: "showPassword"
  }
}

export default fields;