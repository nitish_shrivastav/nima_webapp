
const fields = {
  firstName: {
    value: '',
    error: null,
    label: "First Name",
    name: "firstName",
    isRequired: true
  },
  lastName: {
    value: '',
    error: null,
    label: "Last Name",
    name: "lastName",
    isRequired: true
  },
  email: {
    value: '',
    error: null,
    label: "Email",
    name: "email",
    isRequired: true
  },
  phone: {
    value: '',
    error: null,
    label: "Phone Number",
    name: "phone",
    isRequired: true
  },
  password: {
    value: '',
    error: null,
    type: "password",
    label: "Password",
    name: "password",
    isRequired: true
  },
  showPassword: {
    value: false,
    label: "Show Password",
    name: "showPassword"
  }
}

export default fields;