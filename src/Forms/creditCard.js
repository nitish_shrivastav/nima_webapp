const cantBeEmptyValidation = (value) => {
  return value !== null && value !== "";
}

const expirationMonthValidation = (month) => {
  if (cantBeEmptyValidation(month) === false) {
    return false;
  }
  else if (month.length > 2) {
    return false;
  }
  else if (isNaN(month)) {
    return false;
  }
  else {
    return true;
  }
}

const expirationYearValidation = (year) => {
  if (cantBeEmptyValidation(year) === false) {
    return false;
  }
  else if (isNaN(year)) {
    return false;
  }
  else if (year.length < 2 || year.length > 4) {
    return false;
  }
  else {
    return true;
  }
}


const cvcValidation = (cvc) => {
  if (cantBeEmptyValidation(cvc) === false) {
    return false;
  }
  else if (isNaN(cvc)) {
    return false;
  }
  else if (cvc.length < 3) {
    return false;
  }
  else {
    return true;
  }
}

const creditCardValidation = (cardNumber) => {
  if (cantBeEmptyValidation(cardNumber) === false) {
    return false;
  }
  else if (cardNumber.length < 15) {
    return false;
  }
  else {
    return true;
  }
}

const fields = {
  credit_card_number: {
    value: '',
    label: 'Credit card number',
    name: 'credit_card_number',
    error: null,
    isRequired: true,
    validation: creditCardValidation
  },
  credit_card_exp_month: {
    value: '',
    label: 'MM',
    name: 'credit_card_exp_month',
    info: "Expiration month on your credit card",
    isRequired: true,
    error: null,
    validation: expirationMonthValidation
  },
  credit_card_exp_year: {
    value: '',
    label: 'YY',
    info: "Expiration year on your credit card",
    name: 'credit_card_exp_year',
    isRequired: true,
    error: null,
    validation: expirationYearValidation
  },
  credit_card_cvc: {
    value: '',
    label: 'CVC',
    name: 'credit_card_cvc',
    info: "3 number verification number on your credit card",
    isRequired: true,
    error: null,
    validation: cvcValidation
  }
}

export default fields;