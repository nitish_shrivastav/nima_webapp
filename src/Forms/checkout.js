import creditCardFields from './creditCard';

const cantBeEmptyValidation = (value) => {
  return value !== null && value !== "";
}

const emailValidation = (email) => {
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const phoneNumberValidation = (phoneNumber) => {
  let re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return re.test(String(phoneNumber));
}

const countryOptions = [
  {
    text: "United States",
    value: "US"
  },
  {
    text: "Canada",
    value: "CA"
  }
]

let fields = {
  first_name: {
    value: '',
    error: null,
    label: "First name",
    name: "first_name",
    validation: cantBeEmptyValidation,
    isRequired: true
  },
  last_name: {
    value: '',
    error: null,
    label: "Last name",
    name: "last_name",
    isRequired: true,
    validation: cantBeEmptyValidation
  },
  email: {
    value: '',
    error: null,
    label: "Email",
    name: "email",
    isRequired: true,
    validation: emailValidation,
    autocomplete: "email"
  },
  billing_check: {
    value: true,
    label: "Same as billing address",
    name: "billing_check"
  },
  use_custom_address: {
    value: false,
    label: "Can't find your address?",
    name: "use_custom_address",
  },
  shipping_address: {
    value: '',
    label: "Shipping Address (USA/CAN)",
    name: "shipping_address",
    error: null,
    validation: cantBeEmptyValidation,
    isRequired: true,
    info: "Only ships to USA and Canada for now.",
    autocomplete: "disabled"
  },
  shipping_city: {
    value: '',
    label: "Shipping City",
    name: "shipping_city",
    error: null,
    isRequired: true,
    validation: cantBeEmptyValidation
  },
  shipping_state: {
    value: '',
    label: "State",
    name: "shipping_state",
    error: null,
    isRequired: true,
    validation: cantBeEmptyValidation
  },
  shipping_country: {
    value: "US",
    text: "United States",
    label: "Country",
    name: "shipping_country",
    error: null,
    isRequired: true,
    options: countryOptions
  },
  shipping_zip: {
    value: '',
    label: "Zip",
    name: "shipping_zip",
    error: null,
    isRequired: true,
    validation: cantBeEmptyValidation
  },
  shipping_suite: {
    value: '',
    label: "Apt/Suite #",
    name: "shipping_suite"
  },
  billing_country: {
    value: "US",
    text: "United States",
    label: "Country",
    name: "billing_country",
    error: null,
    isRequired: true,
    options: countryOptions
  },
  billing_address: {
    value: '',
    label: "Billing Address",
    name: "billing_address",
    error: null,
    validation: cantBeEmptyValidation,
    autocomplete: "disabled"
  },
  billing_city: {
    value: '',
    label: "City",
    name: "billing_city",
    error: null,
    validation: cantBeEmptyValidation
  },
  billing_state: {
    value: '',
    label: "State",
    name: "billing_state",
    error: null,
    validation: cantBeEmptyValidation
  },
  billing_zip: {
    value: '',
    label: "Billing Address",
    name: "billing_zip",
    error: null,
    validation: cantBeEmptyValidation
  },
  billing_suite: {
    value: '',
    label: "Apt/Suite #",
    name: "billing_suite"
  },
  phone_number: {
    value: '',
    label: 'Phone Number',
    name: 'phone_number',
    isRequired: true,
    error: null,
    validation: phoneNumberValidation
  },
  payment_source: {
    value: "",
    label: "Payment Method",
    name: "payment_source",
    for: "payment_source",
    isRequired: false,
    error: null,
    validation: cantBeEmptyValidation
  },
  is_gift: {
    value: false,
    label: "This is a gift.",
    name: "is_gift"
  },
  gift_message: {
    value: "",
    label: "Gift Message",
    name: "gift_message"
  },
  shipping_method: {
    value: '',
    label: 'Shipping Speed',
    name: 'shipping_method',
    text: ''
  }
}

export default Object.assign(fields, creditCardFields)
