import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from './Components/Loader';
import { withRouter } from 'react-router-dom';
import config from './config';

class App extends Component {
  componentWillMount() {
    let talkableScript = document.createElement('script');
    talkableScript.type = 'text/javascript';
    talkableScript.async = true;
    talkableScript.src = `https://d2jjzw81hqbuqv.cloudfront.net/integration/clients/${config.TALKABLE_ID}.min.js`
    window.document.body.appendChild(talkableScript);
    talkableScript.onload = () => {
      window._talkableq = window._talkableq || [];
      window._talkableq.unshift(['init', { site_id: config.TALKABLE_ID }]);
      window._talkableq.push(['authenticate_customer', {
        email: '', // Optional, pass when available. Example: 'customer@example.com'
        first_name: '', // Optional, pass when available. Example: 'John'
        last_name: '' // Optional, pass when available. Example: 'Smith'
      }]);
      window._talkableq.push(['register_affiliate', {}]);
    }
  }

  componentDidMount() {
    window.fbAsyncInit = function() {
      window.FB.init({
        appId: config.FACEBOOK.APP_ID,
        cookie: true,
        xfbml: true,
        version: config.FACEBOOK.VERSION 
      });
      window.FB.AppEvents.logPageView();   
      window.FB.getLoginStatus(function(res) {
      })
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }


  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.analytics.page({
        path: this.props.location.pathname,
        referrer: `${window.location.origin}${prevProps.location.pathname}${prevProps.location.search}`,
        search: this.props.location.search,
        url: `${window.location.origin}${this.props.location.pathname}`
      })
      window.scrollTo(0, 0)
    }
  }

  render() {
    return (
      <div className="nm-layout">
        <div id="nm-vwo-hook"/>
        {
          this.props.loader.isLoading !== 0 &&
          <Loader message={this.props.loader.message}/>
        }
        <div className="nm-layout">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default withRouter(connect(
  (state) => ({
    loader: state.loader
  })
)(App))
