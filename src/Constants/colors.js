export const ALLERGEN_COLOR = {
  "gluten": "#00C6A4",
  "peanut": "#551D5C",
  "under70": "#DB6F21"
}

export const YELLOW = "#ffbc00";
export const RED = "#f33b4d";
export const DARK_GREY = "#999999"; 
export const TEXT = "#333333";
export const GREEN = "#00C6A4";
export const PURPLE = "#551D5C";
export const GREY = "#D9D9D9";
export const LIGHT_GREY = "#F6F6F6";
export const ORANGE = "#DB6F21";
export const TEXT_OPAQUE = "rgba(54, 44, 42, .6)";
