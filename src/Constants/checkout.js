export default {
  RETRIEVE_CHECKOUT: "RETRIEVE_CHECKOUT",
  CLEAR_CHECKOUT: "CLEAR_CHECKOUT",
  SET_CHECKOUT_ERROR: "SET_CHECKOUT_ERROR",
  CLEAR_CHECKOUT_ERROR: "CLEAR_CHECKOUT_ERROR",
  SET_CHECKOUT_VALID_DISCOUNTS: "SET_CHECKOUT_VALID_DISCOUNTS",
  SET_CHECKOUT_TOTALS: "SET_CHECKOUT_TOTALS",
  SET_VALID_DISCOUNT: "SET_VALID_DISCOUNT",
  SET_DISCOUNT_ERROR: "SET_DISCOUNT_ERROR",
  SET_DISCOUNT_LOADING: "SET_DISCOUNT_LOADING",
  SET_SHIPPING_ADDRESS_LOADING: "SET_SHIPPING_ADDRESS_LOADING",
  SET_SHIPPING_RATES_LOADING: "SET_SHIPPING_RATES_LOADING"
};
