
export const GLUTEN_ASSETS = {
  "peanut": `${process.env.PUBLIC_URL}/images/icons/gluten-icon-gray@2x.png`,
  "gluten": `${process.env.PUBLIC_URL}/images/icons/gluten-icon@2x.png`
}

export const PEANUT_ASSETS = {
  "gluten": `${process.env.PUBLIC_URL}/images/icons/peanut-icon-gray@2x.png`,
  "peanut": `${process.env.PUBLIC_URL}/images/icons/peanut-icon@2x.png`
}

export const ALLERGEN_COLOR = {
  "gluten": "#00C6A4",
  "peanut": "#551D5C"
}

export const YELLOW = "#ffbc00";
