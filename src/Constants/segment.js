export default {
  PRODUCT_LIST_VIEWED: 'Product List Viewed',
  PRODUCT_VIEWED: 'Product Viewed',

  PRODUCT_ADDED:  'Product Added',
  PRODUCT_REMOVED: 'Product Removed',

  CHECKOUT_STARTED: 'Checkout Started',
  PAYMENT_INFO_ENTERED: 'Payment Info Entered',
  ORDER_COMPLETED: 'Order Completed',

  COUPON_ENTERED: 'Coupon Entered',
  COUPON_APPLIED: 'Coupon Applied',
  COUPON_DENIED: 'Coupon Denied',

  ALK_LINK_CLICKED: "Alk Link Clicked"  
};
