export const SET_ORDER_DETAIL = "SET_ORDER_DETAIL";
export const SET_ORDER_ERROR = "SET_ORDER_ERROR";
export const SET_ORDER_LIST = "SET_ORDER_LIST";

export default {
  SET_ORDER_DETAIL,
  SET_ORDER_ERROR,
  SET_ORDER_LIST
}
