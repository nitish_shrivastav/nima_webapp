export default {
  "gluten": {
    "-1": `${process.env.PUBLIC_URL}/images/icons/no_test_triangle@2x.png`,
    "0": `${process.env.PUBLIC_URL}/images/icons/result_testerror.png`,
    "1": `${process.env.PUBLIC_URL}/images/icons/gluten-test-results-icon@2x.png`,
    "2": `${process.env.PUBLIC_URL}/images/icons/gluten-test-results-icon@2x.png`,
    "3": `${process.env.PUBLIC_URL}/images/icons/gluten-3-icon.png`
  },
  "peanut": {
    "-1": `${process.env.PUBLIC_URL}/images/icons/no_test_triangle@2x.png`,
    "0": `${process.env.PUBLIC_URL}/images/icons/result_testerror.png`,
    "1": `${process.env.PUBLIC_URL}/images/icons/peanut-test-results-icon@2x.png`,
    "2": `${process.env.PUBLIC_URL}/images/icons/peanut-test-results-icon@2x.png`,
    "3": `${process.env.PUBLIC_URL}/images/icons/peanut-free-icon@2x.png`
  }
}
