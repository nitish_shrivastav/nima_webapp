export const MAP_ICONS = {
  "gluten": {
    "selected": `${process.env.PUBLIC_URL}/images/map/pin_green_s.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/pin_green.png`,
    "scaled_size": {
      "width": 42,
      "height": 42
    }
  },
  "gluten_chain": {
    "selected": `${process.env.PUBLIC_URL}/images/map/green-chain-icon.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/green-chain-icon-selected.png`,
    "scaled_size": {
      "width": 42,
      "height": 50
    }
  },
  "peanut": {
    "selected": `${process.env.PUBLIC_URL}/images/map/pin_purple_s@2x.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/pin_purple@2x.png`,
    "scaled_size": {
      "width": 42,
      "height": 42
    }
  },
  "peanut_chain": {
    "selected": `${process.env.PUBLIC_URL}/images/map/purple-chain-icon.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/purple-chain-icon-selected.png`,
    "scaled_size": {
      "width": 42,
      "height": 50
    }
  },
  "under70": {
    "selected": `${process.env.PUBLIC_URL}/images/map/pin_orange-outline.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/pin_orange-filled.png`,
    "scaled_size": {
      "width": 42,
      "height": 42
    }
  },
  "default": {
    "selected": `${process.env.PUBLIC_URL}/images/map/pin_grey_s.png`,
    "unselected": `${process.env.PUBLIC_URL}/images/map/pin_grey.png`,
    "scaled_size": {
      "width": 42,
      "height": 42
    }
  }
}
