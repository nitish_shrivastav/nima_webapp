export const PAYPAL = "paypal";
export const STRIPE = "stripe";
export const AFFIRM = "affirm";

export default {
  PAYPAL,
  STRIPE,
  AFFIRM
}
