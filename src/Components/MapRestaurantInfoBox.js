import React from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import StarRating from '../SVG/StarRating';
import FormatHelper from '../Util/FormatHelper';
import { ALLERGEN_COLOR, DARK_GREY, RED, TEXT } from '../Constants/colors';

const MapRestaurantInfoBox = (props) => {
  const experienceScore = _.get(props.selectedPlace, `scores.${props.allergen}.experience_score`);
  const reviewCount = `${_.get(props.selectedPlace, `counts[${props.allergen}].total_reviews`) ? props.selectedPlace.counts[props.allergen].total_reviews : "0"}`;
  return (
    <div className="nm-map-info" style={{border: `1px solid ${ALLERGEN_COLOR[props.allergen]}`}}>
      <h4 className="nm-layout" style={{fontSize: 18}}>
        {props.selectedPlace.name}
      </h4>
      <p className="nm-layout" style={{fontSize: 12, color: DARK_GREY}}>
        {props.selectedPlace.short_address}
        {
          props.selectedPlace.price_level &&
          <span style={{color: TEXT }}>
            {` ${"$".repeat(props.selectedPlace.price_level)}`}
          </span>
        }
      </p>
      {
        _.get(props.selectedPlace, `scores.${props.allergen}.allergen_free_percentage`) &&
        <p className="nm-layout" style={{fontSize: 14, color: ALLERGEN_COLOR[props.allergen], fontWeight: 600}}>
          {`${parseInt(props.selectedPlace.scores[props.allergen].allergen_free_percentage)}% ${FormatHelper.uppercaseFirstLetter(props.allergen)} Free`}
        </p>
      }
      {
        experienceScore &&
        <StarRating
          key={`map_detail-${props.link}`}
          startColor={ALLERGEN_COLOR[props.allergen]}
          stroke={ALLERGEN_COLOR[props.allergen]}
          idPrefix={`map_detail-${props.link}`}
          percent={100 * experienceScore / 5}
          style={{width: 20, height: 20, marginRight: 5}}/>
      }
      {
        reviewCount !== "0" &&
        <p className="nm-layout">
          {reviewCount} Reviews
        </p>
      }
      {
        reviewCount === "0" && !props.selectedPlace.chain_counts &&
        <p className="nm-layout" style={{ fontSize: 12, color: RED, fontStyle: 'italic'}}>
          Be the first to write a review!
        </p>
      }
      <Link
        to={`/map/places/${props.selectedPlace.id}?allergen=${props.allergen}`}
        className="nm-layout nm-link" style={{fontSize: 14}}>
        {`${reviewCount !== "0" ? "Read Reviews / " : ""}`}See Details →
      </Link>
      <div className="nm-clear"/>
    </div> 
  );
}

MapRestaurantInfoBox.propTypes = {
  selectedPlace: PropTypes.object.isRequired,
  allergen: PropTypes.string.isRequired
}

export default MapRestaurantInfoBox;
