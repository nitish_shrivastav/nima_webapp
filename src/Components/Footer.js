import React, { Component } from 'react';
import config from '../config';

class Footer extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    window.location = e.target.value;
  }

  render() {
    return (
      <footer className="nm-footer">
        <div className="nm-footer__contain">
          <div className="nm-footer__section-container">
            <div className="nm-footer__section">
              <h3 className="nm-footer__title">Products</h3>
              <p className="nm-footer__dash">-</p>
              <a className="nm-footer__item" href="/gluten">Nima Gluten Sensor</a>
              <a className="nm-footer__item" href="/peanut">Nima Peanut Sensor</a>
              <a className="nm-footer__item" href="/nima-app-review-contribute-discover/">Nima App</a>
              <div className="nm-spacer-48" />
              <img src={`${process.env.PUBLIC_URL}/images/image-paypal_now_accepting.png`} className="nm-image" style={{maxWidth: 150}} alt="affirm" />
              <div className="nm-spacer-12" />
              <img src={`${process.env.PUBLIC_URL}/images/image-affirm_pay_over_time.png`} className="nm-image" style={{maxWidth: 150}} alt="affirm" />
            </div>
            <div className="nm-footer__section">
              <h3 className="nm-footer__title">Support</h3>
              <p className="nm-footer__dash">-</p>
              <a className="nm-footer__item" href="https://help.nimasensor.com">FAQ</a>
              <a className="nm-footer__item" href="/coaches">Nima Coaches</a>
              <a className="nm-footer__item" href="/account/login">Your Account</a>
              <select className="nm-footer__item" onChange={this.handleChange}>
                <option selected>More &#x25BE;</option>
                <option value="/where-can-i-use-nima/">Where To Use</option>
                <option value="/nima-travel-tips-going-road-nima/">Travel Tips</option>
                <option value="/nima-warranty/">Warranty</option>
                <option value="/privacy">Privacy Policy</option>
                <option value="https://s3-us-west-2.amazonaws.com/nima-prod-downloadable-pdfs/nima_declaration_of_conformity.pdf">Declaration of Conformity</option>
                <option value="/nima-manual-use-nima/">Gluten Manual</option>
                <option value="/peanut/manual">Peanut Manual</option>
                <option value="/coaches">Nima Coaches</option>
                <option value="/nimoji-app">Nimoji</option>
                <option value="/register/">Register</option>
              </select>
            </div>
          </div>
          <div className="nm-footer__section-container">
            <div className="nm-footer__section">
              <h3 className="nm-footer__title">Community</h3>
              <p className="nm-footer__dash">-</p>
              <a className="nm-footer__item" href="/blog/">Blog</a>
              <a className="nm-footer__item" href="/nima-toolkit-dietitians-nutritionists/">For Doctors/Dietitians</a>
              <a className="nm-footer__item" href="/nima-for-restaurants-what-does-this-portable-gluten-sensor-mean/">For Restaurant Pros</a>
              <a className="nm-footer__item" href="/nima-college-ambassador-program/">For College Students</a>
              <a className="nm-footer__item" href="/gluten-free-restaurants/">Nima-Tested</a>
              <a className="nm-footer__item" href="/community-stories/">Nima-Tested Photos</a>
              <a className="nm-footer__item" href="/nima-community-values-live/">Values</a>
              <a className="nm-footer__item" href="/nima-videos/">Videos</a>
              <a className="nm-footer__icon" href="https://www.facebook.com/nimasensor">
                <i className="icon-facebook"></i>
              </a>
              <a className="nm-footer__icon" href="https://twitter.com/nimasensor">
                <i className="icon-twitter"></i>
              </a>
              <a className="nm-footer__icon" href="https://instagram.com/nimasensor/">
                <i className="icon-instagram"></i>
              </a>
              <a className="nm-footer__icon" href="https://www.pinterest.com/nimasensor/">
                <i className="icon-pinterest"></i>
              </a>
              <a className="nm-footer__icon" href="http://nimasensor.tumblr.com/">
                <i className="icon-tumblr"></i>
              </a>
              <a className="nm-footer__icon" href="https://www.youtube.com/channel/UCcaG6jwBbt16BHlcAa6OWww">
                <i className="icon-youtube-play"></i>
              </a>
            </div>
            <div className="nm-footer__section">
              <h3 className="nm-footer__title">Company</h3>
              <p className="nm-footer__dash">-</p>
              <a className="nm-footer__item" href="/nima-mission-products-company-history/">Mission</a>
              <a className="nm-footer__item" href="/science-nima-understanding-device/">Scientific Advisors</a>
              <a className="nm-footer__item" href="/press/">Press</a>
              <a className="nm-footer__item" href="/nima-careers-values-openings-come-work-with-us/">Careers</a>
              <a href="/">
                <img className="nm-footer__logo" src={`${process.env.PUBLIC_URL}/images/NimaPartnersOfficiallogo.png`} alt="Nima Logo"/>
              </a>
              <p className="nm-footer__item nm-footer__item--margin_0">&copy; 2018 Nima Labs, Inc.</p>
              <a className="nm-footer__item nm-footer__item--margin_0" href="/privacy">Privacy Policy</a>
              <p className="nm-footer__item nm-footer__item--margin_0">V {config.version}</p>
            </div>
          </div>
          <div className="nm-clear"></div>
        </div>
      </footer>
    );
  }
}

export default Footer;
