import React, { Component } from 'react';
import queryString from 'query-string';


const NorthAmerica = (props) => {
  const qs = queryString.parse(props.location.search)
  const allergenType = qs.allergen_type;
  const url = (qs) => {
    switch(qs) {
      case 'gluten':
        return "/shop/products/subscriptions";
      case 'peanut':
        return "/shop/products/peanut-test-capsules";
      default:
        return "/shop"
    }
  }
  return (
    <a href={`${url(allergenType)}`}>
      <img
        className="nm-map-image"
        src={`${process.env.PUBLIC_URL}images/northamerica-gray-unselected.png`}
        alt="north-america"
      />
      <h4 className="nm-map-text">North America</h4>
    </a>
  )
}

const MapRedirect = (props) => {
  return (
    <div>
      <div>
        <h4 style={{textAlign: 'center', marginTop: '60px'}}>We want to make sure we take you to the right store.</h4>
        <h4 style={{textAlign: 'center', marginBottom: '60px'}}>Please choose your current location.</h4>
      </div>
      <div id="map-redirect">
        <NorthAmerica {...props} />
        <h4 style={{marginRight: '60px', marginLeft: '60px'}}>or</h4>
        <a href="https://klarifyme.de/">
          <img
            className="nm-map-image"
            src={`${process.env.PUBLIC_URL}/images/europe-gray-unselected.png`}
            alt="europe"
          />
          <h4 className="nm-map-text">Europe</h4>
          <div id="test">
          </div>
        </a>
      </div>
    </div>
  )
}

export default MapRedirect;
