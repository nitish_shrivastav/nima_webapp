import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputError from './InputError';
import Close from '../SVG/Close';


const FormCard = (props) => {

  return (
    <div
      className="nm-layout nm-layout__max-center nm-layout__max-center--short nm-layout__max-center--mobile-support"
      style={{...props.style, overflow: 'scroll'}}>
      <div className="nm-card nm-card__shadow" style={props.cardStyle}>
        <div className="nm-card__bar">
          {
            props.onCloseClick &&
            <Close className="nm-card__close" onClick={props.onCloseClick}/>
          }
          <h2 className="nm-text-center nm-layout nm-bold">
            {props.title}
          </h2>
          {
            props.subtitle &&
            <p className="nm-text-center nm-layout">
              {props.subtitle}
            </p>
          }
          {
            props.error &&
            <InputError
              className="nm-text-center"
              show={true}
              error={props.error}/>
          }
        </div>
        <div className="nm-layout nm-layout__padding">
          <div className="nm-spacer-48"></div>
          {props.children}
          <div className="nm-spacer-48"></div>
          <div className="nm-clear"></div>
        </div>
      </div>
      <div className="nm-clear"></div>
      <div className="nm-spacer-48"></div>
      <div className="nm-clear"></div>
    </div>
  );
}

FormCard.defaultProps = {
  submitText: "Submit",
  style: {},
  cardStyle: {}
}

FormCard.propTypes = {
  submitText: PropTypes.string,
  children: PropTypes.element.isRequired,
  onCloseClick: PropTypes.func,
  error: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  onSubmit: PropTypes.func,
  style: PropTypes.object
}

export default FormCard;
