import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RadioInput from './RadioInput';
import Check from '../SVG/Check';


const methods = [
  //{
  //  value: "applepay",
  //  img: {
  //    src: "/images/payment/apple_pay.svg",
  //    alt: "apple_pay",
  //    style: {
  //      height: 33
  //    }
  //  }
  //},
  {
    title: "Debit or Credit Card",
    value: "stripe"
  },
  {
    value: "paypal",
    img: {
      src: `${process.env.PUBLIC_URL}/images/payment/paypal.svg`,
      alt: "paypal",
      style: {
        position: 'relative',
        top: 4
      }
    }
  },
  {
    value: "affirm",
    min_amount: 200,
    disabledText: "Only applies to orders over $200",
    img: {
      src: `${process.env.PUBLIC_URL}/images/payment/affirm.svg`,
      alt: "affirm",
      style: {
        position: 'relative',
        top: 2
      }
    }
  }
]
class PaymentMethodRadio extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(value, disabled) {
    if (!disabled) {
      this.props.onClick(value);
    } 
  }

  render() {
    return (
      <div className="nm-checkout__section">
        <h4>How would you like to pay?</h4>
        <div className="nm-payment">
          <ul className="nm-form__checkselect">
            {
              methods.map((method, key) => {
                let isDisabled = method.min_amount && this.props.totals.total < method.min_amount;
                return (
                  <li
                    className={`nm-form__checkselect-item ${isDisabled ? "nm-form__checkselect-item--disabled" : ""}`}
                    key={key}
                    onClick={() => this.handleClick(method.value, isDisabled)}>
                    <RadioInput checked={this.props.value === method.value}/>
                    {
                      method.img &&
                      <img
                        className="nm-image nm-image__inline nm-image__inline--icon"
                        src={method.img.src}
                        alt={method.img.alt}
                        style={method.img.style}/>
                    }
                    {
                      method.title &&
                      <span>{ method.title }</span>
                    } 
                    {
                      isDisabled && method.disabledText &&
                      <span style={{fontSize: 12, float: 'right', lineHeight: 2}}>{ method.disabledText }</span>
                    } 
                    {
                      method.value === this.props.authorizedMethod &&
                      <span style={{float: 'right', paddingRight: 30}}>
                        Authorized
                        <Check
                          style={{ right: 5, top: "calc(50% - 14px)" }}
                          animate={true}
                          className="nm-btn__check--input"/>
                      </span>
                    }
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

export default PaymentMethodRadio;
