import React, { Component } from 'react';

class Tooltip extends Component {
  
  render() {
    return (
      <span className="nm-tooltip__info">
        ⓘ
        <p className="nm-tooltip">
          {this.props.text}
        </p>
      </span>
    );
  }
}

export default Tooltip;