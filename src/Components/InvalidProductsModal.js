import React from 'react';
import _ from 'lodash';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import { AFFIRM, PAYPAL } from '../Constants/payment';
import Button from './Button';
import OrLine from './OrLine';
import Modal from './Modal';
import StorefrontInteractor from '../Util/StorefrontInteractor';

let InvalidProductsModal = (props) => {

  const getInvalidCheckoutItems = () =>{
    if (props.checkout.detail) {
      let invalidCheckoutItems = props.checkout.detail.lineItems.edges.map((lineItem) => {
        let variantId = StorefrontInteractor.decodeId(lineItem.node.variant.id, "ProductVariant");
        if (variantId in props.subscriptionProducts) {
          return lineItem;
        }
      }).filter(item => item);

      return invalidCheckoutItems.length > 0 ? invalidCheckoutItems : false;
    }
    else {
      return false;
    }
  }

  const onRemoveItemsClick = () => {
    let validCheckoutItems = _.filter(props.checkout.detail.lineItems.edges, (item) => {
      let variantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
      return !(variantId in props.subscriptionProducts)
    });
    let validLineItems = validCheckoutItems.map((item) => {
      return {
        quantity: item.node.quantity,
        variantId: item.node.variant.id
      }
    });
    props.onRemoveItemsClick(validLineItems);
  }

  const invalidCheckoutItems = getInvalidCheckoutItems();
  return (
    <div>
      {
        (props.provider === AFFIRM || props.isGuest) && invalidCheckoutItems &&
        <Modal>
          <div className="nm-modal__message" style={{maxWidth: 480}}>
            {
              props.provider === AFFIRM &&
              <img src={`${process.env.PUBLIC_URL}/images/payment/affirm.svg`} className="nm-image nm-image__center" style={{maxWidth: 100}}/>
            }
            <div className="nm-spacer-48"/>
            <h2 className="nm-layout nm-text-center nm-error" style={{fontWeight: 400}}>
              Wait! {invalidCheckoutItems.length > 1 ? "These items " : "This item "}
              { props.provider === AFFIRM ? "and Affirm won't mix." : "can't be purchased without an account" }
            </h2>
            {
              props.provider !== AFFIRM &&
              <div>
                <div className="nm-spacer-12"/>
                <Link to="/account/register" className="nm-link">
                  <Button
                    text="Register now."
                    style={{maxWidth: 265}}
                    preventDefault={false}
                    center={true}/>
                </Link>
                <div className="nm-clear"/>
              </div>
            }
            <div className="nm-spacer-12"/>
            <div className="nm-layout">
              {
                invalidCheckoutItems.map((item) => {
                  let variantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
                  let intervalType = props.subscriptionProducts[variantId].interval_frequency > 1 ? "months" : "month"; 
                  return (
                    <div className="nm-layout">
                      <img
                        className="nm-image nm-image__center nm-image__contain"
                        style={{width: 120, height: 120}}
                        src={item.node.variant.image.originalSrc} />
                      <div className="nm-clear"></div>
                      <div className="nm-spacer-12"></div>
                      <span className="nm-layout nm-text-center">{item.node.title}</span>
                      <span className="nm-layout nm-text-center" style={{fontSize: 10}}>
                        {item.node.quantity} every {props.subscriptionProducts[variantId].interval_frequency} {intervalType}
                      </span>
                      <div className="nm-spacer-12"/>
                    </div>
                  )
                })
              }
            </div>
            <div className="nm-layout nm-flex">
              <p className="nm-layout nm-text-center" style={{lineHeight: 1.2, fontSize: 13, maxWidth: 300}}>
                Subscription based items cannot be purchased
                { props.provider === AFFIRM ? " with Affirm " : " without an account " }
                due to recurring payments
              </p>
            </div>
            <div className="nm-spacer-12"/>
            <div className="nm-spacer-12"/>
            <div className="nm-clear"/>
            {
              props.isGuest === false &&
              <div>
                <Button
                  text="Select other payment method"
                  center={true}
                  style={{maxWidth: 285}}
                  onClick={props.onOtherPaymentMethodClick} />
                <div className="nm-spacer-12"/>
                <div className="nm-layout__max-center nm-padding-clear" style={{maxWidth: 275}}>
                  <OrLine/> 
                  <div className="nm-clear"/>
                </div>
              </div>
            }
            <div className="nm-spacer-12"/>
            <div className="nm-clear"/>
            <Button
              text={`Remove item${invalidCheckoutItems.length > 1 ? "s" : ""} from cart`}
              style={{maxWidth: 265}}
              center={true}
              onClick={onRemoveItemsClick}/>
            <div className="nm-clear"/>
            <div className="nm-spacer-12"/>
          </div>
        </Modal>
      }
    </div>
  )
}

InvalidProductsModal.propTypes = {
  onRemoveItemsClick: PropTypes.func.isRequired,
  checkout: PropTypes.object.isRequired,
  provider: PropTypes.string.isRequired,
  onOtherPaymentMethodClick: PropTypes.func.isRequired,
  subscriptionProducts: PropTypes.object.isRequired,
  isGuest: PropTypes.bool
}

export default InvalidProductsModal;
