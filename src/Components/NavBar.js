import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Close from '../SVG/Close';
import Modal from './Modal';
import fields from '../Forms/login';
import StorefrontStorage from '../Util/StorefrontStorage';
import CartButton from './CartButton';

const mobileItems = [
  {
    href: "/gluten",
    className: "nm-nav__item-mobile",
    isExternal: true,
    text: "Gluten Sensor"
  }, {
    href: "/peanut",
    className: "nm-nav__item-mobile",
    isExternal: true,
    text: "Peanut Sensor"
  }, {
    href: "/blog",
    className: "nm-nav__item-mobile",
    isExternal: true,
    text: "Blog"
  }, {
    href: "/map",
    className: "nm-nav__item-mobile",
    text: "Map"
  }, {
    href: "https://help.nimasensor.com",
    isExternal: true,
    className: "nm-nav__item-mobile",
    text: "Support"
  }, {
    href: "/shop",
    className: "nm-nav__item-mobile",
    text: "Shop"
  }, {
    href: "/account",
    className: "nm-nav__item-mobile",
    text: "Account"
  }
]

class NavBar extends Component {
  constructor() {
    super();
    this.handleMobileClick = this.handleMobileClick.bind(this);
    this.handleMobileCloseClick = this.handleMobileCloseClick.bind(this);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleCheckChange = this.handleCheckChange.bind(this);
  }

  componentWillMount() {
    this.setState(Object.assign({
      menuIconClass: "",
      mobileClass: "",
      closeClass: "",
      mobileItems: mobileItems,
      showModal: false,
      isLoading: false,
      loginError: null,
      user: StorefrontStorage.getUser()
    }, fields));
  }

  handleMobileClick() {
    let context = this;
    context.setState({
      menuIconClass: "shrink",
      mobileClass: "nm-nav__mobile--active"
    })
    setTimeout(() => {
      context.setState({
        closeClass: "nm-nav__close--active"
      })
    }, 300);
    for (let index in this.state.mobileItems) {
      setTimeout(() => {
        let items = JSON.parse(JSON.stringify(this.state.mobileItems));
        items[index].className = "nm-nav__item-mobile--active";
        context.setState({
          ...context,
          mobileItems: items
        })
      }, index * 50);
    }
  }

  handleLogin(user) {
    this.setState({
      showModal: false,
      user: user
    })
    this.props.onLogin(user)
  }

  handleMobileCloseClick() {
    let context = this;
    context.setState({
      mobileItems: mobileItems,
      mobileClass: "",
      closeClass: ""
    })
    setTimeout(() => {
      context.setState({
        menuIconClass: ""

      })
    }, 300)
  }

  handleLoginClick(e) {
    e.preventDefault();
    this.setState({
      showModal: true
    })

  }

  handleChange(val, name) {
    this.setState({
      [name]: {
        ...this.state[name],
        value: val
      }
    })
  }

  handleCloseClick() {
    this.setState({
      showModal: false
    })
  }

  handleCheckChange(val, name) {
    this.handleChange(val, name)
    this.setState({
      password: {
        ...this.state.password,
        type: val ? "text" : "password"
      }
    })
  }

  trackGAEvent(label) {
    let gaEvent = {
      category: 'link:global-header',
      action: 'click',
      label: label
    }
    if (window.dataLayer) {
      window.dataLayer.push(gaEvent);
    }
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-spacer-48"/>
        <div className="nm-nav__wrap">
          <div className="nm-nav">
            <div className="nm-layout__max-center nm-nav__max-center">
              <svg
                onClick={this.handleMobileClick}
                className={`nm-nav__menu-icon ${this.state.menuIconClass}`}
                id="Capa_1"
                x="0px"
                y="0px"
                viewBox="0 0 459 459">
                <g>
                  <path d="M0,382.5h459v-51H0V382.5z M0,255h459v-51H0V255z M0,76.5v51h459v-51H0z" fill="#333333"></path>
                </g>
              </svg>
              <svg
                className={`nm-nav__close ${this.state.closeClass}`}
                onClick={this.handleMobileCloseClick}
                width="18px"
                height="18px"
                viewBox="0 0 44 44"
                version="1.1">
                <g stroke="none" fill="#333333" strokeWidth="1" fillRule="evenodd">
                  <g>
                    <polygon points="30.73 15.04 23.77 22 30.73 28.96 28.96 30.73 22 23.77 15.04 30.73 13.27 28.96 20.23 22 13.27 15.04 15.04 13.27 22 20.23 28.96 13.27"></polygon>
                  </g>
                </g>
              </svg>
              {
                !this.props.hideCart &&
                <Link
                  to="/shop/cart"
                  onClick={() => this.trackGAEvent('cart-button')}
                  className="nm-nav__item keep"
                  style={{paddingTop: 4, paddingBottom: 0}}>
                  <CartButton checkout={this.props.checkout.detail}/>
                </Link>
              }
              <a
                href="https://help.nimasensor.com"
                className="nm-nav__item">
                Support
              </a>
              <Link
                to="/account"
                className="nm-nav__item">
                Account
              </Link>
              <Link
                to="/shop"
                className="nm-nav__item">
                Shop
              </Link>
              <a
                className="nm-nav__logo-wrap"
                href="/">
                <img
                  className="nm-pull--left nm-nav__logo-secondary"
                  src={`${process.env.PUBLIC_URL}/images/NimaPartnersOfficiallogo.png`}
                  alt="Nima Logo"/>
                <div className="nm-clear"></div>
              </a>
              <a
                href="/gluten"
                className="nm-nav__item nm-nav__item--left">
                Gluten Sensor
              </a>
              <a
                href="/peanut"
                className="nm-nav__item nm-nav__item--left">
                Peanut Sensor
              </a>
              <a
                href="/blog"
                className="nm-nav__item nm-nav__item--left">
                Blog
              </a>
              <a
                href="/map"
                className="nm-nav__item nm-nav__item--left">
                Map
              </a>
              <div className="nm-clear"></div>
              <div className={`nm-nav__mobile ${this.state.mobileClass}`}>
                {
                  this.state.mobileItems.map((item, index) => {
                    if (item.isExternal) {
                      return (
                        <a
                          key={`mobile-nav-${index}`}
                          href={item.href}
                          className={`nm-nav__item-mobile ${item.className}`}>
                          {item.text}
                        </a>
                      )
                    }
                    else {
                      return (
                        <Link
                          key={`mobile-nav-${index}`}
                          to={item.href}
                          className={`nm-nav__item-mobile ${item.className}`}>
                          {item.text}
                        </Link>
                      )
                    }
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

NavBar.propTypes = {
  secondary: PropTypes.bool,
  onLogin: PropTypes.func,
  user: PropTypes.object
}

export default connect(
  (state) => ({
    checkout: state.checkout,
    auth: state.auth
  })
)(NavBar);

