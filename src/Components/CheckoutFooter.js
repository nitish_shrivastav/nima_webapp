import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Lock from '../SVG/Lock';
import Heart from '../SVG/Heart';

class CheckoutFooter extends Component {
  render() {
    let cornerClass = this.props.hasTopCorners ? "nm-checkout__footer--corner" : "";
    return (
      <div className={`nm-layout nm-flex nm-flex__column-space nm-checkout__footer ${cornerClass}`}>
        <div style={{maxWidth: 200}}>
          <div className="nm-flex">
            <div className="nm-image nm-image__icon--large " style={{width: 120}}>
              <a href="https://trustsealinfo.websecurity.norton.com/splash?form_file=fdf/splash.fdf&dn=nimasensor.com&lang=en" target="_blank">
                <img className="nm-layout" src="https://seal.websecurity.norton.com/getseal?at=0&sealid=0&dn=nimasensor.com&lang=en&tpt=transparent"/>
              </a>
            </div>
          </div>
        </div>
        <div style={{maxWidth: 200}}>
          <div className="nm-flex">
            <div className="nm-image nm-image__icon--large">
              <img className="nm-layout" src={`${process.env.PUBLIC_URL}/images/basic-heart.png`}/>
            </div>
          </div>
          <a href="" className="nm-layout nm-text-center nm-link" target="_blank">
            100% Satisfaction Guaranteed
          </a>
          <div className="nm-spacer-12"></div>
        </div>
        <div style={{maxWidth: 200}}>
          <div className="nm-flex">
            <div className="nm-image nm-image__icon--large">
              <img className="nm-layout" src={`${process.env.PUBLIC_URL}/images/support-icon.png`}/>
            </div>
          </div>
          <a href="https://nimasensor.com/contact" className="nm-layout nm-text-center nm-link" target="_blank">
            Questions? Chat, email us, call 844-NIMA-WOW
          </a>
          <div className="nm-spacer-12"></div>
        </div>
        <div style={{maxWidth: 200}}>
          <div className="nm-flex">
            <div className="nm-image nm-image__icon--large">
              <img className="nm-layout" src={`${process.env.PUBLIC_URL}/images/simple-lock.png`}/>
            </div>
          </div>
          <a href="https://nimasensor.com/privacy-policy" className="nm-layout nm-text-center nm-link" target="_blank">
            Privacy Policy
          </a>
        </div>
        <div style={{maxWidth: 200}}>
          <div className="nm-flex">
            <div className="nm-image nm-image__icon--large">
              <img className="nm-layout" src={`${process.env.PUBLIC_URL}/images/300-reviews.png`}/>
            </div>
          </div>
          <div className="nm-spacer-12"></div>
        </div>
      </div>
    );
  }
}

CheckoutFooter.defaultProps = {
  hasTopCorners: false
}

CheckoutFooter.propTypes = {
  hasTopCorners: PropTypes.bool
}

export default CheckoutFooter;
