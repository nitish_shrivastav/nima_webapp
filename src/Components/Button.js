import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Check from '../SVG/Check';

class Button extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    this.setState({
      shakeClass: "",
      buttonCheckClass: ""
    })
  }

  handleClick(e) {
    if (e && this.props.preventDefault) {
      e.preventDefault();
    }
    if (this.props.useCheck) {
      this.setState({
        buttonCheckClass: "nm-btn--green"
      })
      setTimeout(() => {
        this.props.onClick(this.props.disabled)
        this.setState({
          buttonCheckClass: ""
        })
      }, 1000)
    }
    if (this.props.disabled) {
      this.setState({
        shakeClass: "nm-btn__shake",
      })
      setTimeout(() => {
        this.setState({
          shakeClass: ""
        })
      }, 500)
    }
    if (!this.props.useCheck && this.props.onClick) {
      this.props.onClick(this.props.disabled)
    }
  }

  render() {
    const { disabled, isLoading, className, center, loaderStyle } = this.props;
    const { shakeClass, buttonCheckClass } = this.state;
    let disabledClass = disabled || isLoading ? "nm-btn__disabled" : "nm-btn__hover";
    let buttonClass = className ? className : "";
    let loaderClass = isLoading ? "" : "nm-loader__hidden";
    let centerClass = center ? "nm-btn__center" : "";
    return (
      <button
        type={this.props.type}
        onClick={this.handleClick}
        style={this.props.style}
        className={`nm-btn ${buttonClass} ${centerClass} ${disabledClass} ${shakeClass} ${buttonCheckClass}`}>
        {
          buttonCheckClass === "" &&
          this.props.text
        }
        {
          buttonCheckClass !== "" &&
          <div>
            <div style={{position: 'absolute', top: 0, left: 'calc(50% - 21px)', width: 42}}>
              <Check animate={true} />
            </div>
            <span className="nm-layout nm-text-center" style={{color: "transparent"}}>
              {this.props.text}
            </span>
          </div>
        }
        { this.props.children }
        {
          this.props.useLoader &&
          <div className="nm-loader__button">
            <div className={`nm-loader ${loaderClass}`} style={{fontSize: 18, ...loaderStyle}}></div>
          </div>
        }
      </button>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  useLoader: PropTypes.bool,
  center: PropTypes.bool,
  style: PropTypes.object,
  loaderStyle: PropTypes.object,
  useCheck: PropTypes.bool,
  preventDefault: PropTypes.bool
}

Button.defaultProps = {
  loaderStyle: {},
  preventDefault: true,
  style: {},
  useCheck: false,
  type: "button"
}

export default Button;
