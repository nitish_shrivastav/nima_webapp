import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { reduxForm, submit } from 'redux-form';
import Button from '../Button';
import CreditCardFormFields from './CreditCardFormFields';


let PaymentMethodForm = (props) => {

  return (
    <form onSubmit={props.onSubmit} className="nm-layout">
      <CreditCardFormFields />
      <Button
        type="submit"
        onClick={() => props.dispatch(submit(props.form))}
        text="Add payment method"/>
    </form>
  );
}

PaymentMethodForm.propTypes = {
  form: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired
}

PaymentMethodForm = reduxForm({})(PaymentMethodForm);

export default connect()(PaymentMethodForm);
