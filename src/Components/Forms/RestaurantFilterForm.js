import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FieldArray, Field, reduxForm, formValueSelector, submit } from 'redux-form';
import InputField from '../ReduxFormFields/InputField';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import Button from '../Button';

let RestaurantFilterForm = (props) => {
  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <Field component={CheckboxField} name="glutenFree" label="Gluten Free"/>
      <Field component={CheckboxField} name="glutenFound" label="Gluten Found"/>
      <Field component={CheckboxField} name="peanutFound" label="Peanut Found"/>
      <Field component={CheckboxField} name="peanutFree" label="Peanut Free"/>

      <Field component={CheckboxField} name="restaurants" label="Restaurants"/>
      <Field component={CheckboxField} name="packaged_food" label="Packaged Food"/>
      <Field component={CheckboxField} name="other" label="Other food"/>
      <Button text="Filter" onClick={() => props.dispatch(submit('restaurantFilter'))}/>
      <div className="nm-clear"></div>
    </form>
  )
}

RestaurantFilterForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

RestaurantFilterForm = reduxForm({
  form: 'restaurantFilter'
})(RestaurantFilterForm);

export default connect(
  (state) => ({
  })
)(RestaurantFilterForm);
