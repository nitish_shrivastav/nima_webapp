import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import PropTypes from 'prop-types';

import { emailValidation, notEmptyValidation } from '../../Util/FormValidation';
import Button from '../Button';
import InputField from '../../Components/ReduxFormFields/InputField';
import InputError from '../../Components/InputError';

let ForgotPasswordForm = (props) => {
  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <Field
        component={InputField}
        validate={[notEmptyValidation, emailValidation]}
        name="email"
        label="Email"/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <div className="nm-btn__max nm-btn__center">
        <Button
          center={true}
          onClick={() => props.dispatch(submit('forgotPasswordForm'))}
          text="Send Instructions"
          preventDefault={true}
          type="submit"/>
      </div>
      <InputError
        className="nm-text-center"
        show={props.auth.error}
        error={props.auth.error} />
    </form>
  )
}

ForgotPasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

ForgotPasswordForm = reduxForm({
  form: 'forgotPasswordForm'
})(ForgotPasswordForm);

export default connect(
  (state) => ({
    auth: state.auth
  })
)(ForgotPasswordForm);
