import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FieldArray, Field, reduxForm, formValueSelector, submit } from 'redux-form';
import StepperField from '../ReduxFormFields/StepperField';
import MoneyHelper from '../../Util/MoneyHelper';
import StorefrontInteractor from '../../Util/StorefrontInteractor';

const renderItems = (props) => {
  return (
    <div>
      {
        props.fields.map((item, index) => {
          const lineItem = props.fields.get(index);
          return (
            <div className="nm-cart__row">
              <div className="nm-layout" style={{maxWidth: 400}}>
                <Link to={`/shop/products/${lineItem.node.variant.product.handle}`}>
                  <img className="nm-cart__image" src={lineItem.node.variant.image.transformedSrc}/>
                </Link>
                <div className="nm-cart__info-wrap">
                  <Link className="nm-cart__title" to={`/shop/products/${lineItem.node.variant.product.handle}`}>
                    <h4 className="nm-layout__sub-title-dark">
                      {lineItem.node.title}
                    </h4>
                    {
                      (StorefrontInteractor.decodeId(lineItem.node.variant.id, "ProductVariant") in props.subscriptionProducts) && lineItem.node.variant.title !== "Default Title" &&
                      <p className="nm-layout" style={{fontSize: 12, lineHeight: 1.2}}>
                        {lineItem.node.variant.title}
                      </p>
                    }
                  </Link>
                  <p className="nm-cart__remove nm-link" onClick={() => props.onRemoveClick(lineItem.node.id)}>
                    Remove
                  </p>
                </div>
              </div>
              <div className="nm-cart__label-wrap" style={{maxWidth: 350}}>
                <p className="nm-cart__label">Price</p>
                <p className="nm-cart__label">Quantity</p>
                <div className="nm-clear"></div>
                <p className="nm-cart__label" style={{lineHeight: 2}}>
                  { MoneyHelper.moneyWithoutTrailingZeros(lineItem.node.variant.price) }
                </p>
                <Field
                  name={`${item}.node.quantity`}
                  component={(quantity) => {
                    return (
                      <div className="nm-cart__label">
                        <StepperField {...quantity} className="nm-cart__stepper-wrap"/>
                        {
                          quantity.meta && quantity.meta.dirty &&
                          <p
                            style={{fontSize: 12}}
                            className="nm-text-center nm-link"
                            onClick={props.onSubmit}>
                            Update quantity
                          </p>
                        }
                      </div>
                    )}} />
                <div className="nm-clear"></div>
              </div>
            </div>
          )
        })
      }

    </div>
  )
}

let CartForm = (props) => {
  return (
    <form onSubmit={props.onSubmit}>
        <div className="">
          <FieldArray
           onSubmit={() => props.dispatch(submit('cart'))}
           name="lineItems.edges"
           component={renderItems}
           subscriptionProducts={props.subscription_product.list}
           onRemoveClick={props.onRemoveClick}/>
        </div>
    </form>
  )
}

CartForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

CartForm = reduxForm({
  form: 'cart'
})(CartForm);

const selector = formValueSelector('cart');
export default connect(
  (state) => ({
    initialValues: state.checkout.detail,
    subscription_product: state.subscription_product
  })
)(CartForm);
