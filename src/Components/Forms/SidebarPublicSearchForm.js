import React, { Fragment } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FieldArray, Field, reduxForm, formValueSelector, submit } from 'redux-form';
import SearchIcon from '../../SVG/SearchIcon';
import Pin from '../../SVG/Pin';
import { GLUTEN_ASSETS, PEANUT_ASSETS, ALLERGEN_COLOR } from '../../Constants/allergen';
import DropSelect from '../DropSelect';

let SidebarPublicSearchForm = (props) => {

  const handleAllergenClick = (allergenType) => {
    if (props.allergen !== allergenType) {
      props.change("allergen", allergenType);
    }
  }

  const dropItem = (item) => {
    return (
      <Fragment>
        <div className="nm-pull--left" style={{ margin: '6px 24px 0 16px' }}>
          <Pin width="18px" height="18px"/>
        </div>
        <p className="nm-form__li" style={{width: 'calc(100% - 58px)'}}>
          {item.description.replace(", USA", "")}
        </p>
      </Fragment>
    )
  }

  const handleCityClick = (city) => {
    props.onCityAutocompleteClick(city);
  }

  return (
    <form
      className="nm-search nm-search--simple"
      onSubmit={props.handleSubmit}>
      <div className="nm-layout">
        <p>Please fill out both search fields</p>
      </div>
      <div className="nm-spacer-12"/>
      <div className="nm-layout">
        <div className="nm-search__icon-wrap nm-search__icon-wrap--sidebar">
          <SearchIcon fill={ALLERGEN_COLOR[props.allergen]}/>
        </div>
        <div  className="nm-layout">
          <Field
            component="input"
            name="search"
            onBlur={props.onPlaceBlur}
            onChange={props.onPlaceChange}
            className="nm-search__input nm-search__input--simple"
            autoComplete="off"
            placeholder="Search restaurants, types of food, etc."/>
          <div className="nm-layout">
            {
              props.autocompletePlaces && props.autocompletePlaces.length > 0 &&
              <DropSelect
                onItemClick={handleCityClick}
                keyPrefix="sidebar_public_search_place_"
                renderDropItem={dropItem}
                ulStyle={{top: 0}}
                items={props.autocompleteCities}>
                <li className="nm-layout" style={{padding: 5}}>
                  <img
                    src={`${process.env.PUBLIC_URL}/images/powered_by_google.png`}
                    style={{ height: 20 }}
                    alt="Powered By Google"
                    className="nm-pull--right"/>
                </li>
              </DropSelect>
            }
          </div>
        </div>
      </div>
      <div className="nm-spacer-12"/>
      <div className="nm-layout">
        <div className="nm-search__icon-wrap nm-search__icon-wrap--sidebar">
          <Pin fill={ALLERGEN_COLOR[props.allergen]}/>
        </div>
        <Field
          component="input"
          name="where"
          autoComplete="off"
          className="nm-search__input nm-search__input--simple"
          onChange={props.onLocationChange}
          onBlur={props.onLocationBlur}
          placeholder="Ex: San Francisco, CA"/>
        <div className="nm-layout">
          {
            props.autocompleteCities && props.autocompleteCities.length > 0 &&
            <DropSelect
              onItemClick={handleCityClick}
              keyPrefix="sidebar_public_search_place"
              renderDropItem={dropItem}
              ulStyle={{top: 0}}
              items={props.autocompleteCities}>
              <li className="nm-layout" style={{padding: 5}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/powered_by_google.png`}
                  style={{ height: 20 }}
                  alt="Powered By Google"
                  className="nm-pull--right"/>
              </li>
            </DropSelect>
          }
        </div>
      </div>
      <div className="nm-spacer-12"/>
      <Field
        component="input"
        type="hidden"
        name="allergen"/>
      <div className="nm-pull--left">
        <div
          className="nm-pull--left nm-pointer"
          style={{marginRight: 12}}
          onClick={() => handleAllergenClick("gluten")}>
          <img className="nm-image nm-image__icon--med" src={GLUTEN_ASSETS[props.allergen]} />
        </div>
        <div
          className="nm-pull--left nm-pointer"
          style={{marginRight: 12}}
          onClick={() => handleAllergenClick("peanut")}>
          <img className="nm-image nm-image__icon--med" src={PEANUT_ASSETS[props.allergen]} />
        </div>
      </div>
      <button
        type="submit"
        style={{background: ALLERGEN_COLOR[props.allergen], maxWidth: 150, padding: 8}}
        className="nm-btn nm-pull--right">
        Search
      </button>
      <div className="nm-clear"></div>
    </form>
  )
}

SidebarPublicSearchForm.propTypes = {
  onLocationChange: PropTypes.func.isRequired,
  onCityAutocompleteClick: PropTypes.func.isRequired
}

SidebarPublicSearchForm = reduxForm({
  //gets form name from parent
  enableReinitialize: true
})(SidebarPublicSearchForm);

const selector = formValueSelector('publicPlaceSearch');

export default connect(
  (state) => ({
    allergen: selector(state, "allergen"),
    initialValues: {
      allergen: "gluten"
    }
  })
)(SidebarPublicSearchForm);
