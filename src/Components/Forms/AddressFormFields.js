import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Field } from 'redux-form';
import { STRIPE, AFFIRM, PAYPAL } from '../../Constants/payment';
import InputField from '../ReduxFormFields/InputField';
import DropDownField from '../ReduxFormFields/DropDownField';
import GooglePlacesSearchField from '../ReduxFormFields/GooglePlacesSearchField';
import countryOptions from '../../Data/countryOptions';

import { notEmptyValidation } from '../../Util/FormValidation';

let AddressFormFields = (props) => {

  const handlePlaceClick = (address) => {
    let formattedAddress = {
      address1: address.name,
      city: address.city,
      province: address.province_short,
      zip: address.postal_code,
      country: "US"
    }
    if (_.findIndex(countryOptions, { value: address.country_short }) > -1) {
      formattedAddress.country = address.country_short;
    }
    props.change(props.name, formattedAddress);
    if (props.onAddressChange) {
      props.onAddressChange(formattedAddress);
    }
  }

  const addressDropDownFormatter = (value, name) => {
    for (let option of countryOptions) {
      if (option.value === value) {
        return option.text;
      }
    }
    return value;
  }

  const handleAddressChange = (field, value) => {
    let address = JSON.parse(JSON.stringify(props.address));
    address[field] = value;
    props.change(props.name, address);
    if (props.onAddressChange) {
      props.onAddressChange(address);
    }
  }


  const addressClass = _.get(props, "address.address1") ? "" : "hide";
  const visibilityClass = props.isCollapsed ? "nm-checkout__section--collapse" : "";
  return (
    <div className="nm-layout">
      <div className={`nm-checkout__section ${visibilityClass}`}>
        <Field
          component={GooglePlacesSearchField}
          autocomplete="none"
          info="Only ships to USA and Canada for now."
          name={`${props.name}.address1`}
          onPlaceClick={(address) => handlePlaceClick(address, props.name)}
          validate={[notEmptyValidation]}
          label={props.label}/>
        <div className="nm-layout">
          <div className="nm-layout__field-half">
            <Field
              component={InputField}
              className={addressClass}
              name={`${props.name}.address2`}
              label="Apt/Suite #"/>
          </div>
          <div className="nm-layout__field-half">
            <Field
              component={DropDownField}
              options={countryOptions}
              className={addressClass}
              format={addressDropDownFormatter}
              onChange={(event, newValue) => {
                handleAddressChange('country', newValue)
              }}
              name={`${props.name}.country`}
              label="Country"/>
          </div>
        </div>
        <div className="nm-layout">
          <div className="nm-layout__field-half">
            <Field
              component={InputField}
              className={addressClass}
              validate={[notEmptyValidation]}
              name={`${props.name}.city`}
              label="City"/>
          </div>
          <div className="nm-layout__field-half">
            <Field
              component={InputField}
              className={`${addressClass} nm-layout__field-half`}
              validate={[notEmptyValidation]}
              onChange={(event) => handleAddressChange('province', event.target.value)}
              name={`${props.name}.province`}
              label="State/Province"/>
            <Field
              component={InputField}
              className={`${addressClass} nm-layout__field-half`}
              validate={[notEmptyValidation]}
              onChange={(event) => handleAddressChange('zip', event.target.value)}
              name={`${props.name}.zip`}
              label="Zip"/>
          </div>
        </div>
      </div>
    </div>
  )
}

AddressFormFields.propTypes = {
  name: PropTypes.string.isRequired,
  isCollapsed: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  //change is a redux form function
  change: PropTypes.func.isRequired,
  //selector is an instance of redux-form's formValueSelector
  selector: PropTypes.func.isRequired
}

export default connect(
  (state, props) => {
  return {
    address: props.selector(state, props.name)
  }
}
)(AddressFormFields);
