import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Field, reduxForm, submit } from 'redux-form';
import InputField from '../ReduxFormFields/InputField';
import InputError from '../InputError';
import { notEmptyValidation, emailValidation } from '../../Util/FormValidation';
import Button from '../Button';


let SSOLoginForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <Field 
        label="Email Address"
        component={InputField}
        validate={[emailValidation]}
        name="email"/>
      <Field
        label="Password"
        type="password"
        component={InputField}
        validate={[notEmptyValidation]}
        name="password"/>
      {
        props.showCreateAccount &&
        <div className="nm-layout">
          <Link to="/account/register" className="nm-link" style={{float: 'right'}}>
            Don't have an account? Register now.
          </Link>
        </div>
      }
      <div className="nm-layout">
        <Link to="/account/password-forgot" className="nm-link" style={{float: 'right'}}>
          Forgot Password?
        </Link>
      </div>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        type="submit"
        center={true}
        style={{maxWidth: 300}}
        text="Login"
        onClick={() => props.dispatch(submit('login'))}/>
      <div className="nm-spacer-12"/>
      <InputError
        show={props.auth.error}
        error={props.auth.error}/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <div
        className="nm-btn nm-btn__facebook"
        style={{width: 240, margin: '0 auto', float: 'none'}}
        onClick={props.onFacebookClick}>
        <img className="" src={`${process.env.PUBLIC_URL}/images/icons/facebook-logo-white.png`}/>
        <span style={{position: 'relative', top: -5, left: 8, color: 'white'}}>
          Continue with facebook
        </span>
        <div className="nm-clear"></div>
      </div>
    </form>
  )
}

SSOLoginForm.propTypes = {
  handlSubmit: PropTypes.func.isRequired,
  onFacebookClick: PropTypes.func.isRequired,
  showCreateAccount: PropTypes.bool
}

SSOLoginForm = reduxForm({
  form: 'login'
})(SSOLoginForm);

export default connect(
  (state) => ({
    auth: state.auth
  })
)(SSOLoginForm);
