import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Field, reduxForm, submit } from 'redux-form';
import InputField from '../ReduxFormFields/InputField';
import RadioField from '../ReduxFormFields/RadioField';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import { notEmptyValidation, emailValidation } from '../../Util/FormValidation';
import Button from '../Button';


let SidebarPlacesFilterForm = (props) => {
  return (
    <form onSubmit={props.onSubmit} className="nm-layout" style={{maxWidth: 200, padding: '0 12px'}}>
      <div className="nm-layout">
        <p className="nm-layout" style={{fontWeight: 500}}>
          Review Type
        </p>
        <Field 
          label="Gluten Free"
          component={CheckboxField}
          name="gluten_free"/>
        <Field 
          label="Peanut Free"
          component={CheckboxField}
          name="peanut_free"/>
          <div className="nm-spacer-12" />
          <div className="nm-layout nm-layout__bar"/>
          <div className="nm-spacer-12" />
      </div>
      <div className="nm-layout">
        <p className="nm-layout" style={{fontWeight: 500}}>
          Number of Reviews
        </p>
        <div className="nm-layout">
          <Field 
            label="0 - 50"
            component={RadioField}
            name="0-50"/>
        </div>
        <div className="nm-layout">
          <Field 
            label="50 - 100"
            component={RadioField}
            name="50-100"/>
        </div>
        <div className="nm-spacer-12" />
        <div className="nm-layout nm-layout__bar"/>
        <div className="nm-spacer-12" />
      </div>
      <div className="nm-layout">
        <p className="nm-layout" style={{fontWeight: 500}}>
          Food Types
        </p>
        <Field 
          label="Chinese"
          component={CheckboxField}
          name="chinese"/>
        <Field 
          label="Mexican"
          component={CheckboxField}
          name="mexican"/>
        <Field 
          label="Thai"
          component={CheckboxField}
          name="thai"/>
        <Field 
          label="Japanese"
          component={CheckboxField}
          name="japanese"/>
        <Field 
          label="Italian"
          component={CheckboxField}
          name="italian"/>
        <div className="nm-spacer-12" />
        <div className="nm-layout nm-layout__bar"/>
        <div className="nm-spacer-12" />
      </div>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
    </form>
  )
}

SidebarPlacesFilterForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

SidebarPlacesFilterForm = reduxForm({
  form: 'sidebarPublicPlacesFilter'
})(SidebarPlacesFilterForm);

export default connect(
  (state) => ({
  })
)(SidebarPlacesFilterForm);
