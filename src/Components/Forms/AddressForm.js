import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import AddressFormFields from './AddressFormFields';
import Button from '../Button';
import InputField from '../ReduxFormFields/InputField';
import { notEmptyValidation, phoneValidation } from '../../Util/FormValidation';

let AddressForm = (props) => {

  return (
    <form onSubmit={props.onSubmit} className="nm-layout">
      <div className="nm-checkout__section">
        <Field
          component={InputField}
          name="first_name"
          validate={[notEmptyValidation]}
          label="First Name"/>
        <Field
          component={InputField}
          name="last_name"
          validate={[notEmptyValidation]}
          label="Last Name"/>
        <Field
          component={InputField}
          name="phone"
          validate={[notEmptyValidation, phoneValidation]}
          label="Phone Number"/>
      </div>
      <AddressFormFields {...props}/>
      <Button
        type="submit"
        style={{maxWidth: 300}}
        center={true}
        text={props.submitText}
        onClick={() => props.dispatch(submit(props.form))}/>
    </form>
  )
}

AddressForm.propTypes = {
  name: PropTypes.string.isRequired,
  form: PropTypes.string.isRequired,
  submitText: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  selector: PropTypes.func.isRequired,
  isEdit: PropTypes.bool,
  onAddressChange: PropTypes.func
}

AddressForm = reduxForm({})(AddressForm);

export default connect(
  (state, props) => {
    let firstName = _.get(state, 'address.detail.first_name');
    let lastName = _.get(state, 'address.detail.last_name');
    let phone = _.get(state, 'address.detail.phone');
    let address1 = _.get(state, 'address.detail.address1');
    let address2 = _.get(state, 'address.detail.address2');
    let country = _.get(state, 'address.detail.country_code');
    let province = _.get(state, 'address.detail.province_code');
    let city = _.get(state, 'address.detail.city');
    let zip = _.get(state, 'address.detail.zip');
    return {
      initialValues: {
        first_name: firstName && props.isEdit ? firstName : "",
        last_name: lastName && props.isEdit ? lastName : "",
        phone: phone && props.isEdit ? phone : "",
        [props.name]: {
          address1: address1 && props.isEdit ? address1 : "",
          address2: address2 && props.isEdit ? address2 : "",
          country: country && props.isEdit ? country : "US",
          province: province && props.isEdit ? province : "",
          city: city && props.isEdit ? city : "",
          zip: zip && props.isEdit ? zip : "" 
        }
      }
    }
  }
)(AddressForm);
