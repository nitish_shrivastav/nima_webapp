import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, submit, formValueSelector } from 'redux-form';
import InputField from '../ReduxFormFields/InputField';
import Button from '../Button';

let SubscriptionDiscountForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <Field 
        label="Discount Code"
        component={InputField}
        name="discount"/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        type="submit"
        center={true}
        style={{maxWidth: 300}}
        text="Submit"
        onClick={() => props.dispatch(submit('subscriptionDiscount'))}/>
    </form>
  )
}


const selector = formValueSelector('subscriptionDiscount');

SubscriptionDiscountForm.propTypes = {
  handlSubmit: PropTypes.func.isRequired
}

SubscriptionDiscountForm = reduxForm({
  form: 'subscriptionDiscount'
})(SubscriptionDiscountForm);

export default connect(
  (state) => {
    return {
    initialValues: {
      discount: _.get(state, 'subscription.detail.discounts[0].id', "")
    }
  }}
)(SubscriptionDiscountForm);
