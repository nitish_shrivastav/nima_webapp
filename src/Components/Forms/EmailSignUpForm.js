import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Field, reduxForm, submit } from 'redux-form';
import InputField from '../ReduxFormFields/InputField';
import { emailValidation } from '../../Util/FormValidation';
import Button from '../Button';


let EmailSignUpForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <Field 
        label="Email Address"
        component={InputField}
        validate={[emailValidation]}
        name="email"/>
      <Button
        type="submit"
        center={true}
        style={{maxWidth: 300}}
        text="Join the community"
        onClick={() => props.dispatch(submit('email-signup'))}/>
      <div className="nm-clear"></div>
    </form>
  )
}

EmailSignUpForm.propTypes = {
  handlSubmit: PropTypes.func.isRequired
}

EmailSignUpForm = reduxForm({
  form: 'email-signup'
})(EmailSignUpForm);

export default connect()(EmailSignUpForm);
