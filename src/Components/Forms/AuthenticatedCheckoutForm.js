import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { Field, reduxForm, formValueSelector, submit } from 'redux-form';
import { STRIPE, AFFIRM, PAYPAL } from '../../Constants/payment';
import Add from '../../SVG/Add';
import Button from '../Button';
import Check from '../../SVG/Check';
import RadioSelect from '../ReduxFormFields/RadioSelect';
import InputField from '../ReduxFormFields/InputField';
import InputError from '../InputError';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import CheckoutButton from '../CheckoutButton';
import InvalidProductsModal from '../InvalidProductsModal';
import OrderSummary from '../OrderSummary';
import paymentOptions from '../../Data/paymentOptions';
import CheckoutHelper from '../../Util/CheckoutHelper';
import AddressLine from '../AddressLine';

import { notEmptyValidation } from '../../Util/FormValidation';

let AuthenticatedCheckoutForm = (props) => {


  const handlePaymentMethodChange = (method) => {
    props.change('provider', method);
    props.change('payment_source', "");
    props.onPaymentMethodClick(method);
  }

  const creditCardVisibilityClass = props.provider === PAYPAL || props.provider === AFFIRM ? "nm-checkout__section--collapse" : "";
  const shippingRates = _.get(props, 'checkout.detail.availableShippingRates.shippingRates');
  return (
    <div className="nm-layout">
      <form onSubmit={props.onSubmit} className="nm-checkout">
        <input type="hidden" value="remove-autocomplete"/>
        <div className="nm-checkout__section">
          <Field
            component={RadioSelect}
            options={
              paymentOptions.map((option) => {
                return {
                  ...option,
                  isDisabled: option.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200
                }
              })
            }
            onClick={(option) => handlePaymentMethodChange(option.value)}
            renderOption={(index) => {
              let method = paymentOptions[index];
              return (
                <div>
                  {
                    method.img &&
                    <img
                      className="nm-image nm-image__inline nm-image__inline--icon"
                      src={method.img.src}
                      alt={method.img.alt}
                      style={method.img.style}/>
                  }
                  {
                    method.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200 &&
                    <span style={{fontSize: 12, float: 'right', lineHeight: 2}}>
                      Only applies to orders over $200
                    </span>
                  }
                  {
                    method.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200 &&
                    method.value === PAYPAL && props.provider === PAYPAL && props.payment_source &&
                    <span style={{float: 'right', paddingRight: 30}}>
                      Authorized
                      <Check
                        style={{ right: 5, top: "calc(50% - 14px)" }}
                        animate={true}
                        className="nm-btn__check--input"/>
                    </span>
                  }
                </div>
              )
            }}
            name="provider"
            label="How would you like to pay?" />
        </div>
        <div className={`nm-checkout__section ${creditCardVisibilityClass}`}>
          {
            props.provider === PAYPAL &&
            <Field
              component="input"
              type="hidden"
              name="paypal_nonce"/>
          }
          {
            props.provider === STRIPE && _.get(props, 'payment_method.list.sources.data') &&
            <div className="nm-layout">
              <Field
                component={RadioSelect}
                validate={[notEmptyValidation]}
                options={
                  props.payment_method.list.sources.data.map((paymentMethod) => {
                    return {
                      value: paymentMethod.id,
                      text: ""
                    }
                  })
                }
                renderOption={(index) => {
                  const option = props.payment_method.list.sources.data[index];
                  const card = CheckoutHelper.getCreditCardDetails(option);
                  const checked = props.payment_source === option.id;
                  return (
                    <div>
                    •••• {card.last4}
                    <span style={{position: "relative", left: 10}}>
                      exp: {card.exp_month}/{card.exp_year}
                    </span>
                    <img
                      src={`${process.env.PUBLIC_URL}/images/credit_cards/${card.brand.toLowerCase().replace(' ', '_')}.svg`}
                      style={{top: 10, opacity: checked ? 1 : .3}}
                      className="nm-image nm-image__card show"
                      alt="credit_card_image"/>
                    </div>
                  )
                }}
                name="payment_source"
                label="Select a payment method" />
            </div>
          }
          {
            props.payment_method && props.payment_method.error &&
            <div className="nm-layout">
              <p className="nm-form__error" style={{fontSize: 20, textAlign: 'center'}}>
                You must verify your email to add a payment method.
              </p>
              <div className="nm-spacer-12"/>
              <Link to="/account/email-verify/resend">
                <Button center={true} text="Resend validation email." style={{maxWidth: 300}} preventDefault={false}/>
              </Link>
            </div>
          }
          {
            props.payment_method && !props.payment_method.error &&
            <div className="nm-layout">
              <ul className="nm-form__checkselect">
                <li className="nm-form__checkselect-item" style={{paddingLeft: 12}} onClick={props.onNewCardClick}>
                  <Add />
                  Add a new credit card
                </li>
              </ul>
            </div>
          }
        </div>
        <div className="nm-layout nm-layout__bar"/>
        <div className="nm-checkout__section" style={{overflow: "scroll"}}>
          <div className="nm-layout">
            <Field
              component={RadioSelect}
              options={
                props.address.list.map((option) => {
                  return {
                    ...option,
                    value: option.id
                  }
                })
              }
              onClick={(option) => {
                props.change("shipping_address", option.id);
                props.onShippingAddressChange(option);
              }}
              renderOption={(index) => {
                return (
                  <AddressLine
                    address={props.address.list[index]}
                    onAddressDeleteClick={(address) => { props.onAddressDeleteClick(address)}}
                    onAddressEditClick={(address) => { props.onAddressEditClick(address)}}/> 
                )
              }}
              name="shipping_address"
              label="Select Shipping Address" />
              <ul className="nm-form__checkselect">
                <li className="nm-form__checkselect-item" style={{paddingLeft: 12}} onClick={props.onNewAddressClick}>
                  <Add />
                  Add new address
                </li>
              </ul>
          </div>
        </div>
        <div className={`nm-layout nm-layout__bar ${creditCardVisibilityClass}`}/>
        <div className="nm-layout nm-layout__bar"/>
        {
          (props.provider !== PAYPAL || props.payment_source) &&
          <div className="nm-checkout__section">
            <Field
              component={CheckboxField}
              name="is_gift"
              label="This is a gift"/>
            <div className="nm-spacer-12"/>
            {
              props.is_gift &&
              <Field
                component={InputField}
                name="gift_message"
                label="Gift Message"/>
            }
            <p className="nm-layout" style={{fontSize: 12}}>
              - If you are trying to redeem a voucher, <Link to="/shop/voucher/">Click here.</Link>
            </p>
            <Field
              component={InputField}
              success={props.checkout.validDiscount}
              name="discount_codes"
              label="Discount Code"/>
            <InputError
              className="nm-text-center"
              show={props.checkout.discountError}
              error={props.checkout.discountError}/>
            <div className="nm-clear"/>
            {
              props.discount_codes && props.discount_codes !== "" &&
              <Button
                text="Apply Discount"
                className="nm-btn__small nm-btn__center"
                onClick={() => props.onSubmitDiscountValidate(props.discount_codes, props.email)} />
            }
          </div>
        }
        <div className="nm-layout nm-layout__bar"/>
        <div className="nm-checkout__section">
          {
            _.get(props, 'checkout.detail.requiresShipping') && (!shippingRates || shippingRates.length === 0) &&
            <p className="nm-layout nm-text-center" style={{ fontStyle: 'italic' }}>
              Enter a valid shipping address to see available shipping rates.
            </p>
          }
          {
            _.get(props, 'checkout.detail.requiresShipping') &&
            shippingRates &&
            shippingRates.length > 0 &&
            (props.provider !== PAYPAL || props.payment_source) &&
            <Field
              isLoading={props.checkout.shippingRatesIsLoading}
              component={RadioSelect}
              options={
                shippingRates.map((rate) => {
                  return {
                    value: rate.handle,
                    title: `${rate.title} - $${rate.price}`
                  }
                })
              }
              renderOption={(option) => option.price}
              validate={[notEmptyValidation]}
              onClick={(option) => {
                props.change('shipping_method', option.value);
                props.onShippingRateClick(option.value);

              }}
              name="shipping_method"
              label="Shipping speed" />
          }
        </div>
        <div className="nm-checkout__section">
          <div className="nm-spacer-12"></div>
          <p className="nm-layout nm-text-center">
            Total: ${props.checkout.totals.total}
          </p>
          <div className="nm-spacer-12"></div>
          <div className="nm-clear"></div>
          <CheckoutButton
            type_={props.provider}
            total={props.checkout.totals.total}
            hasPaymentToken={
              (props.provider === STRIPE && props.payment_source && props.payment_source !== "") ||
              (props.provider === PAYPAL && props.paypal_nonce && props.paypal_nonce !== "")
            }
            disabled={props.invalid}
            isLoading={false}
            paypalButtonClass="authenticated-paypal-button"
            onClick={() => props.dispatch(submit('authenticatedCheckoutForm'))} />
          <InputError
            className="nm-text-center"
            show={props.order.error}
            error={props.order.error} />
        </div>
        <InvalidProductsModal
          onRemoveItemsClick={props.onRemoveItemsClick}
          checkout={props.checkout}
          subscriptionProducts={props.subscription_product.list}
          isGuest={false}
          onOtherPaymentMethodClick={() => handlePaymentMethodChange(STRIPE)}
          provider={props.provider}/>
        <div className="nm-clear"></div>
      </form>
      <OrderSummary
        country={props.shipping_country}
        checkout={props.checkout.detail}
        subscriptionProducts={props.subscription_product.list}
        totals={props.checkout.totals}
        hasPremium={false}/>
    </div>
  )
}

AuthenticatedCheckoutForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onRemoveItemsClick: PropTypes.func.isRequired,
  onPaymentMethodClick: PropTypes.func.isRequired,
  invalidAffirmCheckoutItems: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.array
  ])
}

AuthenticatedCheckoutForm = reduxForm({
  form: 'authenticatedCheckoutForm'
})(AuthenticatedCheckoutForm);

const selector = formValueSelector('authenticatedCheckoutForm');

export default connect(
  (state) => {
  return {
    auth: state.auth,
    order: state.order,
    checkout: state.checkout,
    address: state.address,
    payment_method: state.payment_method,
    subscription_product: state.subscription_product,
    membership: state.membership,
    provider: selector(state, 'provider'),
    shipping_address: selector(state, 'shipping_address'),
    billing_address: selector(state, 'billing_address'),
    billing_check: selector(state, 'billing_check'),
    payment_source: selector(state, 'payment_source'),
    paypal_nonce: selector(state, 'paypal_nonce'),
    is_gift: selector(state, 'is_gift'),
    discount_codes: selector(state, 'discount_codes'),
    initialValues: {
      billing_check: true,
      provider: STRIPE,
      is_gift: false,
      shipping_address: {
        country: "US"
      }
    }
  }
}
)(AuthenticatedCheckoutForm);
