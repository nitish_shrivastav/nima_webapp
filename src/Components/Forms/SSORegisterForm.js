import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, submit, formValueSelector } from 'redux-form';
import { RED, GREEN, LIGHT_GREY, GREY } from '../../Constants/colors';
import InputField from '../ReduxFormFields/InputField';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import InputError from '../InputError';
import {
  notEmptyValidation,
  emailValidation,
  atLeastOneLowerValidation,
  atLeastOneNumberValidation,
  atLeastOneUpperValidation,
  atLeastOneSpecialValidation,
  char8Validation 
} from '../../Util/FormValidation';
import Button from '../Button';
import OrLine from '../OrLine';
import Check from '../../SVG/Check';


const passwordValidation = [
  {
    description: "Lower case (a-z)",
    validation: atLeastOneLowerValidation
  },
  {
    description: "Upper case (A-Z)",
    validation: atLeastOneUpperValidation
  },
  {
    description: "Number (0-9)",
    validation: atLeastOneNumberValidation
  },
  {
    description: "Special characters (!@#$%^&*)",
    validation: atLeastOneSpecialValidation 
  },
  {
    description: "Must have 8 charactors",
    validation: char8Validation
  }
]


let SSORegisterForm = (props) => {

  const validatePassword = () => {
    for (let section of passwordValidation) {
      if (!props.password || section.validation(props.password)) {
        return false; 
      }
    }
    return true;
  }

  const validPassword = validatePassword();
  return (
    <form onSubmit={props.handleSubmit}>
      <Field 
        label="First Name"
        component={InputField}
        validate={[notEmptyValidation]}
        name="first_name"/>
      <Field 
        label="Last Name"
        component={InputField}
        validate={[notEmptyValidation]}
        name="last_name"/>
      <Field 
        label="Email Address"
        component={InputField}
        validate={[emailValidation]}
        name="email"/>
      <Field
        label="Phone"  
        component={InputField}
        name="phone"/>
      <Field
        label="Password"
        type={props.show_password ? "input" : "password"}
        component={InputField}
        validate={[
          notEmptyValidation,
          atLeastOneLowerValidation,
          atLeastOneUpperValidation,
          atLeastOneNumberValidation,
          atLeastOneSpecialValidation,
          char8Validation
        ]}
        name="password"/>
      {/* <Field
        component={CheckboxField}
        name="show_password"
        label="Show password?"/> */}
      <div className="nm-spacer-12"></div>
      <div className="nm-layout" style={{borderRadius: 6, background: LIGHT_GREY, padding: 12, border: `1px solid ${validPassword ? GREEN : GREY}` }}>
        {
          passwordValidation.map((pV) => {
            const isValid = props.password && !pV.validation(props.password);
            return (
              <div className="nm-layout">
                <div className="nm-layout">
                  <div
                    className="nm-flex"
                    style={{
                      width: 20,
                      height: 20,
                      background: isValid ? GREEN : GREY,
                      marginRight: 12,
                      borderRadius: 20,
                      float: 'left' }}>
                    {
                      isValid &&
                      <Check style={{width: 20, height: 20}} animate={true}/>
                    }
                  </div>
                  <span style={{color: isValid ? GREEN : "inherit", fontSize: 12, fontWeight: 400, position: 'relative', top: -2}}>
                    {pV.description}
                  </span>
                </div>
              </div>
            )
          })
        }
      </div>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        type="submit"
        center={true}
        style={{maxWidth: 300}}
        text="Register"
        onClick={() => props.dispatch(submit('register'))}/>
      <div className="nm-clear"></div>
      <InputError
        show={props.auth.error}
        error={props.auth.error}/>
      <div className="nm-spacer-12"></div>
      <OrLine backgroundColor="white"/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <div class="nm-btn nm-btn__facebook" style={{width: 250, margin: '0 auto', float: 'none'}} onClick={props.onFacebookClick}>
        <img className="" src={`${process.env.PUBLIC_URL}/images/icons/facebook-logo-white.png`}/>
        <span style={{position: 'relative', top: -5, left: 8, color: 'white'}}>
          Register through facebook
        </span>
      </div>
    </form>
  )
}


const selector = formValueSelector('register');

SSORegisterForm.propTypes = {
  handlSubmit: PropTypes.func.isRequired,
  onFacebookClick: PropTypes.func.isRequired
}

SSORegisterForm = reduxForm({
  form: 'register'
})(SSORegisterForm);

export default connect(
  (state) => ({
    auth: state.auth,
    // show_password: selector(state, 'show_password'),
    password: selector(state, 'password'),
    initialValues: {
      // show_password: false,
      password: ""
    }
  })
)(SSORegisterForm);
