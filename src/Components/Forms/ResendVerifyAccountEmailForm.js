import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import PropTypes from 'prop-types';

import { emailValidation, notEmptyValidation } from '../../Util/FormValidation';
import Button from '../Button';
import InputField from '../../Components/ReduxFormFields/InputField';
import InputError from '../../Components/InputError';

let ResendVerifyAccountEmailForm= (props) => {

  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <div className="nm-checkout__section">
        <Field
          component={InputField}
          validate={[notEmptyValidation, emailValidation]}
          name="email"
          label="Email"/>
        <div className="nm-spacer-12"></div>
        <div className="nm-clear"></div>
        <div className="nm-btn__max nm-btn__center">
          <Button
            center={true}
            text="Resend Email"
            preventDefault={true}
            type="submit"
            onClick={() => props.dispatch(submit('resendVerifyAccountEmailForm'))}>
          </Button>
        </div>
        <InputError
          className="nm-text-center"
          show={props.auth.error}
          error={props.auth.error} />
      </div>
    </form>
  )
}

ResendVerifyAccountEmailForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

ResendVerifyAccountEmailForm = reduxForm({
  form: 'resendVerifyAccountEmailForm'
})(ResendVerifyAccountEmailForm);

export default connect(
  (state) => ({
    auth: state.auth
  })
)(ResendVerifyAccountEmailForm);
