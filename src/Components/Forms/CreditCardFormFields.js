import React, { Component } from 'react';
import { Field } from 'redux-form';
import { connect } from 'react-redux';
import InputField from '../ReduxFormFields/InputField';
import CreditCardField from '../ReduxFormFields/CreditCardField';
import {
  notEmptyValidation,
  creditCardValidation,
  expirationMonthValidation,
  expirationYearValidation,
  cvcValidation
} from '../../Util/FormValidation';


const CreditCardFormFields = (props) => {

  return (
    <div className="nm-layout">
      <Field
        autocomplete="cc-number"
        component={CreditCardField}
        name="credit_card_number"
        validate={[notEmptyValidation, creditCardValidation]}
        label="Credit card number"/>
      <div className="nm-layout">
        <div className="nm-layout__field-half">
          <Field
            autocomplete="cc-exp"
            className="nm-layout__field-half"
            component={InputField}
            info="Expiration month on your credit card"
            name="credit_card_exp_month"
            validate={[notEmptyValidation, expirationMonthValidation]}
            label="MM"/>
          <Field
            autocomplete="cc-exp"
            className="nm-layout__field-half"
            info="Expiration year on your credit card"
            component={InputField}
            validate={[notEmptyValidation, expirationYearValidation]}
            name="credit_card_exp_year"
            label="YY"/>
        </div>
        <div className="nm-layout__field-half">
          <Field
            autocomplete="cc-csc"
            info="3 number verification number on your credit card"
            component={InputField}
            validate={[notEmptyValidation, cvcValidation]}
            name="credit_card_cvc"
            label="CVC"/>
        </div>
      </div>
    </div>
  );
}


export default connect(
  (state) => ({
  })
)(CreditCardFormFields);
