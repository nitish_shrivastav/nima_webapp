import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import _ from 'lodash';
import { TEXT } from '../../Constants/colors';

import Button from '../Button';
import InputError from '../InputError';


let EmailPreferencesForm = (props) => {

  return (
    <div className="nm-layout">
      <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
        <div className="nm-spacer-48"/>
        <div className="nm-spacer-48"/>
        <h2
          className="nm-layout"
          style={{textAlign: 'center', borderBottom: `1px solid ${TEXT}`, paddingBottom: 12}}>
          PREFERENCE CENTER
        </h2>
        <div className="nm-spacer-12"/>
        <div className="nm-layout" style={{padding: 12}}>
          <p className="nm-layout" style={{fontStyle: 'italic'}}>
            No matter your preferences -- you will
            always receive emails about transactions account information, and products/service updates.
          </p>
          <div className="nm-spacer-12"/>
          <form className="nm-layout" onSubmit={props.onSubmit}>
            <div className="nm-layout">
              {
                _.get(props, 'iterable.messageTypes') && props.iterable.messageTypes.map((item) => {
                  return (
                    <div className="nm-layout">
                      <Field
                        component={CheckboxField}
                        name={`MessageTypeIds_${item.id}`}
                        label={item.name}/>
                    </div>
                  )
                })
              }
            </div>
            <div className="nm-spacer-12"/>
            <p className="nm-layout nm-link nm-text-center" onClick={props.handleUnsubscribeAll}>
              Unsubscribe from all notifications
            </p>
            <div className="nm-spacer-12"/>
            {
              _.get(props, 'iterable.successMessage') &&
              <h4 className="nm-layout nm-green nm-text-center">
                Your subscription preferences were successfully updated
                <div className="nm-spacer-12"/>
              </h4>
            }
            {
              _.get(props, 'iterable.errorMessage') &&
                <div className="nm-layout nm-text-center nm-error">
                  <h4>There was an error updating your subscription preferences</h4>
                </div>
            }
            <Button
              center={false}
              className="nm-btn__max nm-btn__center"
              style={{maxWidth: 300}}
              text="Save"
              onClick={() => props.dispatch(submit('emailPreferences'))}/>
            <InputError show={true} error={props.errorMessage}/>
          </form>
        </div>
      </div>
    </div>
  )
}

EmailPreferencesForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

EmailPreferencesForm = reduxForm({
  form: 'emailPreferences'
})(EmailPreferencesForm);

export default connect(
  (state) => ({
    iterable: state.iterable
  })
)(EmailPreferencesForm);
