import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Field, reduxForm, formValueSelector, submit } from 'redux-form';
import { STRIPE, AFFIRM, PAYPAL } from '../../Constants/payment';
import { Link } from 'react-router-dom';
import Button from '../Button';
import Check from '../../SVG/Check';
import RadioSelect from '../ReduxFormFields/RadioSelect';
import InputField from '../ReduxFormFields/InputField';
import InputError from '../InputError';
import CheckboxField from '../ReduxFormFields/CheckboxField';
import CreditCardFormFields from './CreditCardFormFields';
import CheckoutButton from '../CheckoutButton';
import InvalidProductsModal from '../InvalidProductsModal';
import OrderSummary from '../OrderSummary';
import paymentOptions from '../../Data/paymentOptions';
import AddressFormFields from './AddressFormFields';


import {
  notEmptyValidation,
  emailValidation,
  phoneValidation
} from '../../Util/FormValidation';

const selector = formValueSelector('checkout');

let CheckoutForm = (props) => {

  const handlePaymentMethodChange = (method) => {
    props.change('provider', method);
    props.change('payment_source', "");
    props.onPaymentMethodClick(method);
  }

  const paypalVisibilityClass = props.provider === PAYPAL ? "nm-checkout__section--collapse" : "";
  const creditCardVisibilityClass = props.provider === PAYPAL || props.provider === AFFIRM ? "nm-checkout__section--collapse" : "";
  const shippingRates = _.get(props, 'checkout.detail.availableShippingRates.shippingRates');
  return (
    <div className="nm-layout">
      <form onSubmit={props.onSubmit} className="nm-checkout">
        <input type="hidden" value="remove-autocomplete"/>
        <div className="nm-checkout__section">
          <Field
            component={RadioSelect}
            options={
              paymentOptions.map((option) => {
                return {
                  ...option,
                  isDisabled: option.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200
                }
              })
            }
            onClick={(option) => handlePaymentMethodChange(option.value)}
            renderOption={(index) => {
              let method = paymentOptions[index];
              return (
                <div>
                  {
                    method.img &&
                    <img
                      className="nm-image nm-image__inline nm-image__inline--icon"
                      src={method.img.src}
                      alt={method.img.alt}
                      style={method.img.style}/>
                  }
                  {
                    method.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200 &&
                    <span style={{fontSize: 12, float: 'right', lineHeight: 2}}>
                      Only applies to orders over $200
                    </span>
                  }
                  {
                    method.value === AFFIRM && _.get(props, 'checkout.totals.total') < 200 &&
                    method.value === PAYPAL && props.provider === PAYPAL && props.payment_source &&
                    <span style={{float: 'right', paddingRight: 30}}>
                      Authorized
                      <Check
                        style={{ right: 5, top: "calc(50% - 14px)" }}
                        animate={true}
                        className="nm-btn__check--input"/>
                    </span>
                  }
                </div>
              )
            }}
            name="provider"
            label="How would you like to pay?" />
        </div>
        <div className="nm-layout nm-layout__bar"/>
        <div className={`nm-checkout__section ${paypalVisibilityClass}`}>
          <div className="nm-layout">
            <Field
              component={InputField}
              name="first_name"
              label="First name"
              validate={[notEmptyValidation]}
              className="nm-layout__field-half"/>
            <Field
              component={InputField}
              name="last_name"
              label="Last name"
              validate={[notEmptyValidation]}
              className="nm-layout__field-half"/>
            <Field
              label="Email"
              component={InputField}
              onChange={(event) => {
                props.onEmailChange(event.target.value);
              }}
              validate={[notEmptyValidation, emailValidation]}
              name="email"/>
          </div>
        </div>
        <div className={`nm-layout nm-layout__bar ${paypalVisibilityClass}`}/>
        <AddressFormFields
          change={props.change}
          selector={selector}
          onAddressChange={(address) => {
            props.onShippingAddressChange(address, props.last_name)
          }}
          label="Shipping Address (USA/CAN)"
          name="shipping_address"
          isCollapsed={props.provider === PAYPAL}/>
          {
            props.provider !== PAYPAL &&
            <div className="nm-checkout__section">
              <Field
                component={InputField}
                validate={[notEmptyValidation, phoneValidation]}
                name="phone_number"
                label="Phone Number"/>
            </div>
          }
        <div className={`nm-layout nm-layout__bar ${creditCardVisibilityClass}`}/>
        <div className={`nm-checkout__section ${creditCardVisibilityClass}`}>
          {
            props.provider === STRIPE &&
            <CreditCardFormFields />
          }
        </div>
        <div className="nm-layout nm-layout__bar"/>
        {
          (props.provider !== PAYPAL || props.payment_source) &&
          <div className="nm-checkout__section">
            <Field
              component={CheckboxField}
              name="is_gift"
              label="This is a gift"/>
            <div className="nm-spacer-12"/>
            {
              props.is_gift &&
              <Field
                component={InputField}
                name="gift_message"
                label="Gift Message"/>
            }
            <p className="nm-layout" style={{fontSize: 12}}>
              - If you are trying to redeem a voucher, <Link to="/shop/voucher/">Click here.</Link>
            </p>
            <Field
              component={InputField}
              success={props.checkout.validDiscount}
              name="discount_codes"
              label="Discount Code (Email and Full Name Required)"/>
            <InputError
              className="nm-text-center"
              show={props.checkout.discountError}
              error={props.checkout.discountError}/>
            <div className="nm-clear"/>
            {
              props.discount_codes && props.discount_codes !== "" &&
              <Button
                text="Apply Discount"
                className="nm-btn__small nm-btn__center"
                onClick={() => props.onSubmitDiscountValidate(props.discount_codes, props.email)} />
            }
          </div>
        }
        <div className="nm-layout nm-layout__bar"/>
        <div className="nm-checkout__section">
          {
            _.get(props, 'checkout.detail.requiresShipping') && (!shippingRates || shippingRates.length === 0) &&
            <p className="nm-layout nm-text-center" style={{ fontStyle: 'italic' }}>
              Enter a valid shipping address to see available shipping rates.
            </p>
          }
          {
            _.get(props, 'checkout.detail.requiresShipping') &&
            shippingRates &&
            shippingRates.length > 0 &&
            (props.provider !== PAYPAL || props.payment_source) &&
            <Field
              isLoading={props.checkout.shippingRatesIsLoading}
              component={RadioSelect}
              options={
                shippingRates.map((rate) => {
                  return {
                    value: rate.handle,
                    title: `${rate.title} - $${rate.price}`
                  }
                })
              }
              renderOption={(option) => option.price}
              validate={[notEmptyValidation]}
              onClick={(option) => {
                props.change('shipping_method', option.value);
                props.onShippingRateClick(option.value);

              }}
              name="shipping_method"
              label="Shipping speed"
              subText="Orders placed after 2pm EST will not ship the same day. Please note that orders do not ship on Saturdays or Sundays."
            />
          }
        </div>
        {/* {
          props.provider !== AFFIRM && (props.provider !== STRIPE || !user) &&
          <Field component="input" type="hidden" name="payment_source"/>
        } */}
        <div className="nm-checkout__section">
          <div className="nm-spacer-12"></div>
          <p className="nm-layout nm-text-center">
            Total: ${props.checkout.totals.total}
          </p>
          <div className="nm-spacer-12"></div>
          <div className="nm-clear"></div>
          <CheckoutButton
            type_={props.provider}
            total={props.checkout.totals.total}
            hasPaymentToken={props.payment_source && props.payment_source !== ""}
            disabled={props.invalid}
            isLoading={false}
            onClick={() => props.dispatch(submit('checkout'))} />
          <InputError
            className="nm-text-center"
            show={props.order.error}
            error={props.order.error} />
        </div>
        <InvalidProductsModal
          onRemoveItemsClick={props.onRemoveItemsClick}
          checkout={props.checkout}
          subscriptionProducts={props.subscription_product.list}
          isGuest={true}
          onOtherPaymentMethodClick={() => handlePaymentMethodChange(STRIPE)}
          provider={props.provider}/>
        <div className="nm-clear"></div>
      </form>
      <OrderSummary
        country={props.shipping_country}
        checkout={props.checkout.detail}
        subscriptionProducts={props.subscription_product.list}
        totals={props.checkout.totals}
        hasPremium={false}/>
    </div>
  )
}

CheckoutForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onRemoveItemsClick: PropTypes.func.isRequired,
  onPaymentMethodClick: PropTypes.func.isRequired,
  invalidAffirmCheckoutItems: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.array
  ]).isRequired
}

CheckoutForm = reduxForm({
  form: 'checkout'
})(CheckoutForm);


export default connect(
  (state) => {
  return {
    auth: state.auth,
    order: state.order,
    checkout: state.checkout,
    email: selector(state, 'email'),
    provider: selector(state, 'provider'),
    last_name: selector(state, 'last_name'),
    shipping_address: selector(state, 'shipping_address'),
    payment_source: selector(state, 'payment_source'),
    subscription_product: state.subscription_product,
    is_gift: selector(state, 'is_gift'),
    discount_codes: selector(state, 'discount_codes'),
    initialValues: {
        provider: STRIPE,
        is_gift: false,
        shipping_address: {
          country: "US"
        }
    }
  }
}
)(CheckoutForm);
