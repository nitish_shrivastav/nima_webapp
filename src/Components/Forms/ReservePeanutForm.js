import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FieldArray, Field, reduxForm, formValueSelector, submit } from 'redux-form';
import StepperField from '../ReduxFormFields/StepperField';
import InputField from '../ReduxFormFields/InputField';
import Button from '../Button';
import RadioSelect from '../ReduxFormFields/RadioSelect';
import InputError from '../InputError';
import { notEmptyValidation, emailValidation } from '../../Util/FormValidation';

const whoForOptions = [
  {
    value: "self",
    title: "Myself"
  },
  {
    value: "else",
    title: "Someone Else"
  }
]

let ReservePeanutForm = (props) => {
  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <Field
        component={InputField}
        name="EMAIL"
        validate={[emailValidation]}
        label="Email address"/>
      <div className="nm-layout">
        <div className="nm-layout__field-half">
          <Field
            component={InputField}
            name="FNAME"
            validate={[notEmptyValidation]}
            label="First Name"/>
        </div>
        <div className="nm-layout__field-half">
          <Field
            component={InputField}
            validate={[notEmptyValidation]}
            name="LNAME"
            label="Last Name"/>
        </div>
      </div>
      <p className="nm-layout" style={{fontWeight: 400}}>
        Which foods are you looking forward to testing?
      </p>
      <Field
        component={InputField}
        name="TOTEST"
        label="Your answer (optional)"/>
      <p className="nm-layout" style={{fontWeight: 400}}>
        Quantity:
        <div style={{display: 'inline-block'}}>
          <Field
            component={StepperField}
            name="QUANTITY"
            label="Quantity"/>
        </div>
      </p>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout" style={{fontWeight: 400}}>
        Who are you reserving a Peanut Sensor for?
      </p>
      <Field
        component={RadioSelect}
        validate={[notEmptyValidation]}
        name="WHOFOR"
        options={whoForOptions}/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        center={true}
        style={{maxWidth: 300}}
        text="Reserve Sensor"
        onClick={() => props.dispatch(submit('peanutReserve'))}/>
      <InputError show={true} error={props.errorMessage}/>
      <div className="nm-clear"></div>
    </form>
  )
}

ReservePeanutForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

ReservePeanutForm= reduxForm({
  form: 'peanutReserve'
})(ReservePeanutForm);

export default connect(
  (state) => ({
    initialValues: {
      QUANTITY: 1
    }
  })
)(ReservePeanutForm);
