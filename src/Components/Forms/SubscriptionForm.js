import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import InlineDropDownField from '../ReduxFormFields/InlineDropDownField';
import { notEmptyValidation } from '../../Util/FormValidation';
import Button from '../Button';

const quantityOptions = [
  {
    text: "1",
    value: 1
  },
  {
    text: "2",
    value: 2
  },
  {
    text: "3",
    value: 3
  },
  {
    text: "4",
    value: 4
  }
];

const generateSendDayOptions = () => {
  let sendDayOptions = [];
  let now = moment();
  for (let i = 1; i < 29; i++) {
    sendDayOptions.push({
      text: now.date(i).format("Do"),
      value: `months|${i}`
    });
  }
  return sendDayOptions;
}

const sendDayOptions = generateSendDayOptions();

const shippingIntervalFrequencyOptions = [
  {
    text: "every month.",
    value: 1
  },
  {
    text: "bi-monthly.",
    value: 2
  }
]


let SubscriptionForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div className="nm-layout nm-text-center">
        Ship 
        <Field 
          component={InlineDropDownField}
          validate={[notEmptyValidation]}
          options={quantityOptions}
          style={{width: 60, marginLeft: 12, marginRight: 12}}
          name="quantity"/>
        capsule packs
      </div>
      <div className="nm-layout nm-text-center">
        on the 
        <Field 
          component={InlineDropDownField}
          validate={[notEmptyValidation]}
          onChange={
            (e, newValue) => {
              props.change('send_day', _.find(sendDayOptions, { 'text': newValue }).value)
            }
          }
          options={sendDayOptions}
          style={{width: 60, marginLeft: 12, marginRight: 12}}
          name="send_day_dropdown"/>
        <Field 
          component={InlineDropDownField}
          validate={[notEmptyValidation]}
          options={shippingIntervalFrequencyOptions}
          style={{width: 120}}
          onChange={
            (e, newValue) => {
              props.change('shipping_interval_frequency', _.find(shippingIntervalFrequencyOptions, { 'text': newValue }).value)
            }
          }
          name="shipping_interval_frequency_dropdown"/>
      </div>
      <Field component="hidden" name="send_day"/>
      <Field component="hidden" name="shipping_interval_frequency"/>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        type="submit"
        center={true}
        style={{maxWidth: 300}}
        preventDefault={true}
        onClick={() => props.dispatch(submit('subscriptionForm'))}
        text="Update Subcription"/>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
    </form>
  )
}

SubscriptionForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired
}

SubscriptionForm = reduxForm({
  form: 'subscriptionForm',
  enableReinitialize: true
})(SubscriptionForm);

export default connect(
  (state) => {
    let shippingInterval = _.find(shippingIntervalFrequencyOptions, { value: parseInt(state.subscription.detail.shipping_interval_frequency) });
    return {
      subscription: state.subscription,
      initialValues: {
        send_day: state.subscription.detail.send_day,
        send_day_dropdown: _.find(sendDayOptions, { value: state.subscription.detail.send_day }).text,
        quantity: state.subscription.detail.quantity,
        shipping_interval_frequency: state.subscription.detail.shipping_interval_frequency,
        shipping_interval_frequency_dropdown: shippingInterval ? shippingInterval.text : shippingIntervalFrequencyOptions[0].text 
      }
    }
  }
)(SubscriptionForm);
