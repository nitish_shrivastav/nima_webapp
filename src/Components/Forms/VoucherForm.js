import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { emailValidation, notEmptyValidation, phoneValidation } from '../../Util/FormValidation';
import Button from '../Button';
import DropDownField from '../ReduxFormFields/DropDownField';
import GooglePlacesSearchField from '../ReduxFormFields/GooglePlacesSearchField'
import InputField from '../../Components/ReduxFormFields/InputField';
import InputError from '../../Components/InputError';


let VoucherForm = (props) => {

  const addressDropDownFormatter = (value) => {
    return value;
  }
  
  const handlePlaceClick = (address) => {
    let storefrontFormattedAddress = {
      address1: address.name,
      city: address.city,
      province: address.province_short,
      zip: address.postal_code,
      country: "US"
    }
    props.change("shipping_address", storefrontFormattedAddress);
  }

  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <div className="nm-checkout__section">
        <Field
          component={InputField}
          name="first_name"
          label="First name"
          validate={[notEmptyValidation]}
          className="nm-layout__field-half"/>
        <Field
          component={InputField}
          name="last_name"
          label="Last name"
          validate={[notEmptyValidation]}
          className="nm-layout__field-half"/>
        <Field
          label="Email"
          component={InputField}
          validate={[notEmptyValidation, emailValidation]}
          name="email"/>
        <Field
          component={InputField}
          validate={[notEmptyValidation, phoneValidation]}
          name="phone_number"
          label="Phone Number"/>
      </div>
      <div className="nm-layout nm-layout__bar"/>
        <div className="nm-checkout__section">
          <Field
            component={GooglePlacesSearchField}
            autocomplete="none"
            name="shipping_address.address1"
            onPlaceClick={(address) => handlePlaceClick(address)}
            validate={[notEmptyValidation]}
            label="Shipping Address (USA only)"/>
          <div className="nm-layout">
            <div className="nm-layout__field-half">
              <Field
                component={InputField}
                name="shipping_address.address2"
                label="Apt/Suite #"/>
            </div>
            <div className="nm-layout__field-half">
              <Field
                component={DropDownField}
                options={[{
                  text: "United States",
                  value: "US"
                }]}              
                format={addressDropDownFormatter}
                name="shipping_address.country"
                label="Country"/>
            </div>
          </div>
          <div className="nm-layout">
            <div className="nm-layout__field-half">
              <Field
                component={InputField}
                validate={[notEmptyValidation]}
                name="shipping_address.city"
                label="Shipping City"/>
            </div>
            <div className="nm-layout__field-half">
              <Field
                component={InputField}
                className={`nm-layout__field-half`}
                validate={[notEmptyValidation]}
                name="shipping_address.province"
                label="State/Province"/>
              <Field
                component={InputField}
                className={`nm-layout__field-half`}
                validate={[notEmptyValidation]}
                name="shipping_address.zip"
                label="Zip"/>
            </div>
          </div>
          <div className="nm-spacer-12"></div>
          <div className="nm-clear"></div>
            <div className={`nm-btn__max nm-btn__center`}>
              <Button
                center={true}
                className="nm-btn__max"
                text="Redeem Voucher"
                useLoader={true}
                isLoading={props.isLoading}
                onClick={() => props.dispatch(submit('voucherRedeemForm'))}>
              </Button>
            </div>
            <InputError
              className="nm-text-center"
              show={props.voucher.error}
              error={props.voucher.error} />
        </div>
    </form>
  )
}

VoucherForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

VoucherForm = reduxForm({
  form: 'voucherRedeemForm'
})(VoucherForm);

export default connect(
  (state) => ({
    voucher: state.voucher,
    initialValues: {
      shipping_address: {
        country: "US"
      }
    }
  })
)(VoucherForm);
