import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';

class CheckoutUserFields extends Component {
  render() {
    return (
      <div className="nm-layout">
        <Input
          className="nm-layout__field-half"
          field={this.props.firstNameField}
          showError={this.props.showErrors}
          onChange={this.props.onChange}/>
        <Input
          className="nm-layout__field-half"
          field={this.props.lastNameField}
          showError={this.props.showErrors}
          onChange={this.props.onChange}/>
        <Input
          field={this.props.emailField}
          showError={this.props.showErrors}
          onChange={this.props.onEmailChange}/>
      </div>
    );
  }
}

CheckoutUserFields.propTypes = {
  onChange: PropTypes.func.isRequired,
  showErrors: PropTypes.bool.isRequired,
  onEmailChange: PropTypes.func.isRequired,
  emailField: PropTypes.shape({
    value: PropTypes.string.isRequired,
    error: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    isRequired: PropTypes.bool,
    validation: PropTypes.func.isRequired,
    autocomplete: PropTypes.string
  }),
  firstNameField: PropTypes.shape({
    value: PropTypes.string.isRequired,
    error: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    isRequired: PropTypes.bool,
    validation: PropTypes.func.isRequired,
    autocomplete: PropTypes.string
  }),
  lastNameField: PropTypes.shape({
    value: PropTypes.string.isRequired,
    error: PropTypes.string,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    isRequired: PropTypes.bool,
    validation: PropTypes.func.isRequired,
    autocomplete: PropTypes.string
  })
}

export default CheckoutUserFields;
