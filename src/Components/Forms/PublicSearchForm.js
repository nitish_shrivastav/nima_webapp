import React, { Fragment } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FieldArray, Field, reduxForm, formValueSelector, submit } from 'redux-form';
import SearchIcon from '../../SVG/SearchIcon';
import Pin from '../../SVG/Pin';
import DropSelect from '../DropSelect';
import { GLUTEN_ASSETS, PEANUT_ASSETS, ALLERGEN_COLOR } from '../../Constants/allergen';

let PublicSearchForm = (props) => {

  const handleAllergenClick = (allergenType) => {
    if (props.allergen !== allergenType) {
      props.change("allergen", allergenType);
    }
  }

  const dropItem = (item) => {
    return (
      <Fragment>
        <div className="nm-pull--left" style={{ margin: '6px 24px 0 16px' }}>
          <Pin width="18px" height="18px"/>
        </div>
        <p className="nm-form__li" style={{width: 'calc(100% - 58px)'}}>
          {item.description.replace(", USA", "")}
        </p>
      </Fragment>
    )
  }

  const handleCityClick = (city) => {
    props.onCityAutocompleteClick(city);
  }

  return (
    <form
      className="nm-search nm-search__map-wrap"
      onSubmit={props.handleSubmit}
      style={{background: 'white', border: `2px solid ${ALLERGEN_COLOR[props.allergen]}`}}>
      <div className={`nm-search__map-place-wrap ${props.allergen}-border`}>
        <div className="nm-search__icon-wrap" onClick={() => {props.dispatch(submit(props.form))}}>
          <SearchIcon fill={ALLERGEN_COLOR[props.allergen]}/>
        </div>
        <Field
          component="input"
          onBlur={props.onPlaceBlur}
          onChange={props.onPlaceChange}
          name="search"
          autoComplete="off"
          className="nm-search__input nm-search__input--mobile-with-icon"
          placeholder="Search name, type of food, etc."/>
        <div className="nm-layout">
          {
            props.autocompletePlaces && props.autocompletePlaces.length > 0 &&
            <DropSelect
              onItemClick={handleCityClick}
              keyPrefix="public_search_place_"
              renderDropItem={dropItem}
              ulStyle={{top: 0}}
              items={props.autocompleteCities}>
              <li className="nm-layout" style={{padding: 5}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/powered_by_google.png`}
                  style={{ height: 20 }}
                  alt="Powered By Google"
                  className="nm-pull--right"/>
              </li>
            </DropSelect>
          }
        </div>
      </div>
      <div className="nm-spacer-12 nm-tablet-and-up-hidden"/>
      <div className={`nm-search__map-place-wrap  ${props.allergen}-border`}>
        <div className="nm-search__icon-wrap nm-search__icon-wrap--small">
          <Pin fill={ALLERGEN_COLOR[props.allergen]}/>
        </div>
        <Field
          component="input"
          name="where"
          autoComplete="off"
          onBlur={props.onLocationBlur}
          onChange={props.onLocationChange}
          className="nm-search__input nm-search__input--mobile-with-icon"
          placeholder="Ex: San Francisco, CA"/>
        <div className="nm-layout">
          {
            props.autocompleteCities && props.autocompleteCities.length > 0 &&
            <DropSelect
              onItemClick={handleCityClick}
              keyPrefix="public_search_where_"
              renderDropItem={dropItem}
              ulStyle={{top: 0}}
              items={props.autocompleteCities}>
              <li className="nm-layout" style={{padding: 5}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/powered_by_google.png`}
                  style={{ height: 20 }}
                  alt="Powered By Google"
                  className="nm-pull--right"/>
              </li>
            </DropSelect>
          }
        </div>
      </div>
      <Field
        component="input"
        type="hidden"
        name="allergen"/>
      <div className="nm-spacer-12 nm-tablet-and-up-hidden"/>
      <div className="nm-pull--left">
        <div
          className="nm-search__map-allergen-icon"
          onClick={() => handleAllergenClick("gluten")}>
          <img className="nm-image nm-image__contain" src={GLUTEN_ASSETS[props.allergen]} style={{height: '100%'}}/>
        </div>
        <div
          className="nm-search__map-allergen-icon"
          onClick={() => handleAllergenClick("peanut")}>
          <img className="nm-image nm-image__contain" src={PEANUT_ASSETS[props.allergen]} style={{height: '100%'}}/>
        </div>
      </div>
      <button
        type="submit"
        style={{background: ALLERGEN_COLOR[props.allergen], maxWidth: 150, padding: 8}}
        className="nm-search__submit nm-tablet-and-down-hidden">
        Search
      </button>
      <button
        type="submit"
        style={{background: ALLERGEN_COLOR[props.allergen], maxWidth: 150, padding: 8}}
        className="nm-btn nm-pull--right nm-tablet-and-up-hidden">
        Search
      </button>
      <div className="nm-clear"></div>
    </form>
  )
}

PublicSearchForm.propTypes = {
}

PublicSearchForm = reduxForm({
  enableReinitialize: true
})(PublicSearchForm);

const selector = formValueSelector('publicPlaceSearch');

export default connect(
  (state) => ({
    allergen: selector(state, "allergen"),
    initialValues: {
      allergen: "gluten"
    }
  })
)(PublicSearchForm);
