import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, submit, reduxForm } from 'redux-form';
import SearchField from '../ReduxFormFields/SearchField';
import PropTypes from 'prop-types';
import Button from '../Button';

let MapSearch = (props) => {
  return (
    <form className="nm-layout" onSubmit={props.onSubmit}>
      <Field
        label="Search"
        component={SearchField}
        name="search"/>
      <Button text="Submit" onClick={() => props.dispatch(submit('mapSearch'))}/>
    </form>
  );
}

MapSearch.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

MapSearch = reduxForm({
  form: 'mapSearch'
})(MapSearch);

export default connect(
  (state) => ({})
)(MapSearch);
