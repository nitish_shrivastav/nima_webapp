import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, submit } from 'redux-form';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { notEmptyValidation } from '../../Util/FormValidation';
import Button from '../Button';
import InputError from '../InputError';
import InputField from '../ReduxFormFields/InputField';


let VoucherSearchForm = (props) => {

  return (
    <form className="nm-layout" onSubmit={props.handleSubmit}>
      <div className="nm-layout">
        <Field
          component={InputField}
          name="voucher_id"
          label="Voucher number"
          validate={[notEmptyValidation]}/>
          <div className="nm-spacer-12"></div>
          <div className="nm-clear"></div>
          <div className="nm-btn__max nm-btn__center">
            <Button
              center={true}
              className="nm-btn__max"
              text="Lookup Voucher"
              onClick={() => props.dispatch(submit('voucherSearchForm'))} />
          </div>
          <InputError
            className="nm-text-center"
            show={props.voucher.error}
            error={props.voucher.error} />
        </div>
    </form>
  )
}

VoucherSearchForm.propTypes = {
  onSubmit: PropTypes.func.isRequired
}

VoucherSearchForm = reduxForm({
  form: 'voucherSearchForm'
})(VoucherSearchForm);

export default connect(
  (state) => ({
    voucher: state.voucher
  })
)(VoucherSearchForm);
