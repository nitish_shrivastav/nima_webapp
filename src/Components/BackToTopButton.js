import React from 'react';
import Button from './Button';

const BackToTopButton = (props) => {
  const handleClick = () => {
    window.scrollTo(0, 0);
  }

  return (
    <div className="nm-layout">
      <div className="nm-spacer-48"></div>
      <div className="nm-flex">
        <svg viewBox="0 0 20 19" style={{width: 20, height: 20}}>
          <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g fill="#00AEEF">
              <polygon points="3.32 10 10 3.32 16.68 10 15.46 11.18 10.82 6.52 10.82 16.68 9.18 16.68 9.18 6.52 4.5 11.18"></polygon>
            </g>
          </g>
        </svg>
      </div>
      <div className="nm-spacer-12"></div>
      <div className="nm-clear"></div>
      <Button
        text="Back up to top"
        center={true}
        style={{maxWidth: 200}}
        onClick={handleClick}/>
      <div className="nm-clear"></div>
      <div className="nm-spacer-48"></div>
    </div>
  );
}

export default BackToTopButton;
