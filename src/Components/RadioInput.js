import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputError from './InputError';

class RadioInput extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    if (this.props.onChange) {
      this.props.onChange(e.target.value, this.props.field.name);
    }
  }

  render() {
    return (
      <div className="nm-checkbox__back">
        <input
          {...this.props}
          for={this.props.field.for}
          type="radio"
          name={this.props.field.name}
          className="nm-form__checkbox"
          onChange={this.handleChange}
          checked={this.props.checked}/>
      </div>
    );
  }
}

RadioInput.defaultProps = {
  field: {
    checked: false,
    for: "",
    name: "default",
  }
}

RadioInput.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    for: PropTypes.string.isRequired
  })
}

export default RadioInput;
