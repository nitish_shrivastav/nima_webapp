import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DropSelect extends Component {
  constructor() {
    super();
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentWillMount() {
    this.setState({
      selectedIndex: -1
    });
  }

  handleKeyPress(e) {
    //down key
    if (e.keyCode === 40 && this.state.autocompleteItems.length > this.state.selectedIndex + 1) {
      this.setState({
        selectedIndex: this.state.selectedIndex + 1
      })
    }
    //up key
    else if (e.keyCode === 38 && this.state.selectedIndex > 0) {
      this.setState({
        selectedIndex: this.state.selectedIndex - 1
      })
    }
    //enter key
    else if (e.keyCode === 13 && this.state.selectedIndex > -1) {
      //this.handlePlaceClick(this.state.autocompleteItems[this.state.selectedIndex])
    }
  }

  render() {
    const { selectedIndex } = this.state;
    const { items, onItemClick, renderDropItem, children, keyPrefix } = this.props;
    return (
      <ul className="nm-form__ul" style={this.props.ulStyle}>
        {
          items.map((item, key) => {
            return (
              <li
                key={`${keyPrefix}${key}`}
                className={`nm-layout nm-form__li-wrap ${key === selectedIndex ? "selected" : ""}`}
                onMouseDown={() => {onItemClick(item)}}>
                { renderDropItem(item) }
              </li>
            )
          })
        }
        { children }
      </ul>
    );
  }
}

DropSelect.propTypes = {
  items: PropTypes.array.isRequired,
  onItemClick: PropTypes.func.isRequired,
  renderDropItem: PropTypes.func.isRequired,
  keyPrefix: PropTypes.string.isRequired,
  ulStyle: PropTypes.object
}

export default DropSelect;
