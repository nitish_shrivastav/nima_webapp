import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from './Button';
import HoverSlideBox from './HoverSlideBox';


const PopularChains = (props) => {
  return (
    <div className="nm-layout">
      <div className="nm-bar nm-bar__line"/>
      <div className="nm-layout nm-flex" style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start' }}>
        {
          props.popularChains.map((popularChain, index) => {
            return (
              <HoverSlideBox
                key={`popularHoverBox-${index}`}
                style={{marginRight: 24, marginBottom: 24}}
                imageSrc={`${popularChain.image_url}`}>
                <Link to={`/map/chains/${popularChain.chain_id}?allergen=${props.allergen}`}>
                  <Button
                    center={true}
                    style={{maxWidth: 200}}
                    preventDefault={false}
                    className="nm-btn__inverse"
                    text={popularChain.name}/>
                </Link>
              </HoverSlideBox>
            )
          })
        }
      </div>
    </div>
  );
}

PopularChains.propTypes = {
  allergen: PropTypes.string.isRequired,
  popularChains: PropTypes.array.isRequired
}

export default PopularChains;
