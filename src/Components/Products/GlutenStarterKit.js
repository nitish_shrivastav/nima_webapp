import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Product from '../Product';

class GlutenStarterKit extends Component {
  render() {
    const { product } = this.props;
    return (
      <div className="nm-layout">
        <Product
          onAddToCart={this.props.onAddToCart}
          affirmEligible={true}
          product={product.detail}>
          <p class="nm-basic nm-basic__p nm-text-center">
            Other options:
            <Link to="/shop/products/nima-gluten-sensor" className="nm-link">
              Sensor Only
            </Link> |
            <Link to="/shop/products/subscriptions" className="nm-link">
              Capsules Only
            </Link>
          </p>
        </Product>
      </div>
    );
  }
}

export default GlutenStarterKit;
