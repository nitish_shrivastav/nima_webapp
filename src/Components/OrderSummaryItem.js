import React from 'react';
import PropTypes from 'prop-types';

const OrderSummaryItem = (props) => {
  let style = { width: 65 };
  let right = props.right;
  if (right === "$0.00" && props.allowFree) {
    style.color = "#00C6A4";
    right = "FREE";
  }
  return (
    <div className={`nm-layout ${props.className}`} style={props.style}>
      <p className="nm-pull--left" style={{width: 'calc(100% - 65px)'}}>
        {props.left}
        { props.children }
        {
          props.leftSecond &&
          <span style={{fontSize: 12}}>
            <br/>
            {props.leftSecond}
          </span>
        }
      </p>
      <p
        className={`nm-pull--right nm-right-text ${props.rightClassName}`}
        style={style}>
        {right}
      </p>
    </div>
  );
}

OrderSummaryItem.defaultProps = {
  className: "",
  rightClassName: "",
  style: {}
}

OrderSummaryItem.propTypes = {
  left: PropTypes.string.isRequired,
  right: PropTypes.string.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  rightClassName: PropTypes.string,
  leftSecond: PropTypes.string,
  children: PropTypes.element
}

export default OrderSummaryItem;
