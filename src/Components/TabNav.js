import React from 'react';
import PropTypes from 'prop-types';

let TabNav = (props) => {
  const pieceWidth = 100 / props.items.length;
  return (
    <div className="nm-tabnav">
      { props.children }
      {
        props.items.map((item, index) => {
          return (
            <div
              className={`nm-tabnav__item ${props.activeIndex === index ? "active" : ""}`}
              onClick={() => props.onItemClick(index)}
              style={{width: `${pieceWidth}%`}}>
              {
                props.getTabNavText &&
                <p className="nm-layout nm-text-center nm-tabnav__item">
                  {props.getTabNavText(index)}
                </p>
              }
              {item}
            </div>
          )
        })
      }
      <div
        style={{width: `${pieceWidth}%`, left: `${props.activeIndex * pieceWidth}%`}}
        className="nm-tabnav__bar">
      </div>
    </div>
  );
}

TabNav.propTypes = {
  activeIndex: PropTypes.number.isRequired,
  items: PropTypes.array.isRequired,
  onItemClick: PropTypes.func.isRequired,
  getTabNavText: PropTypes.func
}

export default TabNav;
