import React from 'react';
import PropTypes from 'prop-types';
import StarRating from '../SVG/StarRating';
import { ALLERGEN_COLOR, RED } from '../Constants/colors';
import FormatHelper from '../Util/FormatHelper';

const RestaurantHeader = (props) => {

  return (
    <div className="nm-layout">
      <div className="nm-mobile-hidden">
        <div className="nm-layout">
          <div
            className="nm-layout nm-image__background nm-image__background--cover"
            style={{ height: 400, backgroundImage: `url('${props.backgroundImage}')`}}>
            {
              props.mainImage &&
              <img className="nm-image__chain" src={props.mainImage} alt={`${props.name}`}/>
            }
            <div className="nm-bar nm-bar__docked" style={{padding: 24}}>
              <div className="nm-layout nm-layout__max-center">
                <h2 className="nm-layout">
                  <span className="nm-bold">{props.name}</span>
                </h2>
                {
                  props.formattedAddress &&
                  <p className="nm-layout nm-text-center" style={{fontSize: 16}}>
                    {props.formattedAddress}
                  </p>
                }
                <div className="nm-layout nm-text-center" style={{fontSize: 16}}>
                  {
                    props.experienceScore &&
                    <StarRating
                      key={props.starKey}
                      idPrefix="restaurant_detail"
                      startColor={ALLERGEN_COLOR[props.allergen]}
                      stroke={ALLERGEN_COLOR[props.allergen]}
                      percent={100 * props.experienceScore / 5 }
                      style={{width: 20, height: 20, marginRight: 5}}/>
                  }
                  {
                    !props.experienceScore &&
                    <p className="nm-layout" style={{ color: RED, fontStyle: 'italic'}}>
                      No {FormatHelper.uppercaseFirstLetter(props.allergen)} reviews.
                    </p>
                  }
                  {` ${props.reviewCount} total reviews`}
                </div>
                <div className="nm-clear"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="nm-desktop-hidden">
        <div className="nm-layout">
          <img
            className="nm-layout nm-image nm-image__cover"
            alt="Nima Restaurant"
            src={props.backgroundImage}
            style={{position: 'absolute', height: '100%'}}/>
          {
            props.mainImage &&
            <img className="nm-image__chain__mobile" src={props.mainImage} alt={`${props.name}`}/>
          }
          <div className="nm-spacer-12"/>
          <div className="nm-clear"/>
          <div className="nm-layout nm-layout__max-center" style={{background: "rgba(246,246,246,0.9)", maxWidth: 300, borderRadius: 6}}>
            <div className="nm-spacer-12"/>
            <h2 className="nm-layout nm-text-center" style={{fontSize: 16}}>
              {props.name}
            </h2>
            {
              props.formattedAddress &&
              <p className="nm-layout nm-text-center" style={{fontSize: 16}}>
                {props.formattedAddress}
              </p>
            }
            <div className="nm-layout nm-text-center" style={{fontSize: 16}}>
              {
                props.experienceScore &&
                <StarRating
                  key={`${props.starKey}-mobile`}
                  idPrefix="restaurant_detail_mobile"
                  startColor={ALLERGEN_COLOR[props.allergen]}
                  stroke={ALLERGEN_COLOR[props.allergen]}
                  percent={100 * props.experienceScore / 5 }
                  style={{width: 20, height: 20, marginRight: 5}}/>
              }
              {
                !props.experienceScore &&
                <p className="nm-layout" style={{ color: RED, fontStyle: 'italic'}}>
                  No {FormatHelper.uppercaseFirstLetter(props.allergen)} reviews.
                </p>
              }
              {props.reviewCount} total reviews
            </div>
            <div className="nm-spacer-12"/>
            <div className="nm-clear"/>
          </div>
          <div className="nm-spacer-12"/>
        </div>
      </div>
    </div>
  );
}

RestaurantHeader.defaultProps = {
  starKey: "place-detail-star-"
}

RestaurantHeader.propTypes = {
  starKey: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string,
  experienceScore: PropTypes.number,
  formattedAddress: PropTypes.string,
  name: PropTypes.string.isRequired,
  allergen: PropTypes.string.isRequired,
  reviewCount: PropTypes.string,
  mainImage: PropTypes.string
}

export default RestaurantHeader;
