import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import Button from '../Components/Button';
import moment from 'moment';
import TimeHelper from '../Util/TimeHelper';
import { GREEN, DARK_GREY, RED, YELLOW, PURPLE } from '../Constants/colors';

const SubscriptionCard = (props) => {

  const prepProduct = () => {
    let subscriptionVariantId = props.subscription.order.split('|')[1];
    if ("replacement_variant" in props.subscription) {
      subscriptionVariantId = props.subscription.replacement_variant;
    }
    let price;
    let shippingPrice = 0;
    let imageSrc;
    let name;
    for (let product of props.products) {
      name = product.node.title;
      for (let variant of product.node.variants.edges) {
        let decodedId = StorefrontInteractor.decodeId(variant.node.id, "ProductVariant");
        if (decodedId === subscriptionVariantId) {
          price = variant.node.price;
          imageSrc = product.node.images.edges[0].node.originalSrc;
          break;
        }
      }
      if (price) {
        break;
      }
    }
    if (props.subscription.constant_price && parseFloat(price) > props.subscription.constant_price) {
      shippingPrice = props.subscription.constant_shipping_price;
      price = props.subscription.constant_price;
    }
    return {
      price: `$${price + shippingPrice}`,
      imageSrc: imageSrc,
      name: name
    };
  };

  const skipExists = (skips, timestamp) => {
    for (let skip of skips) {
      if (moment.utc(parseInt(skip.created_at)).isSame(timestamp, 'day')) {
        return true;
      }
    }
    return false;
  }

  const isAllowedToCancel = () => {
    return !(props.subscription.can_modify_at && parseInt(props.subscription.can_modify_at) > new Date().getTime());
  }

  const getNextSkips = () => {
    let nextCharges = []
    for (let i = 0; i < 6; i++) {
      let dateDelta = (i + 1) * parseInt(props.subscription.shipping_interval_frequency);
      let dayOfMonth = parseInt(props.subscription.send_day.split("|")[1])
      let shipDate = moment.utc(parseInt(props.subscription.last_sent)).date(dayOfMonth).add(dateDelta, props.subscription.shipping_interval_type)
      nextCharges.push(shipDate)
    }
    return nextCharges;
  }

  let canCancel = isAllowedToCancel();
  let product = prepProduct();
  let productColor = product.name.toLowerCase().indexOf("peanut") > -1 ? PURPLE : GREEN;
  return (
    <div className="nm-card__subscription" style={{border: `2px solid ${canCancel ? productColor : YELLOW}`}}>
      {
        !canCancel &&
        <div className="nm-layout nm-text-center">
          <div className="nm-spacer-12"/>
          <div className="nm-layout nm-flex">
            <img
              className="nm-image nm-image__contain nm-image__thumb"
              style={{maxWidth: 150}}
              src={product.imageSrc}/>
          </div>
          <div className="nm-spacer-12"/>
          <h2 className="nm-layout">
            { product.name }
          </h2>
          <div className="nm-spacer-12"/>
          <h4 className="nm-layout">
            Charges monthly on the
            {` ${moment().date(props.subscription.send_day.split('|')[1]).format("Do")}`}
          </h4>
          <p className="nm-layout">
            Started on
            {` ${moment(props.subscription.created_at).format("MM/DD/YYYY")}`}
          </p>
          <div className="nm-spacer-12"/>
          <p className="nm-layout">
            Quantity: {props.subscription.quantity}
          </p>
          <p className="nm-layout">
            Price: { product.price }<br/>
            { product.shippingPrice }
          </p>
          <div className="nm-spacer-12"/>
          <p className="nm-layout" style={{fontSize: 14, fontStyle: 'italic'}}>
            Skips and discounts are not allowed on subscriptions with a commitment.
          </p>
          <p className="nm-layout" style={{fontSize: 14, fontStyle: 'italic'}}>
            Commitment lasts until {moment(parseInt(props.subscription.can_modify_at)).format("MM/DD/YYYY")}
          </p>
          <div className="nm-spacer-12"/>
        </div>
      }
      {
        canCancel &&
        <div className="nm-layout">
          <div className="nm-card__subscription-image" style={{paddingRight: 12}}>
            <img
              className="nm-image nm-image__contain nm-image__thumb"
              src={product.imageSrc}/>
          </div>
          <div className="nm-card__subscription-info">
            <h2 className="nm-layout">
              { product.name }
            </h2>
            <h2 className="nm-layout">
              {parseInt(props.subscription.shipping_interval_frequency) > 1 ? "Bi-" : ""}
              Monthly on the
              {` ${moment().date(props.subscription.send_day.split('|')[1]).format("Do")}`}
            </h2>
            <p className="nm-layout">
              Quantity: {props.subscription.quantity}
            </p>
            <p className="nm-layout">
              Price: { product.price }<br/>
              { product.shippingPrice }
            </p>
            {
              _.get(props, 'subscription.discounts.length', 0) > 0 &&
              <p className="nm-layout">
                Applied discount: <span style={{ color: GREEN }}>{props.subscription.discounts[0].id}</span>
              </p>
            }
            <p className="nm-layout nm-link nm-bold" onClick={() => props.onEditClick(props.subscription)}>
              Edit
            </p>
          </div>
          <div className="nm-spacer-48"/>
          <div className="nm-layout">
            <div className="nm-layout">
              {
                 _.get(props, 'subscription.discounts.length', 0) === 0 && !props.cannot_skip &&
                <Button
                  text="Add Discount"
                  onClick={() => props.onDiscountClick(props.subscription)}
                  style={{width: 180, float: 'right'}}/>
              }
              {
                 _.get(props, 'subscription.discounts.length', 0) > 0 &&
                <Button
                  text="Remove Discount"
                  onClick={() => props.onRemoveDiscountClick(props.subscription)}
                  className="nm-btn__cancel"
                  style={{width: 180, float: 'right'}}/>
              }
              <span
                style={{color: RED, cursor: 'pointer', textDecoration: 'underline'}}
                onClick={() => props.onCancelClick(props.subscription)}>
                Cancel Subscription
              </span>
              <br/>
              <span>Upcoming charges</span>
            </div>
            <div className="nm-layout" style={{ height: 2, background: DARK_GREY, margin: '12px 0' }}/>
            <div className="nm-layout">
              {
                props.subscription.cannot_skip &&
                <p className="nm-layout nm-text-center" style={{fontStyle: 'italic'}}>
                  Skips are not allowed on this product
                </p>
              }
              {
                !props.subscription.cannot_skip &&
                getNextSkips().map((skip) => {
                  let hasSkip = skipExists(props.subscription.skips, skip);
                  return (
                    <div className="nm-layout">
                      <span style={{ fontSize: 20, textDecoration: hasSkip ? "line-through" : "none"}}>
                        {TimeHelper.dayFormat(skip)}
                      </span>
                      {
                        hasSkip &&
                        <div className="nm-pull--right">
                          <span>Skipped! | </span>
                          <span className="nm-link" onClick={() => { props.onUnskipClick(props.subscription, skip)}}>
                            Unskip
                          </span>
                        </div>
                      }
                      {
                        !hasSkip &&
                        <div className="nm-pull--right">
                          <span>Scheduled | </span>
                          <span className="nm-link" onClick={() => { props.onSkipClick(props.subscription, skip)}}>
                            Skip?
                          </span>
                        </div>
                      }
                      <div className="nm-spacer-12"></div>
                    </div>
                  )
                })
              }
            </div>
          </div>
        </div>
      }
    </div>
  );
}

SubscriptionCard.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  subscription: PropTypes.object.isRequired,
  onSkipClick: PropTypes.func.isRequired,
  onUnskipClick: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  products: PropTypes.array.isRequired,
  onCancelClick: PropTypes.func.isRequired,
  onDiscountClick: PropTypes.func.isRequired,
  onRemoveDiscountClick: PropTypes.func.isRequired
}

export default SubscriptionCard;
