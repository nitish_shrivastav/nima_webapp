import React from 'react';
import PropTypes from 'prop-types';

const SuccessCard = (props) => {
  return (
    <div className="nm-layout">
      <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
        <div className="nm-spacer-48"/>
        <div className="nm-card">
          <p className="nm-bar">
            <img
              className="nm-image nm-relative"
              style={{width: 20, display: 'inline-block', marginRight: 10, top: 4}}
              alt="Green Check"
              src={`${process.env.PUBLIC_URL}/images/greenCheck.png`}/>
              {props.statusText}
          </p>
          <div className="nm-card__center nm-flex__column-pad">
            <h1 className="nm-margin-clear nm-text-center">
              {props.title}
            </h1>
            <div className="nm-spacer-12"></div>
            <div className="nm-spacer-12"></div>
            <div className="nm-layout">
              {props.children}
            </div>
            <div className="nm-clear"></div>
            <div className="nm-spacer-48"></div>
          </div>
        </div>
        <div className="nm-clear"/>
      </div>
      <div className="nm-clear"/>
    </div>
  );
}

SuccessCard.propTypes = {
  title: PropTypes.string.isRequired,
  statusText: PropTypes.string.isRequired
}

export default SuccessCard;
