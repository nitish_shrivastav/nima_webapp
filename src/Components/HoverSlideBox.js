import React from 'react';
import PropTypes from 'prop-types';

const HoverSlideBox = (props) => {
  return (
    <div className="nm-slider__hover-slider" style={props.style}>
      {
        props.imageSrc && 
        <img src={props.imageSrc} className="nm-layout nm-image nm-image__background"/>
      }
      <div className="nm-slider__overlay">
        {props.children}
      </div>
    </div>
  )
}

HoverSlideBox.defaultProps = {
  style: {},
  className: ""
}

HoverSlideBox.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  imageSrc: PropTypes.string,
  children: PropTypes.element
}

export default HoverSlideBox;
