import React, { Component } from 'react';
import OrderSummaryItem from './OrderSummaryItem';
import StorefrontInteractor from '../Util/StorefrontInteractor';

const getLeftSecondText = (item, subscriptionProducts) => {
  let text;
  let variantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
  if (variantId in subscriptionProducts) {
    text = `Ships every ${subscriptionProducts[variantId].interval_frequency} ${subscriptionProducts[variantId].interval_unit_type}`;
  }
  return text;
}

class OrderSummary extends Component {
  constructor() {
    super();
    this.handleDropClick = this.handleDropClick.bind(this);
  }

  componentWillMount() {
    this.setState({
      dropClass: "",
      itemsLabelClass: ""
    })
  }

  handleDropClick() {
    this.setState({
      dropClass: "active",
      itemsLabelClass: "nm-hidden"
    })
  }

  render() {
    const { checkout, country } = this.props;
    let subtotal = checkout ? `$${checkout.subtotalPrice}` : `??`;
    let shippingTotal = checkout && checkout.shippingLine && checkout.shippingLine.price ? `$${checkout.shippingLine.price}` : "??";
    let discountedSavings = parseFloat(this.props.totals.premiumDiscountTotal) + parseFloat(this.props.totals.totalDiscount);
    let promotionalTotal = parseFloat(this.props.totals.promotionDiscountTotal);
    return (
    <div className="nm-order">
      <h3>Order Summary</h3>
      <div className="nm-layout">
        <p
          className="nm-pull--left"
          onClick={this.handleDropClick}
          style={{width: 'calc(100% - 80px)'}}>
          Subtotal &nbsp;
          <span className={`nm-link ${this.state.itemsLabelClass}`}>(items ▾)</span>
        </p>
        <p className="nm-pull--right nm-right-text">
          {subtotal}
        </p>
      </div>
      <div className={`nm-order__items ${this.state.dropClass}`}>
        {
          checkout && checkout.lineItems &&
          checkout.lineItems.edges.map((item, key) => {
            let left = item.node.title;
            if (item.node.quantity > 1) left = `${left} (x${item.node.quantity})`;
            return (
              <OrderSummaryItem
                key={key}
                style={{marginBottom: 12}}
                left={left}
                leftSecond={getLeftSecondText(item, this.props.subscriptionProducts)}
                right={`$${item.node.variant.price}`}/>
            )
          })
        }
      </div>
      <div className="nm-spacer-12" />
      <div className="nm-layout nm-layout__bar"/>
      <div className="nm-spacer-12" />
      <div className="nm-spacer-12" />
      {
        this.props.totals.totalDiscount > 0 &&
        <OrderSummaryItem
          left="Discount Code"
          rightClassName={discountedSavings < promotionalTotal ? "nm-error-strike" : "nm-green" } 
          right={`- $${this.props.totals.totalDiscount}`}/>
      }
      {
        this.props.totals.premiumDiscountTotal > 0 &&
        <OrderSummaryItem
          left="Premium Savings"
          rightClassName={discountedSavings < promotionalTotal ? "nm-error-strike" : "nm-green" } 
          right={`- $${this.props.totals.premiumDiscountTotal}`}/>
      }
      {
        this.props.totals.promotionDiscountTotal > 0 &&
        <OrderSummaryItem
          left="Promotional Savings"
          rightClassName={discountedSavings > promotionalTotal ? "nm-error-strike" : "nm-green" } 
          right={`- $${this.props.totals.promotionDiscountTotal}`}>
          <span className="nm-error">*</span>
        </OrderSummaryItem>
      }
      {
        checkout && checkout.requiresShipping &&
        <OrderSummaryItem
          left="Shipping"
          allowFree={true}
          right={shippingTotal}/>
      }
      <OrderSummaryItem
        left="Tax *"
        right={`$${this.props.totals.taxes}`}/>
      <OrderSummaryItem
        className="nm-order__bold"
        left={`Total${country === "US" ? "" : " (USD)"}`}
        right={`$${this.props.totals.total}`}/>
      <div className="nm-spacer-12"/>
      <div className="nm-spacer-12"/>
      <div className="nm-clear"/>
      <div className="nm-layout">
        <p className="" style={{fontSize: 12}}>
          * Taxes will be re-calculated during order processing
          and should not exceed the value above.
        </p>
        <div className="nm-spacer-12" />
        <div className="nm-spacer-12" />
      </div>
      {
        discountedSavings < promotionalTotal && discountedSavings > 0 &&
        <div className="nm-layout">
          <p className="nm-error" style={{fontSize: 12}}>
            * Great news! We have determined that you will recieve the greatest savings
            with a promotional discount.
            Discount codes and premium memberships can not be mixed with promotional discounts.
          </p>
          <div className="nm-spacer-12" />
          <div className="nm-spacer-12" />
        </div>
      }
      {
        discountedSavings > promotionalTotal && promotionalTotal > 0 && 
        <div className="nm-layout">
          <p className="nm-error" style={{fontSize: 12}}>
            * Great news! We have determined that you will recieve the greatest savings
            without promotional discounts.
            Discount codes and premium memberships can not be mixed with promotional discounts.
          </p>
          <div className="nm-spacer-12" />
          <div className="nm-spacer-12" />
        </div>
      }
      {
        this.props.hasSubscription && 
        <div className="nm-layout">
          <p className="nm-layout" style={{fontSize: 12}}>
            You’ll receive future capsule subscription deliveries for $59/mo +
            shipping & tax. Change, skip, or cancel anytime. 
          </p>
          <div className="nm-spacer-12" />
          <div className="nm-spacer-12" />
        </div>
      }
      {
        this.props.hasPremium && 
        <div className="nm-layout">
          <p className="nm-text-center" style={{fontSize: 12}}>
            You’ll receive all the benefits of Premium Membership for $9.99/mo,
            with a 6 month commitment. You can turn off automatic renewals anytime. 
          </p>
        </div>
      }
      <div className="nm-clear"></div>
    </div>
    );
  }
}

export default OrderSummary;
