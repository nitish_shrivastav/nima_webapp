import React, { Component } from 'react';
import PropTypes from 'prop-types';

let Modal = (props) => {

  const handleClick = () => {
    if (props.onClick) {
      props.onClick();
    }
  }

  return (
    <div className="nm-modal nm-modal__back nm-modal__center" onClick={handleClick} style={props.style}>
      { props.children }
    </div>
  );
}

Modal.propTypes = {
  children: PropTypes.element.isRequired,
  onClick: PropTypes.func,
  style: PropTypes.object
}

export default Modal;
