


import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from './Button';
import { AFFIRM, PAYPAL } from '../Constants/payment';


let CheckoutButton = (props) => {
  const { disabled, hasPaymentToken, type_, total } = props;
  let paypalButtonClass = (type_ === PAYPAL && !hasPaymentToken) ? "" : "nm-hidden";
  return (
    <div>
        <div className={`nm-btn__max nm-btn__center ${paypalButtonClass}`}>
          <p className={props.paypalButtonClass}/>
          <div className="nm-clear"/>
        </div>
      {
        !hasPaymentToken && type_ === AFFIRM && total >= 200 &&
        <Button
          center={true}
          className="nm-btn__max nm-btn__inverse"
          text="Authorize with "
          disabled={disabled}
          useLoader={true}
          isLoading={props.isLoading}
          loaderStyle={{color: "#0898CC" }}
          onClick={props.onClick}>
          <img
            className="nm-image nm-image__inline nm-image__inline--icon"
            src={`${process.env.PUBLIC_URL}/images/payment/affirm.svg`}
            style={{
              position: 'relative',
              top: 1
            }}
            alt="affirm"/>
        </Button>
      }
      {
        (hasPaymentToken || (type_ !== PAYPAL && type_ !== AFFIRM)) &&
        <Button
          center={true}
          disabled={disabled}
          className="nm-btn__max"
          text="Place Order"
          useLoader={true}
          isLoading={props.isLoading}
          onClick={props.onClick}>
        </Button>
      }
    </div>
  );
}

CheckoutButton.defaultProps = {
  paypalButtonClass: 'paypal-button'
}

CheckoutButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  hasPaymentToken: PropTypes.bool.isRequired,
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  useLoader: PropTypes.bool,
  center: PropTypes.bool
}

export default CheckoutButton;
