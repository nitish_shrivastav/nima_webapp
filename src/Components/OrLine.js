import React, { Component } from 'react';
import PropTypes from 'prop-types';

const OrLine = (props) => {
  return (
    <div className="nm-layout">
      <div className="nm-spacer-12"></div>
      <div className="nm-layout nm-layout__bar" style={{position: 'absolute', top: 'calc(50% - 1px)'}}/>
      <p className="nm-base__or" style={{ background: props.backgroundColor }}>
        or
      </p>
      <div className="nm-spacer-12"></div>
    </div>
  );
}

OrLine.defaultProps = {
  backgroundColor: "#F6F6F6"
}

OrLine.propTypes = {
  backgroundColor: PropTypes.string
}

export default OrLine;
