import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DropDown extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  
  componentWillMount() {
    this.setState({
      type: this.props.type ? this.props.type : "text",
      labelClass: "",
      hasFocused: false,
      isOpen: false,
      ulClass: "closed"
    })
  }

  handleClick(option, index) {
    this.props.onChange(option, index);
  }

  handleFocus() {
    this.setState({
      ulClass: ""
    })
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
    setTimeout(() => {
      this.setState({
        ulClass: "closed"
      })
    }, 100)
  }


  render() {
    let wrapClass = this.props.className ? this.props.className : "";
    let loaderClass = this.props.isLoading ? "" : "nm-loader__hidden";
    return (
      <div className={`nm-layout nm-form__input-wrap ${wrapClass}`}>
        <label className="nm-form__label">
          {
            this.props.field.label &&
            this.props.field.label
          }
        </label>
        <div className="nm-layout">
          <input
            name={this.props.field.name}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            className="nm-form__input nm-form__input--shadow"
            onChange={this.handleChange}
            readOnly
            style={{marginBottom: 20}}
            value={this.props.field.text}/>
          <div className="nm-loader__field" style={{position: 'absolute', left: 'calc(50% - 7px)', top: 13}}>
            <div className={`nm-loader nm-loader__blue ${loaderClass}`} style={{fontSize: 14}}></div>
          </div>
          <span className="nm-form__drop-icon">▾</span>
          <ul className={`nm-form__ul ${this.state.ulClass}`} style={{top: 0}}>
            {
              this.props.options.map((option, index) => {
                return (
                  <li
                    style={{paddingLeft: 12}}
                    className="nm-form__li"
                    onClick={() => {this.handleClick(option, index)}}>
                    {this.props.textExtractor(option)}
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

DropDown.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    text: PropTypes.string.isRequired
  }),
  useCheck: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired,
  textExtractor: PropTypes.func.isRequired
}

export default DropDown;
