import React, { Component } from 'react';
import PropTypes from 'prop-types';

class WhatsIncluded extends Component {
  render() {
    return (
      <div className="nm-layout">
        <h2 className="nm-base__section-title">
          What's Included
        </h2>
        <div className="nm-clear"></div>
        <div className="nm-card nm-layout__max-center--mobile-support">
          <div className="nm-layout__max-center" style={{maxWidth: 700}}>
            <div className="nm-spacer-48"></div>
            {
              this.props.included.image &&
              <img className="" src={this.props.included.image}/>
            }
            {
              this.props.included.items &&
              this.props.included.items.map((item, index) => {
                return (
                  <div key={`w_i_${index}`} className="nm-card__section nm-card__section--right nm-clear-bottom-padding">
                    <div className="nm-card__section--content">
                      <h4 className="nm-card__sub-title">
                        {item.title}
                      </h4>
                      <div className="nm-spacer-12"></div>
                      <p className="nm-card__description" dangerouslySetInnerHTML={{__html: item.body}}>
                      </p>
                    </div>
                    <div className="nm-spacer-12"></div>
                    <div className="nm-spacer-12"></div>
                  </div>
                )
              })
            }
            <div className="nm-clear"></div>
          </div>
          <div className="nm-spacer-48"></div>
        </div>
      </div>
    );
  }
}

WhatsIncluded.propTypes = {
  included: PropTypes.object.isRequired
}

export default WhatsIncluded;
