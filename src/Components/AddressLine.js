import React, { Component } from 'react';
import PropTypes from 'prop-types';

const AddressLine = (props) => {
  const { address } = props;
  return (
    <div>
      <span style={{fontSize: 12}}>
        {address.first_name} {address.last_name}
      </span>
      <span style={{fontSize: 12, fontWeight: 300}}>
        {` ${address.address1} ${address.address2} ${address.city} ${address.province_code} ${address.zip}`}
      </span>
      <div className="nm-layout" style={{textAlign: 'right'}}>
        {
          address.default &&
          <span style={{fontSize: 12}}>
            (Default Address)
          </span>
        }
        {
          !address.default &&
          <span className="nm-error" onClick={() => { props.onAddressDeleteClick(address.id) }} style={{fontSize: 12}}>
            Delete
          </span>
        }
        {` | `}
        <span className="nm-link" onClick={() => { props.onAddressEditClick(address) }} style={{fontSize: 12}}>
          Edit
        </span>
      </div>
    </div>
  )
}

AddressLine.propTypes = {
  address: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    address1: PropTypes.string.isRequired,
    address2: PropTypes.string,
    city: PropTypes.string,
    provice_code: PropTypes.string,
    zip: PropTypes.string,
    default: PropTypes.bool
  }).isRequired,
  onAddressDeleteClick: PropTypes.func.isRequired,
  onAddressEditClick: PropTypes.func.isRequired
}

export default AddressLine;
