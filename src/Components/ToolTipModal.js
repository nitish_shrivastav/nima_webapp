import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DropArrow from '../SVG/DropArrow';
import Modal from './Modal';

class ToolTipModal extends Component {
  constructor() {
    super();
    this.handleFilterClick = this.handleFilterClick.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
  }

  componentWillMount() {
    this.setState({ filterIsActive: true });
  }

  handleFilterClick() {
    //this.setState({ filterIsActive: !this.state.filterIsActive });
  }

  handleCloseClick() {
    this.setState({ filterIsActive: false });
  }
  
  render() {
    return (
      <span className={`nm-tooltip__drop ${this.state.filterIsActive ? "active": ""}`} >
        <span onClick={this.handleFilterClick}>
          Filter
          <DropArrow isActive={this.state.filterIsActive} />
          <div className="nm-tooltip nm-tooltip__below" style={{ maxWidth: 300 }}>
            <p className="nm-layout nm-push--right" onClick={this.handleCloseClick}>
              Close
            </p>
            {this.props.children}
          </div>
          <div className="nm-clear"/>
        </span>
        <div className="nm-clear"/>
        {
          this.state.filterIsActive &&
          <Modal style={{zIndex: 100, background: 'transparent'}} onClick={this.handleCloseClick}/>
        }
      </span>
    );
  }
}

export default ToolTipModal;
