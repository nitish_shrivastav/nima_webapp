import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ReviewRow extends Component {
  render() {
    return (
      <div className="nm-feed">
        <div className="nm-feed__header">
          <h3 className="nm-layout nm-feed__title">
            {this.props.title}
          </h3>
          <p className="nm-layout nm-feed__location">

          </p>
        </div>
      </div>
    );
  }
}

export default ReviewRow;
