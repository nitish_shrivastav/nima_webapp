import React from 'react';
import PropTypes from 'prop-types';
import { GREY } from '../Constants/colors';


let Toggle = (props) => {
  return (
    <div className="nm-toggle" onClick={props.onClick} style={{border: `2px solid ${props.color}`}}>
        <p className="nm-toggle__item">
          { props.leftText }
        </p>
        <p className="nm-toggle__item">
          { props.rightText }
        </p>
      <div
        className={`nm-toggle__slider ${props.isActive ? "right" : ""}`}
        style={{background: props.color}}/>
      <div className="nm-clear"></div>
    </div>
  );
}

Toggle.defaultProps = {
  color: GREY
}

Toggle.propTypes = {
  leftText: PropTypes.string.isRequired,
  rightText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired,
  color: PropTypes.string
}

export default Toggle;
