import React from 'react';
import Check from '../../SVG/Check';

let CheckboxField = (props) => {
    const { className, input, label, meta } = props;
    return (
      <div className={`nm-layout ${props.className}`}>
        <div className="nm-form__checkbox-wrap">
          <input
            {...input}
            checked={input.value}
            type="checkbox"
            className="nm-form__checkbox"/>
          <div className="nm-form__checkbox-check">
            <Check /> 
          </div>
        </div>
        <label className="nm-form__checkbox-label">
          {label}
        </label>
      </div>
    );
}

export default CheckboxField;
