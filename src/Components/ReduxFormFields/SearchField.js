import React from 'react';
import PropTypes from 'prop-types';

let SearchField = (props) => {
  return (
    <div className="nm-layout">
      <input {...props.input} className="nm-form__input"/>
    </div>
  );
}

SearchField.propTypes = {
  input: PropTypes.object.isRequired
}

export default SearchField;
