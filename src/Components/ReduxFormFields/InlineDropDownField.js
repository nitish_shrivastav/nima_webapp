import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InlineDropDownField extends Component {
  constructor() {
    super();
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  
  componentWillMount() {
    this.setState({ ulClass: "closed" });
  }

  handleFocus() {
    this.setState({ulClass: ""});
    this.props.input.onFocus();
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
    else {
      this.props.input.onBlur(e);
    }
    setTimeout(() => {
      this.setState({
        ulClass: "closed"
      })
    }, 100);
  }

  render() {
    const loaderClass = this.props.isLoading ? "" : "nm-loader__hidden";
    return (
      <div style={{display: 'inline-block', position: 'relative', ...this.props.style}}>
        <input
          {...this.props.input}
          onChange={() => {}}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          className="nm-form__input nm-form__input--inline"
          readOnly />
        <div className="nm-loader__field" style={{position: 'absolute', left: 'calc(50% - 7px)', top: 13}}>
          <div className={`nm-loader nm-loader__blue ${loaderClass}`} style={{fontSize: 14}}></div>
        </div>
        <ul className={`nm-form__ul ${this.state.ulClass}`} style={{top: 0}}>
          {
            this.props.options.map((option, index) => {
              return (
                <li
                  key={`${this.props.input.name}_${index}`}
                  style={{paddingLeft: 12}}
                  className="nm-form__li"
                  onMouseDown={() => {
                    this.props.input.onChange(option.text)
                  }}>
                  {
                    this.props.textExtractor &&
                    this.props.textExtractor(option)
                  }
                  {
                    !this.props.textExtractor &&
                    option.text
                  }
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
}

InlineDropDownField.defaultProps = {
  style: {}
}

InlineDropDownField.propTypes = {
  options: PropTypes.array.isRequired,
  textExtractor: PropTypes.func,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  isLoading: PropTypes.bool,
  className: PropTypes.string
}

export default InlineDropDownField;
