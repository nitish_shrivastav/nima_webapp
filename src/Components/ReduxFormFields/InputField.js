import React from 'react';
import PropTypes from 'prop-types';
import InputError from '../InputError';
import Tooltip from '../Tooltip';

let InputField = (props) => {
  const handleKeyPress = (e) => {
    if (props.onKeyDown) {
      props.onKeyDown(e)
    }
  }

  const handleChange = (e) => {
    if (props.onChange) {
      props.onChange(e.target.value);
    }
    else {
      props.input.onChange(e);
    }
  }

  const handleBlur = (e) => {
    if (props.onBlur) {
      props.onBlur(e);
    }
    else {
      props.input.onBlur(e);
    }
  }

  const { input, label, meta } = props;
  const inputClass = (meta.touched && meta.error) ? "nm-form__input-error" : "";
  const successClass = props.success ? "nm-form__input-success" : "";
  return (
    <div className={`nm-layout nm-form__input-wrap ${props.className}`}>
      <label className="nm-form__label">
        {label}
        {
          props.info &&
          <Tooltip text={props.info} />
        }
        {
          props.isLoading &&
          <div className="nm-loader__field">
            <div className="nm-loader nm-loader__blue" style={{fontSize: 10}}/>
          </div>
        }
      </label>
      <input
        {...input}
        type={props.type}
        onChange={handleChange}
        onBlur={handleBlur}
        onKeyDown={handleKeyPress}
        autocomplete={props.autocomplete}
        className={`nm-form__input ${inputClass} ${successClass}`}/>
      { props.children }
      <InputError
        show={meta.touched && meta.error}
        error={meta.error}/>
    </div>
  );
}

InputField.defaultProps = {
  className: "",
  autocomplete: "",
  success: false,
  type: "text"
}

InputField.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  info: PropTypes.string,
  children: PropTypes.element,
  success: PropTypes.bool
}

export default InputField;
