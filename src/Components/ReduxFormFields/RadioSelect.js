import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RadioField from './RadioField';
import InputError from '../InputError';

let RadioSelect = (props) => {

  const handleClick = (option) => {
    if (props.onClick) {
      props.onClick(option);
    }
    else {
      props.input.onChange(option.value);
    }
  }

  const { input, label, meta, options, renderOption, subText  } = props;
  let errorClass = (meta.touched && meta.error) ? "nm-form__input-error" : "";
  return (
    <div className="nm-layout">
      <h4>
        {label}
        {
          props.isLoading &&
          <div className="nm-loader__field">
            <div className="nm-loader nm-loader__blue" style={{fontSize: 10}}/>
          </div>
        }
      </h4>
      <div className="nm-payment">
        <p style={{fontSize: '13px' }}>{ subText }</p>
        <div className="nm-spacer-12"/>
        <ul className={`nm-form__checkselect ${errorClass}`}>
          {
            options.map((option, key) => {
              return (
                <li
                  className={`nm-form__checkselect-item ${option.isDisabled ? "nm-form__checkselect-item--disabled" : ""}`}
                  key={key}
                  onClick={() => handleClick(option)}>
                  <div style={{pointerEvents: 'none'}}>
                    <RadioField {...props} checked={input.value === option.value}/>
                  </div>
                  {
                    option.title &&
                    <span>{ option.title }</span>
                  }
                  { renderOption && renderOption(key) }
                </li>
              )
            })
          }
        </ul>
        <InputError
          show={meta.touched && meta.error}
          error={meta.error}/>
      </div>
    </div>
  );
}

RadioSelect.propTypes = {
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  renderOption: PropTypes.func,
  isLoading: PropTypes.bool
}

export default RadioSelect;
