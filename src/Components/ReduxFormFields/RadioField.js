import React, { Component } from 'react';
import PropTypes from 'prop-types';

let RadioField = (props) => {
  return (
    <div className="nm-checkbox__back">
      <input
        {...props.input}
        type="radio"
        className="nm-form__checkbox"
        checked={props.checked}/>
    </div>
  );
}

RadioField.defaultProps = {
  checked: false
}

RadioField.propTypes = {
  checked: PropTypes.bool.isRequired
}

export default RadioField;
