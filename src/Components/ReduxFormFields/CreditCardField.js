import React, { Component } from 'react';
import { formValueSelector } from 'redux-form';
import InputField from './InputField';
import _ from 'lodash';

const cardTypes = {
  visa: {
    niceType: 'Visa',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/visa.svg`,
    exactPattern: /^4\d*$/,
    gaps: [4, 8, 12],
    lengths: [16, 18, 19],
    code: {
      size: 3
    }
  },
  mastercard: {
    niceType: 'Mastercard',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/mastercard.svg`,
    exactPattern: /^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[0-1]|2720)\d*$/,
    gaps: [4, 8, 12],
    lengths: [16],
    code: {
      size: 3
    }
  },
  american_express: {
    niceType: 'American Express',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/american_express.svg`,
    exactPattern: /^3[47]\d*$/,
    isAmex: true,
    gaps: [4, 10],
    lengths: [15],
    code: {
      size: 4
    }
  },
  dinners_club: {
    niceType: 'Diners Club',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/diners_club.svg`,
    exactPattern: /^3(0[0-5]|[689])\d*$/,
    gaps: [4, 10],
    lengths: [14, 16, 19],
    code: {
      size: 3
    }
  },
  discover: {  
    niceType: 'Discover',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/discover.svg`,
    exactPattern: /^(6011|65|64[4-9])\d*$/,
    gaps: [4, 8, 12],
    lengths: [16, 19],
    code: {
      size: 3
    }
  },
  jcb: {
    niceType: 'JCB',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/jcb.svg`,
    exactPattern: /^(2131|1800|35)\d*$/,
    gaps: [4, 8, 12],
    lengths: [16, 17, 18, 19],
    code: {
      size: 3
    }
  },
  unionpay: {  
    niceType: 'UnionPay',
    image: `${process.env.PUBLIC_URL}/images/credit_cards/unionpay.svg`,
    exactPattern: /^(((620|(621(?!83|88|98|99))|622(?!06|018)|62[3-6]|627[02,06,07]|628(?!0|1)|629[1,2]))\d*|622018\d{12})$/,
    gaps: [4, 8, 12],
    lengths: [16, 17, 18, 19],
    code: {
      size: 3
    }
  }
}

class CreditCardField extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    this.setState({
      cardImage: null,
      cardClass: ""
    })
  }

  getCardType(number) {
    for (let card in cardTypes) {
      if (cardTypes[card].exactPattern.test(number)) {
        return cardTypes[card];
      }
    }
    return null;
  }

  prettyField(input, gaps, joinChar) {
    let offsets = [0].concat(gaps).concat([input.length]);
    let components = [];

    for (let i = 0; offsets[i] < input.length; i++) {
      let start = offsets[i];
      let end = Math.min(offsets[i + 1], input.length);
      components.push(input.substring(start, end));
    }
    return components.join(joinChar);
  }

  handleChange(number) {
    number = number.replace(new RegExp(' ', 'g'), '');
    let cardType = this.getCardType(number);
    if (cardType) {
      number = this.prettyField(number, cardType.gaps, " ")
      this.setState({
        cardImage: cardType.image,
        cardClass: "show"
      })
    } else {
      this.setState({
        cardClass: ""
      })
    }
    this.props.input.onChange(number);
  }


  render() {
    return (
      <div className="nm-layout">
        <InputField
          {...this.props}
          onChange={this.handleChange}/>
        <img
          src={this.state.cardImage}
          className={`nm-image nm-image__card ${this.state.cardClass}`}/>
      </div>
    );
  }
}

export default CreditCardField;
