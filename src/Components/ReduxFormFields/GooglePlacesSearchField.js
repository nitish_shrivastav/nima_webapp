import React, { Component } from 'react';
import { debounce } from 'lodash';
import Pin from '../../SVG/Pin';
import InputField from './InputField';

class GooglePlacesSearchField extends Component {
  constructor() {
    super();
    this.handleAddressChange = this.handleAddressChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.autocompleteCallback = this.autocompleteCallback.bind(this);
    this.handlePlaceClick = this.handlePlaceClick.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.autocompleteService = new window.google.maps.places.AutocompleteService();
    this.placeDetailService = new window.google.maps.places.PlacesService(document.createElement('div'));
    this.debounceFetchedPlaces = debounce(
      this._fetchPlaces,
      300
    );
  }

  componentWillMount() {
    this.setState({
      autocompleteItems: [],
      selectedIndex: -1,
      isLoading: false
    })
  }


  extractPlaceData(placeData){
    let returnData = {
      name: placeData.name
    }
    if (placeData.address_components){
      for (let component of placeData.address_components){
        if (component.types.indexOf("administrative_area_level_1") !== -1){
          returnData.province = component.long_name
          returnData.province_short = component.short_name
        }
        if (component.types.indexOf("locality") !== -1 || component.types.indexOf("postal_town") !== -1){
          returnData.city = component.long_name
        }
        if (component.types.indexOf("country") !== -1){
          returnData.country = component.long_name
          returnData.country_short = component.short_name
        }
        if (component.types.indexOf("postal_code") !== -1){
          returnData.postal_code = component.long_name
        }
      }
    }
    return returnData
  }

  handleAddressChange(e) {
    this.props.input.onChange(e);
    this.debounceFetchedPlaces();
    this.setState({
      isLoading: true
    })
  }

  autocompleteCallback(predictions, status) {
    this.setState({
      isLoading: false
    })
    if (status === 'OK') {
      this.setState({
        autocompleteItems: predictions
      })
    }
  }

  handlePlaceClick(autocomplete) {
    this.setState({ isLoading: true })
    this.placeDetailService.getDetails({placeId: autocomplete.place_id}, (place, status) => {
      this.setState({ isLoading: false })
      if (status === window.google.maps.places.PlacesServiceStatus.OK) {
        let address = this.extractPlaceData(place)
        this.props.onPlaceClick(address)
      }
    })
  }

  _fetchPlaces() {
    if (this.props.input.value.length) {
      this.autocompleteService.getPlacePredictions(
        {
          input: this.props.input.value
        },
        this.autocompleteCallback
      );
    }
  }

  handleBlur(e) {
    this.setState({
      autocompleteItems: [],
      selectedIndex: -1
    })
    this.props.input.onBlur(e);
  }

  handleKeyPress(e) {
    //down key
    if (e.keyCode === 40 && this.state.autocompleteItems.length > this.state.selectedIndex + 1) {
      this.setState({
        selectedIndex: this.state.selectedIndex + 1
      })
    }
    //up key
    else if (e.keyCode === 38 && this.state.selectedIndex > 0) {
      this.setState({
        selectedIndex: this.state.selectedIndex - 1
      })
    }
    //enter key
    else if (e.keyCode === 13 && this.state.selectedIndex > -1) {
      this.handlePlaceClick(this.state.autocompleteItems[this.state.selectedIndex])
      this.handleBlur()
    }
  }


  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout nm-pull--left nm-relative">
          <InputField
            {...this.props}
            onChange={this.handleAddressChange}
            onBlur={this.handleBlur}
            onKeyDown={this.handleKeyPress}
            isLoading={this.state.isLoading}/>
        </div>
        <div className="nm-layout">
          {
            this.state.autocompleteItems.length > 0 &&
            <ul className="nm-form__ul">
              {
                this.state.autocompleteItems.map((item, key) => {
                  let className = key === this.state.selectedIndex ? "selected" : "";
                  return (
                    <li
                      key={key}
                      className={`nm-layout nm-form__li-wrap ${className}`}
                      onMouseDown={() => {this.handlePlaceClick(item)}}>
                      <div className="nm-pull--left" style={{ margin: '6px 24px 0 16px' }}>
                        <Pin width="18px" height="18px"/>
                      </div>
                      <p className="nm-form__li" style={{width: 'calc(100% - 58px)'}}>
                        {item.description}
                      </p>
                    </li>
                  )
                })
              }
              <li className="nm-layout" style={{padding: 5}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/powered_by_google.png`}
                  style={{ height: 20 }}
                  alt="Powered By Google"
                  className="nm-pull--right"/>
              </li>
            </ul>
          }
        </div>
      </div>
    );
  }
}

GooglePlacesSearchField.defaultProps = {
}

GooglePlacesSearchField.propTypes = {

}

export default GooglePlacesSearchField;
