import React, { Component } from 'react';
import PropTypes from 'prop-types';

class DropDownField extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  
  componentWillMount() {
    this.setState({ ulClass: "closed" });
  }

  handleClick(option, index) {
    if (this.props.onChange) {
      this.props.onChange(option, index);
    }
    else {
      this.props.input.onChange(option.value);
    }
  }

  handleFocus() {
    this.setState({ulClass: ""});
    this.props.input.onFocus();
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
    else {
      this.props.input.onBlur(e);
    }
    setTimeout(() => {
      this.setState({
        ulClass: "closed"
      })
    }, 100);
  }

  render() {
    const wrapClass = this.props.className ? this.props.className : "";
    const loaderClass = this.props.isLoading ? "" : "nm-loader__hidden";
    return (
      <div className={`nm-layout nm-form__input-wrap ${wrapClass}`}>
        <label className="nm-form__label">
          {
            this.props.label &&
            this.props.label
          }
        </label>
        <div className="nm-layout">
          <input
            {...this.props.input}
            onChange={() => {}}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            className="nm-form__input nm-form__input--shadow"
            readOnly
            style={{marginBottom: 20}}/>
          <div className="nm-loader__field" style={{position: 'absolute', left: 'calc(50% - 7px)', top: 13}}>
            <div className={`nm-loader nm-loader__blue ${loaderClass}`} style={{fontSize: 14}}></div>
          </div>
          <span className="nm-form__drop-icon">▾</span>
          <ul className={`nm-form__ul ${this.state.ulClass}`} style={{top: 0}}>
            {
              this.props.options.map((option, index) => {
                return (
                  <li
                    key={`${this.props.input.name}_${index}`}
                    style={{paddingLeft: 12}}
                    className="nm-form__li"
                    onClick={() => {this.handleClick(option, index)}}>
                    {
                      this.props.textExtractor &&
                      this.props.textExtractor(option)
                    }
                    {
                      !this.props.textExtractor &&
                      option.text
                    }
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

DropDownField.defaultProps = {
  options: [],
  isLoading: false,
  className: ""
}

DropDownField.propTypes = {
  options: PropTypes.array.isRequired,
  textExtractor: PropTypes.func,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  isLoading: PropTypes.bool,
  className: PropTypes.string
}

export default DropDownField;
