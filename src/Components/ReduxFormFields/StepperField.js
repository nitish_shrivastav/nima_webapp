import React, { Component } from 'react';
import PropTypes from 'prop-types';

class StepperField extends Component {
  constructor() {
    super();
    this.handleDownClick = this.handleDownClick.bind(this);
    this.handleUpClick = this.handleUpClick.bind(this);
  }

  handleDownClick() {
    if (this.props.input.value > 0) {
      this.props.input.onChange(parseInt(this.props.input.value) - 1);
    }
  }

  handleUpClick() {
    this.props.input.onChange(parseInt(this.props.input.value) + 1);
  }

  render() {
    return (
      <div className={this.props.className} style={{width: 62, padding: 0}}>
        <span className="nm-stepper" onClick={this.handleDownClick}>
          <svg className="nm-stepper__icon minus-stepper" viewBox="0 0 20 20">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g className="nm-stepper__fill" transform="translate(-123.000000, -214.000000)">
                <path d="M135.926,224.674 L135.926,223.526 L130.074,223.526 L130.074,224.674 L135.926,224.674 Z M133,218.276 C134.064005,218.276 135.048662,218.541997 135.954,219.074 C136.831338,219.578003 137.521997,220.268662 138.026,221.146 C138.558003,222.051338 138.824,223.035995 138.824,224.1 C138.824,225.164005 138.558003,226.148662 138.026,227.054 C137.521997,227.931338 136.831338,228.621997 135.954,229.126 C135.048662,229.658003 134.064005,229.924 133,229.924 C131.935995,229.924 130.951338,229.658003 130.046,229.126 C129.168662,228.612664 128.478003,227.917338 127.974,227.04 C127.441997,226.134662 127.176,225.154672 127.176,224.1 C127.176,223.045328 127.441997,222.065338 127.974,221.16 C128.487336,220.282662 129.182662,219.587336 130.06,219.074 C130.965338,218.541997 131.945328,218.276 133,218.276 Z"></path>
              </g>
            </g>
          </svg>
        </span>
        <input {...this.props.input} className="nm-form__quantity" readOnly/>
        <span className="nm-stepper" onClick={this.handleUpClick}>
          <svg className="nm-stepper__icon plus-stepper" viewBox="0 0 20 20">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g className="nm-stepper__fill" transform="translate(-148.000000, -214.000000)">
                <path d="M160.926,224.674 L160.926,223.526 L158.574,223.526 L158.574,221.174 L157.426,221.174 L157.426,223.526 L155.074,223.526 L155.074,224.674 L157.426,224.674 L157.426,227.026 L158.574,227.026 L158.574,224.674 L160.926,224.674 Z M158,218.276 C159.064005,218.276 160.048662,218.541997 160.954,219.074 C161.831338,219.578003 162.521997,220.268662 163.026,221.146 C163.558003,222.051338 163.824,223.035995 163.824,224.1 C163.824,225.164005 163.558003,226.148662 163.026,227.054 C162.521997,227.931338 161.831338,228.621997 160.954,229.126 C160.048662,229.658003 159.064005,229.924 158,229.924 C156.935995,229.924 155.951338,229.658003 155.046,229.126 C154.168662,228.612664 153.478003,227.917338 152.974,227.04 C152.441997,226.134662 152.176,225.154672 152.176,224.1 C152.176,223.045328 152.441997,222.065338 152.974,221.16 C153.487336,220.282662 154.182662,219.587336 155.06,219.074 C155.965338,218.541997 156.945328,218.276 158,218.276 Z" id="add_circle---material"></path>
              </g>
            </g>
          </svg>
        </span>
      </div>
    );
  }
}

StepperField.defaultProps = {
  className: ""
}

StepperField.propTypes = {
  className: PropTypes.string
}

export default StepperField;
