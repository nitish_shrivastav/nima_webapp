import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import aboutCapsules from '../Data/abountCapsules';
import { Link } from 'react-router-dom';
import Button from './Button';
import CheckoutFooter from './CheckoutFooter';
import MoneyHelper from '../Util/MoneyHelper';
import FAQ from './FAQ';
import BackToTopButton from './BackToTopButton';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import WhatsIncluded from './WhatsIncluded';
import Dropdown from './Dropdown';
import OrLine from './OrLine';
import { ALLERGEN_COLOR, RED } from '../Constants/colors';


const Product = (props) => {
  const { product, productInfo, onAddToCart  } = props;
  let adHocVariantIndex = (productInfo.ad_hoc_index && product.variants.edges[productInfo.ad_hoc_index]) ? productInfo.ad_hoc_index : 0;
  const variant = product.variants.edges[adHocVariantIndex].node;
  const decodedProductId = StorefrontInteractor.decodeId(product.id, "Product");
  return (
    <div className="nm-layout">
      {
        productInfo.banner &&
        <div className="nm-layout">
          <div
            className="nm-layout nm-card nm-flex nm-flex--row nm-flex__space-between nm-flex--row-mobile-column"
            style={{
              overflow: 'hidden',
              border: `1px solid ${ALLERGEN_COLOR[productInfo.allergen]}`,
              padding: 12
            }}>
            {
              productInfo.banner.text &&
              <p className="nm-layout__mobile-text-center" style={{ paddingRight: 12, fontWeight: 600 }}>
                {productInfo.banner.text}
              </p>
            }
            <div className="nm-spacer-12 nm-tablet-and-up-hidden"/>
            {
              productInfo.banner.button &&
              <Link to={productInfo.banner.button.link}>
                <Button
                  preventDefault={false}
                  style={{background: RED, width: 200}}
                  preventDefault={false}
                  text={productInfo.banner.button.text}/>
              </Link>
            }
          </div>
          <div className="nm-spacer-48"/>
        </div>
      }
      <div className="nm-card nm-card__block nm-card__block--cornerless">
        <div className="nm-spacer-48"></div>
        <div className="nm-card__section">
          <img
            className="nm-layout nm-image nm-image__product"
            src={productInfo.alt_image ? productInfo.alt_image : _.get(product, 'images.edges[0].node.originalSrc')}
            alt={product.title}/>
        </div>
        <div className="nm-card__section nm-card__section--right" style={{paddingBottom: 18}}>
          <div className="nm-card__section--content" style={{paddingBottom: 18}}>
            <h2 className="nm-card__title">
              { _.get(productInfo, 'title_override') ? productInfo.title_override : product.title }
              {
                _.get(productInfo, 'subtitle') &&
                <React.Fragment>
                  <div className="nm-spacer-12"></div>
                  <p>{productInfo.subtitle}</p>
                </React.Fragment>
              }
              {
                productInfo && productInfo.show_reviews &&
                <div className="nm-layout" dangerouslySetInnerHTML={{__html: `<div data-bv-show="rating_summary" data-bv-product-id="${decodedProductId}"></div>` }}/>
              }
            </h2>
            <div className="nm-card__price-wrap" style={{marginBottom: 12}}>
              {
                variant.compareAtPrice && variant.price !== variant.compareAtPrice &&
                <div>
                  <span className="nm-card__price push--left">
                    { MoneyHelper.moneyWithoutTrailingZeros(variant.price) }
                  </span>
                  <span className="nm-card__price--line">
                    <span className="nm-card__price--old" style={{ marginLeft: 10 }}>
                      { MoneyHelper.moneyWithoutTrailingZeros(variant.compareAtPrice) }
                    </span>
                  </span>
                </div>
              }
              {
                (!variant.compareAtPrice || variant.price === variant.compareAtPrice) &&
                <span className="nm-card__price push--left">
                  { MoneyHelper.moneyWithoutTrailingZeros(variant.price) }
                  {
                    productInfo && productInfo.commitment &&
                    <span style={{fontSize: 16}}>/mo</span>
                  }
                </span>
              }
            </div>
            <p className="nm-card__description" dangerouslySetInnerHTML={{__html: product.meta.description}}>
            </p>
            <div className="nm-spacer-12"></div>
            <div className="nm-spacer-12"></div>
            {
              productInfo && productInfo.premium_link &&
              <div className="nm-layout">
                {/* Temporarily removed, currently no premium membership for peanut */}
                {/* <Link to={productInfo.premium_link} className="nm-link">
                  <span
                    className="nm-bold"
                    style={{fontSize: 14, top: -1, position: 'relative', color: 'inherit'}}>
                    &#9432;
                  </span>
                  $59 for Premium Members
                </Link> */}
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
              </div>
            }
            <div className="nm-clear"></div>
            {
              !productInfo.reserve && product.tags && product.tags.indexOf("backorder") === -1 &&
              <Button
                text="Add to Cart"
                useCheck={true}
                onClick={() => { onAddToCart(variant); }}/>
            }
            {
              !productInfo.reserve && product.tags && product.tags.indexOf("backorder") !== -1 &&
              <div>
                <Button
                  text={_.get(productInfo, 'order_now_button_message', 'Order Now (Shipping within a week)')}
                  useCheck={true}
                  onClick={() => {
                    if (productInfo.subscription_index) onAddToCart(variant, productInfo.subscription_index);
                    else onAddToCart(variant);
                  }}/>
                <p className="nm-layout nm-text-center" style={{fontStyle: "italic"}}>
                  {_.get(productInfo, 'order_now_subtext', 'We will ship orders as soon as inventory becomes available')}
                </p>
              </div>
            }
            {
              product.variants.edges.length !== 1 &&
              <div>
                <div className="nm-spacer-12"></div>
                <OrLine backgroundColor='white'/>
                <div className="nm-spacer-12"></div>
                <p className="nm-layout">
                  <span className="nm-bold">Automatic refills &mdash;</span>
                  {MoneyHelper.moneyWithoutTrailingZeros(product.variants.edges[0].node.price)} per 12-pack
                </p>
                <div className="nm-spacer-12"></div>
                <Dropdown
                  options={product.variants.edges.slice(0, 3)}
                  textExtractor={(option) => {return option.node.title}}
                  onChange={(selectedVariant, index) => onAddToCart(selectedVariant.node, index)}
                  field={{
                    name: "variant",
                    text: "Choose delivery frequency"
                  }}/>
              </div>
            }
            <div className="nm-spacer-12"></div>
            {
              productInfo && productInfo.affirm_eligable &&
              <div className="nm-layout" dangerouslySetInnerHTML={{
                __html: `<p class="affirm-as-low-as" data-page-type="product" data-amount="${parseInt(parseFloat(variant.price) * 100)}"></p>`
              }}/>
            }
            <div className="nm-spacer-12"></div>
            {
              productInfo && productInfo.other_options &&
              <p className="nm-basic nm-basic__p nm-text-center">
                { "Other options: " }
                <Link to={productInfo.other_options[0].href} className="nm-link">
                  {productInfo.other_options[0].text}
                </Link>
                { " | " }
                <Link to={productInfo.other_options[1].href} className="nm-link">
                  {productInfo.other_options[1].text}
                </Link>
              </p>
            }
            {
              productInfo && productInfo.continue_link &&
              <Link to={productInfo.continue_link.href} className="nm-link nm-text-center nm-layout">
                {productInfo.continue_link.text}
              </Link>
            }
          </div>
        </div>
      </div>
      <CheckoutFooter hasTopCorners={true}/>
      {
        productInfo && productInfo.included &&
        <WhatsIncluded included={productInfo.included} />
      }
      {
        productInfo && productInfo.about && productInfo.about === "capsules" &&
        <div className="nm-layout">
          <div className="nm-spacer-48"></div>
          <div className="nm-layout" dangerouslySetInnerHTML={{__html: aboutCapsules}}/>
          <div className="nm-spacer-48"></div>
        </div>
      }
      {
        productInfo && productInfo.faq &&
        <FAQ items={productInfo.faq} />
      }
      <div className="nm-spacer-48"></div>
      {
        productInfo && productInfo.show_reviews &&
        <div className="nm-layout">
          <h2 className="nm-base__section-title">
            Reviews
          </h2>
          <div className="nm-clear"></div>
          <div className="nm-layout__max-center nm-layout__max-center--mobile-support" style={{maxWidth: 620}}>
            <div className="nm-card">
              <div className="nm-layout" dangerouslySetInnerHTML={{__html: `<div data-bv-show="reviews" data-bv-product-id="${decodedProductId}"></div>` }}/>
            </div>
            <div className="nm-clear"></div>
          </div>
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
        </div>
      }
      <div className="nm-spacer-48"></div>
      <div className="nm-clear"></div>
      <p className="nm-grey-text nm-text-center nm-bold">
        Questions? we would love to help. <a href="https://help.nimasensor.com/hc/en-us" className="nm-link nm-bold" target="_blank"><br/>
        Search our helpful FAQ</a> or <a href="/contact" className="nm-link nm-bold" target="_blank">Contact Us</a>
      </p>
      <div className="nm-clear"></div>
      <BackToTopButton />
    </div>
  );
}

Product.propTypes = {
  productInfo: PropTypes.shape({
    allergen: PropTypes.string.isRequired,
    show_reviews: PropTypes.bool,
    next_step_link: PropTypes.string,
    premium_link: PropTypes.string,
    continue_link: PropTypes.shape({
      text: PropTypes.string.isRequired,
      href: PropTypes.string.isRequired
    }),
    ad_hoc_index: PropTypes.number,
    faq: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired,
      extra_link: PropTypes.shape({
        href: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired
      })
    })),
    about: PropTypes.string,
    order_now_button_message: PropTypes.string,
    order_now_subtext: PropTypes.string
  }),
  product: PropTypes.object.isRequired,
  onAddToCart: PropTypes.func.isRequired
}

export default Product;
