import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RadioInput from './RadioInput';

const methods = [];

class RadioList extends Component {
  render() {
    return (
      <div className="nm-layout">
        <h4>How</h4>
        <div className="nm-payment">
          <ul className="nm-form__checkselect">
            {
              methods.map((method, key) => {
                return (
                  <li
                    className="nm-form__checkselect-item"
                    key={key}
                    onClick={() => this.props.onClick(method)}>
                    <RadioInput checked={this.props.value === method.value}/>
                    {
                      method.title &&
                      <span>{ method.title }</span>
                    } 
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    );
  }
}

RadioList.propTypes = {
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default RadioList;
