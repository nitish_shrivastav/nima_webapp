import React, { Component } from 'react';
import Lock from '../SVG/Lock';

class FormHeader extends Component {
  render() {
    return (
      <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
        <div className="nm-clear"/>
        <div className="nm-checkout nm-checkout__overlay">
          <div className="nm-checkout__section nm-checkout__section--mobile-color">
            <h3 className="nm-margin-clear nm-mobile-white">Place your order</h3>
            <div className="nm-layout">
              <div className="nm-image nm-image__lock">
                <Lock />
              </div>
              <p className="nm-mobile-white">Secure Checkout</p>
            </div>
          </div>
          <div className="nm-clear"/>
        </div>
        <div className="nm-clear"/>
      </div>
    );
  }
}

export default FormHeader;