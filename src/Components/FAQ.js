import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FAQ extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    this.setState({ activeIndex: 0 });
  }

  handleClick(index) {
    this.setState({ activeIndex: index });
  }

  render() {
    return (
      <div className="nm-layout">
        <h2 className="nm-base__section-title">
          Frequently asked questions
        </h2>
        <div className="nm-clear"></div>
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support" style={{maxWidth: 620}}>
          <div className="nm-card">
            {
              this.props.items.map((item, index) => {
                return (
                  <div key={`faq_${index}`} className="nm-layout nm-card__drop" onClick={() => this.handleClick(index)}>
                    <div className="nm-spacer-12"></div>
                    <div className="nm-spacer-12"></div>
                    <h4 className="nm-layout nm-bold">
                      {item.title}
                    </h4>
                    <div className="nm-spacer-12"></div>
                    <div className={`nm-card__faq ${this.state.activeIndex === index ? "active" : ""}`}>
                      <p className="nm-card__description" dangerouslySetInnerHTML={{__html: item.body}}>
                      </p>
                      {
                        item.extra_link &&
                        <a href={item.extra_link.href} className="nm-link" target="_blank" style={{fontWeight: 500}}>
                          <div className="nm-spacer-12"></div>
                          {item.extra_link.text}
                        </a>
                      }
                    </div>
                  </div>
                )
              })
            }
          </div>
          <div className="nm-clear"></div>
        </div>
      </div>
    );
  }
}

FAQ.propTypes = {
  items: PropTypes.array.isRequired
}

export default FAQ;
