import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import StarRating from '../SVG/StarRating';
import _ from 'lodash';
import config from '../config';
import { ALLERGEN_COLOR, RED, DARK_GREY, TEXT } from '../Constants/colors';


const PlaceTicket = (props) => {
  const { placeItem, isSidebar, allergen } = props;

  const handleTicketClick = () => {
    if (props.onTicketClick) {
      props.onTicketClick(placeItem);
    }
  };

  let imageStyle = {};
  if (isSidebar) {
    imageStyle.width = 90;
    imageStyle.height = 100;
  }
  const experienceScore = _.get(placeItem, `scores.${allergen}.experience_score`);
  const reviewCount = `${_.get(placeItem, `counts[${props.allergen}].total_reviews`) ? placeItem.counts[props.allergen].total_reviews : "0"}`;
  const open_now = _.get(placeItem, 'opening_hours.open_now');
  const exp = new RegExp('https\:\/\/');
  let image_src = "https://s3-us-west-2.amazonaws.com/nima-prod-web-assets/default_restaurant_1.jpg";
  if (placeItem.photos){
    image_src = exp.exec(placeItem.photos[0].photo_reference) ?
      `${placeItem.photos[0].photo_reference}` :
      `https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=${placeItem.photos[0].photo_reference}&key=${config.GOOGLE_PLACES_KEY}`;
  }
  return (
    <div className="nm-layout">
      <div
        className={`nm-layout nm-ticket nm-ticket__full ${isSidebar ? "nm-ticket__hover": ""}`}
        style={{background: "#F6F6F6"}}>
        <div className="nm-layout">
          <Link to={`/map/places/${placeItem.id}?allergen=${props.allergen}`}>
            {
              placeItem.photos && placeItem.photos[0] &&
              <img
                className={`nm-image nm-ticket__image nm-ticket__image--fit ${isSidebar ? "nm-pull--right " : ""}`}
                style={imageStyle}
                src={image_src} />
            }
            {
              (!placeItem.photos || !placeItem.photos[0]) && placeItem.reference &&
              <img
                className={`nm-image nm-ticket__image nm-ticket__image--fit ${isSidebar ? "nm-pull--right " : ""}`}
                style={imageStyle}
                src={image_src} />
            }
          </Link>
          <div
            className={`${isSidebar ?  "nm-layout nm-ticket__map" : "nm-ticket__main-column"}`}
            onClick={handleTicketClick}>
            <h4 className="nm-layout">
              {placeItem.name}
            </h4>
            <p className="nm-layout" style={{fontSize: 12, color: DARK_GREY}}>
              {placeItem.short_address} {placeItem.city} {placeItem.province_short}
              {
                placeItem.price_level &&
                <span style={{color: TEXT }}>
                  {` ${"$".repeat(placeItem.price_level)}`}
                </span>
              }
            </p>
            <div className="nm-layout">
              {
                isSidebar && experienceScore &&
                <div className="nm-pull--left" style={{ maxWidth: 100 }}>
                  <StarRating
                    startColor={ALLERGEN_COLOR[allergen]}
                    stroke={ALLERGEN_COLOR[allergen]}
                    idPrefix={`isSidebar-${placeItem.id}`}
                    percent={100 * experienceScore / 5}
                    style={{width: 10, height: 10, marginRight: 2}}/>
                </div>
              }
              {
                !isSidebar && experienceScore &&
                <div className="nm-pull--left" style={{ maxWidth: 125 }}>
                  <StarRating
                    startColor={ALLERGEN_COLOR[allergen]}
                    stroke={ALLERGEN_COLOR[allergen]}
                    percent={100 * experienceScore / 5}
                    idPrefix={`notSidebar-${placeItem.id}`}
                    style={{width: 20, height: 20, marginRight: 5}}/>
                </div>
              }
              {
                reviewCount !== "0" &&
                <p className="nm-pull--left" style={{lineHeight: isSidebar ? "23px" : "inherit"}}>
                  {reviewCount} Reviews
                </p>
              }
              {
                reviewCount === "0" && !placeItem.chain_counts &&
                <p className="nm-layout nm-pull--left" style={{lineHeight: isSidebar ? "1" : "inherit", color: RED, fontStyle: 'italic'}}>
                  Be the first to write a review!
                </p>
              }
            </div>
            <div className="nm-layout">
              <Link to={`/map/places/${placeItem.id}?allergen=${props.allergen}`} className="nm-link" style={{fontSize: isSidebar ? 14 : "auto"}}>
                Reviews & details →
              </Link>
            </div>
          </div>
          {
            !isSidebar &&
            <div className="nm-ticket__right-column nm-tablet-and-down-hidden">
              <div className="nm-layout">
                <a href={`https://www.google.com/maps/dir/?q=place_id:${placeItem.id}`} className="nm-link" target="_blank">
                  <img style={{width: 12, height: 12, marginRight: 18}} src={`${process.env.PUBLIC_URL}/images/icons/address.png`}/>
                  Get Directions
                </a>
              </div>
              {
                placeItem.international_phone_number && placeItem.international_phone_number !== "unknown" &&
                <div className="nm-layout">
                  <a href={`tel:${placeItem.international_phone_number}`} className="nm-link" target="_blank">
                    <img style={{width: 12, height: 12, marginRight: 18}} src={`${process.env.PUBLIC_URL}/images/icons/phone.png`}/>
                    {placeItem.international_phone_number}
                  </a>
                </div>
              }
              <div className="nm-layout">
                <p>
                  <img style={{width: 12, height: 12, marginRight: 18}} src={`${process.env.PUBLIC_URL}/images/icons/hours.png`}/>
                  {
                    open_now &&
                    <span className="nm-bold">
                      Open Now
                    </span>
                  }
                  {
                    open_now === false &&
                    <span className="nm-bold" style={{color: RED}}>
                      Closed Now
                    </span>
                  }
                  <Link to={`/map/places/${placeItem.id}?allergen=${props.allergen}`} className="nm-link" style={{fontSize: isSidebar ? 14 : "auto"}}>
                    {` ${"See hours"}`}
                  </Link>
                </p>
              </div>
              {
                placeItem.website &&
                <div className="nm-layout">
                  <a href={placeItem.website} className="nm-link" target="_blank">
                    <img style={{width: 12, height: 12, marginRight: 18}} src={`${process.env.PUBLIC_URL}/images/icons/website.png`}/>
                    Restaurant website
                  </a>
                </div>
              }
            </div>
          }
        </div>
      </div>
      <div className="nm-spacer-12"/>
      <div className="nm-spacer-12"/>
    </div>
  );
}

PlaceTicket.propTypes = {
  placeItem: PropTypes.object.isRequired,
  isSidebar: PropTypes.bool,
  onTicketClick: PropTypes.func
}

export default PlaceTicket;
