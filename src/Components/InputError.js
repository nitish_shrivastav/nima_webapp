import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputError extends Component {
  render() {
    return (
      <div className="nm-layout" style={{height: 18}}>
        {
          this.props.error && this.props.show &&
          <p className={`nm-form__error ${this.props.className}`}>
            {this.props.error}
          </p>
        }
      </div>
    );
  }
}

InputError.defaultProps = {
  className: ""
}

InputError.propTypes = {
  show: PropTypes.bool,
  error: PropTypes.string,
  className: PropTypes.string
}

export default InputError;
