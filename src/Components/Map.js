import React, { Component } from 'react';
import _ from 'lodash';
import { GoogleMap, withGoogleMap, Marker } from 'react-google-maps';
import { InfoBox } from "react-google-maps/lib/components/addons/InfoBox";
import Button from './Button';

const defaultMapOptions = {
  "mapTypeControl": false,
  "streetViewControl": false,
  "fullscreenControl": false,
  "gestureHandling": "greedy",
  "styles": [
    {
      "featureType": "landscape.man_made",
      "elementType": "geometry",
      "stylers": [
        {
          "lightness": -5
        },
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "poi.business",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    },
    {
      "featureType": "transit",
      "stylers": [{
        "visibility": "off"
      }]
    }
  ]
}

const MapComponent = withGoogleMap((props) => {
  let defaultCenter = props.defaultCenter ? props.defaultCenter : { lat: 37.774929, lng: -122.419416  };
  return (
    <div>
      {
        props.showRefreshButton &&
        <Button
          text="Redo search in this area"
          className="nm-map__re-search-btn"
          onClick={props.onResearchClick}/>
      }
      <GoogleMap
        ref={props.setMapRef}
        onBoundsChanged={props.onBoundsChanged}
        onCenterChanged={props.onCenterChanged}
        defaultOptions={defaultMapOptions}
        onClick={props.onClick}
        defaultZoom={props.defaultZoom ? parseInt(props.defaultZoom) : 15}
        defaultCenter={defaultCenter}>
        {
          props.selectedLocation && 
          <InfoBox position={new window.google.maps.LatLng(props.selectedLocation.geometry.location.lat, props.selectedLocation.geometry.location.lng)}>
            { props.renderInfoBoxContent && props.renderInfoBoxContent() }
          </InfoBox>
        }
        {
          props.position &&
          <Marker position={props.position} />
        }
        {
          props.markers.map((point, index) => {
            if (props.renderMarker) {
              return props.renderMarker(point, index);
            }
          })
        }
      </GoogleMap>
    </div>
  )
})


class Map extends Component {
  constructor() {
    super();
    this.handleCenterChanged = this.handleCenterChanged.bind(this);
    this.handleResearchClick= this.handleResearchClick.bind(this);
  }

  componentWillMount() {
    this.setState({ showRefreshButton: false })
  }

  handleCenterChanged() {
    this.setState({ showRefreshButton: true })
    if (this.props.onCenterChanged) {
      this.props.onCenterChanged()
    }
  }

  handleResearchClick() {
    this.setState({ showRefreshButton: false })
    this.props.onResearchClick();
  }

  render() {
    return (
      <div className="nm-layout">
        <MapComponent
          {...this.props}
          renderInfoBoxContent={this.props.renderInfoBoxContent}
          onBoundsChanged={this.props.onCenterChanged}
          onCenterChanged={this.handleCenterChanged}
          onResearchClick={this.handleResearchClick}
          showRefreshButton={this.state.showRefreshButton}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `calc(100vh - 48px)`, position: 'relative' }} />}
          mapElement={<div style={{ height: `100%` }} />} />
      </div>
    );
  }
}

export default Map;
