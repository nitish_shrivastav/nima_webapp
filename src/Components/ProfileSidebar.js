import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

let ProfileSidbar = (props) => {
  return (
    <div className="nm-block nm-block__sidebar">
      <h4 className="nm-layout nm-text-center">          
        My Dashboard
      </h4>
      <p className="nm-layout nm-text-center" style={{fontStyle: 'italic'}}>
        private to you
      </p>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout nm-text-center">
        10,000 points
      </p>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout nm-text-center nm-link" style={{fontSize: 14}}>
        Tips to level up
      </p>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout nm-text-center" style={{fontStyle: 'italic'}}>
        Insights from last 7 days
      </p>
      <p className="nm-layout nm-text-center">
        100
      </p>
      <p className="nm-layout nm-text-center">
        Followers
      </p>
      <div className="nm-spacer-12"></div>
      <div className="nm-layout nm-layout__bar"></div>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout nm-text-center">
        100
      </p>
      <p className="nm-layout nm-text-center">
        Followers
      </p>
      <div className="nm-spacer-12"></div>
      <div className="nm-layout nm-layout__bar"></div>
      <div className="nm-spacer-12"></div>
      <p className="nm-layout nm-text-center">
        81
      </p>
      <p className="nm-layout nm-text-center">
        Profile views
      </p>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <h4 className="nm-layout nm-text-center">
        My Order History
      </h4>
      <div className="nm-spacer-12"></div>
      <div className="nm-layout">
        <img className="nm-image nm-image__icon--med nm-pull--left" src="https://cdn.shopify.com/s/files/1/1344/6725/products/image_sensor2x_preview_small.jpeg?v=1508961411"/>
        <div className="nm-layout" style={{ width: 'calc(100% - 40px)', paddingLeft: 5 }}>
          <h5 className="nm-layout">
            Shop Item
          </h5>
          <p className="nm-layout">
            Ordered 5-5-5
          </p>
          <div className="nm-spacer-12"></div>
        </div>
      </div>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <Link to="/" className="nm-layout nm-text-center nm-link">
        Manage orders
      </Link>
    </div>
  );
}

export default ProfileSidbar;

