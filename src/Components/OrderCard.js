import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import StorefrontInteractor from '../Util/StorefrontInteractor';
import TimeHelper from '../Util/TimeHelper';
import { GREY, RED } from '../Constants/colors';

const OrderCard = (props) => {

  const getImageUrl = (variantId) => {
    for (let product of props.products) {
      for (let variant of product.node.variants.edges) {
        let decodedId = StorefrontInteractor.decodeId(variant.node.id, "ProductVariant");
        if (decodedId === variantId.toString()) {
          return product.node.images.edges[0].node.originalSrc;
        }
      }
    }
  }

  return (
    <div className="nm-layout">
      <div className="nm-card nm-card__block" style={{ border: `1px solid ${GREY}`}}>
        {
          props.order.tags.indexOf('cancelled') > -1 &&
          <h3 className="nm-layout" style={{color: RED, margin: 0}}>
            Cancelled and refunded
          </h3>
        }
        <p className="nm-layout">
          Order #: <span>{props.order.id}</span>
        </p>
        <p className="nm-layout">
          Placed on: <span>{moment(props.order.created_at).format("MM/DD/YYYY")}</span>
        </p>
        <div className="nm-spacer-12"/>
        <div className="nm-order__card-items">
          {
            props.order.line_items.map((line_item, lineIndex) => {
              return (
                <div className="nm-layout" key={`order-card-${lineIndex}`}>
                  <img src={getImageUrl(line_item.variant_id)} className="nm-image nm-image__contain" style={{width: 160, float: 'left'}}/>
                  <div className="nm-layout nm-order__card-info">
                    <div className="nm-spacer-48"/>
                    <h3 className="nm-layout" style={{margin: 0}}>
                      { line_item.title }
                    </h3>
                    <p className="nm-layout">
                      Quantity: <span className="nm-emph">{ line_item.quantity }</span>
                    </p>
                    <div className="nm-spacer-48"/>
                  </div>
                </div>
              )
            })
          }
        </div>
        <div className="nm-order__card-charges">
          <h3 className="nm-layout">
            Charges
          </h3>
          <p className="nm-layout">
            Subtotal: ${ props.order.subtotal_price }
          </p>
          <p className="nm-layout">
            Discount: ${ props.order.total_discounts }
          </p>
          <p className="nm-layout">
            Shipping: ${ props.order.total_shipping_price_set.shop_money.amount }
          </p>
          <p className="nm-layout">
            Tax: ${ props.order.total_tax }
          </p>
          <h3 className="nm-layout">
            Total: ${ props.order.total_price }
          </h3>
        </div>
        <div className="nm-spacer-12"/>
        <div className="nm-layout">
          {
            props.order.fulfillments.map((fulfillment) => {
              if (fulfillment.tracking_number) {
                return (
                  <p className="nm-layout">
                    Tracking number:
                    <a href={fulfillment.tracking_url} className="nm-link" style={{left: 8, position: 'relative'}} target="_blank">
                      {fulfillment.tracking_number}
                    </a>
                  </p>
                )
              }
            })
          }
        </div>
        <div className="nm-clear"></div>
      </div>
      <div className="nm-spacer-48"/>
    </div>
  );
}

OrderCard.propTypes = {
}

export default OrderCard;
