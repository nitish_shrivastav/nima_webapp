import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import StarRating from '../SVG/StarRating';
import { ALLERGEN_COLOR, RED, DARK_GREY, TEXT } from '../Constants/colors';
import FormatHelper from '../Util/FormatHelper';
import TimeHelper from '../Util/TimeHelper';
import TEST_RESULT_ASSETS from '../Constants/testResults';

const DetailReviewTicket = (props) => {

  const getAllergenText = () => {
    if (_.get(props.review, 'readings[0].allergen')) {
      let allergen = FormatHelper.uppercaseFirstLetter(props.review.readings[0].allergen);
      let label = FormatHelper.replaceAll(props.review.readings[0].labeling, "_", " ");
      label = FormatHelper.uppercaseFirstLetter(label);
      return `${label} ${allergen}-free.`;
    }
    else {
      return "No test";
    }
  }

  const getResultImageUrl = () => {
    const { test_result, allergen } = props.review.readings[0];
    if (typeof(test_result) === "number" && allergen) {
      return TEST_RESULT_ASSETS[allergen][test_result.toString()];
    }
    else {
      return TEST_RESULT_ASSETS["gluten"]["-1"];
    }
  }


  const { review, index, isChain, allergen } = props;
  return (
    <div
      key={`${review.id}-${index}`}
      className={`nm-ticket nm-ticket__half ${isChain ? "nm-ticket__chain" : ""}`}
      style={{marginBottom: 12}}>
      <img
        src={getResultImageUrl(review)}
        className="nm-image nm-layout nm-image__contain"
        style={{width: 40, height: 40, marginLeft: 12, marginRight: 12}}/>
      <div className="nm-layout" style={{ width: 'calc(100% - 64px)', float: 'right'}}>
        <p className="nm-layout">
          <span style={{fontWeight: 500}}>{review.reviewer_name} </span>
          {TimeHelper.dayFormat(review.created_at)}
        </p>
        <h4 className="nm-layout nm-bold">
          {review.readings[0].item_name}
        </h4>
        <p className="nm-layout">
          { getAllergenText() }
        </p>
        <div className="nm-spacer-12"/>
        <div className="nm-layout">
          <StarRating
            idPrefix={`detail-${props.review.id}`}
            startColor={ALLERGEN_COLOR[allergen]}
            stroke={ALLERGEN_COLOR[allergen]}
            percent={100 * review.experience_score / 5}
            style={{width: 20, height: 20, marginRight: 5}}/>
        </div>
      </div>
      {
        review.notes &&
        <div className="nm-layout">
          <p className="nm-layout" style={{fontSize: 14, color: 'rgba(0,0,0,0.38)'}}>
            Comments
          </p>
          <p className="nm-layout">
            {review.notes}
          </p>
        </div>
      }
      {
        !review.notes &&
        <p className="nm-layout" style={{fontSize: 14, color: 'rgba(0,0,0,0.38)'}}>
          No Comments
        </p>
      }
      <div className="nm-clear"/>
    </div>
  );
}

DetailReviewTicket.propTypes = {
  review: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  allergen: PropTypes.string.isRequired,
  isChain: PropTypes.bool
}

export default DetailReviewTicket;
