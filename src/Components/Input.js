import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputError from './InputError';
import Tooltip from './Tooltip';

class Input extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleChange(e) {
    this.props.onChange(e.target.value, this.props.field.name);
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
  }

  handleKeyPress(e) {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(e)
    }
  }

  render() {
    let inputClass = (this.props.showError && this.props.field.error) ? "nm-form__input-error" : "";
    return (
      <div className={`nm-layout nm-form__input-wrap ${this.props.className}`}>
        <label className="nm-form__label">
          {this.props.field.label}
          {
            this.props.field.info &&
            <Tooltip text={this.props.field.info} />
          }
          {
            this.props.isLoading &&
            <div className="nm-loader__field">
              <div className={`nm-loader nm-loader__blue`} style={{fontSize: 10}}></div>
            </div>
          }
        </label>
        <input
          type={this.props.field.type}
          name={this.props.field.name}
          onKeyDown={this.handleKeyPress}
          onBlur={this.handleBlur}
          className={`nm-form__input ${inputClass}`}
          onChange={this.handleChange}
          autoComplete={this.props.autocomplete}
          value={this.props.field.value}/>
        { this.props.children }
        <InputError
          show={this.props.showError}
          error={this.props.field.error}/>
      </div>
    );
  }
}

Input.defaultProps = {
  field: {
    type: "text",
    autocomplete: ""
  },
  className: ""
}

Input.propTypes = {
  field: PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    error: PropTypes.string,
    info: PropTypes.string
  }),
  showError: PropTypes.bool,
  autocomplete: PropTypes.string,
  children: PropTypes.element
}

export default Input;
