import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

let ProfileBox = (props) => {
  return (
    <div className="nm-block">
      <div className="nm-profile">
        <div className="nm-profile__image-wrap">
          <img
            src="https://mindovermunch.com/wp-content/uploads/2016/12/placeholder-1000x400.png"
            style={{borderRadius: '50%', height: 200}}
            className="nm-image nm-layout nm-image__cover" />
          <div className="nm-spacer-12"></div>
        </div>
        <div className="nm-profile__content">
          <h3 className="nm-layout">
            {props.user.first_name} {props.user.last_name}
          </h3>
          <div className="nm-profile__facts-wrap">
            <p className="nm-layout">
              Gluten Allergy
            </p>
            <p className="nm-layout">
              San Francisco, CA
            </p>
            <p className="nm-layout">
              Joined Jun 2018
            </p>
            <p className="nm-layout">
              friends: 171
            </p>
          </div>
          <div className="nm-profile__content">
            <h4 className="nm-layout">
              Bio
            </h4>
            <p className="nm-layout">
              Gluten free traveler based in San Francisco, finding the best gluten free restaurants around the world
            </p>
            <p className="nm-layout">
              Gluten Free Influencer
            </p>
          </div>
        </div>
        <div className="nm-layout">
          <Button text="connect to facebook" style={{background: "#3B5998", fontSize: 14, padding: 6, maxWidth: 200}}/>
          <span className="" style={{marginLeft: 18}}>
            10 <span>Followers | </span>
            100 <span>Following</span>
          </span>
          <Button text="Edit Profile" style={{ float: 'right', maxWidth: 150}}/>
        </div>
      </div>
    </div>
  );
}

ProfileBox.propTypes = {
  user: PropTypes.object.isRequired
}

export default ProfileBox;

