
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Button from './Button';
import CheckoutFooter from './CheckoutFooter';
import Check from '../SVG/Check';

const glutenItems = [
  {
    text: "Gluten Kit",
    handle: "nima-starter-kit"
  },
  {
    text: "Premium",
    handle: "premium-membership"
  },
  {
    text: "Capsules",
    handle: "subscriptions"
  }
]

const peanutItems = [
  {
    text: "Peanut Kit",
    handle: "peanut-starter-kit"
  },
  {
    text: "Premium",
    handle: "peanut-premium-membership"
  },
  {
    text: "Capsules",
    handle: "peanut-test-capsules"
  }
]


const Steps = (props) => {

  const isInCart = (checkout, handle) => {
    let variantProduct = {
      node: {
        variant: {
          product: {
            handle: handle
          }
        }
      }
    }
    return checkout &&
           checkout.lineItems &&
           checkout.lineItems.edges &&
           checkout.lineItems.edges &&
           _.findIndex(checkout.lineItems.edges, variantProduct) > -1;
  }

  const { allergen, handle, checkout } = props;
  const stepItems = props.allergen === "gluten" ? glutenItems : peanutItems;
  return (
    <div className="nm-layout">
      <div className="nm-steps nm-steps__large">
        {
          stepItems.map((step, index) => {
            const itemInCart = isInCart(checkout, step.handle);
            const isActive = handle === step.handle || itemInCart;
            return (
              <Link key={`steps-${index}`} className="nm-steps__item" to={`/shop/products/${step.handle}`}>
                {
                  itemInCart &&
                  <div className="nm-steps__check">
                    <Check/>
                  </div>
                }
                {
                  !itemInCart &&
                  <p className={`nm-steps__index ${isActive ? "nm-steps__index--active" : ""}`}>
                    {index + 1}
                  </p>
                }
                <p className={`nm-steps__label ${isActive ? "nm-steps__label--active" : ""}`}>
                  {step.text}
                </p>
              </Link>
            )
          })
        }
        <Link id="desktop-only-cart-step" to="/shop/cart" className="nm-steps__item nm-steps__desktop-only">
          <p className="nm-steps__index">4</p>
          <p className="nm-steps__label">Cart</p>
        </Link>
      </div>
    </div>
  );
}

Steps.propTypes = {
  allergen: PropTypes.oneOf(['peanut', 'gluten']),
  checkout: PropTypes.object,
  handle: PropTypes.string.isRequired
}

export default Steps;
