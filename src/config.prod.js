const config = {
  STRIPE_PUBLIC_KEY: "pk_live_AEZ0N5dfxaG2mKogWwWzQ8R7",
  GOOGLE_PLACES_KEY: "AIzaSyCGuDdauBDBYqVabF11KOrAZVAAY9OGoSU",
  PLACES_DETAIL_URL: "https://maps.googleapis.com/maps/api/place/details/json",
  STOREFRONT_TOKEN: "887fe127f9a9930ada2a55f3792f78bb",
  API_URL: "https://api-prod.nimasensor.com/store",
  APP_API_URL: "https://api-prod.nimasensor.com/v2",
  APP_API_HOST: "api-prod.nimasensor.com",
  SHOP_URL: "https://nima-dev.myshopify.com",
  GLUTEN_PREMIUM: {
    id: "517087428622",
    discounts: {
      "32154760974": 59
    }
  },
  PEANUT_PREMIUM: {
    id: "7511685070911",
    discounts: {
      "7511902191679": 59
    }
  },
  SUBSCRIPTION_VARIANTS: {
    "517087428622": {
      name: "Gluten Premium Membership",
      canSkip: false
    },
    "7511685070911": {
      name: "Peanut Premium Membership",
      canSkip: false
    },
    "26993622531": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "32154760846": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "32154760910": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "7512042143807": {
      name: "Peanut Capsule Pack",
      canSkip: true
    },
    "7512048599103": {
      name: "Peanut Capsule Pack",
      canSkip: true
    },
    "7512057217087": {
      name: "Peanut Capsule Pack",
      canSkip: true
    }
  },
  BRAINTREE: {
    ENV: "production",
    CLIENT_TOKEN: "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiI0Y2FjNjJiMTBkY2UyMTcwZmUwMmU3NTE4NjFlZjgwZGNkNjQ2MjY2NTk1ZWRlMWJlYWI0ZGY3ZDcyOGNmNGU0fGNyZWF0ZWRfYXQ9MjAxOC0wOC0yMFQxOToxODoxOC4yNTY4MDc4NzErMDAwMFx1MDAyNm1lcmNoYW50X2lkPXJjNmh3a3c2M244MjUydHJcdTAwMjZwdWJsaWNfa2V5PXFqemh3ajR2bjR6M2hieXIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL3JjNmh3a3c2M244MjUydHIvY2xpZW50X2FwaS92MS9jb25maWd1cmF0aW9uIiwiY2hhbGxlbmdlcyI6W10sImVudmlyb25tZW50IjoicHJvZHVjdGlvbiIsImNsaWVudEFwaVVybCI6Imh0dHBzOi8vYXBpLmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvcmM2aHdrdzYzbjgyNTJ0ci9jbGllbnRfYXBpIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhdXRoVXJsIjoiaHR0cHM6Ly9hdXRoLnZlbm1vLmNvbSIsImFuYWx5dGljcyI6eyJ1cmwiOiJodHRwczovL2NsaWVudC1hbmFseXRpY3MuYnJhaW50cmVlZ2F0ZXdheS5jb20vcmM2aHdrdzYzbjgyNTJ0ciJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjpmYWxzZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiTmltYSBMYWJzLCBJbmMuIiwiY2xpZW50SWQiOiJBY1ZocUNJekQ4Y0lhRFZFd2xUZGw1VFJlOURxanpGN1dBLTM2bktBWmw5RUloUXptM1RzRENkQ0Z2WlE3MWh1RFlkdXJmUXpZV3hiMHZVSSIsInByaXZhY3lVcmwiOiJodHRwczovL25pbWFzZW5zb3IuY29tL3ByaXZhY3ktcG9saWN5LyIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwczovL25pbWFzZW5zb3IuY29tL3ByaXZhY3ktcG9saWN5LyIsImJhc2VVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFzc2V0c1VybCI6Imh0dHBzOi8vY2hlY2tvdXQucGF5cGFsLmNvbSIsImRpcmVjdEJhc2VVcmwiOm51bGwsImFsbG93SHR0cCI6ZmFsc2UsImVudmlyb25tZW50Tm9OZXR3b3JrIjpmYWxzZSwiZW52aXJvbm1lbnQiOiJsaXZlIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoiQVJLcllSRGgzQUdYRHpXN3NPXzNiU2txLVUxQzdIR191V05DLXo1N0xqWVNETlVPU2FPdElhOXE2VnBXIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6Ik5pbWFMYWJzSW5jX2luc3RhbnQiLCJjdXJyZW5jeUlzb0NvZGUiOiJVU0QifSwibWVyY2hhbnRJZCI6InJjNmh3a3c2M244MjUydHIiLCJ2ZW5tbyI6Im9mZiJ9"
  },
  TALKABLE_ID: 'nima-labs-inc',
  FACEBOOK: {
    APP_ID: "1741188669458685",
    VERSION: "v2.7"
  }
}

export default config;
