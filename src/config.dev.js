const config = {
  STRIPE_PUBLIC_KEY: "pk_test_DLPFu81nA6J0gvHfim6Q7Hrs",
  GOOGLE_PLACES_KEY: "AIzaSyCGuDdauBDBYqVabF11KOrAZVAAY9OGoSU",
  PLACES_DETAIL_URL: "https://maps.googleapis.com/maps/api/place/details/json",
  STOREFRONT_TOKEN: "57161f0b4f32bab734ad1300fa656377",
  API_URL: "https://4x486f0t48.execute-api.us-west-2.amazonaws.com/store",
  APP_API_URL: "https://4x486f0t48.execute-api.us-west-2.amazonaws.com/dev",
  APP_API_HOST: "api-dev.nimasensor.com",
  SHOP_URL: "https://nima-dev3.myshopify.com",
  GLUTEN_PREMIUM: {
    id: "357681397766",
    discounts: {
      "31764509958": 59
    }
  },
  PEANUT_PREMIUM: {
    id: "8112774512752",
    discounts: {
      "8112400892016": 59
    }
  },
  SUBSCRIPTION_VARIANTS: {
    "357681397766": {
      name: "Gluten Premium Membership",
      canSkip: false
    },
    "8112774512752": {
      name: "Peanut Premium Membership",
      canSkip: false
    },
    "31763432838": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "31763539974": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "31763540038": {
      name: "Gluten Capsule Pack",
      canSkip: true
    },
    "8112399843440": {
      name: "Peanut Capsule Pack",
      canSkip: true
    },
    "8112399810672": {
      name: "Peanut Capsule Pack",
      canSkip: true
    },
    "8112400007280": {
      name: "Peanut Capsule Pack",
      canSkip: true
    }
  },
  BRAINTREE: {
    ENV: "sandbox",
    CLIENT_TOKEN: "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIzMjBmMjQ1Y2UwNTIxMTc0MjMzMjJlNzgxMWRkZTE5MTA1MzZkNWE3OTVmMDliYmU4OWY3NGZhYjA4ZmFiM2JlfGNyZWF0ZWRfYXQ9MjAxOC0wNy0zMVQyMDoyNjoxNS44MDYzNDA2MzUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWtuYmJmM3kzaGhqNXA3dmtcdTAwMjZwdWJsaWNfa2V5PXh6eHY3NmtubjgyejUzc3MiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMva25iYmYzeTNoaGo1cDd2ay9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2tuYmJmM3kzaGhqNXA3dmsvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vb3JpZ2luLWFuYWx5dGljcy1zYW5kLnNhbmRib3guYnJhaW50cmVlLWFwaS5jb20va25iYmYzeTNoaGo1cDd2ayJ9LCJ0aHJlZURTZWN1cmVFbmFibGVkIjp0cnVlLCJwYXlwYWxFbmFibGVkIjp0cnVlLCJwYXlwYWwiOnsiZGlzcGxheU5hbWUiOiJOaW1hIExhYnMgTExDLiIsImNsaWVudElkIjoiQVluMWFrdHNMNVBLVl84SzBZM2t2OEZkQkNETzQ4Sll1a1pjRThoc3pwUWU3c0V1QVQ4LUNhTHlERGFzX3BwQjNtYkJ1TGd5MkdCS09JLWIiLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjpmYWxzZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJuaW1hbGFic2xsYyIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoia25iYmYzeTNoaGo1cDd2ayIsInZlbm1vIjoib2ZmIn0="
  },
  TALKABLE_ID: 'nima-staging',
  FACEBOOK: {
    APP_ID: "1859573514286866",
    VERSION: "v2.7"
  }
}

export default config;