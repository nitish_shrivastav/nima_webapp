class AddressFormatter {

  static formatFormAddressForApi(values, shippingName) {
    return {
      ...values.shipping_address,
      first_name: values.first_name,
      last_name: values.last_name,
      country_code: values[shippingName].country,
      phone: values.phone
    }
  }

}

export default AddressFormatter;
