import request from './Request';
const URL = "https://nimasensor.us9.list-manage.com/subscribe/post-json?u=2f723fc3c7487ca778776f790&amp;id="

class MailingListInteractor {
  static create(listId, body) {
    return request.getJSONP(`${URL}${listId}`, body)
  }
}

export default MailingListInteractor;
