import braintree from 'braintree-web';
import _ from 'lodash';
import config from '../config';
import SegmentConstants from '../Constants/segment';
import { PAYPAL } from '../Constants/payment';

class BraintreeInteractor {

  static getPayPalNonce(window, buttonClass, cb) {
    return braintree.client.create({
      authorization: config.BRAINTREE.CLIENT_TOKEN
    })
    .then((clientInstance) => {
      return braintree.paypalCheckout.create({client: clientInstance});
    })
    .then((paypalCheckoutInstance) => {
      return window.paypal.Button.render({
        env: config.BRAINTREE.ENV,
        style: {
          size: "responsive",
          tagline: false,
          label: "pay"
        },
        payment: function () {
          if (window.analytics && window.analytics.track) {
            let cart_id = _.get(window, 'location.pathname');
            if (cart_id) { cart_id = cart_id.split('/')[2] || undefined; }
            window.analytics.track(SegmentConstants.PAYMENT_INFO_ENTERED, { payment_method: PAYPAL, cart_id })
          }
          return paypalCheckoutInstance.createPayment({
            flow: 'vault',
            billingAgreementDescription: '',
            enableShippingAddress: true
          });
        },
        onAuthorize: function (data, actions) {
          return paypalCheckoutInstance.tokenizePayment(data)
            .then(function (payload) {
              if (cb) cb(null, payload);
            });
        },
        onCancel: function (data) {
          let err = JSON.stringify(data, 0, 2);
          console.log('checkout.js payment cancelled', JSON.stringify(data, 0, 2));
          if (cb) cb(err, null);
        },
        onError: function (err) {
          console.error('checkout.js error', err);
          if (cb) cb(err, null);
        }
      }, buttonClass);
    })
    .catch((err) => {
      if (cb) cb(err, null);
      else throw err;
    })
  }

}

export default BraintreeInteractor;
