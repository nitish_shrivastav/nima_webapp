import moment from 'moment';
import BrowserHelper from './BrowserHelper';


class AuthStorage {
  static setUser(user) {
    localStorage.setItem('authUser', JSON.stringify(user));
  }

  static canRefresh() {
    let user = JSON.parse(localStorage.getItem('authUser'));
    return (user && moment(user.tokens.Refresh.Expiration) > moment());
  }

  static needsRefreshing() {
    let user = JSON.parse(localStorage.getItem('authUser'));
    if (user && moment(user.tokens.Expiration) < moment().add(40, 'minutes')) {
      return user;
    }
    else return false;
  }

  static getUser() {
    let user = localStorage.getItem('authUser');
    if (user) return JSON.parse(user);
    else return null;
  }

  static getRefreshBody() {
    let user = localStorage.getItem('authUser');
    if (user) {
      user = JSON.parse(user);
      if (user && user.tokens && moment(user.tokens.Refresh.Expiration) > moment()) {
        return {
          user_id: user.id,
          email: user.email,
          refresh_token: user.tokens.Refresh.Token,
          device_id: BrowserHelper.getName()
        }
      }
    }
    return null;

  }

  static clearUser() {
    localStorage.removeItem('authUser');
  }
}

export default AuthStorage;
