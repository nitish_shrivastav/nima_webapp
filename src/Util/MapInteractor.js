import config from '../config';
import request from './Request';

class MapInteractor {
  constructor() {
    this.autocompleteService = new window.google.maps.places.AutocompleteService();
    this.geocoder = new window.google.maps.Geocoder;
  }

  static _parseReverseGeoCode(placeData) {
    let parsedPlace = {};
    if (placeData && placeData.results && placeData.results.length > 0) {
      for (let component of placeData.results[0].address_components) {
        if (component.types.indexOf("administrative_area_level_1") !== -1) {
          parsedPlace.province = component.long_name
          parsedPlace.province_short = component.short_name
        }
        if (component.types.indexOf("locality") !== -1 || component.types.indexOf("postal_town") !== -1) {
          parsedPlace.city = component.long_name
        }
        if (component.types.indexOf("country") !== -1) {
          parsedPlace.country = component.long_name
          parsedPlace.country_short = component.short_name
        }
        if (component.types.indexOf("postal_code") !== -1) {
          parsedPlace.postal_code = component.long_name
        }
      }
    }
    return parsedPlace;
  }

  static calculateRadius(centerLat, centerLng, northEastLat, northEastLng) {
    const earthRadius = 3963.0;
    const kDecimalDegreeToRadians = 1 / 57.2958;
    const lat1 = centerLat * kDecimalDegreeToRadians;
    const lng1 = centerLng * kDecimalDegreeToRadians;
    const lat2 = northEastLat * kDecimalDegreeToRadians;
    const lng2 = northEastLng * kDecimalDegreeToRadians;

   return parseInt(earthRadius * Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1)) * 1000);
  }

  static getCityNameByLatLng(lat, lng) {
    return request.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${config.GOOGLE_PLACES_KEY}`)
    .then((placeData) => {
      return this._parseReverseGeoCode(placeData);
    })
    .catch((err) => {
      throw err;
    });
  }

  getLatLngByPlaceId(placeId) {
    return new Promise((resolve, reject) => {
      this.geocoder.geocode({placeId: placeId}, (results, status) => {
        if (status === "OK") {
          resolve(results);
        }
        else {
          reject(status);
        }
      })
    })
  }

  searchCities(keyword) {
    return new Promise((resolve, reject) => {
      this.autocompleteService.getPlacePredictions({
        input: keyword,
        types: ["(cities)"]
      }, (predictions, status) => {
        if (status === "OK") {
          resolve(predictions);
        }
        else {
          reject(status);
        }
      })
    })
  }
}

export default MapInteractor;
