import moment from 'moment';

class StorefrontStorage {
  static setCheckoutId(id) {
    localStorage.setItem('checkoutId', id);
  }

  static getCheckoutId() {
    return localStorage.getItem('checkoutId');
  }

  static clearCheckoutId() {
    return localStorage.removeItem('checkoutId');
  }

  static setUser(user) {
    localStorage.setItem('storefrontUser', JSON.stringify(user));
  }

  static getUser() {
    let user = localStorage.getItem('storefrontUser');
    if (user) {
      user = JSON.parse(user);
      if (moment(user.customerAccessToken.expiresAt).isBefore(moment())) {
        user = undefined;
      }
    }
    return user;
  }

  static clearUser() {
    localStorage.removeItem('storefrontUser')
  }
}

export default StorefrontStorage;
