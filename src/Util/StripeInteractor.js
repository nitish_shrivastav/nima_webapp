import config from '../config';

class StripeInteractor {
  constructor(stripe) {
    this.stripe = stripe;
    this.stripe.setPublishableKey(config.STRIPE_PUBLIC_KEY);
  }

  createSource(number, expMonth, expYear, cvc) {
    let source = {
      type: 'card',
      usage: 'reusable',
      card: {
        "number": number,
        "exp_month": expMonth,
        "exp_year": expYear,
        "cvc": cvc
      }
    }
    return new Promise((resolve, reject) => {
      this.stripe.source.create(source, (status, res) => {
        if (res.error) {
          reject(res.error);
        }
        else {
          resolve(res);
        }
      })
    })
  }
}

export default StripeInteractor;
