
export const notEmptyValidation = (value) => {
  if (!value || value === "") {
    return "Invalid Value";
  }
  else {
    return undefined;
  }
}

export const cvcValidation = (value) => {
  if (isNaN(value) || value.length < 3 || value.length > 4) {
    return 'Invalid security code, must be 3-4 digits';
  }
  else {
    return undefined;
  }
}

export const expirationMonthValidation = (month) => {
  if (month.length > 2) {
    return 'Invalid expiration month, cannot be more than 2 digits';
  }
  else if (isNaN(month)) {
    return 'Invalid expiration month, must be a number';
  }
  else {
    return undefined;
  }
}

export const expirationYearValidation = (year) => {
  if (year.length < 2 || year.length > 4) {
    return 'Invalid expiration year, must be 3 or 4 digits';
  }
  else if (isNaN(year)) {
    return 'Invalid expiration year, must be a number';
  }
  else {
    return undefined;
  }
}

export const creditCardValidation = (cardNumber) => {
  if (cardNumber.length < 15) {
    return 'Invalid card number';
  }
  else { return undefined;
  }
}

export const phoneValidation = (value) => {
  if (!value || !/^(0|[1-9][0-9]{9})$/i.test(value)) {
    return 'Invalid phone number, must be 10 digits';
  }
  else {
    return undefined;
  }
}

export const emailValidation = (value) => {
  if (!value || !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Invalid email address';
  }
  else {
    return undefined;
  }
}

export const atLeastOneNumberValidation = (value) => {
  if((!new RegExp("[0-9]").test(value))) {
    return "Must have at least one number";
  }
  else {
    return undefined;
  }
}

export const atLeastOneLowerValidation = (value) => {
  if  (!(new RegExp("[a-z]").test(value))) {
    return "Must have a lowercase letter";
  }
  else {
    return undefined;
  }
}

export const atLeastOneUpperValidation = (value) => {
  if  (!(new RegExp("[A-Z]").test(value))) {
    return "Must have a uppercase letter";
  }
  else {
    return undefined;
  }
}

export const atLeastOneSpecialValidation = (value) => {
  if  (!(new RegExp("[-!@#$%^&*()_+|~=`{}\\[\\]:\";'<>?,.\/]").test(value))) {
    return "Must have a special charactor (!@#$%^&*)";
  }
  else {
    return undefined;
  }
}

export const char8Validation = (value) => {
  if  (!(new RegExp("^.{8,}$").test(value))) {
    return "Must have at least 8 charactors";
  }
  else {
    return undefined;
  }
}
