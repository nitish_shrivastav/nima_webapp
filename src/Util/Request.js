import queryString from 'query-string';
import jsonp from 'jsonp';
import crypto from 'crypto';
import moment from 'moment';
import AuthStorage from './AuthStorage';
import config from '../config';
import aws4 from 'aws4';

const handleErrors = (res) => {
  if (res.ok) {
    return res;
  }
  return res.json()
  .then(err => {
    if (err.hasOwnProperty('message')) {
      throw new Error(err.message);
    } else {
      throw new Error('An unknown error occurred');
    }
  })
}

const _getAmazonRequestOptions = (host, method, keys, uri, body)  => {
  let REGION = "us-west-2";
  let SERVICE = "execute-api";
  let options = {
    host: host,
    service: SERVICE,
    region: REGION,
    path: uri, 
    method: method,
    headers: {
      "Content-Type": "application/json"
    }
  }

  if (body) {
    options.body = JSON.stringify(body);
  }

  let securityKeys = {
    secretAccessKey: keys.tokens.SecretAccessKey,
    accessKeyId: keys.tokens.AccessKeyId,
    sessionToken: keys.tokens.SessionToken
  }
  return aws4.sign(options, securityKeys);
}

class Request {
  static signedGet(url, params={}) {
    let apiUrl = `${url}?${queryString.stringify(params)}`;
    let tokens = AuthStorage.getUser();
    if (!tokens) {
      throw 'Error, token must exist';
    }
    else {
      let route = url.split(config.APP_API_HOST)[1];
      let fetchOptions = _getAmazonRequestOptions(config.APP_API_HOST, "GET", tokens, route);
      return fetch(apiUrl, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
    }
  }

  static signedPost(url, body) {
    let tokens = AuthStorage.getUser();
    if (!tokens) {
      throw 'Error, token must exist';
    }
    else {
      let route = url.split(config.APP_API_HOST)[1];
      let fetchOptions = _getAmazonRequestOptions(config.APP_API_HOST, "POST", tokens, route, body);
      return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
    }
  }

  static signedPut(url, body) {
    let tokens = AuthStorage.getUser();
    if (!tokens) {
      throw 'Error, token must exist';
    } else {
      let route = url.split(config.APP_API_HOST)[1];
      let fetchOptions = _getAmazonRequestOptions(config.APP_API_HOST, "PUT", tokens, route, body);
      return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
    }
  }

  static signedDelete(url, body) {
    let tokens = AuthStorage.getUser();
    if (!tokens) {
      throw 'Error, token must exist';
    } else {
      let route = url.split(config.APP_API_HOST)[1];
      let fetchOptions = _getAmazonRequestOptions(config.APP_API_HOST, "DELETE", tokens, route, body);
      return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
    }
  }

  static get(url, params) {
    if (params) {
      url = `${url}?${queryString.stringify(params)}`;
    }
    const fetchOptions = {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    }
    return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
  }

  static getJSONP(url, params) {
    if (params) {
      url = `${url}&${queryString.stringify(params)}`;
    }
    return new Promise((resolve, reject) => {
      jsonp(url, {param: "c"}, (err, data) => {
        if (err) reject(err);
        else if (data && data.result === "error") reject(data);
        else resolve(data);
      })
    });
  }


  static post(url, body) {
    const fetchOptions = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
  }

  static put(url, body) {
    const fetchOptions = {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
  }

  static delete(url, body) {
    const fetchOptions = {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    return fetch(url, fetchOptions)
      .then(handleErrors)
      .then(res=>res.json());
  }
}

export default Request;
