import moment from 'moment';

class TimeHelper{
  static formatMomentWithTimezone(timestamp) {
    let timezoneOffset = parseInt(moment(timestamp).format("ZZ").substring(0, 3))
    return moment(timestamp).add(timezoneOffset, 'h').format("h:mmA MM/DD/YY (Z)")
  }

  static formatMomentWithoutTimezone(timestamp) {
    return moment(timestamp).format("h:mmA MM/DD/YY")
  }

  static dayFormat(timestamp) {
    return moment(timestamp).format("MMM D, YYYY")
  }
}

export default TimeHelper;
