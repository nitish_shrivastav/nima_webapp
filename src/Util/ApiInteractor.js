import request from './Request';
import config from '../config';
import _ from 'lodash';

const { API_URL, APP_API_URL } = config;

function handleError(err) {
  if (err && err.message) {
    return err.message;
  }
  else {
    return err;
  }
}

class ApiInteractor {
  static createOrder(body) {
    return request.post(`${API_URL}/guest-orders`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static createAuthenticatedOrder(body) {
    return request.signedPost(`${API_URL}/orders/`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static listOrders() {
    return request.signedGet(`${API_URL}/orders/`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static validateDiscount(body) {
    return request.post(`${API_URL}/discounts/validate`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static login(body) {
    return request.post(`${API_URL}/users/login`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static ssoLogin(body) {
    return request.post(`${APP_API_URL}/users/login`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static ssoRegister(body) {
    return request.post(`${APP_API_URL}/users/register`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static ssoVerify(body) {
    return request.post(`${APP_API_URL}/users/verify`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static ssoSendResetPassword(body) {
    return request.post(`${APP_API_URL}/users/forgot`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static ssoResendVerifyEmail(body) {
    return request.post(`${APP_API_URL}/users/resend-verify`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static facebookLogin(body) {
    return request.post(`${APP_API_URL}/users/fblogin`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static refreshToken(body) {
    return request.post(`${APP_API_URL}/users/refresh`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listSubsciptions() {
    return request.signedGet(`${API_URL}/subscriptions`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static updateSubsciption(order, body) {
    return request.signedPut(`${API_URL}/subscriptions/${order}`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static updateSubsciptionDiscount(order, body) {
    return request.signedPut(`${API_URL}/subscriptions/${order}/discount`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static deleteSubsciptionDiscount(orderId) {
    return request.signedDelete(`${API_URL}/subscriptions/${orderId}/discount`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static deleteSubscription(orderId) {
    return request.signedDelete(`${API_URL}/subscriptions/${orderId}`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listReviews(params) {
    return request.signedGet(`${APP_API_URL}/reviews`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrieveReview(id) {
    return request.signedGet(`${APP_API_URL}/users/reviews`, {id})
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listReadings(params) {
    return request.signedGet(`${APP_API_URL}/readings`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrieveReading(id) {
    return request.signedGet(`${APP_API_URL}/readings`, {id})
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listPromotions() {
    return request.get(`${API_URL}/promotions`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static listPlaces(params) {
    return request.signedGet(`${APP_API_URL}/places/nearbysearch`, params)
    .then((res) => {
      return _.get(res, 'data.addresses') ? res.data.addresses : [];
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static searchPublicPlaces(params) {
    return request.get(`${APP_API_URL}/public/places/mapsearch`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static searchPlaces(params) {
    return request.signedGet(`${APP_API_URL}/places/mapsearch`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrievePlace(id) {
    return request.signedGet(`${APP_API_URL}/places/detail`, id)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static publicRetrievePlace(id, params) {
    return request.get(`${APP_API_URL}/public/places/${id}`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrieveChain(id) {
    return request.signedGet(`${APP_API_URL}/chains/${id}`, id)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static publicRetrieveChain(id, params) {
    return request.get(`${APP_API_URL}/public/chains/${id}`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static publicRetrievePopularChain() {
    return request.get(`${APP_API_URL}/public/popular-chains`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listBrands(params) {
    return request.signedGet(`${APP_API_URL}/brands/popular`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static searchBrands(params) {
    return request.signedGet(`${APP_API_URL}/brands/search`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static fullSearchBrands(params) {
    return request.signedGet(`${APP_API_URL}/brands/full_search`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static popularBrands(params) {
    return request.signedGet(`${APP_API_URL}/brands/popular`, params)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrieveBrand(id) {
    return request.signedGet(`${APP_API_URL}/places/detail`, {id})
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static retrieveVoucher(id) {
    return request.get(`${API_URL}/vouchers/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static redeemVoucher(id, body) {
    return request.post(`${API_URL}/vouchers/${id}`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static createAddress(body) {
    return request.signedPost(`${API_URL}/addresses`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static listAddress(params) {
    return request.signedGet(`${API_URL}/addresses`, params)
    .then((res) => {
      return res.data.addresses;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static retieveAddress(id) {
    return request.signedGet(`${API_URL}/addresses/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static updateAddress(id, body) {
    return request.signedPut(`${API_URL}/addresses/${id}`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static deleteAddress(id) {
    return request.signedDelete(`${API_URL}/addresses/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static updateDefaultAddress(id) {
    return request.signedPut(`${API_URL}/addresses/${id}/default`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static listPaymentMethod(body) {
    return request.signedGet(`${API_URL}/payment_methods`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static createPaymentMethod(body) {
    return request.signedPost(`${API_URL}/payment_methods`, body)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static updateDefaultPaymentMethod(id) {
    return request.signedGet(`${API_URL}/payment_methods/${id}/default`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static deletePaymentMethod(id) {
    return request.signedDelete(`${API_URL}/payment_methods/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    })
  }

  static retrieveMemberships() {
    return request.signedGet(`${API_URL}/memberships`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }

  static listSubscriptionProducts() {
    return request.get(`${API_URL}/subscription-products`)
    .then((res) => {
      return res.data;
    })
    .catch(err => {
      throw handleError(err);
    });
  }
}


export default ApiInteractor;
