import _ from 'lodash';

const couponCode = (order) => {
  return _.get(order, 'applied_discount.title') ? _.get(order, 'applied_discount.title').split('|') : null;
}


class TalkableInteractor {
  static registerPurchase (order) {
    // Post Purchase Script http://docs.talkable.com/integration/custom/integration_components.html#post-purchase-script
    const talkablePayload = {
      purchase: {
        order_number: order.id,
        subtotal: order.subtotal_price,
        coupon_code: couponCode(order),
        shipping_zip: _.get(order, 'shipping_address.zip'),
        shipping_address: order.shipping_address, // does this need to be further parsed?
      },
      customer: {
        email: order.email,
      }
    }
    if (window && window._talkableq) {
      window._talkableq.push(['register_purchase', talkablePayload]);
    } else {
      console.log('Unable to register Talkable purchase');
    }
  }
}

export default TalkableInteractor;
