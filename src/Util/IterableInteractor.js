import request from './Request';
import config from '../config';


class IterableInteractor {

  static getByEmail(body) {
    return request.get(`${config.API_URL}/users/email_preferences/${body.hashId}?email=${body.email}`)
  }

  static updateSubscriptions(body) {
    return request.put(`${config.API_URL}/users/email_preferences/${body.hashId}`, body)
  }

}

export default IterableInteractor;
