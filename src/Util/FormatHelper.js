class FormatHelper {

  static uppercaseFirstLetter(phrase) {
    return phrase.charAt(0).toUpperCase() + phrase.slice(1);
  }

  static replaceAll(phrase, search, replacement) {
    return phrase.replace(new RegExp(search, "g"), replacement);
  }
}

export default FormatHelper;
