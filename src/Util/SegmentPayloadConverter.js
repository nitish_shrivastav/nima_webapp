import _ from 'lodash';
import StorefrontInteractor from '../Util/StorefrontInteractor';

const transformCartData = (product) => ({
  product_id: StorefrontInteractor.decodeId(_.get(product, '[0].node.variant.product.id'), 'Product'),
  sku: _.get(product, '[0].node.variant.sku'),
  name: _.get(product, '[0].node.title'),
  handle: _.get(product, 'handle'),
  variant: StorefrontInteractor.decodeId(_.get(product, '[0].node.variant.id'), 'ProductVariant'),
  price: _.get(product, '[0].node.variant.price'),
  quantity: _.get(product, '[0].node.quantity'),
  url: _.get(product, '[0].node.variant.product.onlineStoreUrl'),
  image_url: _.get(product, '[0].node.variant.image.transformedSrc')
});

const transformViewData = (product) => ({
  product_id: StorefrontInteractor.decodeId(_.get(product, 'id'), 'Product'),
  name: _.get(product, 'title'),
  handle: _.get(product, 'handle'),
  url: _.get(product, 'onlineStoreUrl'),
  image_url: _.get(product, 'images.edges[0].node.originalSrc')
});

class SegmentPayloadConverter {

  static productAdded(checkoutEdgesArray, variantId) {
    const product = checkoutEdgesArray.filter((item) => {
      return _.get(item, 'node.variant.id') === variantId;
    })
    return transformCartData(product);
  }

  static productRemoved(checkoutEdgesArray, variantId) {
    const product = checkoutEdgesArray.filter((item) => {
      return _.get(item, 'node.id') === variantId;
    })
    return transformCartData(product);
  }

  static productViewed(product) {
    return transformViewData(product);
  }

  static productListViewed(products) {
    const frontpageProductIds = products.filter((item) => {
      return _.get(item, 'node.availableForSale') && _.get(item, 'node.collections.edges[0].node.handle');
    }).map((item) => {
      return transformViewData(item.node);
    })
    return ({ products: frontpageProductIds });
  }

}

export default SegmentPayloadConverter;
