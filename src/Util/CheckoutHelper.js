import config from '../config';
import _ from 'lodash';
import StorefrontInteractor from './StorefrontInteractor';
import StripeInteractor from '../Util/StripeInteractor';


const getPremiumDiscount = (lineItems, membership) => {
  let premiumDiscount = 0;
  let index = _.findIndex(lineItems, (item) => {
    let decodedVariantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
    return membership.discounts[decodedVariantId];
  })
  if (index > -1) {
    let variantId = StorefrontInteractor.decodeId(lineItems[index].node.variant.id, "ProductVariant");
    let premiumPrice = membership.discounts[variantId];
    if (parseFloat(lineItems[index].node.variant.price) > premiumPrice) {
      premiumDiscount = (parseFloat(lineItems[index].node.variant.price) - premiumPrice) * lineItems[index].node.quantity;
    }
  }
  return premiumDiscount;
}

//This will take in all promotions and pick the best one for the order.
//Only one promotion can be used on an order
const getPromotionDiscounts = (lineItems, promotions) => {
  let promotionalDiscountsTotal = 0;
  for (let promotion of promotions) {
    let promotionCount = 0;
    for (let item of lineItems) {
      let decodedVariantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
      if (promotion.step_variants.indexOf(decodedVariantId) > -1) {
        promotionCount = promotionCount + parseInt(item.node.quantity);
      }
    }
    let tempPromotionalDiscountsTotal = promotionCount * Math.min((promotionCount - 1), promotion.step_cutoff) * promotion.step_value;
    promotionalDiscountsTotal = Math.max(promotionalDiscountsTotal, tempPromotionalDiscountsTotal);
  }
  return promotionalDiscountsTotal;
}


class CheckoutHelper {
  constructor(window) {
    this.stripeInteractor = new StripeInteractor(window.Stripe)
  }

  static checkForSpecialItems(checkout, memberships) {
    let specialItems = {
      hasGlutenPremium: false,
      hasPeanutPremium: false
    }
    if (memberships && memberships.premium_memberships && memberships.premium_memberships.gluten) {
      specialItems.hasGlutenPremium = true;
    }
    if (memberships && memberships.premium_memberships && memberships.premium_memberships.peanut) {
      specialItems.hasPeanutPremium = true;
    }
    for (let item of checkout.lineItems.edges) {
      let decodedVariantId = StorefrontInteractor.decodeId(item.node.variant.id, "ProductVariant");
      switch (decodedVariantId) {
        case config.GLUTEN_PREMIUM.id:
          specialItems.hasGlutenPremium = true;
          break;
        case config.PEANUT_PREMIUM.id:
          specialItems.hasPeanutPremium = true;
          break;
        default:
          break;
      }
    }
    return specialItems;
  }

  static getOrderTotals(checkout, discount, memberships, promotions) {
    let specialItems = CheckoutHelper.checkForSpecialItems(checkout, memberships);
    let totalFixed = 0;
    let totalPercent = 0;
    let premiumDiscounts = 0;
    let discountCodeTotal = 0;
    let promotionDiscounts = getPromotionDiscounts(checkout.lineItems.edges, promotions);
    let taxes = parseFloat(checkout.totalTax);
    let subtotal = parseFloat(checkout.subtotalPrice);
    let total = parseFloat(checkout.totalPrice);
    let taxRate = taxes / subtotal;
    if (specialItems.hasGlutenPremium) {
      premiumDiscounts = premiumDiscounts + getPremiumDiscount(checkout.lineItems.edges, config.GLUTEN_PREMIUM)
    }
    if (specialItems.hasPeanutPremium) {
      premiumDiscounts = premiumDiscounts + getPremiumDiscount(checkout.lineItems.edges, config.PEANUT_PREMIUM)
    }

    if (discount && discount.type_ === "fixed") {
      totalFixed = totalFixed + discount.amount;
    }
    else if (discount && discount.type_ === "percent") {
      totalPercent = discount.amount / 100;
    }
    discountCodeTotal = totalFixed + (totalPercent * subtotal)
    // if discounts make order free
    if (discountCodeTotal + premiumDiscounts > subtotal) {
      discountCodeTotal = subtotal - premiumDiscounts;
    }
    let discountedSubtotal = subtotal - discountCodeTotal - premiumDiscounts;
    let promotionalSubtotal = Math.max(subtotal - promotionDiscounts, 0);
    subtotal = Math.min(discountedSubtotal, promotionalSubtotal);
    total = total - Math.max((discountCodeTotal + premiumDiscounts), promotionDiscounts);
    total = total - taxes + (subtotal * taxRate);


    return {
      total: total.toFixed(2),
      taxes: (subtotal * taxRate).toFixed(2),
      totalDiscount: parseFloat(discountCodeTotal).toFixed(2),
      premiumDiscountTotal: parseFloat(premiumDiscounts).toFixed(2),
      promotionDiscountTotal: parseFloat(promotionDiscounts).toFixed(2)
    }
  }

  getStripeSource(form) {
    let month = parseInt(form.credit_card_exp_month);
    let year = parseInt(form.credit_card_exp_year);
    let cvc = form.credit_card_cvc;
    return this.stripeInteractor.createSource(form.credit_card_number, month, year, cvc)
  }

  getFormValues(cartId, form) {
    return new Promise((resolve, reject) => {
      let body = {
        ...form,
        checkout_id: cartId,
        type_: "guest",
        source: form.payment_source
      }
      if (form.discount_codes && form.discount_codes !== "") {
        body.discount_codes = [{id: form.discount_codes}]
      }
      if (form.billing_check === true) {
        body.billing_address = body.shipping_address;
      }
      if (body.provider === "stripe") {
        this.getStripeSource(form)
        .then((source) => {
          body.source = source.id;
          resolve(body);
        })
        .catch((err) => {
          reject(err);
        })
      }
      else {
        resolve(body);
      }
    })
  }

  static getAffirmFormValues(cartId, form, checkout) {
    let body = {
      merchant: {
        user_confirmation_url: "https://merchantsite.com/confirm",
        user_cancel_url: "https://merchantsite.com/cancel",
        user_confirmation_url_action: "POST",
        name: "Nima Labs"
      },
      shipping: {
        name: {
          first: form.first_name,
          last: form.last_name
        },
        address: {
          line1: form.shipping_address.address1,
          line2: form.shipping_address.address2,
          city: form.shipping_address.city,
          state: form.shipping_address.province,
          zipcode: form.shipping_address.zip,
          country: form.shipping_address.country
        },
        phone_number: form.phone_number,
        email: form.email
      },
      metadata: {
        mode: "modal",
        is_gift: form.is_gift,
      },
      order_id: cartId,
      shipping_amount: 0, 
      tax_amount: parseInt(checkout.totals.taxes * 100),
      total: parseInt(checkout.totals.total * 100)
    }
    if (checkout.detail.shippingLine && checkout.detail.shippingLine.handle) {
      body.metadata.shippingLine = {
        handle: checkout.detail.shippingLine.handle,
        title: checkout.detail.shippingLine.title
      }
    }
    body.billing = body.shipping;
    body.items = checkout.detail.lineItems.edges.map((item) => {
      const { variant } = item.node;
      return {
        display_name: item.node.title,
        sku: variant.sku,
        unit_price: parseInt(parseFloat(variant.price) * 100),
        qty: item.node.quantity,
        item_image_url: variant.image.originalSrc,
        item_url: variant.product.onlineStoreUrl
      };
    });
    return body;
  }

  static getUserFields(user, country_options) {
    let values = {
      first_name: user.firstName,
      last_name: user.lastName,
      email: user.email,
      shipping_address: null
    }
    if (user.defaultAddress) {
      values.shipping_address = {
        address1: user.defaultAddress.address1,  
        address2: user.defaultAddress.address2,
        city: user.defaultAddress.city,
        province: user.defaultAddress.provinceCode,
        zip: user.defaultAddress.zip,
        country: "US"
      }
      for (let option of country_options) {
        if (option.value === user.defaultAddress.provinceCodeV2) {
          values.shipping_address.country
          break;
        }
      }
    }
    return values;
  }

  static getCreditCardDetails(paymentMethod) {
    let cardDetails = {
      exp_month: "", 
      exp_year: "",
      last4: "",
      brand: ""
    }
    if (paymentMethod.type === "card" && paymentMethod.card) {
      cardDetails.exp_month = paymentMethod.card.exp_month;
      cardDetails.exp_year = paymentMethod.card.exp_year;
      cardDetails.last4 = paymentMethod.card.last4;
      cardDetails.brand = paymentMethod.card.brand;
    }
    else if (paymentMethod.last4) {
      cardDetails.exp_month = paymentMethod.exp_month;
      cardDetails.exp_year = paymentMethod.exp_year;
      cardDetails.last4 = paymentMethod.last4;
      cardDetails.brand = paymentMethod.brand;
    }
    return cardDetails;
  }
}

export default CheckoutHelper;
