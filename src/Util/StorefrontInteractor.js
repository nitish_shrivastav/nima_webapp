import { gql } from 'apollo-boost';
import _ from 'lodash';

class StorefrontInteractor {
  static decodeId(encodedId, tableName) {
    if (!encodedId) return undefined;
    let decodedVariantId = new Buffer(encodedId, "base64").toString();
    return decodedVariantId.split(`${tableName}/`)[1];
  }

  static encodeId(id, tableName) {
    return Buffer.from(`gid://shopify/${tableName}/${id}`).toString('base64');
  }

  static retrieveCheckout(client, queryVariables) {
    return client.query({
      query: retrieveCheckoutQuery,
      variables: queryVariables,
      errorPolicy: "ignore",
      fetchPolicy: 'no-cache'
    })
    .then((checkoutRes) => {
      if (checkoutRes && checkoutRes.data && checkoutRes.data.node) {
        return checkoutRes.data.node
      }
      else {
        throw "Checkout does not exist"
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static createCheckout(client) {
    return client.mutate({
      mutation: createCheckoutMutation,
      variables: { input: {} },
      errorPolicy: "ignore"
    })
    .then((checkoutRes) => {
      if (checkoutRes && checkoutRes.data && checkoutRes.data.checkoutCreate && checkoutRes.data.checkoutCreate.checkout) {
        return checkoutRes.data.checkoutCreate.checkout;
      }
      else {
        throw "Checkout create failure";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static updateCheckout(client, mutationVariables) {
    return client.mutate({
      mutation: updateCheckoutMutation,
      variables: mutationVariables
    })
    .then((res) => {
      if (res && res.data && res.data.checkoutShippingAddressUpdateV2 && res.data.checkoutShippingAddressUpdateV2.checkout) {
        return res.data.checkoutShippingAddressUpdateV2.checkout;
      }
      else {
        throw "Checkout update failed";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static checkoutLineItemsReplace(client, mutationVariables) {
    return client.mutate({
      mutation: checkoutLineItemsReplaceMutation,
      variables: mutationVariables,
      errorPolicy: "ignore"
    })
    .then((res) => {
      if (res && res.data && res.data.checkoutLineItemsReplace && res.data.checkoutLineItemsReplace.checkout) {
        return res.data.checkoutLineItemsReplace.checkout;
      }
      else {
        throw "Checkout line item update failed";
      }
    })
    .catch((err) => {
      throw err
    })
  }

  static updateCheckoutEmail(client, mutationVariables) {
    return client.mutate({
      mutation: updateCheckoutEmailMutation,
      variables: mutationVariables,
      errorPolicy: "ignore"
    })
    .then((res) => {
      if (res && res.data && res.data.checkoutEmailUpdate && res.data.checkoutEmailUpdate.checkout) {
        return res.data.checkoutEmailUpdate.checkout;
      }
      else {
        throw "Email update failed";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static activateCustomer(client, variables) {
    return client.mutate({
      mutation: activateCustomerMutation,
      variables: variables
    })
    .then((res) => {
      if (res && res.data) {
        return res.data;
      }
      else {
        throw "Error in activateCustomer";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static updateCheckoutShippingLine(client, mutationVariables) {
    return client.mutate({
      mutation: updateCheckoutShippingLineMutation,
      variables: mutationVariables,
      errorPolicy: "ignore"
    })
    .then((res) => {
      if (res && res.data && res.data.checkoutShippingLineUpdate && res.data.checkoutShippingLineUpdate.checkout) {
        return res.data.checkoutShippingLineUpdate.checkout;
      }
      else {
        throw "Shipping line update failed";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static getCustomerInformation(client, accessToken) {
    return client.query({
      query: getCustomerInformationQuery(accessToken),
      variables: null,
      fetchPolicy: 'no-cache'
    })
    .then((res) => {
      if (res && res.data && res.data.customer) {
        return res.data.customer;
      }
      else {
        throw "Get customer info failed.";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static resetUserPassword(client, variables) {
    return client.mutate({
      mutation: resetUserPasswordMutation,
      variables: variables
    })
    .then((res) => {
      if (res && res.data) {
        return res.data;
      }
      else {
        throw "Error in resetUserPassword";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static recoverUserPassword(client, variables) {
    return client.mutate({
      mutation: recoverUserPasswordMutation,
      variables: variables
    })
    .then((res) => {
      if (res && res.data) {
        return res.data;
      }
      else {
        throw "Error in recoverUserpassword";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static getShopProducts(client) {
    return client.query({
      query: getShopProductsQuery,
      variables: null,
      fetchPolicy: 'no-cache'
    })
    .then((res) => {
      if (res && res.data && res.data.shop && res.data.shop.products && res.data.shop.products.edges) {
        return res.data.shop.products.edges;
      }
      else {
        throw "Error in getShopProducts";
      }
    })
    .catch((err) => {
      throw err;
    })
  }

  static getShopProduct(client, handle) {
    return client.query({
      query: getShopProductQuery(handle),
      variables: null,
      fetchPolicy: 'no-cache'
    })
    .then((res) => {
      if (res && res.data && res.data.shop && res.data.shop.productByHandle) {
        return res.data.shop.productByHandle;
      }
      else {
        throw "Error in getShopProduct";
      }
    })
    .catch((err) => {
      throw err;
    })
  }
}

const checkoutUpdateReturnValues = `
  id
  email
  subtotalPrice
  totalTax
  totalPrice
  requiresShipping
  shippingLine {
    handle
    price
    title
  }
  customer {
    id
    email
  }
  availableShippingRates {
    ready
    shippingRates {
      handle
      price
      title
    }
  }
  shippingAddress {
    address1
    city
    country
    countryCode
    provinceCode
    province
    zip
  }
  lineItems(first: 100) {
    edges {
      node {
        id
        title
        quantity
        customAttributes {
          key
          value
        }
        variant {
          id
          sku
          price
          title
          image {
            originalSrc
            transformedSrc(maxWidth: 300)
          }
          product {
            onlineStoreUrl
            handle
            id
          }
        }
      }
    }
  }`;

const userErrors = `userErrors {
  field
  message
}`;

const retrieveCheckoutQuery  = gql`
  query node($id: ID!) {
    node(id: $id) {
      ... on Checkout {
        ${checkoutUpdateReturnValues}
      }
    }
  }`

const createCheckoutMutation = gql`
  mutation checkoutCreate($input: CheckoutCreateInput!) {
    checkoutCreate(input: $input) {
      checkout {
        ${checkoutUpdateReturnValues}
      }
      ${userErrors}
    }
  }`

const updateCheckoutMutation = gql`
  mutation checkoutShipppingAddressUpdateV2($input: MailingAddressInput!, $id: ID!) {
    checkoutShippingAddressUpdateV2(shippingAddress: $input, checkoutId: $id) {
      checkout {
        ${checkoutUpdateReturnValues}
      }
      ${userErrors}
    }
  }`

const checkoutLineItemsReplaceMutation = gql`
  mutation checkoutLineItemsReplace($checkoutId: ID!, $lineItems: [CheckoutLineItemInput!]!) {
    checkoutLineItemsReplace(checkoutId: $checkoutId, lineItems: $lineItems) {
      checkout {
        ${checkoutUpdateReturnValues}
      }
      ${userErrors}
    }
  }`

const updateCheckoutEmailMutation = gql`
  mutation checkoutEmailUpdate($email: String!, $id: ID!) {
    checkoutEmailUpdate(email: $email, checkoutId: $id) {
      checkout {
        ${checkoutUpdateReturnValues}
      }
      ${userErrors}
    }
  }`

const updateCheckoutShippingLineMutation = gql`
  mutation checkoutShipppingLineUpdate($handle: String!, $id: ID!) {
    checkoutShippingLineUpdate(shippingRateHandle: $handle, checkoutId: $id) {
      checkout {
        ${checkoutUpdateReturnValues}
      }
      ${userErrors}
    }
  }`

const activateCustomerMutation =  gql`
  mutation customerActivate($input: CustomerActivateInput!, $id: ID!) {
	  customerActivate(input: $input, id: $id) {
      ${userErrors}
    }
  }`

function getCustomerInformationQuery(accessToken) {
  return gql`{
    customer(customerAccessToken: "${accessToken}") {
      id
      email
      firstName
      lastName
      phone
      orders(first: 10) {
        edges {
          node {
            id
            email
            totalPrice
          }
        }
      }
      defaultAddress {
        address1
        address2
        city
        company
        countryCodeV2
        firstName
        lastName
        provinceCode
        zip
      }
    }
  }`
}

const resetUserPasswordMutation =  gql`
  mutation customerReset($id: ID!, $input: CustomerResetInput!) {
	  customerReset(id: $id, input: $input) {
      ${userErrors}
    }
  }`

const recoverUserPasswordMutation =  gql`
  mutation customerRecover($email: String!) {
	  customerRecover(email: $email) {
      ${userErrors}
    }
  }`


const productReturnValues =  `
  collections(first: 100) {
    edges {
      node {
        handle
      }
    }
  }
  availableForSale
  tags
  descriptionHtml
  description
  priceRange {
    maxVariantPrice {
      amount
    }
    minVariantPrice {
      amount
    }
  }
  handle
  id
  title
  onlineStoreUrl
  variants(first:10) {
    edges {
      node {
        id
        price
        compareAtPrice
        title
      }
    }
  }
  images(first: 1) {
    edges {
      node {
        originalSrc
        transformedSrc(maxWidth: 300)
      }
    }
  }`

const getShopProductsQuery =  gql`{
    shop {
      products(first: 50) {
        edges {
          node {
            ${productReturnValues}
          }
        }
      }
    }
  }`

function getShopProductQuery(handle) {
  return gql`{
    shop {
      productByHandle(handle: "${handle}") {
        ${productReturnValues}
      }
    }
  }`
}


export default StorefrontInteractor;
