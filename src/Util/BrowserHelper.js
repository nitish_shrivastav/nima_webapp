class BrowserHelper {

  static getName() {
    // Opera 8.0+
    if ((!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
      return "opera";
    }
    // Firefox 1.0+
    else if (typeof InstallTrigger !== 'undefined') {
      return "firefox";
    }
    // Safari 3.0+ "[object HTMLElementConstructor]" 
    else if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && window.safari.pushNotification))) {
      return "safari";
    }
    // Internet Explorer 6-11
    else if (/*@cc_on!@*/false || !!document.documentMode) {
      return "internet_explorer";
    }
    else if (!(/*@cc_on!@*/false || !!document.documentMode) && !!window.StyleMedia) {
      return "edge"
    }
    else if (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime)) {
      return "chrome";
    }
    else {
      return "web";
    }
  }

}

export default BrowserHelper;
