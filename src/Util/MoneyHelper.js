class MoneyHelper {

  static moneyWithoutTrailingZeros(amount) {
    amount = amount.toString();
    if (amount.indexOf('.0') > -1) {
      amount = amount.slice(0, amount.indexOf('.0'));
    }
    return `$${amount}`;
  }
}

export default MoneyHelper;
