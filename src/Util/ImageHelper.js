import config from '../config';

class ImageHelper {

  static getGoogleMapsUrl(photoReference, maxWidth) {
    if (!maxWidth) maxWidth = "200";
    return `https://maps.googleapis.com/maps/api/place/photo?maxwidth=${maxWidth}&photoreference=${photoReference}&key=${config.GOOGLE_PLACES_KEY}`
  }
}

export default ImageHelper;
