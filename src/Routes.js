import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import App from './App';
import GuestCheckout from './Containers/GuestCheckout';
import SSORoute from './SSORoute';
import OrderThankYou from './Containers/OrderThankYou';
import { ApolloConsumer } from 'react-apollo';
import SSOLogin from './Containers/SSOLogin';
import CreateAccount from './Containers/CreateAccount';
import SubscriptionsList from './Containers/SubscriptionList';
import OrdersList from './Containers/OrdersList';
import SSOPasswordForgot from './Containers/SSOPasswordForgot';
import ProductList from './Containers/ProductList';
import ProductDetail from './Containers/ProductDetail';
import PublicRoute from './PublicRoute';
import Cart from './Containers/Cart';
import MapRedirect from './Components/MapRedirect';
//import BrandList from './Containers/BrandList';
//import ReviewList from './Containers/ReviewList';
//import ReadingList from './Containers/ReadingList';
//import UserDetail from './Containers/UserDetail';
import TalkablePage from './Containers/TalkablePage';
import PublicPlaceMap from './Containers/PublicPlaceMap';
import PublicPlaceDetail from './Containers/PublicPlaceDetail';
import PublicChainDetail from './Containers/PublicChainDetail';
import DefaultRoute from './DefaultRoute';
import Page404 from  './Containers/Page404';
import EmailPreferences from './Containers/EmailPreferences';
import VoucherRedeem from './Containers/VoucherRedeem';
import VerifyAccount from './Containers/VerifyAccount';
import CheckoutMethod from './Containers/CheckoutMethod';
import AuthenticatedCheckout from './Containers/AuthenticatedCheckout';
import ResendVerifyAccountEmail from './Containers/ResendVerifyAccountEmail';
import CustomerPortalWrapper from './CustomerPortalWrapper';
import CustomerPortalHub from './Containers/CustomerPortalHub';
import ProfileManagement from './Containers/ProfileManagement';

const StoreRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      return (
        <PublicRoute client={rest.client}>
          <Component {...props} client={rest.client}/>
        </PublicRoute>
      )
    }}/>
  )
}

const BasicRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      return (
        <DefaultRoute>
          <Component {...props} />
        </DefaultRoute>
      )
    }}/>
  )
}

const AppRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      return (
        <SSORoute location={props.location}>
          <Component {...props} client={rest.client}/>
        </SSORoute>
      )
    }} />
  )
}

const CustomerPortalRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => {
      return (
        <SSORoute location={props.location}>
          <CustomerPortalWrapper>
            <Component {...props} client={rest.client}/>
          </CustomerPortalWrapper>
        </SSORoute>
      )
    }} />
  )
}


class Routes extends Component {
  render() {
    return (
      <ApolloConsumer>
        {client => (
          <BrowserRouter>
            <App>
              <Switch>
                <AppRoute path="/shop/cart/auth/:cart_id" component={AuthenticatedCheckout} client={client}/>
                <Route path="/shop/order-thanks" component={OrderThankYou} />
                <Route path="/shop/share" component={TalkablePage} />
                <Route path="/shop/advocate" component={TalkablePage} />
                <Route path="/shop/advocate-ali" component={TalkablePage} />
                <StoreRoute path="/shop/products/:handle" client={client} component={ProductDetail} />
                <StoreRoute path="/shop/cart/:cart_id" component={GuestCheckout} client={client}/>
                <StoreRoute path="/shop/cart" client={client} component={Cart} />
                <StoreRoute path="/shop/checkout-method" client={client} component={CheckoutMethod} />
                <StoreRoute path="/shop/locationmap/" client={client} component={MapRedirect} />
                <StoreRoute exact path="/shop" client={client} component={ProductList} />
                <StoreRoute path="/shop/voucher" client={client} component={VoucherRedeem} />
                <BasicRoute path="/map/places/:place_id" component={PublicPlaceDetail} />
                <BasicRoute path="/map/chains/:chain_id" component={PublicChainDetail} />
                <BasicRoute path="/map" component={PublicPlaceMap} />
                
                <StoreRoute path="/account/verify" client={client} component={VerifyAccount} />
                <StoreRoute path="/account/email-preferences/:email_hash" client={client} component={EmailPreferences} />
                <StoreRoute path="/account/email-verify/resend" client={client} component={ResendVerifyAccountEmail} />
                {/* <AppRoute exact path="/app" client={client} component={AppNav} /> */}
                {/* <AppRoute exact path="/app/readings" client={client} component={ReadingList} /> */}
                {/* <AppRoute exact path="/app/reviews" client={client} component={ReviewList} /> */}
                {/* <AppRoute exact path="/app/brands" client={client} component={BrandList} /> */}
                {/* <AppRoute exact path="/app/profile" client={client} component={UserDetail} /> */}
                <BasicRoute path="/account/login" client={client} component={SSOLogin} />
                <BasicRoute path="/account/register" client={client} component={CreateAccount} />
                <BasicRoute path="/account/create" component={CreateAccount} />
                <BasicRoute path="/account/password-forgot" component={SSOPasswordForgot} />
                <CustomerPortalRoute exact path="/account/profile" client={client} component={ProfileManagement} />
                <CustomerPortalRoute exact path="/account/subscriptions" client={client} component={SubscriptionsList} />
                <CustomerPortalRoute exact path="/account/orders" client={client} component={OrdersList} />
                <AppRoute exact path="/account" client={client} component={CustomerPortalHub} />
                <BasicRoute path="*" component={Page404} />
              </Switch>
            </App>
          </BrowserRouter>
        )}
      </ApolloConsumer>
    );
  }
}

export default Routes;
