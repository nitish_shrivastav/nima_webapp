import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Profile from '../SVG/Profile';
import Calandar from '../SVG/Calandar';
import Box from '../SVG/Box';
import Back from '../SVG/Back';

const arrowStyle = {
  width: 20,
  height: 20,
  transform: 'rotateY(180deg)',
  fill: "#AAAAAA"
}

const iconStyle = {
  width: 40,
  height: 40,
  marginRight: 20
}


const CustomerPortalHub = (props) => {

  return (
    <div className="nm-layout">
      <div className="nm-layout__max-center nm-layout__max-center--mobile-support nm-layout__max-center--large">
        <div className="nm-spacer-48"/>
        <h1 className="nm-layout" style={{paddingLeft: 12}}>
          Nima Customer Portal
        </h1>
        {
          props.auth.sso &&
          <p className="nm-layout" style={{paddingLeft: 12}}>
            Good Afternoon {props.auth.sso.first_name}
          </p>
        }
        <div className="nm-spacer-48"/>
        <div className="nm-layout">
          <Link className="nm-sidebar__item" to="/account/profile" style={{justifyContent: 'space-between'}}>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <Profile style={iconStyle} strokeClass="nm-sidebar__icon"/>
              Profile Settings
            </div>
            <Back style={arrowStyle}/> 
          </Link>
          <div className="nm-sidebar__item-bar"/>
          <Link className="nm-sidebar__item" to="/account/orders" style={{justifyContent: 'space-between'}}>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <Box style={iconStyle} strokeClass="nm-sidebar__icon"/>
              Order History
            </div>
            <Back style={arrowStyle}/> 
          </Link>
          <div className="nm-sidebar__item-bar"/>
          <Link className="nm-sidebar__item" to="/account/subscriptions" style={{justifyContent: 'space-between'}}>
            <div style={{display: 'flex', alignItems: 'center'}}>
              <Calandar style={iconStyle} strokeClass="nm-sidebar__icon"/>
              Manage Subscriptions
            </div>
            <Back style={arrowStyle}/> 
          </Link>
          <div className="nm-sidebar__item-bar"/>
        </div>
        <div className="nm-spacer-48"/>
        <div className="nm-spacer-48"/>
      </div>
    </div>
  );
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(CustomerPortalHub);
