import React, { Component } from 'react';
import { initialize } from 'redux-form';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import Button from '../Components/Button';
import StorefontStorage from '../Util/StorefrontStorage';
import CheckoutActions from '../Actions/CheckoutActions';
import CartForm from '../Components/Forms/CartForm';
import OrLine from '../Components/OrLine';
import BraintreeInteractor from '../Util/BraintreeInteractor';
import { PAYPAL } from '../Constants/payment';
import SegmentActions from '../Actions/SegmentActions';
import SegmentConstants from '../Constants/segment';
import SegmentPayloadConverter from '../Util/SegmentPayloadConverter';


class Cart extends Component {
  constructor() {
    super();
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
    this.updateCartSubmit = this.updateCartSubmit.bind(this);
    this.payPalListener = this.payPalListener.bind(this);
  }

  componentDidMount() {
    this.payPalListener();
  }

  componentDidUpdate() {
    if (window.affirm && window.affirm.ui) {
      window.affirm.ui.ready(() => {
        window.affirm.ui.refresh();
      });
    }
  }

  payPalListener() {
    BraintreeInteractor.getPayPalNonce(window, '.paypal-cart-button', (err, res) => {
      if (err) {
        console.log('err in payPalListener', err)
      }
      else if (res) {
        let email = _.get(this, 'props.auth.user.email');
        if (!email) {
          email = res.details.email;
        }
        let initialValues = {
          first_name: res.details.firstName,
          last_name: res.details.lastName,
          email: email,
          payment_source: res.nonce,
          billing_check: true,
          is_gift: false,
          shipping_address: {
            address1: res.details.shippingAddress.line1,
            address2: res.details.shippingAddress.line2,
            city: res.details.shippingAddress.city,
            province: res.details.shippingAddress.state,
            country: res.details.shippingAddress.countryCode,
            zip: res.details.shippingAddress.postalCode
          }
        }
        this.props.dispatch(initialize('checkout',  initialValues));
        let checkoutBody = {
          input:  {
            ...initialValues.shipping_address,
            lastName: initialValues.last_name
          },
          id: this.props.checkout.detail.id
        }
        this.props.dispatch(CheckoutActions.updateCheckout(this.props.client, checkoutBody));
        this.props.history.push(`/shop/cart/${this.props.checkout.detail.id}?method=${PAYPAL}`);
      }
    })
  }

  handleRemoveClick(id) {
    let mutationVariables = {
      checkoutId: StorefontStorage.getCheckoutId(),
      lineItems: _.get(this, 'props.checkout.detail.lineItems.edges', [])
        .filter((item) => id !== item.node.id)
        .map((item) => ({
          variantId: item.node.variant.id,
          quantity: item.node.quantity
        }))
    }
    const payload = SegmentPayloadConverter.productRemoved(_.get(this, 'props.checkout.detail.lineItems.edges', []), id);
    this.props.dispatch(SegmentActions.track(SegmentConstants.PRODUCT_REMOVED, payload));
    this.props.dispatch(CheckoutActions.checkoutLineItemsReplace(this.props.client, mutationVariables));
  }

  hasCheckoutItems(checkout) {
    return checkout && checkout.detail && checkout.detail.lineItems && checkout.detail.lineItems.edges.length > 0;
  }

  updateCartSubmit(values) {
    let mutationVariables = {
      checkoutId: StorefontStorage.getCheckoutId(),
      lineItems: values.lineItems.edges.map((item) => {
        return {
          quantity: item.node.quantity,
          variantId: item.node.variant.id
        }
      })
    }
    this.props.dispatch(CheckoutActions.checkoutLineItemsReplace(this.props.client, mutationVariables));
  }

  render() {
    const { checkout } = this.props;
    let hasItems = this.hasCheckoutItems(checkout);
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
          <div className="nm-spacer-48"></div>
          <div className="nm-card nm-card__block" style={{padding: 0}}>
            <div className="nm-spacer-12"></div>
            {
              hasItems &&
              <div>
                <div className="nm-cart__row">
                  <h2 className="nm-layout" style={{fontSize: 24}}>Your Cart</h2>
                </div>
                <CartForm onSubmit={this.updateCartSubmit} onRemoveClick={this.handleRemoveClick}/>
                <div className="nm-cart__footer">
                  <div className="nm-cart__footer--left">
                    <p className="nm-cart__subtotal">
                      Subtotal: ${checkout.detail.subtotalPrice}
                    </p>
                    <p className="nm-cart__disclaimer">
                      If you have a coupon code, you can add it when you Checkout. Also,
                      shipping and taxes will be calculated at Checkout.
                    </p>
                    <div className="nm-spacer-12"></div>
                    <div className="nm-spacer-12"></div>
                  </div>
                  <div className="nm-cart__submit-wrap">
                    <Link to="/shop/checkout-method">
                      <Button text="Checkout" preventDefault={false}/>
                      <div className="nm-clear"/>
                    </Link>
                    <div className="nm-clear"></div>
                    <p className="nm-text-center">
                      <div className="nm-layout" dangerouslySetInnerHTML={{
                        __html: `<p class="affirm-as-low-as" data-page-type="product" data-amount="${parseInt(parseFloat(checkout.totals.total) * 100)}"></p>`
                      }}/>
                    </p>
                    <OrLine backgroundColor="white" />
                    <div className="nm-spacer-12"></div>
                    <p className="nm-text-center">
                      Express guest checkout with PayPal
                      <div className="nm-btn__max nm-btn__center">
                        <p className="paypal-cart-button"/>
                        <div className="nm-clear"/>
                      </div>
                    </p>
                  </div>
                </div>
              </div>
            }
            {
              !hasItems &&
              <div>
                <div className="nm-spacer-48"></div>
                <h3 className="nm-text-center">Your cart is empty</h3>
                <Link to="/shop">
                  <Button text="Browse All Products" className="nm-btn__max" center={true} preventDefault={false}/>
                </Link>
                <p className="nm-cart__orlearn">
                  Or learn more about Nima <a className="nm-link" href="/">on our home page</a>
                </p>
                <div className="nm-clear"></div>
              </div>
            }
          </div>
        </div>
        <div className="nm-spacer-48"></div>
        <div className="nm-clear"></div>
        <p className="nm-layout nm-text-center nm-grey-text">
          Continue shopping capsules, <br/>Nima-tested reports, and more.
        </p>
        <div className="nm-spacer-48"></div>
        <div className="nm-clear"></div>
        <Link to="/shop">
          <Button center={true} text="Shop All Products" style={{maxWidth: 200}} preventDefault={false}/>
        </Link>
        <div className="nm-clear"></div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    checkout: state.checkout
  })
)(Cart);
