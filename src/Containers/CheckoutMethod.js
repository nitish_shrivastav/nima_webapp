import React, { Component } from 'react';
import _ from 'lodash';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import SSOLoginForm from '../Components/Forms/SSOLoginForm';
import Button from '../Components/Button';
import OrLine from '../Components/OrLine';

import AuthActions from '../Actions/AuthActions';

class CheckoutMethod extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFacebookClick = this.handleFacebookClick.bind(this);
  }

  componentWillMount() {
    this.setState({ showRegisterSuccess: false });
  }

  handleSubmit(values) {
    this.props.dispatch(AuthActions.loginAuth0User(values));
  }

  handleFacebookClick() {
    if (window.FB) {
      window.FB.login((res) => {
        if (res.status === "connected" && _.get(res, 'authResponse.accessToken')) {
          this.props.dispatch(AuthActions.loginFacebookUser({ access_token: res.authResponse.accessToken }));
        }
      },{scope: 'public_profile,email'});
    }
  }


  render() {
    if (_.get(this, 'props.auth.sso')) {
      return (
        <Redirect to={`/shop/cart/auth/${this.props.checkout.detail.id}`} />
      );
    }
    else {
      return (
        <div className="nm-layout">
          <div className="nm-spacer-48" />
          <div className="nm-clear"/>
          <div className="nm-layout__max-center nm-layout__max-center--medium-gray nm-layout__max-center--mobile-support">
            <div className="nm-spacer-48" />
            <div className="nm-card" style={{display: "block"}}>
              <div className="nm-layout nm-layout__half nm-layout__right-border" style={{padding: 24}}>
                <h2 className="nm-layout">
                  Have a Nima account already?
                </h2>
                <p className="nm-layout">
                  Sign in to checkout faster.
                </p>
                <div className="nm-spacer-12" />
                <SSOLoginForm
                  onFacebookClick={this.handleFacebookClick}
                  onSubmit={this.handleSubmit}/>
                <div className="nm-spacer-48" />
              </div>
              <div className="nm-layout nm-layout__half" style={{padding: 24}}>
                <h2 className="nm-layout">
                  Don't have an account yet?
                </h2>
                <div className="nm-spacer-48"></div>
                <Link to="/account/register">
                  <Button text="Get a Nima account" style={{maxWidth: 300}} preventDefault={false} center={true}/>
                </Link>
                <div className="nm-spacer-12"></div>
                <OrLine backgroundColor="white" />
                <div className="nm-spacer-12"></div>
                {
                  _.get(this.props.checkout, 'detail.id') &&
                  <Link to={`/shop/cart/${this.props.checkout.detail.id}`}>
                    <Button text="Continue as Guest" style={{maxWidth: 300}} preventDefault={false} center={true}/>
                  </Link>
                }
              </div>
            </div>
            <div className="nm-clear"></div>
          </div>
        </div>
      );
    }
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    checkout: state.checkout
  })
)(CheckoutMethod);
