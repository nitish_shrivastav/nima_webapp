import React, { Component } from 'react';
import queryString from 'query-string';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ChainActions from '../Actions/ChainActions';
import StarRating from '../SVG/StarRating';
import FormatHelper from '../Util/FormatHelper';
import { PEANUT_ASSETS, GLUTEN_ASSETS } from '../Constants/allergen';
import { ALLERGEN_COLOR, RED, YELLOW, TEXT, GREY } from '../Constants/colors';
import Toggle from '../Components/Toggle';
import Tooltip from '../Components/Tooltip';
import Button from '../Components/Button';
import FormCard from '../Components/FormCard';
import DetailReviewTicket from "../Components/DetailReviewTicket";
import TEST_RESULT_ASSETS from '../Constants/testResults';
import PopularChains from '../Components/PopularChains';
import RestaurantHeader from '../Components/RestaurantHeader';
import EmailSignUpForm from '../Components/Forms/EmailSignUpForm';

const infoBoxStyle = {
  width: "100%",
  maxWidth: 250,
  borderRadius: 6,
  minHeight: 160,
  border: `1px solid ${GREY}`,
  margin: 12
}

const labelBoxStyle = {
  width: "100%",
  maxWidth: 250,
  borderRadius: 6,
  minHeight: 75,
  border: `1px solid ${GREY}`,
  margin: 12
}

class PublicChainDetail extends Component {
  constructor() {
    super();
    this.handleAllergenClick = this.handleAllergenClick.bind(this);
    this.getChainReviews = this.getChainReviews.bind(this);
    this.handleChainToggleClick = this.handleChainToggleClick.bind(this);
    this.handleAcceptClick = this.handleAcceptClick.bind(this);
    this.renderInfoBoxes = this.renderInfoBoxes.bind(this);
    this.handleSignUpSubmit = this.handleSignUpSubmit.bind(this);
  }

  componentWillMount() {
    let params = queryString.parse(this.props.chain_id);
    this.props.dispatch(ChainActions.publicPopularRetrieve());
    this.props.dispatch(ChainActions.publicRetrieve(this.props.match.params.chain_id))
    .then((detail) => {
      if (detail) {
        document.title = `Nima Reviews | ${detail.name}`;
      }
    })
    .catch();
    if (!params.allergen || (params.allergen !== "peanut" && params.allergen !== "gluten")) {
      params.allergen = "gluten";
      this.props.history.replace({
        search: `?${queryString.stringify(params)}`
      });
    }
    this.setState({
      allergen: params.allergen,
      showChain: true,
      hasAccepted: localStorage.getItem("chainAccepted")
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.props.dispatch(ChainActions.publicRetrieve(this.props.match.params.chain_id))
      .then((detail) => {
        if (detail) {
          document.title = `Nima Reviews | ${detail.name}`;
        }
      })
      .catch();
    }
  }

  handleAcceptClick() {
    localStorage.setItem("chainAccepted", new Date().getTime());
    this.setState({hasAccepted: true});
  }

  getAllergenText(review) {
    if (_.get(review, 'readings[0].allergen')) {
      let allergen = FormatHelper.uppercaseFirstLetter(review.readings[0].allergen);
      let label = FormatHelper.replaceAll(review.readings[0].labeling, "_", " ");
      label = FormatHelper.uppercaseFirstLetter(label);
      return `${label} ${allergen}-free.`;
    }
    else {
      return "No test";
    }
  }

  getResultImageUrl(review) {
    const { test_result, allergen } = review.readings[0];
    if (typeof(test_result) === "number" && allergen) {
      return TEST_RESULT_ASSETS[allergen][test_result.toString()];
    }
    else {
      return TEST_RESULT_ASSETS.gluten["-1"];
    }
  }

  handleAllergenClick(allergen) {
    if (this.state.allergen !== allergen) {
      this.setState({ allergen: allergen });
      let params = queryString.parse(this.props.location.search);
      params.allergen = allergen;
      this.props.history.replace({
        search: `?${queryString.stringify(params)}`
      });
    }
  }

  getChainReviews() {
    const { detail } = this.props.chain;
    if (detail && detail.chain_reviews) {
      return detail.chain_reviews.filter((item) => _.get(item, 'readings[0].allergen') === this.state.allergen);
    }
    else {
      return [];
    }
  }

  handleChainToggleClick() {
    this.setState({ showChain: !this.state.showChain });
  }

  handleSignUpSubmit(values) {
    console.log('values', values)
  }

  renderInfoBoxes(allergenInfo, chainId) {
    return (
      <div className="nm-layout nm-flex nm-flex--row" style={{alignItems: 'normal'}}>
        <div className="nm-layout nm-flex" style={infoBoxStyle}>
          <div className="nm-flex nm-layout" style={{flexDirection: 'row', alignItems: 'none'}}>
            <div
              className="nm-search__map-allergen-icon"
              onClick={() => this.handleAllergenClick("gluten")}>
              <img className="nm-image nm-image__contain" src={GLUTEN_ASSETS[this.state.allergen]} style={{height: '100%'}}/>
            </div>
            <div
              className="nm-search__map-allergen-icon"
              onClick={() => this.handleAllergenClick("peanut")}>
              <img className="nm-image nm-image__contain" src={PEANUT_ASSETS[this.state.allergen]} style={{height: '100%'}}/>
            </div>
          </div>
          <a href={allergenInfo} className="nm-layout nm-text-center">
            Allergen information
          </a>
          <div className="nm-clear"/>
        </div>
        <div className="nm-layout" style={infoBoxStyle}>
          <img src={`${process.env.PUBLIC_URL}/images/map/map_placeholder.png`} className="nm-image nm-image__cover nm-layout" style={{height: '100%'}}/>
          <div className="nm-layout nm-flex" style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}>
            <Link to={`/map?allergen=${this.state.allergen}&keyword=${chainId}`}>
              <Button center={true} preventDefault={false} text="Find on Nima map"/>
            </Link>
          </div>
        </div>
      </div>
    )
  }

  render() {
    console.log("PROPS: ", this.props)
    const { detail } = this.props.chain;
    const { popular_chains } = this.props.chain;
    const { allergen, showChain } = this.state;
    const mainImage = _.get(detail, "image_url");
    const backgroundImage = mainImage ? `${mainImage.split('.jpg')[0]}_blur.jpg` : "https://s3-us-west-2.amazonaws.com/nima-prod-web-assets/chain_assets/default.jpg";
    const chainReviewCount = _.get(detail, 'chain_reviews.length', 0);
    const chainExperienceScore = _.get(detail, `chain_scores.${allergen}.experience_score`);
    const chainReviews = this.getChainReviews();
    const glutenPercent = _.get(detail, 'chain_scores.gluten.allergen_free_percentage');
    const peanutPercent = _.get(detail, 'chain_scores.peanut.allergen_free_percentage');
    const allergenInfo = _.get(detail, 'allergy_statement_url');
    const headquartersLoc = _.get(detail, 'headquarters_city');
    const foundedDate = _.get(detail, 'year_established');
    return (
      <div className="nm-layout" style={{background: 'white'}}>
        {
          !detail && this.props.loader.isLoading === 0 &&
          <div className="nm-layout">
            <div className="nm-spacer-48"/>
            <h2 className="nm-layout nm-text-center">
              Sorry!  We don't have any details for this chain...
              <br/>
              Please check back in the future.
            </h2>
            <div className="nm-spacer-48"/>
            <h2 className="nm-layout nm-text-center">
              Popular Chains:
            </h2>
            <div className="nm-spacer-12"/>
            <div className="nm-card__section--content nm-card__center nm-layout__gray" style={{padding: 24}}>
              {
                popular_chains && popular_chains.length > 0 &&
                popular_chains.map((chain_info) => {
                  return (
                    <a href={`/map/chains/${chain_info.id}`}>
                      {chain_info.name}
                      <br/>
                    </a>
                  )
                })
              }
            </div>
            <div className="nm-spacer-48"/>
          </div>
        }
        {
          detail &&
          <div className="nm-layout">
            <RestaurantHeader
              starKey={`chain-detail-star-${detail.chain_id}`}
              experienceScore={chainExperienceScore}
              reviewCount={chainReviewCount}
              allergen={this.state.allergen}
              name={detail.name}
              mainImage={mainImage}
              backgroundImage={backgroundImage} />
            <div className="nm-spacer-12"/>
            <div className="nm-spacer-12"/>
            <div className="nm-clear"/>
            <div className="nm-layout nm-layout__max-center nm-layout__max-center--large nm-mobile-padding-clear">
              <div className="nm-sidebar__small">
                <div className="nm-layout nm-ticket nm-ticket__full">
                  {
                    detail && chainReviewCount !== 0 && showChain &&
                    <p className="nm-ticket nm-ticket__full nm-ticket__chain" style={{ fontStyle: 'italic', padding: 12, marginBottom: 12, fontSize: 12 }}>
                      Chain reviews are Nima tests conducted on items from that brand/restaurant chain by the Nima team.
                      Tests were not conducted at a specific location.
                      <br/>
                      <br/>
                      <span>Warning:</span> high volume chain restaurants have a higher risk of cross contamination. Please use caution. Nima cannot test hydrolyzed wheat, soy sauce or alcohol.
                    </p>
                  }
                  <div className="nm-layout nm-layout__gray" style={{padding: 12}}>
                    <h4 className="nm-layout nm-tablet-lg-and-down-hidden">
                      Details
                    </h4>
                    <p className="nm-layout nm-gluten nm-bold">
                      {(glutenPercent || glutenPercent === 0) ? parseInt(glutenPercent) : "? "}% Gluten Free
                    </p>
                    <p className="nm-layout nm-peanut nm-bold">
                      {(peanutPercent || peanutPercent === 0) ? parseInt(peanutPercent) : "? "}% Peanut Free
                    </p>
                    {
                      foundedDate &&
                      <div className="nm-layout nm-bold">
                        Founded {foundedDate}
                      </div>
                    }
                    {
                      headquartersLoc &&
                      <div className="nm-layout nm-bold">
                        HQ: {headquartersLoc}
                      </div>
                    }
                    <div className="nm-layout">
                      <div className="nm-spacer-12"/>
                      <p className="nm-layout" style={{ fontSize: 12 }}>
                        Contribute on the Nima app!
                      </p>
                      <div className="nm-spacer-12"/>
                      <a href="https://play.google.com/store/apps/details?id=com.nimasensor.android.nima&hl=en">
                        <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/google-store.png`}/>
                      </a>
                      <div className="nm-spacer-12"/>
                      <a href="https://itunes.apple.com/us/app/nima/id1150645786?mt=8">
                        <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/apple-store.png`}/>
                      </a>
                    </div>
                  </div>
                  <div className="nm-clear"/>
                </div>
              </div>
              <div className="nm-layout nm-layout__main-with-2sidebars">
                <div className="nm-spacer-12 nm-tablet-lg-and-up-hidden"/>
                { this.renderInfoBoxes(allergenInfo, detail.chain_id) }
                <div className="nm-ticket__wrap">
                  <h4 className="nm-layout nm-tablet-lg-and-up-hidden" style={{padding: 12}}>
                    Reviews
                  </h4>
                  <div className="nm-layout nm-flex nm-flex--row nm-flex__space-between" style={{alignItems: 'flex-end', padding: 12}}>
                    {
                      chainReviewCount !== 0 &&
                      <div>
                        <h3 style={{marginBottom: 6}}>
                          See Chain Reviews
                          <Tooltip text="Chain reviews are Nima tests conducted on items from that brand/restaurant chain by the Nima team. Tests were not conducted at a specific location. Results are intended to gove you an idea of the brand’s overall performance." />
                        </h3>
                        <div style={{width: 120}}>
                          <Toggle
                            color={YELLOW}
                            leftText="Hide"
                            rightText="Show"
                            isActive={showChain}
                            onClick={this.handleChainToggleClick}/>
                          <div className="nm-clear"/>
                        </div>
                      </div>
                    }
                  </div>
                  <div className="nm-layout">
                    {
                      detail && chainReviewCount !== 0 && showChain &&
                      <div className="nm-bar" style={{padding: 24, background: "rgba(246,246,246,0.9)"}}>
                        <div className="nm-layout nm-layout__max-center">
                          <h2 className="nm-layout nm-bold">
                            {detail.name} chain reviews
                          </h2>
                          <div className="nm-layout nm-text-center" style={{fontSize: 16}}>
                            {
                              chainExperienceScore &&
                              <StarRating
                                key={`chain-detail-star-${detail.id}`}
                                idPrefix="chain_detail"
                                startColor={ALLERGEN_COLOR[this.state.allergen]}
                                stroke={ALLERGEN_COLOR[this.state.allergen]}
                                percent={100 * chainExperienceScore / 5 }
                                style={{width: 20, height: 20, marginRight: 5}}/>
                            }
                            {` ${chainReviewCount} chain reviews`}
                          </div>
                          <div className="nm-clear"></div>
                        </div>
                      </div>
                    }
                    <div className="nm-clear"/>
                    <div className="nm-layout">
                      <div className="nm-ticket__wrap" style={{filter: this.state.hasAccepted ? "none" : "blur(5px)"}}>
                        {
                          chainReviews.length > 0 && showChain &&
                          chainReviews.map((review, index) => {
                            return (
                              <DetailReviewTicket
                                review={review}
                                index={index}
                                allergen={this.state.allergen}
                                isChain={true}/>
                            )
                          })
                        }
                      </div>
                      {
                        chainReviews.length > 0 && showChain && !this.state.hasAccepted &&
                        <div className="nm-layout nm-flex" style={{position: 'absolute'}}>
                          <div className="nm-spacer-48"/>
                          <FormCard title={`${detail.name} Chain Reviews`}>
                            <div className="nm-layout nm-text-center">
                              These chain reviews are Nima tests conducted on items from a specific brand/restaurant chain by the Nima team. Tests were not conducted at a specific location.
                              <br></br>
                              When eating at a specific chain restaurant location, we always recommend that you research ahead of time, talk with restaurant staff, and test your meal before you eat.
                              <div className="nm-spacer-12"/>
                              <Button center={true} style={{maxWidth: 200}} text="I Understand" onClick={this.handleAcceptClick}/>
                              <div className="nm-clear"/>
                              <div className="nm-spacer-12"/>
                              <div className="nm-clear"/>
                              <div className="nm-ticket__chain" style={{padding: 12}}>
                              Warning: high volume chain restaurants have a higher risk of cross contamination. Please use caution. Nima cannot test hydrolyzed wheat, soy sauce or alcohol.
                              </div>
                            </div>
                          </FormCard>
                        </div>
                      }
                    </div>
                  </div>
                </div>
              </div>
              <div className="nm-sidebar__small nm-sidebar__small--right">
                <div className="nm-layout nm-ticket nm-ticket__full">
                  <div className="nm-layout nm-layout__gray nm-flex" style={{padding: 12}}>
                    <h4 className="nm-layout nm-text-center" style={{fontSize: 22}}>
                      Contribute your reviews on our app!
                    </h4>
                    <div className="nm-spacer-12"/>
                    <a href="https://play.google.com/store/apps/details?id=com.nimasensor.android.nima&hl=en">
                      <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/google-store.png`}/>
                      <div className="nm-spacer-12"/>
                    </a>
                    <a href="https://itunes.apple.com/us/app/nima/id1150645786?mt=8">
                      <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/apple-store.png`}/>
                      <div className="nm-spacer-12"/>
                    </a>
                  </div>
                  <div className="nm-clear"/>
                </div>
                <div className="nm-spacer-12"/>
                <div className="nm-layout nm-ticket nm-ticket__full">
                  <div className="nm-layout nm-layout__gray nm-flex" style={{padding: 12}}>
                    <h4 className="nm-layout nm-text-center" style={{fontSize: 22}}>
                      Learn more about Nima
                    </h4>
                    <div className="nm-spacer-12"/>
                    <EmailSignUpForm onSubmit={this.handleSignUpSubmit}/>
                  </div>
                  <div className="nm-clear"/>
                </div>
                <div className="nm-spacer-12"/>
                <div className="nm-layout nm-ticket nm-ticket__full nm-border">
                  <div className="nm-layout" style={{padding: 12}}>
                    <h3 className="nm-layout nm-text-center">
                      Shop Now
                    </h3>
                    <h4 className="nm-layout nm-text-center">
                      Gluten Starter Kit
                    </h4>
                    <img src={`${process.env.PUBLIC_URL}/images/gluten-starter-kit.png`} className="nm-layout nm-image" />
                    <Link to="/shop/products/nima-starter-kit">
                      <Button className="nm-btn__cancel" center={true} style={{maxWidth: 200}} text="Buy Now" preventDefault={false}/>
                    </Link>
                    <div className="nm-bar nm-bar__line"/>
                    <h4 className="nm-layout nm-text-center">
                      Peanut Starter Kit
                    </h4>
                    <img src={`${process.env.PUBLIC_URL}/images/peanut-starter-pack.png`} className="nm-layout nm-image" />
                    <Link to="/shop/products/peanut-starter-kit">
                      <Button className="nm-btn__cancel" center={true} style={{maxWidth: 200}} text="Buy Now" preventDefault={false}/>
                    </Link>
                    <div className="nm-spacer-48"/>
                    <Link to="/shop/products/peanut-starter-kit" className="nm-text-center nm-layout nm-link" style={{fontSize: 20}}>
                      See all products
                    </Link>
                    <div className="nm-spacer-12"/>
                  </div>
                  <div className="nm-clear"/>
                </div>
              </div>
              <div className="nm-spacer-48"/>
              {
                popular_chains &&
                <PopularChains popularChains={popular_chains} allergen={this.state.allergen}/>
              }
              <div className="nm-clear"/>
            </div>
          </div>
        }
        <div className="nm-spacer-48"/>
        <div className="nm-spacer-48"/>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    chain: state.chain,
    loader: state.loader
  })
)(PublicChainDetail);
