import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import queryString from 'query-string';

import EmailPreferencesForm from '../Components/Forms/EmailPreferencesForm';
import IterableActions from '../Actions/IterableActions';


class EmailPreferences extends Component {
  constructor() {
    super();
    this.handleUnsubscribeAll = this.handleUnsubscribeAll.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const body = this.createBody();
    this.props.dispatch(IterableActions.getUserByEmail(body));
  }

  createBody(messageTypes) {
    const { location, match } = this.props;
    return ({
      email: _.get(location, 'search') ? queryString.parse(location.search).email : null,
      hashId: _.get(match, 'params.email_hash', null),
      messageTypes
    });
  }

  handleUnsubscribeAll() {
    let messageTypes = this.props.iterable.messageTypes.map((item) => ({
      id: item.id,
      type_is_subscribed: false
    }));
    const body = this.createBody(messageTypes);
    this.props.dispatch(IterableActions.updateSubscriptions(body));
  }

  handleSubmit(values) {
    let messageTypes = Object.keys(values).map((key) => ({
      id: parseInt(key.split('_')[1]),
      type_is_subscribed: values[key]
    }));
    const body = this.createBody(messageTypes);
    this.props.dispatch(IterableActions.updateSubscriptions(body));
  }

  render() {
    return (
      <div>
        <EmailPreferencesForm
          onSubmit={this.handleSubmit}
          handleUnsubscribeAll={this.handleUnsubscribeAll}
        />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    iterable: state.iterable
  })
)(EmailPreferences);
