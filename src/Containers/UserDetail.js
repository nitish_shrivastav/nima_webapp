import React, { Component } from 'react';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Toggle from '../Components/Toggle';
import TabNav from '../Components/TabNav';
import ToolTipModal from '../Components/ToolTipModal';
import ProfileBox from '../Components/ProfileBox';
import ProfileSidebar from '../Components/ProfileSidebar';
import RestaurantFilterForm from '../Components/Forms/RestaurantFilterForm';
import ReviewRow from '../Components/ReviewRow';
import ReviewActions from '../Actions/ReviewActions';
import ReadingActions from '../Actions/ReadingActions';
import Map from '../Components/Map';

const navItems = [
  "My Reviews",
  "My Tests",
  "My Bookmarks"
]

class UserDetail extends Component {
  constructor() {
    super();
    this.handleViewToggleClick = this.handleViewToggleClick.bind(this);
    this.handleFeedTypeTabClick = this.handleFeedTypeTabClick.bind(this);
    this.filterSubmit = this.filterSubmit.bind(this);
    this.state = {
      viewToggleIsActive: false,
      feedTypeTabIndex: 0
    }
  }

  componentWillMount() {
    this.props.dispatch(ReviewActions.list());
    this.props.dispatch(ReadingActions.list());
  }

  handleViewToggleClick() {
    this.setState({viewToggleIsActive: !this.state.viewToggleIsActive});
  }

  handleFeedTypeTabClick(index) {
    this.setState({feedTypeTabIndex: index});
  }

  getTabNavText(index) {
    return navItems[index];
  }

  filterSubmit(values) {
    let newQueryParams = queryString.parse(this.props.location.search);
    if (values.glutenFree) {
      newQueryParams.allergen = "gluten";
    }
    else if (values.peanutFree) {
      newQueryParams.allergen = "peanut";
    }
    this.props.history.push({
      search: `?${queryString.stringify(newQueryParams)}`
    });
    this.props.dispatch(ReviewActions.list(newQueryParams));
  }

  render() {
    return (
      <div className="nm-layout">
        <Link to="/app" className="nm-link">Back</Link>
        {
          this.props.auth.sso &&
          <ProfileBox user={this.props.auth.sso}/>
        }
        <div className="nm-spacer-12"></div>
        <div className="nm-spacer-12"></div>
        <ProfileSidebar />
        <div className="nm-block nm-block__main">
          <TabNav
            activeIndex={this.state.feedTypeTabIndex}
            onItemClick={this.handleFeedTypeTabClick}
            getTabNavText={this.getTabNavText}
            items={[
              this.props.review.list.length,
              this.props.reading.list.length,
              "0"
            ]}>
          </TabNav>
          <ToolTipModal>
            <RestaurantFilterForm onSubmit={this.filterSubmit}/>
          </ToolTipModal>
          <div className="nm-layout nm-layout__bar"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>

          <h4 className="nm-layout nm-text-center">
            My Drafts
          </h4>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>

          <div className="nm-spacer-12"></div>
          <p className="nm-layout nm-text-center nm-error" style={{fontStyle: 'italic', fontSize: 14}}>
            Finish your reviews to help build the Nima Community! 
          </p>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-feed">
            <img className="nm-image nm-feed__image" src="https://cdn.shopify.com/s/files/1/1344/6725/products/image_sensor2x_preview_small.jpeg?v=1508961411"/>
            <div className="nm-feed__header">
              <h3 className="nm-layout nm-feed__title">
                Tartine Bakery
              </h3>
              <p className="nm-layout nm-feed__location">
                Sacramento St. San Francisco, CA
              </p>
            </div>
          </div>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-layout nm-layout__bar"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-layout">
            <div className="nm-layout__max-center" style={{maxWidth: 260}}>
              <Toggle
                onClick={this.handleViewToggleClick}
                isActive={this.state.viewToggleIsActive}
                leftText="List View"
                rightText="Map View" />
              <div className="nm-clear"></div>
            </div>
            <div className="nm-spacer-12"></div>
            <h4 className="nm-layout nm-text-center">
              Restaurant Reviews
            </h4>
            {
              this.state.viewToggleIsActive &&
              <Map markers={[]} />
            }
            {
              !this.state.viewToggleIsActive &&
              this.props.review.list.map((item, index) => {
                let reviewTitle = ""
                if (item.type_ === "restaurant_dish") {
                  reviewTitle = item.place_id
                }
                return (
                  <ReviewRow title={reviewTitle}/>
                )
              })
            }
          </div>



        </div>

      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    review: state.review,
    reading: state.reading
  })
)(UserDetail);
