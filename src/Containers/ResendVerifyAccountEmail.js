import React, { Component } from 'react';
import { connect } from 'react-redux';
import AuthActions from '../Actions/AuthActions';
import ResendVerifyAccountEmailForm from '../Components/Forms/ResendVerifyAccountEmailForm';

class ResendVerifyAccountEmail extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    this.setState({ resentEmail: false });
  }

  handleSubmit(values) {
    this.setState({ resentEmail: false });
    this.props.dispatch(AuthActions.resendVerificationEmail(values))
    .then((res) => {
      if (res) {
        this.setState({ resentEmail: true });
      }
    })
    .catch();
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
          {
            this.state.resentEmail &&
            <div className="nm-layout">
              <div className="nm-spacer-48"/>
              <div className="nm-card">
                <p className="nm-bar">
                  <img
                    className="nm-image nm-relative"
                    style={{width: 20, display: 'inline-block', marginRight: 10, top: 4}}
                    src={`${process.env.PUBLIC_URL}/images/greenCheck.png`}/>
                    Resent verification email!
                </p>
                <div className="nm-card__center nm-flex__column-pad">
                  <h1 className="nm-margin-clear nm-text-center">
                    Check your email.
                  </h1>
                  <div className="nm-spacer-12"></div>
                  <div className="nm-spacer-12"></div>
                  <p className="nm-text-center nm-base__p" style={{maxWidth: 400}}>
                    You should have a email in your inbox. Click on the link in that email
                    to verify your Nima account.
                  </p>
                  <div className="nm-clear"></div>
                  <div className="nm-spacer-48"></div>
                </div>
              </div>
              <div className="nm-clear"/>
            </div>
          }
          <div className="nm-layout">
            <div className="nm-spacer-48"/>
            <div className="nm-spacer-48"/>
            <div className="nm-card">
              <div className="nm-spacer-48"></div>
              <div className="nm-card__center nm-flex__column-pad">
                <h1 className="nm-margin-clear nm-text-center">
                  Enter email address
                </h1>
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
                <p className="nm-margin-clear nm-text-center">
                  Send yourself a verification email.
                </p>
                <div className="nm-spacer-12"></div>
                <ResendVerifyAccountEmailForm onSubmit={this.handleSubmit}/>
                <div className="nm-clear"></div>
                <div className="nm-spacer-48"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(ResendVerifyAccountEmail);
