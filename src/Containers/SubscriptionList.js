import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import { connect } from 'react-redux';
import SubscriptionActions from '../Actions/SubscriptionActions';
import ProductActions from '../Actions/ProductActions';
import Modal from '../Components/Modal';
import SubscriptionCard from '../Components/SubscriptionCard';
import FormCard from '../Components/FormCard';
import SubscriptionForm from '../Components/Forms/SubscriptionForm';
import SubscriptionDiscountForm from '../Components/Forms/SubscriptionDiscountForm';
import { DARK_GREY } from '../Constants/colors';
import OrLine from '../Components/OrLine';
import Button from '../Components/Button';

class SubscriptionsList extends Component {
  constructor() {
    super();
    this.handleCancelClick = this.handleCancelClick.bind(this);
    this.handleDiscountSubmit = this.handleDiscountSubmit.bind(this);
    this.updateSubscription = this.updateSubscription.bind(this);
    this.handleConfirmDeleteClick = this.handleConfirmDeleteClick.bind(this);
    this.handleSkipClick = this.handleSkipClick.bind(this);
    this.handleUnskipClick = this.handleUnskipClick.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleCancelCloseClick = this.handleCancelCloseClick.bind(this);
    this.handleDiscountModalClick = this.handleDiscountModalClick.bind(this);
    this.handleDiscountCloseClick = this.handleDiscountCloseClick.bind(this);
    this.handleRemoveDiscountClick = this.handleRemoveDiscountClick.bind(this);
  }

  componentWillMount() {
    const { client, dispatch } = this.props;
    dispatch(ProductActions.list(client));
    dispatch(SubscriptionActions.list());
    this.setState({
      showEditModal: false,
      showCancelModal: false,
      showDiscountModal: false
    });
  }

  handleDiscountModalClick(subscription) {
    this.props.dispatch(SubscriptionActions.setDetail(subscription));
    this.setState({ showDiscountModal: true });
  }

  handleDiscountCloseClick() {
    this.setState({ showDiscountModal: false });
  }

  handleCancelClick(subscription) {
    this.props.dispatch(SubscriptionActions.setDetail(subscription));
    this.setState({ showCancelModal: true });
  }

  handleConfirmDeleteClick() {
    this.props.dispatch(SubscriptionActions.delete(this.props.subscription.detail.order))
    .then((isSuccessful) => {
      if (isSuccessful) {
        this.setState({ showCancelModal: false  });
        this.props.dispatch(SubscriptionActions.list());
      }
    })
    .catch();
  }

  handleSkipClick(subscription, skip) {
    subscription = JSON.parse(JSON.stringify(subscription));
    subscription.skips.push({ created_at: new Date(skip).getTime().toString() })
    if (subscription.discounts && subscription.discounts[0]) {
      subscription.discount = subscription.discounts[0];
    }
    this.updateSubscription(subscription.order, subscription)
  }

  handleUnskipClick(subscription, skip) {
    let body = {
      ...subscription,
      skips: subscription.skips.map((subscriptionSkip) => {
        if (!moment.utc(parseInt(subscriptionSkip.created_at)).isSame(moment.utc(skip), 'day')) {
          return {
            created_at: subscriptionSkip.created_at
          }
        }
      }).filter(item => item)
    }
    if (subscription.discounts && subscription.discounts[0]) {
      body.discount = subscription.discounts[0];
    }
    this.updateSubscription(subscription.order, body)
  }

  updateSubscription(order, body) {
    this.props.dispatch(SubscriptionActions.update(order, body))
    .then((isUpdated) => {
      if (isUpdated) {
        this.props.dispatch(SubscriptionActions.list());
        this.props.dispatch(SubscriptionActions.setDetail(null));
        this.setState({ showEditModal: false });
      }
    }).catch();
  }

  handleEditClick(subscription) {
    if (subscription) {
      this.props.dispatch(SubscriptionActions.setDetail(subscription));
    }
    this.setState({ showEditModal: !this.state.showEditModal });
  }

  handleEditSubmit(values) {
    const { detail } = this.props.subscription;
    let body = {
      ...detail,
      send_day: values.send_day,
      quantity: values.quantity,
      shipping_interval_frequency: values.shipping_interval_frequency
    }
    if (values.send_day !== detail.send_day) {
      body.skips = [];
    }
    this.updateSubscription(detail.order, body);
  }

  handleDiscountSubmit(values) {
    let body = {
      ...this.props.subscription.detail,
      discount: values.discount
    }
    this.props.dispatch(SubscriptionActions.updateDiscount(this.props.subscription.detail.order, body))
    .then((isUpdated) => {
      if (isUpdated) {
        this.props.dispatch(SubscriptionActions.list());
        this.props.dispatch(SubscriptionActions.setDetail(null));
        this.setState({ showDiscountModal: false });
      }
    }).catch();
  }

  handleCancelCloseClick() {
    this.setState({ showCancelModal: false  });
  }

  handleRemoveDiscountClick(subscription) {
    this.props.dispatch(SubscriptionActions.deleteDiscount(subscription.order))
    .then((isDeleted) => {
      if (isDeleted) {
        this.props.dispatch(SubscriptionActions.list());
        this.props.dispatch(SubscriptionActions.setDetail(null));
      }
    }).catch();
  }

  render() {
    return (
      <div className="nm-layout">
      {
        this.props.subscription.list.length > 0 && this.props.product.list.length > 0 &&
        <div className="nm-layout">
          <h3 className="nm-layout">
            Manage Subscriptions
          </h3>
          {
            this.props.subscription.list.length === 0 && this.props.loader.isLoading === 0 &&
            <h2 className="nm-layout" style={{borderBottom: `1px solid ${DARK_GREY}`, paddingBottom: 6}}>
              No subscriptions found.
            </h2>
          }
          <div className="nm-layout" style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap'}}>
            {
              this.props.subscription.list.map((subscription, index) => {
                return (
                  <SubscriptionCard
                    subscription={subscription}
                    onUnskipClick={this.handleUnskipClick}
                    onSkipClick={this.handleSkipClick}
                    products={this.props.product.list}
                    onEditClick={this.handleEditClick}
                    onCancelClick={this.handleCancelClick}
                    onDiscountClick={this.handleDiscountModalClick}
                    onRemoveDiscountClick={this.handleRemoveDiscountClick}
                    index={index} />
                )
              })
            }
          </div>
          
        </div>
      }
      {
        this.state.showEditModal && this.props.subscription.detail &&
        <Modal>
          <FormCard onCloseClick={this.handleEditClick} title="Edit Subscription" cardStyle={{overflow: 'visible'}}>
            <SubscriptionForm onSubmit={this.handleEditSubmit}/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showCancelModal && this.props.subscription.detail &&
        <Modal>
          <FormCard
            onCloseClick={this.handleCancelCloseClick}
            title="Delete Subscription"
            subtitle="Are you sure you want to delete this subscription?">
            <Button className="nm-btn__cancel" onClick={this.handleConfirmDeleteClick} center={true} style={{maxWidth: 300}} text="Delete"/>
            <div className="nm-spacer-12"/>
            <OrLine backgroundColor="white"/>
            <div className="nm-spacer-12"/>
            <Button onClick={this.handleCancelCloseClick} center={true} style={{maxWidth: 300}} text="Cancel"/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showDiscountModal && this.props.subscription.detail &&
        <Modal>
          <FormCard
            onCloseClick={this.handleDiscountCloseClick}
            title="Edit Subscription Discount"
            error={this.props.subscription.error}>
            <SubscriptionDiscountForm onSubmit={this.handleDiscountSubmit}/>
          </FormCard>
        </Modal>
      }
      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    subscription: state.subscription,
    product: state.product,
    loader: state.loader
  })
)(SubscriptionsList);
