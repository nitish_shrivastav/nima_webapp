import React, { Component } from 'react';
import queryString from 'query-string';
import { Marker } from 'react-google-maps';
import _ from 'lodash';
import { connect } from 'react-redux';
import { formValueSelector, change, submit } from 'redux-form';
import PlaceActions from '../Actions/PlaceActions';
import PlaceTicket from '../Components/PlaceTicket';
import PublicSearchForm from '../Components/Forms/PublicSearchForm';
import SidebarPublicSearchForm from '../Components/Forms/SidebarPublicSearchForm';
import FormatHelper from '../Util/FormatHelper';
import { MAP_ICONS } from '../Constants/mapIcons';
import { ALLERGEN_COLOR } from '../Constants/colors';
import MapInteractor from '../Util/MapInteractor';
import Map from '../Components/Map';
import MapRestaurantInfoBox from '../Components/MapRestaurantInfoBox';

const PUBLIC_PLACE_SEARCH = "publicPlaceSearch";

class PublicPlaceMap extends Component {
  constructor() {
    super();
    this.mapInteractor = new MapInteractor();
    this.handleMapToggleClick = this.handleMapToggleClick.bind(this);
    this.handleSidebarToggleClick = this.handleSidebarToggleClick.bind(this);
    this.handleResearchClick = this.handleResearchClick.bind(this);
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleLocationSearchChange = this.handleLocationSearchChange.bind(this);
    this.handleCityAutocompleteClick = this.handleCityAutocompleteClick.bind(this);
    this.handleLocationBlur = this.handleLocationBlur.bind(this);
    this.handlePlaceBlur = this.handlePlaceBlur.bind(this);
    this.handlePlaceSearchChange = this.handlePlaceSearchChange.bind(this);
    this.handlePlaceSearchSubmit = this.handlePlaceSearchSubmit.bind(this);
    this._getMapParams = this._getMapParams.bind(this);
    this._loadDefaultPlace = this._loadDefaultPlace.bind(this);
    this.debounceLocationSearchChange = _.debounce(this.handleLocationSearchChange, 300); 
    this.debouncePlaceSearchChange = _.debounce(this.handlePlaceSearchChange, 300); 
    this.setMapRef = this.setMapRef.bind(this);
    this.renderMarker = this.renderMarker.bind(this);
    this.handleSelectedClear = this.handleSelectedClear.bind(this);
    this.mapRef = undefined;
  }

  componentWillMount() {
    let params = queryString.parse(this.props.location.search);
    let listViewIsHidden = params.view && params.view === "map";
    window.scrollTo(0, 0);
    this.setState({
      listViewIsHidden: listViewIsHidden,
      mapIsHidden: !listViewIsHidden,
      sidebarIsHidden: false,
      selectedPlace: null,
      allergen: params.allergen ? params.allergen : "gluten",
      autocompleteCities: [],
      autocompletePlaces: [],
      defaultCenter: null,
      defaultZoom: params.zoom ? parseInt(params.zoom) : 15
    });
    let mapUserGeo = localStorage.getItem("userCenter");
    if (!mapUserGeo && navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        let coordParams = {
          lat: parseFloat(position.coords.latitude),
          lng: parseFloat(position.coords.longitude),
          radius: 1000
        }
        localStorage.setItem("userCenter", JSON.stringify(coordParams));
      })
    }
    if (mapUserGeo && !params.lat && !params.lng & !params.radius) {
      mapUserGeo = JSON.parse(mapUserGeo);
      this.setState({ 
        position: mapUserGeo,
        defaultCenter: mapUserGeo
      });
      this.props.dispatch(PlaceActions.publicSearch(mapUserGeo));
      this.props.dispatch(PlaceActions.reverseGeoCodeCity(mapUserGeo));
    }
    else if (params.lat && params.lng && params.radius) {
      this.setState({ 
        defaultCenter: {
          lat: parseFloat(params.lat),
          lng: parseFloat(params.lng)
        }
      });
      this.props.dispatch(PlaceActions.reverseGeoCodeCity({lat: params.lat, lng: params.lng}));
      this.props.dispatch(PlaceActions.publicSearch(params));
    }
    else {
      this._loadDefaultPlace(params);
    }
  }

  componentDidMount() {
    let params = queryString.parse(this.props.location.search);
    document.title = `Nima Restaurants Map | ${params.keyword ? params.keyword : "Near You"}`;
    if (params.keyword && params.keyword !== "") {
      this.props.dispatch(change(PUBLIC_PLACE_SEARCH, "search", params.keyword));
    }
    if (window.zE) {
      window.zE('webWidget', 'hide');
    }
  }

  componentWillUnmount() {
    if (window.zE) {
      window.zE('webWidget', 'show');
    }
  }

  _loadDefaultPlace(params) {
      params = {
        ...params,
        lng: "-122.419417",
        lat: "37.774929",
        radius: 1000
      }
      this.props.dispatch(PlaceActions.publicSearch(params));
  }

  handleMarkerClick(place) {
    const { location } = place.geometry;
    let nextLat = location.lat;
    if (this.state.mapIsHidden) {
      let bounds = this.mapRef.getBounds();
      let height = window.innerHeight - 44;
      let slimMapCenterPercent = (410 / height) / 2;
      let latDiff = bounds.getNorthEast().lat() - bounds.getSouthWest().lat();
      nextLat = nextLat - ((0.5 - slimMapCenterPercent) * latDiff);
    }
    this.mapRef.panTo({lat: nextLat, lng: location.lng });
    this.setState({ selectedPlace: place });
  }

  handleMapToggleClick() {
    let newQueryParams = queryString.parse(this.props.location.search);
    if (this.state.mapIsHidden) {
      window.scrollTo(0, 0);
      this.setState({ mapIsHidden: false });
      newQueryParams.view = "map";
      setTimeout(() => {
        this.setState({ listViewIsHidden: true });
      }, 400);
    }
    else {
      newQueryParams.view = "list";
      this.setState({ listViewIsHidden: false });
      setTimeout(() => {
        this.setState({ mapIsHidden: true });
      }, 100);
    }
    this.props.history.replace({
      search: `?${queryString.stringify(newQueryParams)}`
    })
  }

  handleSidebarToggleClick() {
    this.setState({ sidebarIsHidden: !this.state.sidebarIsHidden });
  }

  _getMapParams() {
    const center = this.mapRef.getCenter();
    const northEastBound = this.mapRef.getBounds().getNorthEast();
    return {
      lng: center.lng(),
      lat: center.lat(),
      radius: MapInteractor.calculateRadius(center.lat(), center.lng(), northEastBound.lat(), northEastBound.lng())
    }
  }
  
  handleResearchClick() {
    this.props.dispatch(submit(PUBLIC_PLACE_SEARCH));
  }

  handleLocationSearchChange(event) {
    this.mapInteractor.searchCities(event.target.value)
    .then((predictions) => {
      this.setState({ autocompleteCities: predictions });
    })
    .catch((err) => {
      console.log('err', err)
    });
  }

  handlePlaceSearchChange(event) {
  }

  handlePlaceSearchSubmit(values) {
    this.setState({ selectedPlace: null });
    let params = {
      ...this._getMapParams(),
      allergen: values.allergen,
      keyword: values.search && values.search !== "" ? values.search : undefined,
      zoom: this.mapRef.getZoom()
    }
    this.setState({ allergen: values.allergen });
    document.title = `Nima restaurants map | ${params.keyword ? params.keyword : "Near You"}`;
    this.props.dispatch(PlaceActions.publicSearch(params));
    let qs = queryString.parse(this.props.location.search);
    qs = Object.assign(qs, params);
    this.props.history.push({
      search: `?${queryString.stringify(qs)}`
    })
  }

  setMapRef(mapRef) {
    this.mapRef = mapRef;
  }

  handleCityAutocompleteClick(city) {
    this.mapInteractor.getLatLngByPlaceId(city.place_id)
    .then((res) => {
      if (res && res[0]) {
        let lat = res[0].geometry.location.lat();
        let lng = res[0].geometry.location.lng();
        this.props.dispatch(change(PUBLIC_PLACE_SEARCH, "where", city.description.replace(", USA", "")));
        this.mapRef.panTo({lat: lat, lng: lng});
        this.handleLocationBlur();
        let params = this._getMapParams();
        params.lat = lat;
        params.lng = lng;
        params.allergen = this.props.allergen;
        if (this.props.search && this.props.search !== "") {
          params.keyword = this.props.search;
        }
        this.props.dispatch(PlaceActions.publicSearch(params));
      }
    })
    .catch((err) => {
      console.log('err', err)
    });
  }

  handleLocationBlur() {
    this.setState({ autocompleteCities: [] });
  }

  handlePlaceBlur() {
    this.setState({ autocompletePlaces: [] });
  }

  handleSelectedClear() {
    this.setState({ selectedPlace: null });
  }

  renderMarker(point, markerIndex) {
    const { selectedPlace, allergen } = this.state;
    if (point.geometry) {
      let mapIconKey = "unselected";
      let allergenIconKey = "default"; 
      const pointLat = point.geometry.location.lat;
      const pointLng = point.geometry.location.lng;
      let zIndex = 10;
      if (selectedPlace && pointLat === selectedPlace.geometry.location.lat && pointLng === selectedPlace.geometry.location.lng) {
        mapIconKey = "selected";
        zIndex = 20;
      }
      if (_.get(point, `counts.${allergen}`) && _.get(point, `scores.${allergen}.allergen_free_percentage`) >= 70) {
        allergenIconKey = allergen;
      }
      else if (point.counts && point.counts[allergen] ) {
        allergenIconKey = "under70";
      }
      else if (_.get(point, `chain_counts.${allergen}`)) {
        allergenIconKey = `${allergen}_chain`;
      }
      return (
        <Marker
          key={`${point.geometry.location.lat}_${point.geometry.location.lng}_${markerIndex}`}
          onClick={() => this.handleMarkerClick(point)}
          zIndex={zIndex}
          position={{ 
            lat: point.geometry.location.lat,
            lng: point.geometry.location.lng
          }}
          icon={{
            url: MAP_ICONS[allergenIconKey][mapIconKey],
            scaledSize: MAP_ICONS[allergenIconKey].scaled_size
          }}/>
      )
    }
  }

  render() {
    const { mapIsHidden, listViewIsHidden, sidebarIsHidden, selectedPlace } = this.state;
    const { place, allergen } = this.props;
    return (
      <div className="nm-layout" style={{background: 'white'}}>
        <div className={`nm-map__sidebar nm-tablet-and-down-hidden ${!sidebarIsHidden && listViewIsHidden ? "" : "tuck"}`}> 
          <div className="nm-map__sidebar-search-wrap nm-layout">
            {
              listViewIsHidden &&
              <div
                className="nm-map__sidebar-toggle-button"
                onClick={this.handleSidebarToggleClick}
                style={{background: ALLERGEN_COLOR[allergen]}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/icons/back.png`}
                  style={{transform: sidebarIsHidden ? "rotateZ(180deg)" : "rotateZ(0deg)" }}
                  className="nm-image nm-image__icon--mini nm-image__contain"/>
              </div>
            }
            <div className="nm-layout">
              <SidebarPublicSearchForm
                onCityAutocompleteClick={this.handleCityAutocompleteClick}
                onPlaceBlur={this.handlePlaceBlur}
                onLocationBlur={this.handleLocationBlur}
                onPlaceChange={this.debouncePlaceSearchChange}
                autocompletePlaces={this.state.autocompletePlaces}
                autocompleteCities={this.state.autocompleteCities}
                onLocationChange={this.debounceLocationSearchChange}
                onSubmit={this.handlePlaceSearchSubmit}
                form="publicPlaceSearch"/>
            </div>
          </div>
          <div className="nm-layout nm-map__sidebar-content">
            {
              this.props.place.list.map((item) => {
                return (
                  <PlaceTicket 
                    key={`map-place-${item.id}`}
                    onTicketClick={this.handleMarkerClick}
                    allergen={this.state.allergen}
                    placeItem={item}
                    isSidebar={true}/>
                )
              })
            }
          </div>
        </div>
        <div className="nm-layout" style={{position: 'absolute'}}>
          <Map
            renderInfoBoxContent={() => {
              return (
                <MapRestaurantInfoBox
                  allergen={this.state.allergen}
                  selectedPlace={selectedPlace} />
              )
            }}
            onClick={this.handleSelectedClear}
            renderMarker={this.renderMarker}
            defaultZoom={this.state.defaultZoom}
            defaultCenter={this.state.defaultCenter}
            allergen={this.state.allergen}
            setMapRef={this.setMapRef}
            center={this.state.center}
            position={this.state.position}
            selectedLocation={this.state.selectedPlace}
            onResearchClick={this.handleResearchClick}
            markers={this.props.place.list}/>
        </div>
        <div className={`nm-layout nm-map__wrapper ${mapIsHidden ? "collapsed" : ""}`}>
        </div>
        <div className={`nm-layout nm-map__search-wrap ${mapIsHidden ? "" : "hide-desktop-search"}`}>
          <div className="nm-layout nm-layout__max-center nm-tablet-padding-clear">
            <PublicSearchForm
              onCityAutocompleteClick={this.handleCityAutocompleteClick}
              autocompleteCities={this.state.autocompleteCities}
              autocompletePlaces={this.state.autocompletePlaces}
              onPlaceBlur={this.handlePlaceBlur}
              onLocationBlur={this.handleLocationBlur}
              onPlaceChange={this.debouncePlaceSearchChange}
              onLocationChange={this.debounceLocationSearchChange}
              onSubmit={this.handlePlaceSearchSubmit}
              form="publicPlaceSearch"/>
            <div className="nm-clear"/>
          </div>
        </div>
        <div className="nm-layout">
          <div
            className={`nm-btn__ball nm-btn__ball--docked ${mapIsHidden ? "" : "active"}`}
            style={{allergen}}
            onClick={this.handleMapToggleClick}
            style={{
              transform: mapIsHidden ? "rotateZ(-90deg)" : "rotateZ(90deg)",
              background: ALLERGEN_COLOR[allergen]
            }}>
            <img src={`${process.env.PUBLIC_URL}/images/icons/left-arrow.png`} className="nm-image nm-image__icon nm-image__contain"/>
          </div>            
          <div className={`nm-layout ${listViewIsHidden ? "nm-hidden" : ""}`}>
            <div className="nm-bar nm-bar__title nm-map__bar-mobile" style={{marginBottom: 0}}>
              <div className="nm-layout nm-layout__max-center nm-tablet-padding-clear">
                <h2 className="nm-layout">
                  <span className="nm-bold">
                    {this.props.allergen ? `${FormatHelper.uppercaseFirstLetter(this.props.allergen)} ` : "... " }
                    Free Restaurants
                   </span> near 
                   {_.get(place, 'location.city') && _.get(place, 'location.province_short') ? ` ${place.location.city}, ${place.location.province_short}` : " you"}
                </h2>
                <div className="nm-clear"></div>
              </div>
            </div>
            <div className="nm-clear"></div>
            <div className="nm-layout nm-map__list">
              <div className="nm-layout nm-layout__max-center nm-mobile-padding-clear">
                <div className="nm-spacer-48 nm-tablet-and-down-hidden"/>
                <div className="nm-clear"/>
                <div className="nm-layout">
                  {
                    this.props.place.list.map((item) => {
                      return (
                        <PlaceTicket
                          key={`place-${item.id}`}
                          allergen={this.state.allergen}
                          onTicketClick={this.handleMarkerClick}
                          placeItem={item}/>
                      )
                    })
                  } 
                </div>
                <div className="nm-spacer-48"/>
                <div className="nm-spacer-48"/>
                <div className="nm-clear"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const selector = formValueSelector(PUBLIC_PLACE_SEARCH);

export default connect(
  (state) => ({
    place: state.place,
    allergen: selector(state, "allergen"),
    search: selector(state, "search")
  })
)(PublicPlaceMap);
