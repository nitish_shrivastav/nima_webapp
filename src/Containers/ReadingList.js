import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ReadingActions from '../Actions/ReadingActions';


class ReadingList extends Component {
  componentWillMount() {
    this.props.dispatch(ReadingActions.list());
  }


  render() {
    return (
      <div className="nm-layout">
        <Link to="/app" className="nm-link">Back</Link>
        <p className="nm-layout">
          {JSON.stringify(this.props.reading.list)}
        </p>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    reading: state.reading
  })
)(ReadingList);
