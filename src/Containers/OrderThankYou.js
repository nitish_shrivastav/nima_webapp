import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import NavBar from '../Components/NavBar';
import Footer from '../Components/Footer';
import StorefrontStorage from '../Util/StorefrontStorage';
import CheckoutActions from '../Actions/CheckoutActions';
import VoucherActions from '../Actions/VoucherActions';

class OrderThankYou extends Component {

  componentWillMount() {
    let email = localStorage.getItem('email');
    let firstName = localStorage.getItem('firstName');
    let needsToVerify = localStorage.getItem('needsToVerify');
    StorefrontStorage.clearCheckoutId();
    this.props.dispatch(CheckoutActions.clearCheckout());
    this.props.dispatch(VoucherActions.clear());
    this.setState({
      activateText: email ? ` at ${email}.` : ".",
      needsToVerify: needsToVerify === "true",
      firstName: firstName
    });
  }

  render() {
    return (
      <div className="nm-layout">
        <NavBar secondary={true}/>
        <div className="nm-clear"/>
        <div className="nm-layout nm-layout__gray">
          <div className="nm-spacer-48" />
          <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
            <div className="nm-card">
              <p className="nm-bar">
                <img
                  className="nm-image nm-relative"
                  style={{width: 20, display: 'inline-block', marginRight: 10, top: 4}}
                  src={`${process.env.PUBLIC_URL}/images/greenCheck.png`}/>
                Order placed — Thank you for your order!
              </p>
              <div className="nm-spacer-48"></div>
              {
                this.state.needsToVerify &&
                <div className="nm-card__center nm-flex__column-pad">
                  <h1 className="nm-margin-clear nm-text-center" style={{fontWeight: '300'}}>
                    Woohoo!
                  </h1>
                  {
                    _.get(this, 'props.order.detail.order_id') &&
                    <h5 style={{ marginTop: '6px', textAlign: "center"}}>
                      Order ID: {this.props.order.detail.order_id}<br></br>
                      *Your confirmation email will contain a shortened order name (e.g. #1234)
                    </h5>
                  }
                  <div className="nm-spacer-12"></div>
                  <div className="nm-spacer-12"></div>
                  <p className="nm-text-center nm-base__p" style={{maxWidth: 400}}>
                    Creating an account saves you time,
                    and allows you to access all of our app data!
                  </p>
                  <div className="nm-spacer-12"></div>
                  <div className="nm-spacer-12"></div>
                  <div className="nm-clear"></div>
                  <div className="nm-spacer-48"></div>
                </div>
              }
              {
                !this.state.needsToVerify &&
                <div className="nm-card__center nm-flex__column-pad">
                  <h1 className="nm-margin-clear nm-text-center" style={{fontWeight: '300'}}>
                    Thanks {this.state.firstName}!
                  </h1>
                  <h1 className="nm-text-center nm-margin-clear">
                    Your order is on its way!
                  </h1>
                  {
                    _.get(this, 'props.order.detail.order_id') &&
                    <h5 style={{ marginTop: '6px', textAlign: "center"}}>
                      Order ID: {this.props.order.detail.order_id}<br></br>
                      *Your confirmation email will contain a shortened order name (e.g. #1234)
                    </h5>
                  }
                  <div className="nm-spacer-12"></div>
                  <div className="nm-spacer-12"></div>
                  <p className="nm-text-center nm-base__p" style={{maxWidth: 400}}>
                    A confirmation email has been sent to
                    you{this.state.activateText}
                  </p>
                  <div className="nm-clear"></div>
                  <div className="nm-spacer-48"></div>
                </div>
              }
            </div>
            <div className="nm-spacer-48"></div>
            <div className="nm-card">
              <div className="nm-flex">
                <div className="nm-spacer-48"></div>
                <div className="nm-flex nm-flex__column-space nm-flex__column-pad">
                  <div className="nm-card__column">
                    <h2 className="nm-bold">
                      Share Nima with a friend
                    </h2>
                    <div className="nm-spacer-12"></div>
                    <p>
                      With every single test, Nima community members are improving food transparency
                      around the world. Help spread the word.
                    </p>
                    <div className="nm-spacer-12"></div>
                    <div>
                      <a href="#">
                        <img
                          style={{height: 26, marginRight: 8}}
                          className="nm-image nm-image__app"
                          src={`${process.env.PUBLIC_URL}/images/facebook.png`}/>
                      </a>
                      <a href="#">
                        <img
                          style={{height: 26, marginRight: 8}}
                          className="nm-image nm-image__app"
                          src={`${process.env.PUBLIC_URL}/images/twitter.png`}/>
                      </a>
                      <a href="#">
                        <img
                          style={{height: 26, marginRight: 8}}
                          className="nm-image nm-image__app"
                          src={`${process.env.PUBLIC_URL}/images/pinterest.png`}/>
                      </a>
                    </div>
                    <div className="nm-spacer-12"></div>
                  </div>
                  <div className="nm-card__column nm-card__column--self">
                    <h2 className="nm-bold">
                      Improve your Nima skills
                    </h2>
                    <div className="nm-spacer-12"></div>
                    <ul className="nm-base__ul">
                      <li className="nm-base__ul__li-blue"><a href="https://help.nimasensor.com/hc/en-us/sections/115002790628-Videos" target="_blank" rel="noopener">Watch testing tip videos →</a></li>
                      <li className="nm-base__ul__li-blue"><a href="https://help.nimasensor.com/hc/en-us/articles/115009840588-How-can-I-prevent-problems-when-testing-" target="_blank" rel="noopener">Read how to avoid errors →</a></li>
                      <li className="nm-base__ul__li-blue"><a href="/community-stories" target="_blank" rel="noopener">See what others are testing →</a></li>
                    </ul>
                  </div>
                </div>
                <div className="nm-spacer-48"></div>
                <div className="nm-bar__border"></div>
                  <div className="nm-spacer-48"></div>
                  <div className="nm-spacer-48"></div>
                  <div className="nm-flex nm-flex__column-space nm-flex__column-pad">
                    <div className="nm-card__column">
                      <img
                        className="nm-card__column"
                        src="https://nimasensor.com/wp-content/themes/nimatheme/graphics/image-ios-map.png" />
                    </div>
                    <div style={{ maxWidth: 400 }}>
                      <h2 className="nm-bold">What’s tested gluten-free or peanut-free near you? </h2>
                      <div className="nm-spacer-12"></div>
                      <p>
                        Search over 10,000 Nima-tested restaurants and packaged foods.
                        Contribute your test results to help the community.
                      </p>
                      <div className="nm-spacer-12"></div>
                      <div>
                        <a href="https://play.google.com/store/apps/details?id=com.nimasensor.android.nima&hl=en">
                          <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/google-store.png`}/>
                        </a>
                        <a href="https://itunes.apple.com/us/app/nima/id1150645786?mt=8">
                          <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/apple-store.png`}/>
                        </a>
                      </div>
                    </div>
                  </div>
              </div>
              <div className="nm-spacer-48"></div>
            </div>
            <div className="nm-spacer-48"></div>
            <div className="nm-clear"/>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    order: state.order,
    voucher: state.voucher
  })
)(OrderThankYou);
