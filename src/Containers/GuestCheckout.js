import React, { Component } from 'react';
import { debounce } from 'lodash';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { change } from 'redux-form';
import queryString from 'query-string';
import _ from 'lodash';

import BraintreeInteractor from '../Util/BraintreeInteractor';
import CheckoutActions from '../Actions/CheckoutActions';
import LoaderActions from '../Actions/LoaderActions';
import OrderActions from '../Actions/OrderActions';
import SegmentActions from '../Actions/SegmentActions';
import CheckoutHelper from '../Util/CheckoutHelper';
import constants from '../Constants/segment';
import { PAYPAL, STRIPE, AFFIRM } from '../Constants/payment';

import CheckoutFooter from '../Components/CheckoutFooter';
import FormHeader from '../Components/FormHeader';
import StripeInteractor from '../Util/StripeInteractor';
import CheckoutForm from '../Components/Forms/CheckoutForm';


const initialState = {
  showCardModal: false
}


const CHECKOUT_FORM = 'checkout';

class GuestCheckout extends Component {
  constructor() {
    super();
    this.stripeInteractor = new StripeInteractor(window.Stripe);
    this.checkoutHelper = new CheckoutHelper(window);
    this.handleRateClick = this.handleRateClick.bind(this);
    this.handleShippingChange = this.handleShippingChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePaymentMethodClick = this.handlePaymentMethodClick.bind(this);

    this.validateDiscount = this.validateDiscount.bind(this);
    this.handleShippingUpdate = this.handleShippingUpdate.bind(this);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.startAffirmCheckout = this.startAffirmCheckout.bind(this);

    this.debounceShippingUpdate = debounce(this.handleShippingUpdate, 300);
    this.debounceEmailUpdate = debounce(this.handleEmailUpdate, 300);
    this.payPalListener = this.payPalListener.bind(this);
    this.removeInvalidCheckoutItemsFromShopifyCheckout = this.removeInvalidCheckoutItemsFromShopifyCheckout.bind(this);
    this.onBlurCreditCardField = this.onBlurCreditCardField.bind(this);
  }

  componentWillMount() {
    const { client, match, dispatch } = this.props;
    dispatch(SegmentActions.track(constants.CHECKOUT_STARTED));
    this.setState(initialState);
    dispatch(CheckoutActions.retrieveCheckout(client, match.params.cart_id));
    //What do we do here because we will no longer have a logged in view here
    //dispatch(SegmentActions.identify(storefrontCustomerId, email));
    this.payPalListener();
  }

  componentDidMount() {
    let qp = queryString.parse(this.props.location.search);
    if (qp.method && [STRIPE, AFFIRM, PAYPAL].indexOf(qp.method) > -1) {
      this.props.dispatch(change(CHECKOUT_FORM, 'provider', qp.method));
    }
  }

  handlePaymentMethodClick(paymentMethod) {
    let newQueryParams = queryString.parse(this.props.location.search);
    newQueryParams.method = paymentMethod;
    this.props.history.push({
      search: `?${queryString.stringify(newQueryParams)}`
    });
    this.props.dispatch(change(CHECKOUT_FORM, 'payment_source', null));
  }

  //debounceEmailUpdate
  handleEmailUpdate(mutationVariables) {
    this.props.dispatch(CheckoutActions.updateCheckoutEmail(this.props.client, mutationVariables));
  }

  handleEmailChange(email) {
    let mutationVariables = {
      email: email,
      id: this.props.match.params.cart_id
    }
    this.debounceEmailUpdate(mutationVariables)
  }

  handleShippingChange(address, lastName) {
    this.debounceShippingUpdate(address, lastName);
  }

  handleShippingUpdate(address, lastName) {
    let mutationVariables = {
      input: {
        ...address,
        lastName: lastName ? lastName : "Default"
      },
      id: this.props.match.params.cart_id
    }
    this.props.dispatch(CheckoutActions.updateCheckout(this.props.client, mutationVariables, (err, checkout) => {
      if (_.get(checkout, 'availableShippingRates.ready') && _.get(checkout, 'availableShippingRates.shippingRates.length')) {
        this.handleRateClick(checkout.availableShippingRates.shippingRates[0].handle);
      }
    }))
  }

  handleRateClick(handle) {
    let mutationVariables = {
      id: this.props.match.params.cart_id,
      handle: handle
    }
    this.props.dispatch(CheckoutActions.updateCheckoutShippingLine(this.props.client, mutationVariables, (err, checkout) => {
      if (checkout) {
        this.props.dispatch(change('checkout', 'shipping_method', checkout.shippingLine.handle));
      }
    }));
  }

  validateDiscount(code, email) {
    let body = {
      checkout_id: this.props.match.params.cart_id,
      email: email,
      discount_codes: [{ id: code.toLowerCase() }]
    }
    this.props.dispatch(CheckoutActions.validateDiscount(body));
  }

  handleSubmitForm(values) {
    if (values.provider === AFFIRM) {
       this.startAffirmCheckout(values);
    }
    else {
      this.props.dispatch(LoaderActions.setIsLoading(1, "Securing your payment.."))
      this.checkoutHelper.getFormValues(this.props.match.params.cart_id, values)
      .then((body) => {
        this.props.dispatch(LoaderActions.setIsLoading(-1));
        this.props.dispatch(OrderActions.createOrder(body, (err, res) => {
          if (res) this.setState({ redirect: true });
        }))
      })
      .catch((err) => {
        this.props.dispatch(LoaderActions.setIsLoading(-1))
        if (err && err.message) {
          this.props.dispatch(OrderActions.setError(err.message));
        }
        else {
          this.props.dispatch(OrderActions.setError("Credit card error"));
        }
      })
    }
  }

  startAffirmCheckout(values) {
    const { match, checkout, dispatch } = this.props;
    dispatch(SegmentActions.track(constants.PAYMENT_INFO_ENTERED, { payment_method: AFFIRM }));
    let body = CheckoutHelper.getAffirmFormValues(match.params.cart_id, values, checkout);
    window.affirm.checkout(body);
    window.affirm.checkout.open({
      onFail: () => {
        console.log('failed affirm')
      },
      onSuccess: (affirmRes) => {
        values.payment_source = affirmRes.checkout_token;
        this.checkoutHelper.getFormValues(this.props.match.params.cart_id, values)
        .then((submitValues) => {
          this.props.dispatch(OrderActions.createOrder(submitValues, (err, res) => {
            if (res) this.setState({ redirect: true });
          }))
        })
        .catch((err) => {
          if (err && err.message) {
            this.props.dispatch(OrderActions.setError(err.message));
          }
          else {
            this.props.dispatch(OrderActions.setError(JSON.stringify(err)));
          }
        })
      }
    });
  }

  //We get a callback when pay pal is all done
  payPalListener() {
    BraintreeInteractor.getPayPalNonce(window, '.paypal-button', (err, res) => {
      if (err) {
        console.log('err in payPalListener', err)
      }
      else if (res) {
        let email = res.details.email;
        this.handleEmailChange(email, "email");
        let address = {
          address1: res.details.shippingAddress.line1,
          address2: res.details.shippingAddress.line2,
          city: res.details.shippingAddress.city,
          province: res.details.shippingAddress.state,
          country: res.details.shippingAddress.countryCode,
          zip: res.details.shippingAddress.postalCode
        }
        let updatedValues = {
          first_name: res.details.firstName,
          last_name: res.details.lastName,
          email: email,
          payment_source: res.nonce,
          shipping_address: address 
        }
        this.props.dispatch(change('checkout', 'first_name', updatedValues.first_name));
        this.props.dispatch(change('checkout', 'last_name', updatedValues.last_name));
        this.props.dispatch(change('checkout', 'email', updatedValues.email));
        this.props.dispatch(change('checkout', 'payment_source', updatedValues.payment_source));
        this.props.dispatch(change('checkout', 'shipping_address', updatedValues.shipping_address));
        this.handleShippingUpdate(address, res.details.lastName);
      }
    })
  }

  removeInvalidCheckoutItemsFromShopifyCheckout(validLineItems) {
    console.log('validCheckoutItems', validLineItems)
    let mutationVariables = {
      checkoutId: this.props.checkout.detail.id,
      lineItems: validLineItems
    }
    this.props.dispatch(CheckoutActions.checkoutLineItemsReplace(this.props.client, mutationVariables));
  }

  onBlurCreditCardField() {
    this.props.dispatch(SegmentActions.track(constants.PAYMENT_INFO_ENTERED, { payment_method: STRIPE }));
  }

  render() {
    const { redirect } = this.state;
    if (redirect) {
      return (<Redirect to="/shop/order-thanks"/>);
    }
    else {
      return (
        <div className="nm-layout">
          <div className="nm-clear"/>
          <div className="nm-layout nm-layout__gray">
            <div className="nm-spacer-48" />
            <FormHeader />
            <div className="nm-layout__max-center nm-layout__max-center--medium-gray nm-layout__max-center--mobile-support">
              <CheckoutForm
                onSubmit={this.handleSubmitForm}
                onEmailChange={this.handleEmailChange}
                onSubmitDiscountValidate={this.validateDiscount}
                onShippingAddressChange={this.handleShippingChange}
                onPaymentMethodClick={this.handlePaymentMethodClick}
                onShippingRateClick={this.handleRateClick}
                onRemoveItemsClick={this.removeInvalidCheckoutItemsFromShopifyCheckout}
                onBlurCreditCardField={this.onBlurCreditCardField}/>
              <div className="nm-clear"/>
              <div className="nm-layout">
                <div className="nm-spacer-48" />
                <CheckoutFooter/>
              </div>
              <div className="nm-clear"></div>
            </div>
            <div className="nm-spacer-48" />
            <div className="nm-spacer-48" />
            <div className="nm-spacer-48" />
          </div>
        </div>
      );
    }
  }
}

export default connect(
  (state) => ({
    checkout: state.checkout,
    order: state.order
  })
)(GuestCheckout);
