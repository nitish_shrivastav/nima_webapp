import React, { Component } from 'react';
import queryString from 'query-string';
import _ from 'lodash';
import { connect } from 'react-redux';
import PlaceActions from '../Actions/PlaceActions';
import ImageHelper from '../Util/ImageHelper';
import FormatHelper from '../Util/FormatHelper';
import { PEANUT_ASSETS, GLUTEN_ASSETS } from '../Constants/allergen';
import { ALLERGEN_COLOR, RED, YELLOW } from '../Constants/colors';
import Toggle from '../Components/Toggle';
import Tooltip from '../Components/Tooltip';
import Button from '../Components/Button';
import FormCard from '../Components/FormCard';
import DetailReviewTicket from '../Components/DetailReviewTicket';
import RestaurantHeader from '../Components/RestaurantHeader';


class PublicPlaceDetail extends Component {
  constructor() {
    super();
    this.handleAllergenClick = this.handleAllergenClick.bind(this);
    this.getAllergenReviews = this.getAllergenReviews.bind(this);
    this.getChainReviews = this.getChainReviews.bind(this);
    this.handleChainToggleClick = this.handleChainToggleClick.bind(this);
    this.handleAcceptClick = this.handleAcceptClick.bind(this);
  }

  componentWillMount() {
    let params = queryString.parse(this.props.location.search);
    this.props.dispatch(PlaceActions.publicRetrieve(this.props.match.params.place_id))
    .then((placeDetail) => {
      if (placeDetail) {
        document.title = `Nima Reviews | ${placeDetail.name}`;
      }
    })
    .catch();
    if (!params.allergen || (params.allergen !== "peanut" && params.allergen !== "gluten")) {
      params.allergen = "gluten";
      this.props.history.replace({
        search: `?${queryString.stringify(params)}`
      });
    }
    this.setState({
      allergen: params.allergen,
      showChain: true,
      hasAccepted: localStorage.getItem("chainAccepted")
    });
  }

  handleAcceptClick() {
    localStorage.setItem("chainAccepted", new Date().getTime())
    this.setState({ hasAccepted: true });
  }

  handleAllergenClick(allergen) {
    if (this.state.allergen !== allergen) {
      this.setState({ allergen: allergen });
      let params = queryString.parse(this.props.location.search);
      params.allergen = allergen;
      this.props.history.replace({
        search: `?${queryString.stringify(params)}`
      })
    }
  }

  getAllergenReviews() {
    const { detail } = this.props.place;
    if (detail && detail.reviews) {
      return detail.reviews.filter((item) => _.get(item, 'readings[0].allergen') === this.state.allergen);
    }
    else {
      return [];
    }
  }

  getChainReviews() {
    const { detail } = this.props.place;
    if (detail && detail.chain_reviews) {
      return detail.chain_reviews.filter((item) => _.get(item, 'readings[0].allergen') === this.state.allergen);
    }
    else {
      return [];
    }
  }

  handleChainToggleClick() {
    this.setState({ showChain: !this.state.showChain });
  }

  render() {
    const { detail } = this.props.place;
    const { allergen, showChain } = this.state;
    const backgroundImage = (detail && detail.photos && detail.photos[0]) ? ImageHelper.getGoogleMapsUrl(detail.photos[0].photo_reference, 1300) : null;
    const reviewCount = detail && detail.reviews ? detail.reviews.length.toString() : "0";
    const chainReviewCount = _.get(detail, 'chain_reviews.length', 0);
    const experienceScore = _.get(detail, `scores.${allergen}.experience_score`);
    const chainAllergenScore = _.get(detail, `chain_scores.${allergen}.allergen_free_percentage`);
    const reviews = this.getAllergenReviews();
    const chainReviews = this.getChainReviews();
    const glutenPercent = _.get(detail, 'scores.gluten.allergen_free_percentage');
    const peanutPercent = _.get(detail, 'scores.peanut.allergen_free_percentage');
    return (
      <div className="nm-layout" style={{background: 'white'}}>
        {
          detail &&
          <RestaurantHeader
            starKey={`place-detail-star-${detail.id}`}
            experienceScore={experienceScore}
            formattedAddress={detail.formatted_address}
            reviewCount={reviewCount}
            allergen={this.state.allergen}
            name={detail.name}
            backgroundImage={backgroundImage} />
        }
        <div className="nm-spacer-12"/>
        <div className="nm-clear"></div>
        <div className="nm-spacer-12"/>
        <div className="nm-clear"/>
        <div className="nm-layout nm-layout__max-center nm-layout__max-center--large nm-mobile-padding-clear">
          <div className="nm-clear"/>
          <div className="nm-layout nm-ticket__side">
            {
              chainReviewCount !== 0 && showChain &&
              <p className="nm-ticket nm-ticket__full nm-ticket__chain" style={{ fontStyle: 'italic', padding: 12, marginBottom: 12, fontSize: 12 }}>
                Chain reviews are Nima tests conducted on items from that brand/restaurant chain by the Nima team.
                Tests were not conducted at a specific location.
                <br/>
                <br/>
                <span>Warning:</span> high volume chain restaurants have a higher risk of cross contamination. Please use caution. Nima cannot test hydrolyzed wheat, soy sauce or alcohol.
              </p>
            }
            <div className="nm-layout nm-layout__gray" style={{padding: 12}}>
              <h4 className="nm-layout nm-tablet-lg-and-down-hidden">
                Details
              </h4>
              <p className="nm-layout nm-gluten nm-bold">
                {(glutenPercent || glutenPercent === 0) ? parseInt(glutenPercent) : "? "}% Gluten Free
              </p>
              <p className="nm-layout nm-peanut nm-bold">
                {(peanutPercent || peanutPercent === 0) ? parseInt(peanutPercent) : "? "}% Peanut Free
              </p>
              {
                detail && detail.price_level &&
                <p className="nm-layout">
                  Price: {"$".repeat(detail.price_level)}
                </p>
              }
              <div className="nm-spacer-12"/>
              <div className="nm-layout">
                <img className="nm-image__inline--icon" src={`${process.env.PUBLIC_URL}/images/icons/hours.png`}/>
                {(detail && detail.opening_hours && detail.opening_hours.open_now) ? "Open Now" : "Closed Now" }
              </div>
              <div className="nm-spacer-12"/>
              {
                detail && detail.opening_hours && detail.opening_hours.weekday_text &&
                detail.opening_hours.weekday_text.map((time) => {
                  return (
                    <p key={time} className="nm-layout" style={{fontSize: 12}}>
                      {time}
                    </p>
                  )
                })
              }
              <div className="nm-spacer-12"/>
              {
                detail && detail.international_phone_number && detail.international_phone_number !== "unknown" &&
                <div className="nm-layout">
                  <a href={`tel:${detail.international_phone_number}`} className="nm-link" target="_blank">
                    <img className="nm-image__inline--icon" src={`${process.env.PUBLIC_URL}/images/icons/phone.png`}/>
                    {detail.international_phone_number}
                  </a>
                </div>
              }
              {
                detail && detail.website &&
                <div className="nm-layout">
                  <a href={detail.website} className="nm-link" target="_blank">
                    <img className="nm-image__inline--icon" src={`${process.env.PUBLIC_URL}/images/icons/website.png`}/>
                    Restaurant website
                  </a>
                </div>
              }
              <div className="nm-layout">
                <div className="nm-spacer-12"/>
                <p className="nm-layout" style={{ fontSize: 12 }}>
                  Contribute on the Nima app!
                </p>
                <div className="nm-spacer-12"/>
                <a href="https://play.google.com/store/apps/details?id=com.nimasensor.android.nima&hl=en">
                  <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/google-store.png`}/>
                </a>
                <div className="nm-spacer-12"/>
                <a href="https://itunes.apple.com/us/app/nima/id1150645786?mt=8">
                  <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/apple-store.png`}/>
                </a>
              </div>
            </div>
            <div className="nm-clear"/>
          </div>
          <div className="nm-layout nm-layout__main-with-sidebar">
            <div className="nm-spacer-12 nm-tablet-lg-and-up-hidden"/>
            <div className="nm-ticket__wrap">
              <h4 className="nm-layout nm-tablet-lg-and-up-hidden" style={{padding: 12}}>
                Reviews
              </h4>
              <div className="nm-layout nm-flex nm-flex--row nm-flex__space-between" style={{alignItems: 'flex-end', padding: 12}}>
                <div>
                  {
                    chainReviewCount !== 0 &&
                    <div>
                      <h3 style={{marginBottom: 6}}>
                        See Chain Reviews
                        <Tooltip text="Chain reviews are Nima tests conducted on items from that brand/restaurant chain by the Nima team. Tests were not conducted at a specific location. Results are intended to give you an idea of the brand’s overall performance." />
                      </h3>
                      <div style={{width: 120}}>
                        <Toggle
                          color={YELLOW}
                          leftText="Hide"
                          rightText="Show"
                          isActive={showChain}
                          onClick={this.handleChainToggleClick}/>
                        <div className="nm-clear"/>
                      </div>
                    </div>
                  }
                </div>
                <div>
                  <div
                    className="nm-search__map-allergen-icon"
                    onClick={() => this.handleAllergenClick("gluten")}>
                    <img className="nm-image nm-image__contain" src={GLUTEN_ASSETS[this.state.allergen]} style={{height: '100%'}}/>
                  </div>
                  <div
                    className="nm-search__map-allergen-icon"
                    onClick={() => this.handleAllergenClick("peanut")}>
                    <img className="nm-image nm-image__contain" src={PEANUT_ASSETS[this.state.allergen]} style={{height: '100%'}}/>
                  </div>
                </div>
              </div>
              {
                detail && detail.reviews && reviews.length === 0 &&
                <div className="nm-layout">
                  <h2 className="nm-layout" style={{padding: 12}}>
                    No local reviews yet. Be the first to contribute with the Nima App.
                  </h2>
                  <div className="nm-layout">
                    <a href="https://play.google.com/store/apps/details?id=com.nimasensor.android.nima&hl=en">
                      <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/google-store.png`}/>
                    </a>
                    <a href="https://itunes.apple.com/us/app/nima/id1150645786?mt=8">
                      <img className="nm-image nm-image__app" src={`${process.env.PUBLIC_URL}/images/apple-store.png`}/>
                    </a>
                  </div>
                </div>
              }
              {
                reviews.length > 0 &&
                reviews.map((review, index) => {
                  return (
                    <DetailReviewTicket
                      review={review}
                      index={index}
                      allergen={this.state.allergen}/>
                  )
                })
              }
              <div className="nm-layout">
                {
                  detail && chainReviewCount !== 0 && showChain &&
                  <div className="nm-bar" style={{padding: 24, background: "rgba(246,246,246,0.9)"}}>
                    <div className="nm-layout nm-layout__max-center">
                      <h2 className="nm-layout nm-bold">
                        Other {detail.name} reviews
                      </h2>
                      <div className="nm-layout nm-text-center" style={{fontSize: 16}}>
                        {
                          (chainAllergenScore || chainAllergenScore === 0) &&
                          <p className="nm-layout nm-text-center nm-bold" style={{color: ALLERGEN_COLOR[this.state.allergen]}}>
                            {parseInt(chainAllergenScore)}% {FormatHelper.uppercaseFirstLetter(this.state.allergen)} Free
                          </p>
                        }
                        {` ${chainReviewCount} chain reviews`}
                      </div>
                      <div className="nm-clear"></div>
                    </div>
                  </div>
                }
                <div className="nm-clear"/>
                <div className="nm-layout">
                  <div className="nm-ticket__wrap" style={{filter: this.state.hasAccepted ? "none" : "blur(5px)"}}>
                    {
                      chainReviews.length > 0 && showChain &&
                      chainReviews.map((review, index) => {
                        return (
                          <DetailReviewTicket
                            review={review}
                            index={index}
                            allergen={this.state.allergen}
                            isChain={true}/>
                        )
                      })
                    }
                  </div>
                  {
                    chainReviews.length > 0 && showChain && !this.state.hasAccepted &&
                    <div className="nm-layout nm-flex" style={{position: 'absolute'}}>
                      <div className="nm-spacer-48"/>
                      <FormCard title={`${detail.name} Chain Reviews`}>
                        <div className="nm-layout nm-text-center">
                          These chain reviews are Nima tests conducted on items from a specific brand/restaurant chain by the Nima team. Tests were not conducted at a specific location.
                          <br></br>
                          When eating at a specific chain restaurant location, we always recommend that you research ahead of time, talk with restaurant staff, and test your meal before you eat.
                          <div className="nm-spacer-12"/>
                          <Button center={true} style={{maxWidth: 200}} text="I Understand" onClick={this.handleAcceptClick}/>
                          <div className="nm-clear"/>
                          <div className="nm-spacer-12"/>
                          <div className="nm-clear"/>
                          <div className="nm-ticket__chain" style={{padding: 12}}>
                          Warning: high volume chain restaurants have a higher risk of cross contamination. Please use caution. Nima cannot test hydrolyzed wheat, soy sauce or alcohol.
                          </div>
                        </div>
                      </FormCard>
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    place: state.place
  })
)(PublicPlaceDetail);
