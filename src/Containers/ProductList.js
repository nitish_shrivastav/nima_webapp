import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import ProductActions from '../Actions/ProductActions';
import MoneyHelper from '../Util/MoneyHelper';


class ProductList extends Component {
  componentWillMount() {
    this.props.dispatch(ProductActions.list(this.props.client));
    console.log("product list = ",this.props.product);
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
          <h1 className="nm-layout__title nm-text-center nm-gray-text nm-light-text">
            All Products
          </h1>
          {
            this.props.product.list.map((product, index) => {
              if (product.node.availableForSale && _.findIndex(product.node.collections.edges, { node: { handle: "frontpage" }}) > -1) {
                let price = product.node.priceRange.minVariantPrice.amount;
                return (
                  <Link to={`/shop/products/${product.node.handle}`} className="nm-card__third" key={`productlist-${index}`}>
                    <div className="nm-card nm-card__clear" style={{marginBottom: 12, padding: 20}}>
                      {
                        product.node.images.edges[0] &&
                        <img
                          style={{height: 300}}
                          className="nm-image nm-layout nm-image__contain"
                          src={product.node.images.edges[0].node.originalSrc} />
                      }
                      <div className="nm-spacer-12"></div>
                      <h4 className="nm-layout__sub-title-dark nm-text-center">
                        { product.node.title }
                      </h4>
                      {
                        price === product.node.priceRange.maxVariantPrice.amount &&
                        <p className="nm-layout__alt-text">
                          { MoneyHelper.moneyWithoutTrailingZeros(price) }
                        </p>
                      }
                      {
                        price !== product.node.priceRange.maxVariantPrice.amount &&
                        <p className="nm-layout__alt-text">
                          from { MoneyHelper.moneyWithoutTrailingZeros(price) }
                        </p>
                      }
                    </div>
                  </Link>
                )
              }
            })
          }
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
          <div className="nm-clear"></div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    product: state.product
  })
)(ProductList);
