import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ReviewActions from '../Actions/ReviewActions';


class ReviewList extends Component {
  componentWillMount() {
    this.props.dispatch(ReviewActions.list());
  }

  render() {
    return (
      <div className="nm-layout">
        <Link to="/app" className="nm-link">Back</Link>
        <p className="nm-layout">
          {JSON.stringify(this.props.review.list)}
        </p>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    review: state.review
  })
)(ReviewList);
