import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Page404 extends Component {
  render() {
    return (
      <div className="nm-layout">
        <div className="nm-spacer-48"></div>
        <h1 className="nm-layout nm-text-center">
          404 Page not found
        </h1>
        <div className="nm-spacer-48"></div>
        <Link to="/shop" className="nm-layout nm-text-center nm-link">
          Go home
        </Link>
        <div className="nm-spacer-48"></div>
      </div>
    );
  }
}

export default Page404;
