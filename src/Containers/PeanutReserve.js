import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReservePeanutForm from '../Components/Forms/ReservePeanutForm';
import MailListActions from '../Actions/MailListActions';

class PeanutReserve extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(body) {
    this.props.dispatch(MailListActions.add("a3cc8d5f50", body));
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support" style={{maxWidth: 550}}>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-card nm-card__block">
            <h1 className="nm-layout">
              Reserve your Nima Peanut Sensor
            </h1>
            <p className="nm-layout">
              You’ll receive an invitation to order your peanut sensor the moment it’s available.
              Note that we can ship only to USA/Canada addresses.
            </p>
            <div className="nm-spacer-12"></div>
            <div className="nm-spacer-12"></div>
            {
              this.props.mailList.successMessage &&
              <h2 className="nm-layout nm-text-center">
                Thank you!
              </h2>
            }
            {
              !this.props.mailList.successMessage &&
              <ReservePeanutForm onSubmit={this.handleSubmit} errorMessage={this.props.mailList.error}/>
            }
            <div className="nm-clear"></div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    mailList: state.mailList
  })
)(PeanutReserve);
