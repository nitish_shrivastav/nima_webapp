import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { RED } from '../Constants/colors';
import AuthActions from '../Actions/AuthActions';
import Button from '../Components/Button';

class VerifyAccount extends Component {

  componentWillMount() {
    let qp = queryString.parse(this.props.location.search);
    if (qp.user_id && qp.token) {
      this.props.dispatch(AuthActions.verify(qp));
    }
    else {
      this.props.dispatch(AuthActions.setError("Invalid tokens"));
    }
  }

  componentWillUnmount() {
    this.props.dispatch(AuthActions.clearUserError());
  }

  render() {
    return (
      <div className="nm-layout nm-layout__gray">
        <div className="nm-spacer-48"></div>
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
          {
            this.props.auth.verified &&
            <div className="nm-card">
              <p className="nm-bar">
                <img
                  className="nm-image nm-relative"
                  style={{width: 20, display: 'inline-block', marginRight: 10, top: 4}}
                  src={`${process.env.PUBLIC_URL}/images/greenCheck.png`}/>
                  Email verified!
              </p>
              <div className="nm-card__center nm-flex__column-pad">
                <h1 className="nm-margin-clear nm-text-center">
                    Woohoo!
                </h1>
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
                <p className="nm-text-center nm-base__p" style={{maxWidth: 400}}>
                  Thank you for verifying your Nima account.
                </p>
                <div className="nm-clear"></div>
                <div className="nm-spacer-48"></div>
              </div>
            </div>
          }
          {
            !this.props.auth.verified && this.props.auth.error &&
            <div className="nm-card">
              <div className="nm-spacer-48"></div>
              <div className="nm-card__center nm-flex__column-pad">
                <h1 className="nm-margin-clear nm-text-center nm-error">
                  Email validation error
                </h1>
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
                <p className="nm-layout nm-text-center">
                  If you need to resend the verification email, click the button below.
                </p>
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
                <Link to="/shop/email-verify/resend">
                  <Button center={true} text="Resend Verify Email"/>
                </Link>
                <div className="nm-spacer-12"></div>
                <div className="nm-spacer-12"></div>
                <p className="nm-text-center nm-base__p" style={{maxWidth: 400, color: RED}}>
                  {this.props.auth.error}
                </p>
                <div className="nm-clear"></div>
                <div className="nm-spacer-48"></div>
              </div>
            </div>
          }
        </div>
        <div className="nm-spacer-48"></div>
        <div className="nm-spacer-48"></div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(VerifyAccount);
