import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { formValueSelector } from 'redux-form';
import PaymentMethodActions from '../Actions/PaymentMethodActions';
import AddressActions from '../Actions/AddressActions';
import Modal from '../Components/Modal';
import FormCard from '../Components/FormCard';
import AddressFormatter from '../Util/AddressFormatter';
import { GREY, ORANGE, LIGHT_GREY, YELLOW } from '../Constants/colors';
import AddressForm from '../Components/Forms/AddressForm';
import CheckoutHelper from '../Util/CheckoutHelper';
import PaymentMethodForm from '../Components/Forms/PaymentMethodForm';
import Button from '../Components/Button';
import OrLine from '../Components/OrLine';

const newAddressSelector = formValueSelector("newAddressForm");
const editAddressSelector = formValueSelector("editAddressForm");

const dashedBoxStyle = {
  maxWidth: 300,
  minHeight: 200,
  padding: 12,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 6,
  marginRight: 20,
  fontSize: 20,
  marginBottom: 20,
  border: `2px dashed ${GREY}`
}

const boxStyle = {
  maxWidth: 300,
  padding: 12,
  borderRadius: 6,
  marginRight: 20,
  marginBottom: 20,
  border: `1px solid ${GREY}`
}

const flexStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-start',
  flexWrap: 'wrap'
}

const barStyle = {
  borderTopLeftRadius: 12,
  borderTopRightRadius: 12,
  background: LIGHT_GREY,
  borderBottom: `2px solid ${YELLOW}`,
  fontSize: 20,
  padding: 12,
  marginBottom: 24
}

class ProfileManagement extends Component {
  constructor() {
    super();
    this.handleAddressEditClick = this.handleAddressEditClick.bind(this);
    this.handleDefaultAddressClick = this.handleDefaultAddressClick.bind(this);
    this.handleEditAddressSubmit = this.handleEditAddressSubmit.bind(this);
    this.handleNewAddressSubmit = this.handleNewAddressSubmit.bind(this);
    this.handleDefaultCardClick = this.handleDefaultCardClick.bind(this);
    this.handleNewCardClick = this.handleNewCardClick.bind(this);
    this.handleNewPaymentMethodSubmit = this.handleNewPaymentMethodSubmit.bind(this);
    this.handleDeleteAddressModalCloseClick = this.handleDeleteAddressModalCloseClick.bind(this);
    this.handleAddressDelete = this.handleAddressDelete.bind(this);
    this.showDeleteAddressModal = this.showDeleteAddressModal.bind(this);
    this.handleDeleteCard = this.handleDeleteCard.bind(this);
    this.handleDeleteCardModalCloseClick = this.handleDeleteCardModalCloseClick.bind(this);
    this.checkoutHelper = new CheckoutHelper(window);
  }

  componentWillMount() {
    this.props.dispatch(PaymentMethodActions.list());
    this.props.dispatch(AddressActions.list());
    this.setState({
      showEditAddressModal: false,
      showNewAddressModal: false,
      showNewCardModal: false,
      showDeletePaymentModal: false,
      showDeleteAddressModal: false
    })
  }

  handleAddressEditClick(address) {
    this.props.dispatch(AddressActions.setDetail(address));
    this.setState({ showEditAddressModal: true });
  }

  handleAddressDelete() {
    this.props.dispatch(AddressActions.delete(this.props.address.detail.id))
    .then((success) => {
      if (success) {
        this.props.dispatch(AddressActions.setDetail(null));
        this.setState({ showDeleteAddressModal: false });
        this.props.dispatch(AddressActions.list());
      }
    })
    .catch();
  }

  handleNewCardClick() {
    this.setState({showNewCardModal: true});
  }

  handleDefaultAddressClick(addressId) {
    this.props.dispatch(AddressActions.updateDefault(addressId))
    .then((success) => {
      if (success) {
        this.props.dispatch(AddressActions.list());
      }
    })
    .catch();
  }

  handleEditAddressSubmit(values) {
    let body = AddressFormatter.formatFormAddressForApi(values, "shipping_address");
    this.props.dispatch(AddressActions.update(this.props.address.detail.id, body))
    .then((updateAddressRes) => {
      if (updateAddressRes) {
        this.setState({ showEditAddressModal: false });
        this.props.dispatch(AddressActions.list());
      }
    })
    .catch();
  }

  handleNewAddressSubmit(values) {
    let body = AddressFormatter.formatFormAddressForApi(values, "shipping_address");
    this.props.dispatch(AddressActions.create(body))
    .then((createAddressRes) => {
      if (createAddressRes) {
        this.setState({ showNewAddressModal: false });
        this.props.dispatch(AddressActions.list());
      }
    })
    .catch();
  }

  handleDefaultCardClick(sourceId) {
    this.props.dispatch(PaymentMethodActions.setDefault(sourceId))
    .then((success) => {
      if (success) {
        this.props.dispatch(PaymentMethodActions.list());
      }
    })
    .catch();
  }

  handleDeleteCard() {
    this.props.dispatch(PaymentMethodActions.delete(this.props.payment_method.detail.id))
    .then((success) => {
      if (success) {
        this.props.dispatch(PaymentMethodActions.setDetail(null));
        this.setState({ showDeletePaymentModal: false });
        this.props.dispatch(PaymentMethodActions.list());
      }
    })
    .catch();
  }

  handleNewPaymentMethodSubmit(values) {
    this.checkoutHelper.getStripeSource(values)
    .then((stripeSource) => {
      let body = {
        provider: "stripe",
        source: stripeSource.id
      }
      return this.props.dispatch(PaymentMethodActions.create(body));
    })
    .then((detail) => {
      if (detail) {
        this.props.dispatch(PaymentMethodActions.list());
        this.setState({ showNewCardModal: false });
      }
    })
    .catch((err) => {
      if (err && err.message) {
        this.props.dispatch(PaymentMethodActions.setError(err.message));
      }
    })
  }

  handleDeleteAddressModalCloseClick() {
    this.props.dispatch(AddressActions.setDetail(null));
    this.setState({ showDeleteAddressModal: false });
  }

  handleDeleteCardModalCloseClick() {
    this.props.dispatch(PaymentMethodActions.setDetail(null));
    this.setState({ showDeletePaymentModal: false });
  }

  showDeleteAddressModal(address) {
    this.props.dispatch(AddressActions.setDetail(address));
    this.setState({ showDeleteAddressModal: true });
  }

  showDeleteCardModal(paymentMethod) {
    this.props.dispatch(PaymentMethodActions.setDetail(paymentMethod));
    this.setState({ showDeletePaymentModal: true });
  }

  _sortDefaultAddess(addresses) {
    //take copy of addresses
    addresses = JSON.parse(JSON.stringify(addresses));
    
    let defaultAddressIndex = _.findIndex(addresses, { "default": true });
    if (defaultAddressIndex > -1) {
      let defaultAddress = addresses.splice(defaultAddressIndex, 1);
      addresses.unshift(defaultAddress[0]);
    }
    return addresses;
  }

  render() {
    let sortedAddresses = this._sortDefaultAddess(this.props.address.list);
    return (
      <div className="nm-layout">
        <p className="nm-layout" style={barStyle}>
          Account Info
        </p>
        <div className="nm-layout" style={flexStyle}>
          {
            this.props.auth.sso &&
            <div
              className="nm-layout"
              style={{...boxStyle, maxWidth: 430}}>
              <p className="nm-layout" style={{fontSize: 20}}>
                <span>Name: </span>
                {this.props.auth.sso.first_name} {this.props.auth.sso.last_name}
              </p>
              <p className="nm-layout" style={{fontSize: 20}}>
                <span>Email: </span>
                {this.props.auth.sso.email}
              </p>
              <div className="nm-spacer-12"/>
              <p className="nm-layout" style={{fontSize: 12, textTransform: 'italic'}}>
                Please contact Customer Support for changes to your email.
              </p>
              <div className="nm-spacer-12"/>
            </div>
          }
        </div>
        <div className="nm-spacer-48"/>
        <p className="nm-layout" style={barStyle}>
          Shipping Address
        </p>
        <div className="nm-layout" style={flexStyle}>
          {
            sortedAddresses.map((address) => {
              return (
                <div
                  className="nm-layout"
                  key={`address-profile-${address.id}`}
                  style={{...boxStyle, border: `1px solid ${address.default ? ORANGE : GREY}`}}>
                  <p className="nm-layout" style={{fontSize: 20}}>
                    {address.first_name} {address.last_name}
                  </p>
                  <p className="nm-layout" style={{fontSize: 20}}>
                    {address.address1}
                  </p>
                  <p className="nm-layout" style={{fontSize: 20}}>
                    {address.address2}
                  </p>
                  <p className="nm-layout" style={{fontSize: 20}}>
                    {`${address.city}, ${address.province_code} ${address.zip}`}
                  </p>
                  <p className="nm-layout" style={{fontSize: 20}}>
                    {address.country_code}
                  </p>
                  <div className="nm-spacer-12"/>
                  <div className="nm-layout" style={{textAlign: 'right'}}>
                    {
                      !address.default &&
                      <span className="nm-link" style={{float: 'left'}} onClick={() => this.handleDefaultAddressClick(address.id)}>
                        Make Default?
                      </span>
                    }
                    {
                      address.default &&
                      <span style={{fontSize: 12}}>
                        (Default Address)
                      </span>
                    }
                    {
                      !address.default &&
                      <span className="nm-error" onClick={() => this.showDeleteAddressModal(address)} style={{fontSize: 12}}>
                        Delete
                      </span>
                    }
                    {` | `}
                    <span className="nm-link" onClick={() => this.handleAddressEditClick(address)} style={{fontSize: 12}}>
                      Edit
                    </span>
                  </div>
                </div>
              )    
            })
          }
        <div
          className="nm-layout nm-link"
          onClick={() => {
            this.setState({ showNewAddressModal: true });
          }}
          style={dashedBoxStyle}>
            <div style={{ fontSize: 50, lineHeight: 1, color: 'inherit' }}>+</div>
            Add Address
        </div>
      </div>
      <div className="nm-spacer-48"/>
      <div className="nm-spacer-48"/>
      <p className="nm-layout" style={barStyle}>
        Payment Method
      </p>
      <p className="nm-layout">
        If you would like to delete your default credit card please contact support@nimasensor.com.
        Please note if you have a premium membership or active auto refill subscription you need to have a
        credit card on file.
      </p>
      <div className="nm-spacer-48"/>
      <div className="nm-layout" style={flexStyle}>
        {
          _.get(this.props, 'payment_method.list.sources.data', []).map((paymentMethod, index) => {
            const card = CheckoutHelper.getCreditCardDetails(paymentMethod);
            return (
              <div
                className="nm-layout"
                key={`address-profile-${paymentMethod.id}`}
                style={{...boxStyle, border: `1px solid ${index === 0 ? ORANGE : GREY}`}}>
                <img
                  src={`${process.env.PUBLIC_URL}/images/credit_cards/${card.brand.toLowerCase().replace(' ', '_')}.svg`}
                  className="nm-image"
                  style={{width: 60}}
                  alt="credit_card_image"/>
                <div className="nm-spacer-12"/>
                <p className="nm-layout" style={{fontSize: 20}}>
                  {card.brand}: Ending {card.last4}
                </p>
                <p className="nm-layout" style={{fontSize: 20}}>
                  Expiration: {card.exp_month}/{card.exp_year}
                </p>
                <div className="nm-spacer-12"/>
                {
                  this.props.payment_method.list.default_source === paymentMethod.id  &&
                  <span style={{float: 'left'}}>
                    (Default Card)
                  </span>
                }
                {
                  this.props.payment_method.list.default_source !== paymentMethod.id &&
                  <div className="nm-layout">
                    <span className="nm-link" style={{float: 'left'}} onClick={() => this.handleDefaultCardClick(paymentMethod.id)}>
                      Make Default?
                    </span>
                    <span className="nm-error" style={{float: 'right'}} onClick={() => this.showDeleteCardModal(paymentMethod)}>
                      Delete
                    </span>
                  </div>
                }
              </div>
            )
          })
        }
        <div
          className="nm-layout nm-link"
          onClick={() => {
            this.setState({ showNewCardModal: true });
          }}
          style={dashedBoxStyle}>
            <div style={{ fontSize: 50, lineHeight: 1, color: 'inherit' }}>+</div>
            Add Card
        </div>
      </div>
      {
        this.state.showNewAddressModal &&
        <Modal>
          <FormCard
            onCloseClick={() => {this.setState({showNewAddressModal: false})}}
            error={this.props.address.error}
            title="Add a new address.">
            <AddressForm
              label="Shipping Address (USA/CAN)"
              onSubmit={this.handleNewAddressSubmit}
              submitText="Add address"
              form="newAddressForm"
              name="shipping_address"
              isCollapsed={false}
              selector={newAddressSelector}/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showEditAddressModal &&
        <Modal>
          <FormCard
            onCloseClick={() => {this.setState({showEditAddressModal: false})}}
            error={this.props.address.error}
            title="Edit address.">
            <AddressForm
              label="Shipping Address (USA/CAN)"
              onSubmit={this.handleEditAddressSubmit}
              submitText="Update Address"
              form="editAddressForm"
              name="shipping_address"
              isCollapsed={false}
              isEdit={true}
              selector={editAddressSelector}/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showNewCardModal &&
        <Modal>
          <FormCard
            onCloseClick={() => {this.setState({showNewCardModal: false})}}
            error={this.props.payment_method.error}
            title="Add Payment Method">
            <PaymentMethodForm form="newPaymentMethod" onSubmit={this.handleNewPaymentMethodSubmit}/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showDeleteAddressModal && this.props.address.detail &&
        <Modal>
          <FormCard
            onCloseClick={this.handleDeleteAddressModalCloseClick}
            title="Delete Address?"
            subtitle="Are you sure you want to delete this address?">
            <Button className="nm-btn__cancel" onClick={this.handleAddressDelete} center={true} style={{maxWidth: 300}} text="Delete"/>
            <div className="nm-spacer-12"/>
            <OrLine backgroundColor="white"/>
            <div className="nm-spacer-12"/>
            <Button onClick={this.handleDeleteAddressModalCloseClick} center={true} style={{maxWidth: 300}} text="Cancel"/>
          </FormCard>
        </Modal>
      }
      {
        this.state.showDeletePaymentModal && this.props.payment_method.detail &&
        <Modal>
          <FormCard
            onCloseClick={this.handleDeleteCardModalCloseClick}
            title="Delete Payment Method?"
            subtitle="Are you sure you want to delete this card?">
            <Button className="nm-btn__cancel" onClick={this.handleDeleteCard} center={true} style={{maxWidth: 300}} text="Delete"/>
            <div className="nm-spacer-12"/>
            <OrLine backgroundColor="white"/>
            <div className="nm-spacer-12"/>
            <Button onClick={this.handleDeleteCardModalCloseClick} center={true} style={{maxWidth: 300}} text="Cancel"/>
          </FormCard>
        </Modal>
      }
      </div>
    );
  }
}

export default connect(
  (state) => ({
    payment_method: state.payment_method,
    address: state.address,
    auth: state.auth
  })
)(ProfileManagement);
