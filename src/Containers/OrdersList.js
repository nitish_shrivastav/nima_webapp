import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import ProductActions from '../Actions/ProductActions';
import OrderActions from '../Actions/OrderActions';
import OrderCard from '../Components/OrderCard';
import { DARK_GREY } from '../Constants/colors';

class OrdersList extends Component {
  constructor() {
    super();
  }

  componentWillMount() {
    this.props.dispatch(ProductActions.list(this.props.client));
    this.props.dispatch(OrderActions.list());
  }

  render() {
    return (
      <div className="nm-layout">
      {
        this.props.order.list.length === 0 && this.props.loader.isLoading === 0 &&
        <h2 className="nm-layout" style={{borderBottom: `1px solid ${DARK_GREY}`, paddingBottom: 6}}>
          No orders found.
        </h2>
      }
      {
        this.props.order.list.length > 0 && this.props.product.list.length > 0 &&
        <div className="nm-layout">
          {
            this.props.order.list.map((order, index) => {
              return (
                <OrderCard
                  products={this.props.product.list}
                  order={order}
                  index={index}/>
              )
            })
          }
        </div>
      }
      </div>
    );
  }
}

export default connect(
  (state) => ({
    order: state.order,
    product: state.product,
    loader: state.loader
  })
)(OrdersList);
