import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { change, formValueSelector, submit } from 'redux-form';
import queryString from 'query-string';
import _ from 'lodash';

import BraintreeInteractor from '../Util/BraintreeInteractor';
import CheckoutActions from '../Actions/CheckoutActions';
import PaymentMethodActions from '../Actions/PaymentMethodActions';
import OrderActions from '../Actions/OrderActions';
import SegmentActions from '../Actions/SegmentActions';
import CheckoutHelper from '../Util/CheckoutHelper';
import constants from '../Constants/segment';
import { PAYPAL, STRIPE, AFFIRM } from '../Constants/payment';

import CheckoutFooter from '../Components/CheckoutFooter';
import FormHeader from '../Components/FormHeader';
import StripeInteractor from '../Util/StripeInteractor';
import Modal from '../Components/Modal';
import PaymentMethodForm from '../Components/Forms/PaymentMethodForm';
import AuthenticatedCheckoutForm from '../Components/Forms/AuthenticatedCheckoutForm';
import AddressActions from '../Actions/AddressActions';
import PromotionActions from '../Actions/PromotionActions';
import MembershipActions from '../Actions/MembershipActions';
import AddressForm from '../Components/Forms/AddressForm';
import FormCard from '../Components/FormCard';
import AddressFormatter from '../Util/AddressFormatter';


const initialState = {
  showCardModal: false,
  showNewAddressModal: false,
  showEditAddressModal: false
}

const newAddressSelector = formValueSelector("newAddressForm");
const editAddressSelector = formValueSelector("editAddressForm");

const AUTHENTICATED_CHECKOUT_FORM = "authenticatedCheckoutForm";

class AuthenticatedCheckout extends Component {
  constructor() {
    super();
    this.stripeInteractor = new StripeInteractor(window.Stripe);
    this.checkoutHelper = new CheckoutHelper(window);
    this.handleRateClick = this.handleRateClick.bind(this);
    this.handleNewCardClick = this.handleNewCardClick.bind(this);
    this.handleModalCloseClick = this.handleModalCloseClick.bind(this);
    this.handlePaymentMethodClick = this.handlePaymentMethodClick.bind(this);

    this.validateDiscount = this.validateDiscount.bind(this);
    this.handleShippingUpdate = this.handleShippingUpdate.bind(this);
    this.handleSubmitForm = this.handleSubmitForm.bind(this);
    this.startAffirmCheckout = this.startAffirmCheckout.bind(this);

    this.payPalListener = this.payPalListener.bind(this);
    this.removeInvalidCheckoutItemsFromShopifyCheckout = this.removeInvalidCheckoutItemsFromShopifyCheckout.bind(this);

    this.handleAddressDelete = this.handleAddressDelete.bind(this);
    this.handleAddressEditClick = this.handleAddressEditClick.bind(this);
    this.handleNewAddressClick = this.handleNewAddressClick.bind(this);
    this.handleNewAddressSubmit = this.handleNewAddressSubmit.bind(this);
    this.handleEditAddressSubmit = this.handleEditAddressSubmit.bind(this);
    this.handleNewPaymentMethodSubmit = this.handleNewPaymentMethodSubmit.bind(this);
  }

  componentWillMount() {
    const { client, match, dispatch, auth } = this.props;
    dispatch(SegmentActions.track(constants.CHECKOUT_STARTED));
    this.setState(initialState);
    dispatch(CheckoutActions.retrieveCheckout(client, match.params.cart_id));
    dispatch(PromotionActions.list());
    dispatch(SegmentActions.identify(auth.sso.shopify_customer_id, auth.sso.email));
    this.payPalListener();

    this.props.dispatch(AddressActions.list())
    .then((addresses) => {
      if (addresses) {
        for (let address of addresses) {
          if (address.default) {
            this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'shipping_address', address.id));
            this.handleShippingUpdate(address);
            break;
          }
        }
      }
    }).catch();

    this.props.dispatch(PaymentMethodActions.list())
    .then((paymentMethods) => {
      let sources = _.get(paymentMethods, 'sources.data');
      if (sources && sources.length > 0) {
        this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'payment_source', sources[0].id));
      }
    }).catch();

    this.props.dispatch(MembershipActions.list());
  }

  componentDidMount() {
    let qp = queryString.parse(this.props.location.search);
    if (qp.method && [STRIPE, AFFIRM, PAYPAL].indexOf(qp.method) > -1) {
      this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'provider', qp.method));
    }
  }

  handlePaymentMethodClick(paymentMethod) {
    let newQueryParams = queryString.parse(this.props.location.search);
    newQueryParams.method = paymentMethod;
    this.props.history.push({
      search: `?${queryString.stringify(newQueryParams)}`
    });
    this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'payment_source', null));
  }

  handleShippingUpdate(address) {
    let mutationVariables = {
      input: {
        firstName: address.first_name,
        lastName: address.last_name,
        phone: address.phone,
        address1: address.address1,
        address2: address.address2,
        city: address.city,
        country: address.country_code,
        province: address.province_code,
        zip: address.zip
      },
      id: this.props.match.params.cart_id
    }
    this.props.dispatch(CheckoutActions.updateCheckout(this.props.client, mutationVariables, (err, checkout) => {
      if (_.get(checkout, 'availableShippingRates.ready') && _.get(checkout, 'availableShippingRates.shippingRates.length')) {
        this.handleRateClick(checkout.availableShippingRates.shippingRates[0].handle);
      }
    }))
  }

  handleRateClick(handle) {
    let mutationVariables = {
      id: this.props.match.params.cart_id,
      handle: handle
    }
    this.props.dispatch(CheckoutActions.updateCheckoutShippingLine(this.props.client, mutationVariables, (err, checkout) => {
      if (checkout) {
        this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'shipping_method', checkout.shippingLine.handle));
      }
    }));
  }

  validateDiscount(code) {
    let body = {
      checkout_id: this.props.match.params.cart_id,
      email: this.props.auth.sso.email,
      discount_codes: [{ id: code.toLowerCase() }]
    }
    this.props.dispatch(CheckoutActions.validateDiscount(body));
  }

  handleSubmitForm(values) {
    values = JSON.parse(JSON.stringify(values));
    const { address } = this.props;
    if (values.provider === AFFIRM) {
       this.startAffirmCheckout(values);
    }
    else {
      values.checkout_id = this.props.match.params.cart_id;
      values.shipping_address = address.list[_.findIndex(address.list, { id: values.shipping_address })];
      if (values.provider === PAYPAL) {
        values.source = values.paypal_nonce;
      }
      else {
        values.source = values.payment_source;
      }
      if (values.discount_codes && values.discount_codes !== "") {
        values.discount_codes = [{ id: values.discount_codes }];
      }
      this.props.dispatch(OrderActions.createAuthenticatedOrder(values))
      .then((order) => {
        if (order) this.setState({ redirect: true });
      })
      .catch();
    }
  }

  startAffirmCheckout(values) {
    const { match, checkout, dispatch, address, auth } = this.props;
    values = JSON.parse(JSON.stringify(values));
    values.shipping_address = address.list[_.findIndex(address.list, { id: values.shipping_address })];
    values.phone_number = values.shipping_address.phone;
    values.first_name = auth.sso.first_name;
    values.last_name = auth.sso.last_name;
    values.email = auth.sso.email;
    dispatch(SegmentActions.track(constants.PAYMENT_INFO_ENTERED, { payment_method: AFFIRM }));
    let body = CheckoutHelper.getAffirmFormValues(match.params.cart_id, values, checkout);
    window.affirm.checkout(body);
    window.affirm.checkout.open({
      onFail: () => {
        console.log('failed affirm')
      },
      onSuccess: (affirmRes) => {
        values.source = affirmRes.checkout_token;
        values.checkout_id = match.params.cart_id;
        if (values.discount_codes && values.discount_codes !== "") {
          values.discount_codes = [{ id: values.discount_codes }];
        }
        this.props.dispatch(OrderActions.createAuthenticatedOrder(values))
        .then((order) => {
          if (order) this.setState({ redirect: true });
        })
        .catch();
      }
    });
  }

  //We get a callback when pay pal is all done
  payPalListener() {
    BraintreeInteractor.getPayPalNonce(window, '.authenticated-paypal-button', (err, res) => {
      if (err) {
        console.log('err in payPalListener', err)
      }
      else if (res) {
        this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'paypal_nonce', res.nonce));
        let addressAlreadyExists = false;
        for (let address of this.props.address.list) {
          //If address already exists with shopify
          if (
            res.details.firstName === address.first_name &&
            res.details.lastName === address.last_name &&
            res.details.shippingAddress.line1 === address.address1 &&
            res.details.shippingAddress.state === address.province_code &&
            res.details.shippingAddress.countryCode === address.country_code &&
            res.details.shippingAddress.postalCode === address.zip &&
            res.details.shippingAddress.city === address.city
          ) {
            addressAlreadyExists = true;
            this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'shipping_address', address.id));
            this.handleShippingUpdate(address);
            break;
          }
        }

        if (addressAlreadyExists === false) {
          let newAddress = {
            first_name: res.details.firstName,
            last_name: res.details.lastName,
            shipping_address: {
              address1: res.details.shippingAddress.line1,
              address2: res.details.shippingAddress.line2,
              city: res.details.shippingAddress.city,
              country: res.details.shippingAddress.countryCode,
              province: res.details.shippingAddress.state,
              zip: res.details.shippingAddress.postalCode
            }
          }
          this.handleNewAddressSubmit(newAddress);
        }
      }
    })
  }


  handleNewCardClick() {
    this.props.dispatch(SegmentActions.track(constants.PAYMENT_INFO_ENTERED, { payment_method: STRIPE }));
    this.setState({showCardModal: true})
  }

  handleModalCloseClick(modalName) {
    this.setState({[modalName]: false});
  }

  removeInvalidCheckoutItemsFromShopifyCheckout(validLineItems) {
    let mutationVariables = {
      checkoutId: this.props.checkout.detail.id,
      lineItems: validLineItems
    }
    this.props.dispatch(CheckoutActions.checkoutLineItemsReplace(this.props.client, mutationVariables));
  }

  handleAddressDelete(addressId) {
    this.props.dispatch(AddressActions.delete(addressId))
    .then((success) => {
      if (success) {
        this.props.dispatch(AddressActions.list());
      }
    })
    .catch();
  }

  handleAddressEditClick(address) {
    this.props.dispatch(AddressActions.setDetail(address));
    this.setState({ showEditAddressModal: true });
  }

  handleEditAddressSubmit(values) {
    let body = AddressFormatter.formatFormAddressForApi(values, "shipping_address");
    let updatedAddress = undefined;
    this.props.dispatch(AddressActions.update(this.props.address.detail.id, body))
    .then((updateAddressRes) => {
      if (updateAddressRes) {
        updatedAddress = updateAddressRes.customer_address;
        return this.props.dispatch(AddressActions.list());
      }
    })
    .then((listRes) => {
      if (listRes) {
        for (let item of listRes) {
          if (item.id === updatedAddress.id) {
            this.props.dispatch(change(AUTHENTICATED_CHECKOUT_FORM, 'shipping_address', item.id));
            values = { ...values, ...values.shipping_address };
            this.handleShippingUpdate(values);
            break;
          }
        }
        this.handleModalCloseClick("showEditAddressModal");
      }
    })
    .catch();
  }

  handleNewAddressClick() {
    this.props.dispatch(SegmentActions.track(constants.PAYMENT_INFO_ENTERED, { payment_method: STRIPE }));
    this.setState({ showNewAddressModal: true });
  }

  handleNewAddressSubmit(values) {
    let body = AddressFormatter.formatFormAddressForApi(values, "shipping_address");
    let createdAddress = undefined;
    this.props.dispatch(AddressActions.create(body))
    .then((createAddressRes) => {
      if (createAddressRes) {
        createdAddress = createAddressRes.customer_address;
        return this.props.dispatch(AddressActions.list());
      }
    })
    .then((listRes) => {
      if (listRes) {
        for (let item of listRes) {
          if (item.id === createdAddress.id) {
            this.props.dispatch(change('authenticatedCheckoutForm', 'shipping_address', item.id));
            values = { ...values, ...values.shipping_address };
            this.handleShippingUpdate(values);
            break;
          }
        }
        this.handleModalCloseClick("showNewAddressModal");
      }
    })
    .catch();
  }

  handleNewPaymentMethodSubmit(values) {
    this.checkoutHelper.getStripeSource(values)
    .then((stripeSource) => {
      let body = {
        provider: "stripe",
        source: stripeSource.id
      }
      return this.props.dispatch(PaymentMethodActions.create(body));
    })
    .then((detail) => {
      if (detail) {
        //for (let item of listRes) {
        //  if (item.id === createdAddress.id) {
        //    this.props.dispatch(change('authenticatedCheckoutForm', 'shipping_address', item.id));
        //    this.handleShippingUpdate(values);
        //    break;
        //  }
        //}
        this.handleModalCloseClick("showCardModal");
      }
    })
    .catch((err) => {
      if (err && err.message) {
        this.props.dispatch(PaymentMethodActions.setError(err.message));
      }
    })
  }

  render() {
    const { redirect } = this.state;
    if (redirect) {
      return (<Redirect to="/shop/order-thanks"/>);
    }
    else {
      return (
        <div className="nm-layout">
          <div className="nm-clear"/>
          <div className="nm-layout nm-layout__gray">
            <div className="nm-spacer-48" />
            <FormHeader />
            <div className="nm-layout__max-center nm-layout__max-center--medium-gray nm-layout__max-center--mobile-support">
              <AuthenticatedCheckoutForm
                onSubmit={this.handleSubmitForm}
                onAddressDeleteClick={this.handleAddressDelete}
                onAddressEditClick={this.handleAddressEditClick}
                onNewAddressClick={this.handleNewAddressClick}
                onSubmitDiscountValidate={this.validateDiscount}
                onShippingAddressChange={this.handleShippingUpdate}
                onPaymentMethodClick={this.handlePaymentMethodClick}
                onShippingRateClick={this.handleRateClick}
                onRemoveItemsClick={this.removeInvalidCheckoutItemsFromShopifyCheckout}
                onNewCardClick={this.handleNewCardClick}/>
              <div className="nm-clear"/>
              <div className="nm-layout">
                <div className="nm-spacor-48" />
                <CheckoutFooter/>
              </div>
              <div className="nm-clear"></div>
            </div>
            <div className="nm-spacer-48" />
            <div className="nm-spacer-48" />
            <div className="nm-spacer-48" />
          </div>
          {
            this.state.showNewAddressModal &&
            <Modal>
              <FormCard
                onCloseClick={() => {this.handleModalCloseClick("showNewAddressModal")}}
                error={this.props.address.error}
                title="Add a new address.">
                <AddressForm
                  label="Shipping Address (USA/CAN)"
                  onSubmit={this.handleNewAddressSubmit}
                  submitText="Add address"
                  form="newAddressForm"
                  name="shipping_address"
                  isCollapsed={false}
                  selector={newAddressSelector}/>
              </FormCard>
            </Modal>
          }
          {
            this.state.showEditAddressModal &&
            <Modal>
              <FormCard
                onCloseClick={() => {this.handleModalCloseClick("showEditAddressModal")}}
                error={this.props.address.error}
                title="Edit address.">
                <AddressForm
                  label="Shipping Address (USA/CAN)"
                  onSubmit={this.handleEditAddressSubmit}
                  submitText="Update Address"
                  form="editAddressForm"
                  name="shipping_address"
                  isCollapsed={false}
                  isEdit={true}
                  selector={editAddressSelector}/>
              </FormCard>
            </Modal>
          }
          {
            this.state.showCardModal &&
            <Modal>
              <FormCard
                onCloseClick={() => {this.handleModalCloseClick("showCardModal")}}
                error={this.props.payment_method.error}
                title="Add Payment Method">
                <PaymentMethodForm form="newPaymentMethod" onSubmit={this.handleNewPaymentMethodSubmit}/>
              </FormCard>
            </Modal>
          }
        </div>
      );
    }
  }
}

export default connect(
  (state) => ({
    checkout: state.checkout,
    auth: state.auth,
    order: state.order,
    address: state.address,
    payment_method: state.payment_method
  })
)(AuthenticatedCheckout);
