import React, { Component } from 'react';
import { connect } from 'react-redux';
import ForgotPasswordForm from '../Components/Forms/ForgotPasswordForm';
import FormCard from '../Components/FormCard';
import AuthActions from '../Actions/AuthActions';
import SuccessCard from '../Components/SuccessCard';


class SSOPasswordForgot extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    this.setState({ sentEmail: false });
  }

  handleSubmit(values) {
    this.props.dispatch(AuthActions.ssoSendResetPasswordEmail(values))
    .then((res) => {
      if (res) {
        this.setState({ sentEmail: true });
      }
    })
    .catch();
  }

  render() {
    return (
      <div className="nm-layout nm-layout__gray">
        <div className="nm-spacer-48"></div>
        <div className="nm-spacer-48"></div>
        {
          this.state.sentEmail &&
          <SuccessCard statusText="Email sent!" title="Check your email.">
            <div className="nm-flex">
              <p className="nm-text-center nm-base__p" style={{maxWidth: 400}}>
                If you look in your email. You should find instructions to reset your Nima password.
              </p>
            </div>
          </SuccessCard>
        }
        {
          !this.state.sentEmail &&
          <FormCard
            title="Password Reset"
            subtitle="Enter your email to get password reset instructions.">
            <ForgotPasswordForm onSubmit={this.handleSubmit}/>
          </FormCard>
        }
        <div className="nm-spacer-48"></div>
        <div className="nm-spacer-48"></div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(SSOPasswordForgot);
