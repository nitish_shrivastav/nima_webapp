import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import ProductActions from '../Actions/ProductActions';
import Product from '../Components/Product';
import Steps from '../Components/Steps';
import CheckoutActions from '../Actions/CheckoutActions';
import SegmentActions from '../Actions/SegmentActions';
import SegmentConstants from '../Constants/segment';
import SegmentPayloadConverter from '../Util/SegmentPayloadConverter';
import StorefrontStorage from '../Util/StorefrontStorage';
import StorefrontInteractor from '../Util/StorefrontInteractor';


class ProductDetail extends Component {
  constructor() {
    super();
    this.handleAddToCart = this.handleAddToCart.bind(this);
  }

  componentWillMount() {
    const { client, match, dispatch } = this.props;
    dispatch(ProductActions.list(client));
    dispatch(ProductActions.retrieve(client, match.params.handle));
  }

  componentDidUpdate(prevProps) {
    const { client, location, dispatch, match } = this.props;
    if (location.pathname !== prevProps.location.pathname) {
      dispatch(ProductActions.retrieve(client, match.params.handle));
    }
  }

  handleAddToCart(variant) {
    let productData = _.get(this, 'props.product.detail.meta');
    let lineItems = _.get(this, 'props.checkout.detail.lineItems.edges', [])
      .map((item) => {
        return {
          variantId: item.node.variant.id,
          quantity: item.node.quantity
        }
      })
    let variantIndex = lineItems.findIndex((i) => i.variantId === variant.id);
    if (variantIndex >= 0) { //if the lineItem already exists in the cart, goto that index of the array and update quantity by 1
      lineItems[variantIndex] = {
        ...lineItems[variantIndex],
        quantity: lineItems[variantIndex].quantity += 1
      }
    } else { //if it doesn't exist, add it to lineItems
      let variantId = StorefrontInteractor.decodeId(variant.id, "ProductVariant");
      lineItems.push({
        variantId: variant.id,
        quantity: (variantId in this.props.subscription_product.list) ? this.props.subscription_product.list[variantId].quantity : 1
      })
    }
    let body = {
      checkoutId: StorefrontStorage.getCheckoutId(),
      lineItems
    }
    this.props.dispatch(CheckoutActions.checkoutLineItemsReplace(this.props.client, body))
    .then((res) => {
      if (res) {
        let segmentPayload = SegmentPayloadConverter.productAdded(_.get(res, 'lineItems.edges', []), variant.id);
        this.props.dispatch(SegmentActions.track(SegmentConstants.PRODUCT_ADDED, segmentPayload));
        if (productData && productData.next_step_link) {
          this.props.history.push(productData.next_step_link);
        }
      }
    })
    .catch();
    
  }

  render() {
    const { checkout, product, auth } = this.props;
    const { handle } = this.props.match.params;
    const productDefaultProps = {
      productInfo: _.get(product, 'detail.meta'),
      onAddToCart: this.handleAddToCart,
      product: product.detail
    }
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <div className="nm-layout">
            {
              false && // NOTE: temporarily remove STEPS during Holiday Sale
              productDefaultProps.productInfo && productDefaultProps.productInfo.allergen === "gluten" &&
              <Steps
                checkout={checkout.detail}
                allergen={productDefaultProps.productInfo.allergen}
                handle={handle}/>
            }
            {
              product.detail && productDefaultProps.productInfo &&
              <Product {...productDefaultProps} />
            }
          </div>
         <div className="nm-clear"></div>
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    product: state.product,
    auth: state.auth,
    checkout: state.checkout,
    promotion: state.promotion,
    subscription_product: state.subscription_product
  })
)(ProductDetail);
