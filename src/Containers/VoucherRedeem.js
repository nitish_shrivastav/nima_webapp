import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import _ from 'lodash';

import StorefrontInteractor from '../Util/StorefrontInteractor';
import VoucherActions from '../Actions/VoucherActions';
import ProductActions from '../Actions/ProductActions';
import VoucherForm from '../Components/Forms/VoucherForm';
import VoucherSearchForm from '../Components/Forms/VoucherSearchForm';


class VoucherRedeem extends Component {
  constructor() {
    super();
    this.handleVoucherRedeemSubmit = this.handleVoucherRedeemSubmit.bind(this);
    this._getCartItem = this._getCartItem.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(ProductActions.list(this.props.client));
  }

  handleVoucherRedeemSubmit(values) {
    localStorage.setItem('email', values.email);
    this.props.dispatch(VoucherActions.redeem(this.props.voucher.detail.id, values));
  }

  handleSearchSubmit(values) {
    this.props.dispatch(VoucherActions.retrieve(values.voucher_id));
  }

  _getCartItem(item, key) {
    for (let productItem of this.props.product.list) {
      for (let productVariant of productItem.node.variants.edges) {
        if (StorefrontInteractor.decodeId(productVariant.node.id, "ProductVariant") === item.variant_id) {
          return (
            <div className="nm-cart__row" key={`voucher-row-${key}`}>
              <div className="nm-layout">
                <Link to={`/shop/products/${productItem.node.handle}`}>
                  <img className="nm-cart__image" src={productItem.node.images.edges[0].node.transformedSrc}/>
                </Link>
                <div className="nm-cart__info-wrap">
                  <Link className="nm-cart__title" to={`/products/${productItem.node.handle}`}>
                    <h4 className="nm-layout__sub-title-dark">
                      {productItem.node.title}
                    </h4>
                    <p className="nm-layout"> 
                      Quantity: {item.quantity}
                    </p>
                    {
                      productItem.node.tags.indexOf("backorder") !== -1 &&
                      <p className="nm-layout nm-error"> 
                        Redeem Now (Shipping when in stock)
                      </p>
                    }
                  </Link>
                </div>
              </div>
            </div>
          );
        }
      }
    }
  }

  render() {
    //detail.name refers to the order name on the shopify record
    if (_.get(this, 'props.voucher.detail.name')) {
      return (<Redirect to="/shop/order-thanks"/>);
    }
    else {
      return (
        <div className="nm-layout">
          <div className="nm-layout" id="voucher-vwo-header">
          </div>
          <div className="nm-spacer-48"/>
          <div className="nm-clear"/>
          <div className="nm-layout__max-center nm-layout__max-center--medium-gray nm-layout__max-center--mobile-support" style={{maxWidth: 500}}>
            <div className="nm-spacer-12"/>
            <VoucherSearchForm onSubmit={this.handleSearchSubmit}/>
            <div className="nm-clear"/>
          </div>
          <div className="nm-clear"/>
          <div className="nm-spacer-48"/>
          <div className="nm-clear"/>
          {
            this.props.voucher.detail && this.props.product.list.length > 0 &&
            <div className="nm-layout__max-center nm-layout__max-center--mobile-support">
              <div className="nm-layout">
                <div className="nm-card nm-card__block" style={{padding: 0}}>
                  <div className="nm-cart__row">
                    <h2 className="nm-layout" style={{fontSize: 24}}>Your Voucher</h2>
                  </div>
                  {
                    this.props.voucher.detail.line_items.map((item, key) => {
                      return this._getCartItem(item, key);
                    })
                  }
                </div>
                <div className="nm-clear"/>
              </div>
              <div className="nm-clear"/>
              <div className="nm-layout__max-center nm-layout__max-center--mobile-support" style={{maxWidth: 600}}>
                <VoucherForm onSubmit={this.handleVoucherRedeemSubmit} />
                <div className="nm-clear"/>
              </div>
              <div className="nm-clear"/>
            </div>
          }
        </div>
      )
    }
  }
}


export default connect(
  (state) => ({
    product: state.product,
    voucher: state.voucher
  })
)(VoucherRedeem);
