import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { connect } from 'react-redux';
import AuthActions from '../Actions/AuthActions';
import { GREEN } from '../Constants/colors';
import SSORegisterForm from '../Components/Forms/SSORegisterForm';

class CreateAccount extends Component {
  constructor() {
    super();
    this.handleRegisterSubmit = this.handleRegisterSubmit.bind(this);
    this.onFacebookClick = this.onFacebookClick.bind(this);
  }

  componentWillMount() {
    this.setState({ showRegisterSuccess: false });
  }

  handleRegisterSubmit(values) {
    this.props.dispatch(AuthActions.registerSSOUser(values))
    .then((user) => {
      if (user) {
        this.setState({ showRegisterSuccess: true });
      }
    }).catch();
  }

  onFacebookClick() {
    if (window.FB) {
      window.FB.login((res) => {
        if (res.status === "connected" && _.get(res, 'authResponse.accessToken')) {
          this.props.dispatch(AuthActions.loginFacebookUser({ access_token: res.authResponse.accessToken }));
        }
      },{ scope: 'public_profile,email' });
    }
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-spacer-48" />
        <div className="nm-clear"/>
        <div className="nm-layout nm-layout__max-center nm-layout__max-center--short nm-layout__max-center--mobile-support">
          <div className="nm-spacer-48" />
          <div className="nm-card nm-card__shadow" style={{padding: 24}}>
            <div className="nm-spacer-12"/>
            <h2 className="nm-text-center nm-layout nm-bold">
              Register
            </h2>
            <div className="nm-spacer-48"/>
            {
              this.state.showRegisterSuccess &&
              <p className="nm-layout" style={{ color: GREEN }}>
                Thank you for signing up. Please check your email to verify your account.
              </p>
            }
            {
              !this.state.showRegisterSuccess &&
              <SSORegisterForm
                onFacebookClick={this.handleFacebookClick}
                onSubmit={this.handleRegisterSubmit}/>
            }
          </div>
          <div className="nm-spacer-48"/>
          <div className="nm-layout nm-text-center">
            <Link to="/account/login">
              Already have an account? Login here
            </Link>
          </div>
          <div className="nm-spacer-48"/>
          <div className="nm-clear"/>
        </div>
      </div>
    );
  }
}

export default connect()(CreateAccount);
