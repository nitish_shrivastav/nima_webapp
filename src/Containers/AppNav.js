import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class AppNav extends Component {
  render() {
    return (
      <div className="nm-layout">
        <Link to="/app/map" className="nm-layout nm-link">Map</Link>
        <Link to="/app/profile" className="nm-layout nm-link">Profile</Link>
        <Link to="/app/readings" className="nm-layout nm-link">Readings</Link>
        <Link to="/app/reviews" className="nm-layout nm-link">Reviews</Link>
        <Link to="/app/brands" className="nm-layout nm-link">Brands</Link>
      </div>
    );
  }
}


export default AppNav;
