import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import SSOLoginForm from '../Components/Forms/SSOLoginForm';
import AuthActions from '../Actions/AuthActions';


class SSOLogin extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFacebookLogin = this.handleFacebookLogin.bind(this);
  }

  componentWillMount() {
    this.setState({ loginSuccessful: false });
  }

  handleSubmit(values) {
    this.props.dispatch(AuthActions.loginAuth0User(values))
    .then((isSuccessful) => {
      if (isSuccessful) {
        this.setState({ loginSuccessful: true });
      }
    })
    .catch();
  }

  handleFacebookLogin() {
    if (window.FB) {
      window.FB.login((res) => {
        if (res.status === "connected" && res.authResponse && res.authResponse.accessToken) {
          this.props.dispatch(AuthActions.loginFacebookUser({ access_token: res.authResponse.accessToken }))
          .then((isSuccessful) => {
            if (isSuccessful) {
              this.setState({ loginSuccessful: true });
            }
          })
          .catch();
        }
      },{scope: 'public_profile,email'});
    }
  }

  render() {
    if (this.state.loginSuccessful && this.props.auth.sso) {
      return (<Redirect to="/account"/>);
    }
    else {
      return (
        <div className="nm-layout nm-layout__gray">
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
          <div className="nm-layout nm-layout__max-center nm-layout__max-center--short nm-layout__max-center--mobile-support">
            <div className="nm-card nm-card__shadow" style={{padding: 24}}>
              <div className="nm-spacer-48"/>
              <h2 className="nm-text-center nm-layout nm-bold">
                Log In
              </h2>
              <div className="nm-spacer-48"/>
              <SSOLoginForm onSubmit={this.handleSubmit} onFacebookClick={this.handleFacebookLogin}/>
              <div className="nm-spacer-12"/>
              <div className="nm-layout nm-text-center">
                <Link to="/account/register">
                  Don't have an account? Click here
                </Link>
              </div>
            </div>
          </div>
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
        </div>
      );
    }
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(SSOLogin);
