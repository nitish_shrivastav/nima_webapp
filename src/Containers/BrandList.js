import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import BrandActions from '../Actions/BrandActions';


class BrandList extends Component {
  componentWillMount() {
    this.props.dispatch(BrandActions.list());
  }

  render() {
    return (
      <div className="nm-layout">
        <Link to="/app" className="nm-link">Back</Link>
        <p className="nm-layout">
          {JSON.stringify(this.props.brand.list)}
        </p>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    brand: state.brand
  })
)(BrandList);
