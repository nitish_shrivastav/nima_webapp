import { STRIPE, AFFIRM, PAYPAL } from '../Constants/payment';

const paymentOptions = [
  {
    title: "Debit or Credit Card",
    value: STRIPE
  },
  {
    value: "paypal",
    img: {
      src: `${process.env.PUBLIC_URL}/images/payment/paypal.svg`,
      alt: PAYPAL,
      style: {
        position: 'relative',
        top: 4
      }
    }
  },
  {
    value: AFFIRM,
    img: {
      src: `${process.env.PUBLIC_URL}/images/payment/affirm.svg`,
      alt: "affirm",
      style: {
        position: 'relative',
        top: 2
      }
    }
  }
]

export default paymentOptions;
