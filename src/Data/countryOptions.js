const countryOptions = [
  {
    text: "United States",
    value: "US"
  },
  {
    text: "Canada",
    value: "CA"
  }
]

export default countryOptions;
