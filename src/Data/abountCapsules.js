export default
`<div class="nm-card">
  <div class="nm-spacer-48"></div>
  <div class="nm-card__center">
    <div class="nm-card__squeeze" style="width: 100%; max-width: 600px;">
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <div class="nm-image__caption" style="background-image: url('/images/capsule/image_capsules_bullet1.png'); "></div>
            <div class="nm-card__caption-wrap">
              <h4 class="nm-card__sub-title">1</h4>
              <p class="nm-card__description nm-card__description--small">
                Put a sample of food into a new capsule
              </p>
            </div>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-spacer-12"></div>
          <div class="nm-clear"></div>
        </div>
      </div>
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <div class="nm-image__caption" style="background-image: url('/images/capsule/image_capsules_bullet2.png');"></div>
            <div class="nm-card__caption-wrap">
              <h4 class="nm-card__sub-title">2</h4>
              <p class="nm-card__description nm-card__description--small">
                Closing the top mixes the food and chemistry
              </p>
            </div>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-spacer-12"></div>
          <div class="nm-clear"></div>
        </div>
      </div>
      <div class="nm-clear"></div>
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <div class="nm-image__caption" style="background-image: url('/images/capsule/image_capsules_bullet3.png'); "></div>
            <div class="nm-card__caption-wrap">
              <h4 class="nm-card__sub-title">3</h4>
              <p class="nm-card__description nm-card__description--small">
                Chemistry in the capsule binds to any gluten
              </p>
            </div>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-spacer-12"></div>
          <div class="nm-clear"></div>
        </div>
      </div>
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <div class="nm-image__caption" style="background-image: url('/images/capsule/image_capsules_bullet4.png'); "></div>
            <div class="nm-card__caption-wrap">
              <h4 class="nm-card__sub-title">4</h4>
              <p class="nm-card__description nm-card__description--small">
                The Nima Sensor images and shows your result
              </p>
            </div>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-spacer-12"></div>
          <div class="nm-clear"></div>
        </div>
      </div>
      <div class="nm-spacer-48"></div>
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <h4 class="nm-card__sub-title">Note: use fresh capsules</h4>
            <p class="nm-card__description nm-card__description--small">
              To ensure a valid test, capsule chemistry expires 6 months after manufacturing. We will always send you capsules that have at least 2 months of shelf life left.
            </p>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-clear"></div>
        </div>
      </div>
      <div class="nm-card__content-wrap">
        <div class="nm-card__section nm-card__section--right">
          <div class="nm-card__section--content" style="padding: 12px;">
            <h4 class="nm-card__sub-title">Note: storing capsules</h4>
            <p class="nm-card__description nm-card__description--small">
              Capsule chemistry is very precise, please store capsules between 32°F and 95°F. Dispose
              of used capsules in the trash after your test.
            </p>
            <div class="nm-clear"></div>
          </div>
          <div class="nm-clear"></div>
          <div class="nm-spacer-12"></div>
        </div>
      </div>
      <div class="nm-clear"></div>
    </div>
    <div class="nm-spacer-48"></div>
  </div>
  <div class="nm-layout nm-layout__bar"></div>
  <div class="nm-card__center">
    <div class="nm-layout">
      <img class="nm-image nm-layout nm-desktop-hidden" src="${process.env.PUBLIC_URL}/images/capsule/image_12tests_mobile.png" alt="Nima Test">
      <img src="${process.env.PUBLIC_URL}/images/capsule/image_12tests_row1.png" class="nm-layout nm-image nm-mobile-hidden">
      <div class="nm-std-filler"></div>
      <div class="nm-clear"></div>
      <h4 class="nm-text-center nm-card__adtext">Most people test <span class="nm-card__adtext-emph">12</span> times a month</h4>
      <p class="nm-text-center">and use 12 capsules a month</p>
      <div class="nm-spacer-12"></div>
      <img src="${process.env.PUBLIC_URL}/images/capsule/image_12tests_row2.png" class="nm-image nm-layout nm-mobile-hidden">
      <div class="nm-spacer-12"></div>
      <div class="nm-clear"></div>
    </div>
  </div>
</div>`;
