import constants from '../Constants/chain';

const initialState = {
  detail: null,
  error: null,
  popular_chains: null
};

export default function chain(state=initialState, action) {
  switch (action.type) {
    case constants.SET_CHAIN_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_CHAIN_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_CHAIN_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.SET_POPULAR_CHAINS:
      return {
        ...state,
        popular_chains: action.chains
      }
    case constants.CLEAR_CHAIN:
      return initialState;
    default:
      return state;
  }
}
