import constants from '../Constants/brand';

const initialState = {
  detail: null,
  error: null,
  list: []
}

export default function brand(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_BRAND:
      return {
        ...state,
        list: action.list
      }
    case constants.RETRIEVE_BRAND:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_BRAND_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_BRAND_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.CLEAR_BRAND:
      return initialState
    default:
      return state
  }
}
