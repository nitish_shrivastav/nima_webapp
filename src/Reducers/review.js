import constants from '../Constants/review';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function review(state=initialState, action) {
  switch (action.type) {
    case constants.SET_REVIEW_LIST:
      return {
        ...state,
        list: action.list
      }
    case constants.SET_REVIEW_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_REVIEW_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_REVIEW_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.CLEAR_REVIEW:
      return initialState;
    default:
      return state
  }
}
