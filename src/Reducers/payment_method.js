import {
  SET_PAYMENT_METHOD_LIST,
  SET_PAYMENT_METHOD_DETAIL,
  SET_PAYMENT_METHOD_ERROR,
  CLEAR_PAYMENT_METHOD
} from '../Constants/payment_method';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function payment_method(state=initialState, action) {
  switch (action.type) {
    case SET_PAYMENT_METHOD_LIST:
      return {
        ...state,
        list: action.list
      }
    case SET_PAYMENT_METHOD_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case SET_PAYMENT_METHOD_ERROR:
      return {
        ...state,
        error: action.error
      }
    case CLEAR_PAYMENT_METHOD:
      return initialState;
    default:
      return state;
  }
}
