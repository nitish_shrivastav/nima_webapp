import constants from '../Constants/auth';

const initialState = {
  user: null,
  sso: null,
  error: null,
  verified: false
}

export default function auth(state=initialState, action) {
  switch (action.type) {
    case constants.SET_USER:
      return {
        ...state,
        user: action.user
      }
    case constants.SET_SSO_USER:
      return {
        ...state,
        sso: action.user
      }
    case constants.SET_USER_ERROR:
      return {
        ...state,
        sso: null,
        error: action.error
      }
    case constants.SET_USER_VERIFIED:
      return {
        ...state,
        verified: action.verified
      }
    case constants.CLEAR_USER_ERROR:
      return {
        ...state,
        error: null,
        verified: false
      }
    case constants.SET_USER_CLEAR:
      return initialState;
    default:
      return state
  }
}
