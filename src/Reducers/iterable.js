import constants from '../Constants/iterable';

const initialState = {
  successMessage: null,
  errorMessage: null
}

export default function iterable(state=initialState, action) {
  switch (action.type) {
    case constants.SET_MESSAGE_TYPES:
      return {
        ...state,
        messageTypes: action.messageTypes
      }
    case constants.SET_SUCCESS_MESSAGE:
      return {
        ...state,
        successMessage: 'Success'
      }
    case constants.SET_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: `There was an error when updating your email preferences: ${action.err}`
      }

    default:
      return state
  }
}
