import constants from '../Constants/reading';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function reading(state=initialState, action) {
  switch (action.type) {
    case constants.SET_READING_LIST:
      return {
        ...state,
        list: action.list
      }
    case constants.SET_READING_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_READING_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_READING_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.CLEAR_READING:
      return initialState;
    default:
      return state;
  }
}
