import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import address from './address';
import auth from './auth';
import chain from './chain';
import brand from './brand';
import checkout from './checkout';
import iterable from './iterable';
import loader from './loader';
import membership from './membership';
import mailList from './mailList';
import order from './order';
import payment_method from './payment_method';
import place from './place';
import product from './product';
import promotion from './promotion';
import reading from './reading';
import review from './review';
import subscription from './subscription';
import subscription_product from './subscription_product';
import voucher from './voucher';

export default combineReducers({
  router: routerReducer,
  form: formReducer,
  address,
  auth,
  brand,
  chain,
  checkout,
  iterable,
  loader,
  mailList,
  membership,
  order,
  payment_method,
  place,
  product,
  promotion,
  reading,
  review,
  subscription,
  subscription_product,
  voucher
})
