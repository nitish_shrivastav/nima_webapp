import constants from '../Constants/order';

const initialState = {
  error: null,
  list: [],
  detail: null
}

export default function order(state=initialState, action) {
  switch (action.type) {
    case constants.SET_ORDER_LIST:
      return {
        ...state,
        list: action.list
      }
    case constants.SET_ORDER_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_ORDER_ERROR:
      return {
        ...state,
        error: action.error
      }
    default:
      return state
  }
}
