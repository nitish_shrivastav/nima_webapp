import {
  SET_ADDRESS_LIST,
  SET_ADDRESS_DETAIL,
  SET_ADDRESS_ERROR,
  CLEAR_ADDRESSES
} from '../Constants/address';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function address(state=initialState, action) {
  switch (action.type) {
    case SET_ADDRESS_LIST:
      return {
        ...state,
        list: action.list
      }
    case SET_ADDRESS_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case SET_ADDRESS_ERROR:
      return {
        ...state,
        error: action.error
      }
    case CLEAR_ADDRESSES:
      return initialState;
    default:
      return state;
  }
}
