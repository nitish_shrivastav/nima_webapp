import {
  SET_PROMOTION_LIST,
  SET_PROMOTION_ERROR,
  CLEAR_PROMOTIONS
} from '../Constants/promotion';

const initialState = {
  list: [],
  error: null
}

export default function promotion(state=initialState, action) {
  switch (action.type) {
    case SET_PROMOTION_LIST:
      return {
        ...state,
        list: action.list
      }
    case SET_PROMOTION_ERROR:
      return {
        ...state,
        error: action.error
      }
    case CLEAR_PROMOTIONS:
      return initialState;
    default:
      return state
  }
}
