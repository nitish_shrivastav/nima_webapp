import constants from '../Constants/product';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function product(state=initialState, action) {
  switch (action.type) {
    case constants.SET_PRODUCT_LIST:
      return {
        ...state,
        list: action.products
      }
    case constants.SET_PRODUCT_DETAIL:
      return {
        ...state,
        detail: action.product
      }
    case constants.SET_PRODUCT_ERROR:
      return {
        ...state,
        error: action.error
      }
    default:
      return state
  }
}
