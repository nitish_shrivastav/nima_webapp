import constants from '../Constants/place';

const initialState = {
  list: [],
  detail: null,
  error: null,
  location: null
}

export default function place(state=initialState, action) {
  switch (action.type) {
    case constants.SET_PLACE_LIST:
      return {
        ...state,
        list: action.list
      }
    case constants.SET_PLACE_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.SET_PLACE_LOCATION:
      return {
        ...state,
        location: action.location
      }
    case constants.SET_PLACE_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_PLACE_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.CLEAR_PLACE:
      return initialState;
    default:
      return state;
  }
}
