import constants from '../Constants/loader';

const initialState = {
  isLoading: 0,
  message: ""
}

export default function loader(state=initialState, action) {
  switch (action.type) {
    case constants.SET_IS_LOADING:
      let loadingCount = state.isLoading + action.isLoading;
      if (loadingCount < 0) loadingCount = 0;
      let nextState = {
        isLoading: loadingCount
      }
      if (loadingCount === 0) {
        nextState.message = "";
      }
      else if (action.message) {
        nextState.message = action.message;
      }
      return nextState
    default:
      return state
  }
}