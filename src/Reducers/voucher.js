import { SET_VOUCHER_DETAIL, SET_VOUCHER_ERROR, CLEAR_VOUCHERS } from '../Constants/voucher';

const initialState = {
  detail: null,
  error: null
}

export default function voucher(state=initialState, action) {
  switch(action.type) {
    case SET_VOUCHER_DETAIL: 
      return {
        ...state,
        detail: action.detail,
        error: null
      }
    case SET_VOUCHER_ERROR: 
      return {
        ...state,
        error: action.error
      }
    case CLEAR_VOUCHERS: 
      return initialState;
    default:
      return state;
  }
}
