import { SET_MEMBERSHIP_DETAIL, SET_MEMBERSHIP_ERROR, CLEAR_MEMBERSHIP } from '../Constants/membership';

const initialState = {
  detail: null,
  error: null
}

export default function membership(state=initialState, action) {
  switch (action.type) {
    case SET_MEMBERSHIP_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case SET_MEMBERSHIP_ERROR:
      return {
        ...state,
        error: action.error
      }
    case CLEAR_MEMBERSHIP:
      return initialState;
    default:
      return state
  }
}
