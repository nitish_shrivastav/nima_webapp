import {
  SET_SUBSCRIPTION_PRODUCT_LIST,
  SET_SUBSCRIPTION_PRODUCT_ERROR,
  CLEAR_SUBSCRIPTION_PRODUCTS
} from '../Constants/subscription_product';

const initialState = {
  list: null,
  error: null
}

export default function subscription_product(state=initialState, action) {
  switch (action.type) {
    case SET_SUBSCRIPTION_PRODUCT_LIST:
      return {
        ...state,
        list: action.list
      }
    case SET_SUBSCRIPTION_PRODUCT_ERROR:
      return {
        ...state,
        error: action.error
      }
    case CLEAR_SUBSCRIPTION_PRODUCTS:
      return initialState;
    default:
      return state;
  }
}
