import constants from '../Constants/mailList';

const initialState = {
  successMessage: null,
  error: null
}

export default function mailList(state=initialState, action) {
  switch (action.type) {
    case constants.SET_MAIL_ADD_SUCCESS:
      return {
        ...state,
        successMessage: action.message,
        error: null
      }
    case constants.SET_MAIL_ADD_ERROR:
      return {
        ...state,
        successMessage: null,
        error: action.error
      }
    case constants.CLEAR_MAIL_LIST:
      return initialState;
    default:
      return state
  }
}
