import constants from '../Constants/subscription';

const initialState = {
  list: [],
  detail: null,
  error: null
}

export default function subscription(state=initialState, action) {
  switch (action.type) {
    case constants.SET_SUBSCRIPTION_LIST:
      return {
        ...state,
        list: action.list
      }
    case constants.SET_SUBSCRIPTION_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.SET_SUBSCRIPTION_DETAIL:
      return {
        ...state,
        detail: action.detail
      }
    case constants.CLEAR_SUBSCRIPTIONS:
      return initialState;
    default:
      return state
  }
}
