import constants from '../Constants/checkout';

const initialState = {
  detail: null,
  error: null,
  validDiscount: null,
  discountError: null,
  shippingRatesIsLoading: false,
  shippingAddressIsLoading: false,
  totals: {
    total: 0,
    taxes: 0,
    subtotal: 0,
    totalDiscount: 0,
    premiumDiscountTotal: 0
  }
}

export default function checkout(state=initialState, action) {
  switch (action.type) {
    case constants.RETRIEVE_CHECKOUT:
      return {
        ...state,
        detail: action.checkout
      }
    case constants.SET_CHECKOUT_VALID_DISCOUNTS:
      return {
        ...state,
        validDiscounts: action.validDiscounts
      }
    case constants.SET_CHECKOUT_TOTALS:
      return {
        ...state,
        totals: action.totals
      }
    case constants.SET_VALID_DISCOUNT:
      return {
        ...state,
        validDiscount: action.validDiscount
      }
    case constants.SET_CHECKOUT_ERROR:
      return {
        ...state,
        error: action.error
      }
    case constants.CLEAR_CHECKOUT_ERROR:
      return {
        ...state,
        error: null
      }
    case constants.SET_DISCOUNT_ERROR:
      return {
        ...state,
        discountError: action.error
      }
    case constants.SET_SHIPPING_ADDRESS_LOADING:
      return {
        ...state,
        shippingAddressIsLoading: action.isLoading
      }
    case constants.SET_SHIPPING_RATES_LOADING:
      return {
        ...state,
        shippingRatesIsLoading: action.isLoading
      }
    case constants.CLEAR_CHECKOUT:
      return initialState
    default:
      return state
  }
}
