import React from 'react';
import PropTypes from 'prop-types';

let Star = (props) => {
  return (
    <svg viewBox="0 0 19.481 19.481" width="512px" height="512px" style={props.style}>
      <defs>
        <linearGradient id={props.id} x1="0" x2="100%" y1="0" y2="0">
          <stop offset={`${props.percent}%`} stopColor={props.startColor}/>
          <stop offset="0%" stopColor={props.stopColor}/>
        </linearGradient>
      </defs>
      <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z" fill={`url(#${props.id})`} stroke={props.stroke}/>
    </svg>
  );
}

Star.defaultProps = {
  percent: 0,
  id: "starfill",
  startColor: "#00C6A4",
  stopColor: "white",
  stroke: "none"
}

Star.propTypes = {
  percent: PropTypes.number.isRequired,
  id: PropTypes.string.isRequired,
  startColor: PropTypes.string,
  stopColor: PropTypes.string,
  stoke: PropTypes.string
}

export default Star;
