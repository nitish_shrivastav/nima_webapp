import React, { Component } from 'react';
import PropTypes from 'prop-types';

let DropArrow = (props) => {
  const activeClass = props.isActive ? "active" : "";
  return (
    <div className="nm-droparrow">
      <div className={`nm-droparrow__bar nm-droparrow__left ${activeClass}`}>
      </div>
      <div className={`nm-droparrow__bar nm-droparrow__right ${activeClass}`}>
      </div>
    </div>
  );
}

DropArrow.defaultProps = {
  isActive: false
}

DropArrow.propTypes = {
  isActive: PropTypes.bool.isRequired
}

export default DropArrow;
