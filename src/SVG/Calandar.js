import React from 'react';
import PropTypes from 'prop-types';

const Calandar = (props) => {
  return (
    <svg width="33px" height="36px" viewBox="0 0 33 36" version="1.1" xmlns="http://www.w3.org/2000/svg" style={props.style} className={props.className}>
      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Profile-Settings" transform="translate(-106.000000, -321.000000)">
          <g id="Group-23" transform="translate(107.000000, 323.000000)">
            <path d="M28.2941176,32 L2.70588235,32 C1.76764706,32 1,31.2285714 1,30.2857143 L1,6.71428571 C1,5.77142857 1.76764706,5 2.70588235,5 L28.2941176,5 C29.2323529,5 30,5.77142857 30,6.71428571 L30,30.2857143 C30,31.2285714 29.2323529,32 28.2941176,32 Z" id="Stroke-1" className={props.strokeClass} stroke-width="3"></path>
            <path d="M1.70588235,4 L27.2941176,4 C28.2361912,4 29,4.76757143 29,5.71428571 L29,8.28571429 C29,9.23242857 28.2361912,10 27.2941176,10 L1.70588235,10 C0.763808824,10 0,9.23242857 0,8.28571429 L0,5.71428571 C0,4.76757143 0.763808824,4 1.70588235,4" id="Fill-3" className={props.strokeClass}></path>
            <path d="M7.5,9 C6.125,9 5,7.93928571 5,6.64285714 L5,2.35714286 C5,1.06071429 6.125,0 7.5,0 C8.875,0 10,1.06071429 10,2.35714286 L10,6.64285714 C10,7.93928571 8.875,9 7.5,9" id="Fill-5" fill="#FFFFFF"></path>
            <path d="M8.5,9 C7.125,9 6,7.93928571 6,6.64285714 L6,2.35714286 C6,1.06071429 7.125,0 8.5,0 C9.875,0 11,1.06071429 11,2.35714286 L11,6.64285714 C11,7.93928571 9.875,9 8.5,9 Z" id="Stroke-7" className={props.strokeClass} stroke-width="3"></path>
            <path d="M21.5,9 C20.125,9 19,7.93928571 19,6.64285714 L19,2.35714286 C19,1.06071429 20.125,0 21.5,0 C22.875,0 24,1.06071429 24,2.35714286 L24,6.64285714 C24,7.93928571 22.875,9 21.5,9" id="Fill-9" fill="#FFFFFF"></path>
            <path d="M22.5,9 C21.125,9 20,7.93928571 20,6.64285714 L20,2.35714286 C20,1.06071429 21.125,0 22.5,0 C23.875,0 25,1.06071429 25,2.35714286 L25,6.64285714 C25,7.93928571 23.875,9 22.5,9 Z" id="Stroke-11" className={props.strokeClass} stroke-width="3"></path>
            <path d="M8,10 L8,31" id="Stroke-13" className={props.strokeClass} stroke-width="2"></path>
            <path d="M15,9 L15,32" id="Stroke-15" className={props.strokeClass} stroke-width="2"></path>
            <path d="M22,10 L22,31" id="Stroke-17" className={props.strokeClass} stroke-width="2"></path>
            <path d="M30,23 L1,23" id="Stroke-19" className={props.strokeClass} stroke-width="2"></path>
            <path d="M29,17 L0,17" id="Stroke-21" className={props.strokeClass} stroke-width="2"></path>
          </g>
        </g>
      </g>
    </svg>
  );
}

Calandar.defaultProps = {
  style: {},
  className: "",
  strokeClass: ""
}

Calandar.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  strokeClass: PropTypes.string,
}

export default Calandar;
