import React from 'react';
import Star from './Star';

const StarRating = (props) => {
  let stars = [];
  let idPrefix = props.idPrefix ? props.idPrefix : "starfill";
  for (let i = 0; i < 5; i++) {
    let starFill = ((props.percent / 20) - i) * 100;
    stars.push(
      <Star
        stopColor={props.stopColor}
        startColor={props.startColor}
        stroke={props.stroke}
        key={`${idPrefix}_${i}`}
        percent={starFill}
        style={props.style}
        id={`${idPrefix}${i}`}/>
    );
  }
  return (
    <div className="nm-layout">
      { stars }
    </div>
  );
}

export default StarRating;
