import React from 'react';
import PropTypes from 'prop-types';

const Profile = (props) => {
  return (
    <svg width="28px" height="28px" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg" style={props.style} className={props.className}>
        <defs>
            <circle id="path-1" cx="14" cy="14" r="14"></circle>
        </defs>
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Profile-Settings" transform="translate(-108.000000, -188.000000)">
                <g id="Profile" transform="translate(108.000000, 188.000000)">
                    <mask id="mask-2" fill="white">
                        <use href="#path-1"></use>
                    </mask>
                    <circle className={props.strokeClass} stroke-width="2" cx="14" cy="14" r="13"></circle>
                    <circle id="Head" className={props.strokeClass} stroke-width="1.25" mask="url(#mask-2)" cx="14" cy="13" r="5"></circle>
                    <circle id="Chest" className={props.strokeClass} stroke-width="1.25" mask="url(#mask-2)" cx="14.5" cy="27.5" r="8.5"></circle>
                </g>
            </g>
        </g>
    </svg>
  );
}

Profile.defaultProps = {
  style: {},
  className: "",
  strokeClass: ""
}

Profile.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  strokeClass: PropTypes.string,
}

export default Profile;
