import React, { Component } from 'react';

class Add extends Component {

  render() {
    return (
      <svg className="nm-btn__add--wrap" x="0px" y="0px" width="510px" height="510px" viewBox="0 0 510 510">
        <path className="nm-btn__add" d="M255,0C114.75,0,0,114.75,0,255s114.75,255,255,255s255-114.75,255-255S395.25,0,255,0z M382.5,280.5h-102v102h-51v-102
          h-102v-51h102v-102h51v102h102V280.5z"/>
      </svg>
    );
  }
}

export default Add;