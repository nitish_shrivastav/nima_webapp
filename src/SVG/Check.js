import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Check extends Component {
  componentWillMount() {
    this.setState({
      anmiateClass: ""
    })
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        anmiateClass: "active"
      })
    }, 300)
  }

  render() {
    return (
      <div>
        {
          this.props.animate &&  
          <svg className={`nm-layout ${this.props.className}`} viewBox="0 0 24 24" style={this.props.style}>
            <path className={`nm-btn__check ${this.state.anmiateClass}`} d="M 5.705,11.295 10,15.585 18.295,7.29" style={{stroke: this.props.fill}}></path>
          </svg>
        }
        {
          !this.props.animate &&
          <svg className={`nm-layout ${this.props.className}`} viewBox="0 0 24 24" style={this.props.style}>
            <g id="check">
              <path className="nm-btn__check active" d="M 5.705,11.295 10,15.585 18.295,7.29" style={{stroke: this.props.fill}}></path>
            </g>
          </svg>
        }
      </div>
    );
  }
}

Check.defaultProps = {
  className: "",
  style: {},
  animate: false,
  fill: "white"
}

Check.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  animate: PropTypes.bool,
  fill: PropTypes.string
}

export default Check;
