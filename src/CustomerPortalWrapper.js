import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import AuthActions from './Actions/AuthActions';
import Profile from './SVG/Profile';
import Calandar from './SVG/Calandar';
import Box from './SVG/Box';
import Back from './SVG/Back';

class CustomerPortalWrapper extends Component {
  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout__max-center nm-layout__max-center--large">
          <div className="nm-spacer-48"/>
          <h3 className="nm-layout">
            Manage Your Account
          </h3>
          {
            this.props.auth.sso &&
            <p className="nm-layout">
              Good Afternoon {this.props.auth.sso.first_name}
            </p>
          }
          <div className="nm-layout nm-tablet-lg-and-up-hidden">
            <div className="nm-spacer-12"/>
            <Link
              to="/account"
              className="nm-layout nm-flex nm-link"
              style={{flexDirection: 'row', justifyContent: 'flex-start', fontSize: 20}}>
              <Back style={{width: 20, height: 20, marginRight: 16}}/>
              Back to Dashboard
            </Link>
          </div>
          <div className="nm-spacer-48"/>
          <div className="nm-sidebar">
            <div className="nm-layout">
              <Link className="nm-sidebar__item" to="/account/profile">
                <Profile style={{width: 40, height: 40, marginRight: 20}} strokeClass="nm-sidebar__icon"/>
                Profile Settings
              </Link>
              <div className="nm-sidebar__item-bar"/>
              <Link className="nm-sidebar__item" to="/account/orders">
                <Box style={{width: 40, height: 40, marginRight: 20}} strokeClass="nm-sidebar__icon"/>
                Order History
              </Link>
              <div className="nm-sidebar__item-bar"/>
              <Link className="nm-sidebar__item" to="/account/subscriptions">
                <Calandar style={{width: 40, height: 40, marginRight: 20}} strokeClass="nm-sidebar__icon"/>
                Manage Subscriptions
              </Link>
              <div className="nm-sidebar__item-bar"/>
            </div>
          </div>
          <div className="nm-sidebar__rest">
            { this.props.children }
          </div>
          <div className="nm-clear"/>
          <div className="nm-spacer-48"/>
        </div>
        <div className="nm-clear"/>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(CustomerPortalWrapper);
