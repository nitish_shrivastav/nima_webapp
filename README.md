# [Web_app] (https://github.com/nimasensor/web_app)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). Create React App documentation goes into depth about everything React, see the documentation.

This app is live on the following routes

`https://nimasensor.com/shop`

`https://nimasensor.com/shop/*`

`https://nimasensor.com/map`

`https://nimasensor.com/map/*`

`https://nimasensor.com/account`

`https://nimasensor.com/account/*`

There is a staging environment set up at `https://stg.nimasensor.com`

## Gettting Started

To get started make sure you have a current version of NPM and a current version of Nodejs.

In the root of the application run the command `npm install`
 
Once that command has finished, run `npm start` in the same directory and open your browser to `localhost:3000/shop` to see the base of the app.

A knowledge of how to use `react-router` and `redux` is essential for the progress of this app.

## Services

This app integrates with several services including:

	- Shopify Storefront API (Used for product lists, cart contents, gathering totals)
	- Nima REST API (Creating orders, manage user account, discounts, public map integration)
	- Google maps (Public restaurant reviews, Places autocomplete)
	- Segment (Event tracking)
	- Stripe (Credit card sources)
	- Paypal (Payment method)
	- Affirm (Loan Payment Method)
	- Bazaar voice (Product Reviews)
	- Iterable (Customer email lists)
	- Talkable (Affiliates program)
	- Zendesk (Contact support)
	- etc.

## Purpose and Content

This app is used for customer purchase of nima products that is using the Shopify Storefront to manage a customers cart, list products, and gather totals to display on the front end of the checkout page.

## Archetecture

This app utilizes `react-router` and `redux`

Folder structure looks something like this.

```
/build
	/public
/src
	index.js
	/Components
	/Containers
	/Actions
	/Constants
	/Reducers
	/Services
```

The build folder is something that you should never edit because the build script `npm run build-stg` or `npm run build-prod` (See deployments for more detail) will overwrite the build directory every time it is run with the latest version of the build.

#### Container folder

The container folder holds React componets that are defined as a single page for each specific container component. Each page will its own separate container. For example, in this app we have 3 different pages for the public map: `/map -> /src/Containers/PublicPlaceMap.js` `/map/place/:place_id -> /src/Containers/PublicPlaceDetail.js` and `/map/chain/:chain_id -> /src/Containers/PublicChainDetail.js`. Whenever you need a new page in this app a new container will need to be created. 

#### Components folder

The components folder holds React comonents that are not Containers. In this app a component is a reusable piece of UI that is usually used inside of a container. A good example of this would be a `<Button>` This can be found in `/src/Components/Button.js`. Because a button can be used over and over again in many different components or containers it belongs in the components folder. 

#### Actions folder

The actions folder is the first access point for redux. In this app redux is used almost entirely to hold restoful data from the API. Most of the action files in the action folder follow the same model structure that of the backend api. For instance, we have a `/src/Actions/SubscriptionActions.js` to fire off restful http methods such as `GET` `POST` `UPDATE` and `DELETE` for the subscription model.

A action is used by first importing it into a Container or a component then dispatching a redux event to the action class. Ex) 

`this.props.dispatch(SubscriptionActions.list());`

This line of code would fetch subscriptions for the appropriate user and set the results into the subscription reducer, making the data available for the subscripbed components or containers.

#### Services

Services is the layer that handles all of the HTTP requests to external api's. Ther eis a service for each cooresponding restful action file in the actions folder.

The only job of the services is to bring data back into the actions folder so that the data can be dispatched to the reducer. To see the use of services, see each model in the acions folder.


#### Reducers

The reducers folder is the access point for data to get added to the redux store. These files correlate along side that of the services and the actions folder. To initiate a reducer function the code must dispatch  constant event type. By dispatching a constant event type (Something like "SUBSCRIPTION_LIST"), the recuder will find the constant and update the redux state appropriately.

#### Constants

The constants folder is pretty self explanitory. The constants folder holds constant strings so that no issues are presented due to user error of misspelling a constant. These files usually follow the same model structure that of the actions folder, services folder, and the reducers folder.


## Routing

All interal routes are done by using `react-router` and `react-router-dom`.
To create an `<a>` tag to another react page in the same app, use `<Link to="/whatever">` to navigate with react-router. If the link is an external link, use an `<a href="/whatever">` tag. You can also push a route on to the route history programatically by using the `this.props.history.push` function, (See react router documentation for a better understanding on how to use history.push).

All routes are defined in the `/src/Routes.js` file. If a new route is to be added, follow the structure that of the other routes follow.


## Config

There are 3 environments that this app can use:  `dev`, `stg`, and `production`. the commands `npm run build-stg` and `npm run build-prod` You can see the different configuration files by looking at `/src/config.prod.js`, `/src/config.stg.js` and `/src/config.dev.js`. Each file corresponds to the correct api layer. See each file for more details.

If you would like to use a different environment for development, specify which one you would like to use in the `/src/config.js` file. Don't forget to revert the logic for the environment back to its origional state.

## Deployments


Each deployment should itterate the version of the app. The version is displayed in the bottom right hand corner of the app. To iterate the version, look in the `/src/config.js` and there is a line that says `config.version = X.X.X`. Iterate that and run the build script again.

The deployment is set up to run to a s3 bucket which has a cloudfront distribution in from of the s3 bucket. All of this is taken care of including a cloudfront invalidation by running the `npm run build-stg && npm run deploy-stg`. Do deploy production you run `npm run build-prod && npm run deploy-prod`. This is a static application that pulls in data dynamically. There is no server side rendering for this app.



